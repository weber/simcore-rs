//------------------------------------------------------------------------------
//	Copyright (C) 2002 - 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ReportProducer.cpp
 * @author Ralf Gerstenberger
 * @date created at 2002/06/21
 * @brief Implementation of class odemx::data::ReportProducer
 * @sa ReportProducer.h
 * @since 1.0
 */

#include <odemx/data/ReportProducer.h>
#include <odemx/base/Simulation.h>

namespace odemx {
namespace data {

ReportProducer::ReportProducer( base::Simulation& sim, const Label& label )
:	Log::NamedElement( sim, label )
{
}

ReportProducer::~ReportProducer()
{
}

const Label& ReportProducer::getLabel() const
{
	return getName();
}

} } // namespace odemx::data
