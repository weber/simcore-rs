//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file SimRecord.cpp
 * @author Ronald Kluth
 * @date created at 2009/03/16
 * @brief Implementation of class odemx::data::SimRecord
 * @sa SimRecord.h
 * @since 3.0
 */

#include <odemx/data/SimRecord.h>
#include <odemx/base/Simulation.h>
#include <odemx/util/Exceptions.h>

namespace odemx {
namespace data {

//------------------------------------------------------construction/destruction

SimRecord::SimRecord( const base::Simulation& sim, const Producer& sender,
		const StringLiteral& text )
:	text_( text )
,	sim_( &sim )
,	sender_( &sender )
,	time_( sim.getTime() )
,	scope_()
,	details_()
{
}

SimRecord::~SimRecord()
{
}

//-------------------------------------------------------------------data checks

bool SimRecord::hasScope() const
{
	return scope_.isSet();
}

bool SimRecord::hasDetails() const
{
	return !! details_;
}

//------------------------------------------------------------class scope setter

SimRecord& SimRecord::scope( const TypeInfo& type )
{
	scope_ = type;
	return *this;
}

//------------------------------------------------------------------data getters

const StringLiteral& SimRecord::getText() const
{
	return text_;
}

const base::Simulation& SimRecord::getSimulation() const
{
	return *sim_;
}

const Producer& SimRecord::getSender() const
{
	return *sender_;
}

base::SimTime SimRecord::getTime() const
{
	return time_;
}

const TypeInfo& SimRecord::getScope() const
{
	return scope_;
}

const SimRecord::DetailVec& SimRecord::getDetails() const
{
	if( ! details_ )
	{
		throw DataException( "SimRecord::getDetails(): "
				"detail vector pointer not initialized, check hasDetails()" );
	}
	return *details_;
}

const std::shared_ptr< SimRecord::DetailVec > SimRecord::getDetailPointer() const
{
	return details_;
}

void SimRecord::initDetailVec()
{
	details_.reset( new DetailVec() );
}

} } // namespace odemx::data
