//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Producer.cpp
 * @author Ronald Kluth
 * @date created at 2009/03/18
 * @brief Implementation of class odemx::data::Producer
 * @sa Producer.h
 * @since 3.0
 */

#include <odemx/data/Producer.h>
#include <odemx/base/DefaultSimulation.h>
#include <odemx/base/Simulation.h>

namespace odemx {
namespace data {

//------------------------------------------------------construction/destruction

Producer::Producer( base::Simulation& sim, const Label& label )
:	ProducerBase( sim, label )
,	sim_( &sim )
{
}

Producer::~Producer()
{
}

//----------------------------------------------------------------enable/disable

void Producer::enableLogging()
{
	trace = sim_->getChannel( channel_id::trace );
	debug = sim_->getChannel( channel_id::debug );
	info = sim_->getChannel( channel_id::info );
	statistics = sim_->getChannel( channel_id::statistics );
}

void Producer::disableLogging()
{
	trace.reset();
	debug.reset();
	info.reset();
	statistics.reset();
}

//------------------------------------------------------------object information

const Label& Producer::getLabel() const
{
	return getName();
}

const TypeInfo Producer::getType() const
{
	return typeid( *this );
}

//---------------------------------------------------------------record creation

SimRecord Producer::log( const StringLiteral& text ) const
{
	return SimRecord( *sim_, *this, text );
}

//-----------------------------------------------------------convenience methods

SimRecord Producer::reset() const
{
	return log( "reset" );
}

SimRecord Producer::count( const StringLiteral& name, std::size_t value ) const
{
	return log( "count" ).detail( name, value );
}

//-------------------------------------------------------------simulation access

const base::Simulation& Producer::getSimulation() const
{
	return *sim_;
}

base::Simulation& Producer::getSimulation()
{
	return *sim_;
}

//---------------------------------------------------------------------streaming

std::ostream& operator<<( std::ostream& os, const data::Producer& obj )
{
	os << obj.getLabel();
	return os;
}

std::ostream& operator<<( std::ostream& os, const data::Producer* ptr )
{
	assert( ptr );

	os << ptr->getLabel();
	return os;
}

} } // namespace odemx::data
