//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ManagedChannels.cpp
 * @author Ronald Kluth
 * @date created at 2009/09/05
 * @brief Implementation of function odemx::data::channel_id::toString
 * @sa ManagedChannels.h
 * @since 3.0
 */

#include <odemx/data/ManagedChannels.h>

namespace odemx {
namespace data {
namespace channel_id {

const std::string toString( const Log::ChannelId id )
{
	using namespace Log::Detail;

	switch( id )
	{
	case trace: return "trace";
	case debug: return "debug";
	case info: return "info";
	case warning: return "warning";
	case error: return "error";
	case fatal: return "fatal";
	case statistics: return "statistics";
	default: return "user-defined";
	}
}

} } } // namesapce odemx::data::channel_id
