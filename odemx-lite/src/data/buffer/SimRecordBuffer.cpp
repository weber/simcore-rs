//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file SimRecordBuffer.cpp
 * @author Ronald Kluth
 * @date created at 2009/09/14
 * @brief Implementation of class odemx::data::SimRecordBuffer
 * @sa SimRecordBuffer.h
 * @since 3.0
 */

#include <odemx/data/buffer/SimRecordBuffer.h>
#include <odemx/data/ManagedChannels.h>
#include <odemx/data/Producer.h>

namespace odemx {
namespace data {
namespace buffer {

SimRecordBuffer::SimRecordBuffer( SizeType limit )
:	storage_()
,	size_( 0 )
,	limit_( limit )
{
}

void SimRecordBuffer::put( const Log::ChannelId channelId,
		const SimRecord& simRecord, const TableKey simId,
		const std::string& timeString )
{
	const Producer& sender = simRecord.getSender();

	// copy volatile and additional information and store it as InfoType
	StoredRecord record( simRecord.getText(), simRecord.getDetailPointer() );
	record
	+ ChannelInfo( channel_id::toString( channelId ) )
	+ SenderLabelInfo( sender.getLabel() )
	+ SenderTypeInfo( sender.getType().toString() )
	+ StringTimeInfo( timeString )
	+ SimIdInfo( simId );

	if( simRecord.hasScope() )
	{
		record + ClassScopeInfo( simRecord.getScope().toString() );
	}

	// store the extended record
	storage_.push_back( record );
	++size_;
}

void SimRecordBuffer::clear()
{
	storage_.clear();
	size_ = 0;
}

bool SimRecordBuffer::isEmpty() const
{
	return size_ == 0;
}

bool SimRecordBuffer::isFull() const
{
	return size_ >= limit_;
}

const SimRecordBuffer::StorageType& SimRecordBuffer::getStorage() const
{
	return storage_;
}

SimRecordBuffer::SizeType SimRecordBuffer::getSize() const
{
	return size_;
}

SimRecordBuffer::SizeType SimRecordBuffer::getLimit() const
{
	return limit_;
}

} } } // namespace odemx::data
