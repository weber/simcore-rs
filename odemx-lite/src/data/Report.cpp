//------------------------------------------------------------------------------
//	Copyright (C) 2002 - 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Report.cpp
 * @author Ralf Gerstenberger
 * @date created at 2002/06/21
 * @brief Implementation of class odemx::data::Report
 * @sa Report.h
 * @since 1.0
 */

#include <odemx/data/Report.h>
#include <odemx/data/ReportProducer.h>
#include <odemx/util/DeletePtr.h>

#include <cassert>
#include <algorithm>

namespace odemx {
namespace data {

Report::~Report()
{
	std::for_each( tables_.begin(), tables_.end(), DeletePtr< ReportTable >() );
}

void Report::addReportProducer( ReportProducer& reporter )
{
	producers_.insert( &reporter );
}

void Report::removeReportProducer( ReportProducer& reporter )
{
	producers_.erase( &reporter );
}

ReportTable& Report::getTable( const std::string& name, const ReportTable::Definition& def )
{
	ReportTable* tmp = findTable( name, def );

	if( ! tmp )
	{
		tmp = new ReportTable( name, def );
		tables_.push_back( tmp );
	}
	return *tmp;
}

ReportTable* Report::findTable( const std::string& name, const ReportTable::Definition& def ) const
{
	assert( ! name.empty() );

	for( TableVec::const_iterator iter = tables_.begin();
		iter != tables_.end(); ++iter )
	{
		ReportTable* current = *iter;

		// compare table def and name to find a matching table
		if( current->getDefinition() == def
			&& current->getLabel() == name )
		{
			return current;
		}
	}
	return 0;
}



void Report::generateReport()
{
	if( producers_.empty() )
	{
		return;
	}

	startProcessing();

	// call all report producers to create and fill their tables
	for( ReportProducerSet::iterator iter = producers_.begin();
		iter != producers_.end(); ++iter )
	{
		( *iter )->report( *this );
	}

	processTables();

	endProcessing();
}

} } // namespace odemx::data
