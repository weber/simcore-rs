//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ErrorWriter.cpp
 * @author Ronald Kluth
 * @date created at 2009/09/30
 * @brief Implementation of class odemx::data::output::ErrorWriter
 * @sa ErrorWriter.h
 * @since 3.0
 */

#include <odemx/data/output/ErrorWriter.h>
#include <odemx/data/output/DefaultTimeFormat.h>
#include <odemx/data/ManagedChannels.h>
#include <odemx/data/Producer.h>

#include <iostream>
#include <iomanip>

namespace odemx {
namespace data {
namespace output {

//------------------------------------------------------construction/destruction

std::shared_ptr< ErrorWriter > ErrorWriter::create()
{
	return std::shared_ptr< ErrorWriter >( new ErrorWriter() );
}

ErrorWriter::ErrorWriter()
:	format_( new DefaultTimeFormat() )
{
}

ErrorWriter::~ErrorWriter()
{
}

//---------------------------------------------------------------record handling

void ErrorWriter::consume( const Log::ChannelId channelId, const SimRecord& record )
{
	using namespace std;

	switch( channelId )
	{
	case channel_id::warning: cerr << "ODEMx WARNING: "; break;
	case channel_id::error: cerr << "ODEMx ERROR: "; break;
	case channel_id::fatal: cerr << "ODEMx FATAL ERROR: "; break;
	default:
		cerr << "ODEMx ERROR: ErrorWriter::consume(): "
				"connected to wrong log channel" << endl;
		return;
	}

	const Producer& sender = record.getSender();

	cerr
	<< record.getText()
	<< " [Time: " << format_->timeToString( record.getTime() )
	<< "] [Object: " << sender << " (" << sender.getType().toString() << ")]";

	if( record.hasDetails() )
	{
		cerr << " [";
		for( SimRecord::DetailVec::const_iterator detail = record.getDetails().begin();
			detail != record.getDetails().end(); ++detail )
		{
			if( detail != record.getDetails().begin() )
			{
				cerr << " | ";
			}
			cerr << detail->first << "="
				<< detail->second.as<std::string>();
		}
		cerr << "]";
	}

	if( record.hasScope() )
	{
		cerr << " [Scope: " << record.getScope().toString() << "]";
	}

	cerr << std::endl;
}

//-----------------------------------------------------------------------setters

void ErrorWriter::setTimeFormat( TimeFormat* timeFormat )
{
	format_.reset( timeFormat );
}

} } } // namespace odemx::data::output
