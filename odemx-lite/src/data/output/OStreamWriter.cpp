//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file OStreamWriter.cpp
 * @author Ronald Kluth
 * @date created at 2009/02/09
 * @brief Implementation of class odemx::data::output::OStreamWriter
 * @sa OStreamWriter.h
 * @since 3.0
 */

#include <odemx/data/output/OStreamWriter.h>
#include <odemx/data/output/DefaultTimeFormat.h>
#include <odemx/data/ManagedChannels.h>
#include <odemx/data/Producer.h>

#include <iomanip>

namespace odemx {
namespace data {
namespace output {

//------------------------------------------------------construction/destruction

std::shared_ptr< OStreamWriter > OStreamWriter::create( std::ostream& os )
{
	return std::shared_ptr< OStreamWriter >( new OStreamWriter( os ) );
}

OStreamWriter::OStreamWriter( std::ostream& os )
:	os_( os )
,	format_( new DefaultTimeFormat() )
{
	os_ << "ODEMx Log\n"
		<< "=========\n";
}

OStreamWriter::~OStreamWriter()
{
}

//---------------------------------------------------------------record handling

void OStreamWriter::consume( const Log::ChannelId channelId, const SimRecord& record )
{
	using namespace std;

	std::string timeString = format_->timeToString( record.getTime() );
	const Producer& sender = record.getSender();

	os_
	<< timeString << " "
	<< channel_id::toString( channelId ) << ": "
	<< sender.getLabel() << "(" << sender.getType().toString() << ") "
	<< record.getText();

	if( record.hasDetails() )
	{
		os_ << " [";
		for( SimRecord::DetailVec::const_iterator detail = record.getDetails().begin();
			detail != record.getDetails().end(); ++detail )
		{
			if( detail != record.getDetails().begin() )
			{
				os_ << " | ";
			}
			os_ << detail->first << "="
				<< detail->second.as<std::string>();
		}
		os_ << "]";
	}

	if( record.hasScope() )
	{
		os_ << " [Class Scope: " << record.getScope().toString() << "]";
	}

	os_ << std::endl;
}

//-----------------------------------------------------------------------setters

void OStreamWriter::setTimeFormat( TimeFormat* timeFormat )
{
	format_.reset( timeFormat );
}

} } } // namespace odemx::data::output
