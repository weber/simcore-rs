//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file GermanTime.cpp
 * @author Ronald Kluth
 * @date created at 2009/04/03
 * @brief Implementation of class odemx::data::output::GermanTime
 * @sa GermanTime.h
 * @since 2.1
 */

#include <odemx/data/output/GermanTime.h>
#include <odemx/data/output/TimeBase.h>
#include <odemx/data/output/TimeUnit.h>

#include <iomanip>
#include <string>
#include <sstream>

namespace odemx {
namespace data {
namespace output {

//------------------------------------------------------construction/destruction

GermanTime::GermanTime()
:	TimeFormat()
{
}

GermanTime::GermanTime( const TimeBase& base )
:	TimeFormat( base )
{
}

//----------------------------------------------------------time string creation

const std::string GermanTime::makeString() const
{
	using namespace std;

	ostringstream stream;
	stream.fill('0');

	if( (year - 100) > 0 )
	{
		stream << setw(4) << year;
	}
	else
	{
		stream << setw(2) << year;
	}

	stream << '-' << setw(2) << month << '-';
	stream << setw(2) << day;

	stream << " / ";
	stream << setw(2) << hour << ':';
	stream << setw(2) << min;

	if( base.unit == TimeUnit::milliseconds )
	{
		stream << ':' << setw(6) << fixed << setprecision(3) << sec;
	}
	else
	{
		stream << ':' << setw(2) << fixed << setprecision(0) << sec;
	}
	return stream.str();
}

} } } // namespace odemx::data::output
