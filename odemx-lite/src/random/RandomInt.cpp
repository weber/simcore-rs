//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file RandomInt.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::RandomInt
 */

#include <odemx/random/RandomInt.h>

#include <algorithm>
#include <cmath>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

RandomInt::RandomInt( base::Simulation& sim, const data::Label& label, int lowerBound,
		int upperBound )
:	DiscreteDist( sim, label )
{
	if( lowerBound > upperBound )
	{
		std::swap( lowerBound, upperBound );
		warning << log( "RandomInt(): lower bound greater than upper bound; bounds swapped" )
				.scope( typeid(RandomInt) );
	}

	lowerBound_ = lowerBound;
	upperBound_ = upperBound;
	zyqSpan_ = upperBound_ - lowerBound_;

	ODEMX_TRACE << log( "create" ).scope( typeid(RandomInt) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(RandomInt) );
	statistics << param( "lower bound", lowerBound_ ).scope( typeid(RandomInt) );
	statistics << param( "upper bound", upperBound_ ).scope( typeid(RandomInt) );
}

RandomInt::~RandomInt()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(RandomInt) );
}

//------------------------------------------------------------------------sample

int RandomInt::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(RandomInt) );

	statistics << count( "uses" ).scope( typeid(RandomInt) );

	return (int)std::floor( zyqSpan_ * DiscreteDist::getSample() ) + lowerBound_;
}

//-----------------------------------------------------------------------getters

int RandomInt::getLowerBound()
{
	return lowerBound_;
}

int RandomInt::getUpperBound()
{
	return upperBound_;
}

} } // namespace odemx::random
