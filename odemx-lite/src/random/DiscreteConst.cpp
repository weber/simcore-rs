//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file DiscreteConst.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::DiscreteConst
 */

#include <odemx/random/DiscreteConst.h>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

DiscreteConst::DiscreteConst( base::Simulation& sim, const data::Label& label, int value )
:	DiscreteDist( sim, label ),
	value_( value )
{
	ODEMX_TRACE << log( "create" ).scope( typeid(DiscreteConst) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(DiscreteConst) );
	statistics << param( "value", value_ ).scope( typeid(DiscreteConst) );
}

DiscreteConst::~DiscreteConst()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(DiscreteConst) );
}

//------------------------------------------------------------------------sample

int DiscreteConst::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(DiscreteConst) );

	statistics << count( "uses" ).scope( typeid(DiscreteConst) );

	return value_;
}

//-----------------------------------------------------------------------getters

int DiscreteConst::getValue()
{
	return value_;
}

} } // namespace odemx::random
