//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file NegativeExponential.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::NegativeExponential
 */

#include <odemx/random/NegativeExponential.h>

#include <cmath>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

NegativeExponential::NegativeExponential( base::Simulation& sim, const data::Label& label,
		double inverseMean )
:	ContinuousDist( sim, label )
{
	if( inverseMean <= 0.0 )
	{
		inverseMean = ( inverseMean < 0.0 ) ? -inverseMean : 0.001;

		warning << log( "NegativeExponential(): inverseMean <= 0; -inverseMean or 0.001 used")
				.scope( typeid(NegativeExponential) );
	}
	inverseMean_ = inverseMean;

	ODEMX_TRACE << log( "create" ).scope( typeid(NegativeExponential) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(NegativeExponential) );
	statistics << param( "inverse mean", inverseMean_ ).scope( typeid(NegativeExponential) );
}

NegativeExponential::~NegativeExponential()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(NegativeExponential) );
}

//------------------------------------------------------------------------sample

double NegativeExponential::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(NegativeExponential) );

	statistics << count( "uses" ).scope( typeid(NegativeExponential) );

	return ( -std::log( Dist::getSample() ) / inverseMean_ );
}

//-----------------------------------------------------------------------getters

double NegativeExponential::getInverseMean()
{
	return inverseMean_;
}

} } // namespace odemx::random
