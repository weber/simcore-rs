//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Normal.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::Normal
 */

#include <odemx/random/Normal.h>

#include <cmath>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

Normal::Normal( base::Simulation& sim, const data::Label& label, double mean,
		double deviation )
:	ContinuousDist( sim, label )
{
	if( deviation < 0.0 )
	{
		deviation = -deviation;

		warning << log( "Normal(): given deviation < 0; changed to -deviation" )
				.scope( typeid(Normal) );
	}

	mean_ = mean;
	deviation_ = deviation;
	zyqeven = false;
	zyqu = zyqv = 0.0;

	ODEMX_TRACE << log( "create" ).scope( typeid(Normal) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(Normal) );
	statistics << param( "mean", mean_ ).scope( typeid(Normal) );
	statistics << param( "deviation", deviation_ ).scope( typeid(Normal) );
}

Normal::~Normal()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(Normal) );
}

//------------------------------------------------------------------------sample

double Normal::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(Normal) );

	statistics << count( "uses" ).scope( typeid(Normal) );

	double z;
	if( zyqeven )
	{
		zyqeven = false;
		z = zyqu * std::cos( zyqv );
	}
	else
	{
		zyqeven = true;
		zyqu = std::sqrt( -2.0 * std::log( Dist::getSample() ) );
		zyqv = 6.28318530717958647693 * Dist::getSample();
		z = zyqu * std::sin( zyqv );
	};
	return ( z * deviation_ + mean_ );
};

//-----------------------------------------------------------------------getters

double Normal::getMean()
{
	return mean_;
}

double Normal::getDeviation()
{
	return deviation_;
}

} } // namespace odemx::random
