//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file ContinuousConst.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::ContinuousConst
 */

#include <odemx/random/ContinuousConst.h>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

ContinuousConst::ContinuousConst( base::Simulation& sim, const data::Label& label,
		double value )
:	ContinuousDist( sim, label )
,	value_( value )
{
	ODEMX_TRACE << log( "create" ).scope( typeid(ContinuousConst) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(ContinuousConst) );
	statistics << param( "value", value_ ).scope( typeid(ContinuousConst) );
}

ContinuousConst::~ContinuousConst()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(ContinuousConst) );
}

//------------------------------------------------------------------------sample

double ContinuousConst::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(ContinuousConst) );

	statistics << count( "uses" ).scope( typeid(ContinuousConst) );

	return value_;
}

//-----------------------------------------------------------------------getters

double ContinuousConst::getValue()
{
	return value_;
}

} } // namespace odemx::random
