//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Poisson.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::Poisson
 */

#include <odemx/random/Poisson.h>

#include <cmath>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

Poisson::Poisson( base::Simulation& sim, const data::Label& label, double mean )
:	DiscreteDist( sim, label )
{
	if( mean <= 0.0 )
	{
		mean = ( mean < 0.0 ) ? -mean : 0.0010;
		warning << log( "Poisson(): mean value is <= 0.0; changed to -mean" )
				.scope( typeid(Poisson) );
	}
	mean_ = mean;

	ODEMX_TRACE << log( "create" ).scope( typeid(Poisson) );

	statistics << param( "seed", Dist::getSeed() ).scope(typeid(Poisson) );
	statistics << param( "mean", mean_ ).scope(typeid(Poisson) );
}

Poisson::~Poisson()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(Poisson) );
}

//------------------------------------------------------------------------sample

int Poisson::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(Poisson) );

	statistics << count( "uses" ).scope( typeid(Poisson) );

	double q;
	double r;
	int m = 0;

	r = std::exp( -mean_ );
	q = 1.0;

	while( q >= r )
	{
		q *= Dist::getSample();
		m += 1;
	};

	m -= 1;
	return m;
}

//-----------------------------------------------------------------------getters

double Poisson::getMean()
{
	return mean_;
}

} } // namespace odemx::random
