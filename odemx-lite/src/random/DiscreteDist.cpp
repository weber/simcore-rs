//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file DiscreteDist.cpp

	\author Ralf Gerstenberger

	\date created at 2003/05/09

	\brief Implementation of odemx::random::DiscreteDist

	\sa DiscreteDist.h

	\since 1.0
*/

#include <odemx/random/DiscreteDist.h>

namespace odemx {
namespace random {

DiscreteDist::DiscreteDist( base::Simulation& sim, const data::Label& label )
:	Dist( sim, label )
{
}

DiscreteDist::~DiscreteDist()
{
}

} } // namespace odemx::random
