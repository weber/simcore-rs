//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Uniform.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::Uniform
 */

#include <odemx/random/Uniform.h>

#include <algorithm>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

Uniform::Uniform( base::Simulation& sim, const data::Label& label, double lowerBound,
		double upperBound )
:	ContinuousDist( sim, label )
{
	if( lowerBound > upperBound )
	{
		std::swap( lowerBound, upperBound );
		warning << log( "Uniform(): lower bound greater than upper bound; bounds swapped" )
				.scope( typeid(Uniform) );
	}
	lowerBound_ = lowerBound;
	upperBound_ = upperBound;
	zyqSpan_ = upperBound_ - lowerBound_;

	ODEMX_TRACE << log( "create" ).scope( typeid(Uniform) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(Uniform) );
	statistics << param( "lower bound", lowerBound_ ).scope( typeid(Uniform) );
	statistics << param( "upper bound", upperBound_ ).scope( typeid(Uniform) );
}

Uniform::~Uniform()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(Uniform) );
}

//------------------------------------------------------------------------sample

double Uniform::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(Uniform) );

	statistics << count( "uses" ).scope( typeid(Uniform) );

	return ( zyqSpan_ * getSample() + lowerBound_ );
}

//-----------------------------------------------------------------------getters

double Uniform::getLowerBound()
{
	return lowerBound_;
}

double Uniform::getUpperBound()
{
	return upperBound_;
}

} } // namespace odemx::random
