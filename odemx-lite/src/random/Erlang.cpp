//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Erlang.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::Erlang
 */

#include <odemx/random/Erlang.h>

#include <cmath>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

Erlang::Erlang( base::Simulation& sim, const data::Label& label, double rate, int shape )
:	ContinuousDist( sim, label )
{
	if( rate <= 0.0 )
	{
		rate = ( rate < 0.0 ) ? -rate : 0.01;
		warning << log( "Erlang(): rate <= 0.0; -rate or 0.01 used" )
				.scope( typeid(Erlang) );
	}

	if( shape <= 0 )
	{
		shape = ( shape < 0 ) ? -shape : 1;
		warning << log( "Erlang(): shape <= 0; -shape or 1 used" )
				.scope( typeid(Erlang) );
	}
	rate_ = rate;
	shape_ = shape;

	ODEMX_TRACE << log( "create" ).scope( typeid(Erlang) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(Erlang) );
	statistics << param( "rate", rate_ ).scope( typeid(Erlang) );
	statistics << param( "shape", shape_ ).scope( typeid(Erlang) );
}

Erlang::~Erlang()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(Erlang) );
}

//------------------------------------------------------------------------sample

double Erlang::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(Erlang) );

	statistics << count( "uses" ).scope( typeid(Erlang) );

	double erlangTmp = 0.0;
	for( int i = 0; i < shape_; ++i )
	{
		erlangTmp += -( 1 / rate_ ) * std::log( Dist::getSample() );
	}
	return erlangTmp;
}

//-----------------------------------------------------------------------getters

double Erlang::getRate()
{
	return rate_;
}

int Erlang::getShape()
{
	return shape_;
}

} } // namespace odemx::random
