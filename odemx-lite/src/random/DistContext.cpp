//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file DistContext.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::DistContext
 */

#include <odemx/random/DistContext.h>

namespace odemx {
namespace random {

const unsigned long zyqmodulo = 67099547;

DistContext::DistContext()
:	zyqSeed_( 907 )
{
}

DistContext::~DistContext()
{
}

void DistContext::setSeed( int n )
{
	if( n < 0 )
	{
		n = -n;
	}

	if( n >= (int)zyqmodulo )
	{
		n = n - ( (int)(n / zyqmodulo) ) * zyqmodulo;
	}

	if( n == 0 )
	{
		n = (int)( zyqmodulo / 2 );
	}

	zyqSeed_ = n;
}

unsigned long DistContext::getSeed()
{
	return zyqSeed_;
}

unsigned long DistContext::getNextSeed()
{
	computeNextSeed();
	return zyqSeed_;
}

void DistContext::computeNextSeed()
{
	int k;

	k = 7;
	zyqSeed_ = zyqSeed_ * k;
	if( zyqSeed_ >= zyqmodulo )
	{
	    zyqSeed_ = zyqSeed_ - ( (int)( zyqSeed_ / zyqmodulo ) ) * zyqmodulo;
	}

	k = 13;
	zyqSeed_ = zyqSeed_ * k;
	if( zyqSeed_ >= zyqmodulo )
	{
	    zyqSeed_ = zyqSeed_ - ( (int)( zyqSeed_ / zyqmodulo ) ) * zyqmodulo;
	}

	k = 15;
	zyqSeed_ = zyqSeed_ * k;
	if( zyqSeed_ >= zyqmodulo )
	{
	    zyqSeed_ = zyqSeed_ - ( (int)( zyqSeed_ / zyqmodulo ) ) * zyqmodulo;
	}

	k = 27;
	zyqSeed_ = zyqSeed_ * k;
	if( zyqSeed_ >= zyqmodulo )
	{
	    zyqSeed_ = zyqSeed_ - ( (int)( zyqSeed_ / zyqmodulo ) ) * zyqmodulo;
	}
}

} } // namespace odemx::random
