//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Draw.cpp
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Implementation of odemx::random::Draw
 */

#include <odemx/random/Draw.h>

namespace odemx {
namespace random {

//------------------------------------------------------construction/destruction

Draw::Draw( base::Simulation& sim, const data::Label& label, double probability )
:	DiscreteDist( sim, label )
{
	probability_ = probability;

	ODEMX_TRACE << log( "create" ).scope( typeid(Draw) );

	statistics << param( "seed", Dist::getSeed() ).scope( typeid(Draw) );
	statistics << param( "probability", probability_ ).scope( typeid(Draw) );
}

Draw::~Draw()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(Draw) );
}

//------------------------------------------------------------------------sample

int Draw::sample()
{
	ODEMX_TRACE << log( "sample" ).scope( typeid(Draw) );

	statistics << count( "uses" ).scope( typeid(Draw) );

	return ( probability_ > Dist::getSample() ? 1 : 0 );
}

//-----------------------------------------------------------------------getters

double Draw::getProbability()
{
	return probability_;
}

} } // namespace odemx::random
