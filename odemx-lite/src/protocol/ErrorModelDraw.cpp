//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ErrorModelDraw.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/05
 * @brief Implementation of class odemx::protocol::ErrorModelDraw
 * @sa ErrorModelDraw.h
 * @since 3.0
 */

#include <odemx/protocol/ErrorModelDraw.h>

namespace odemx {
namespace protocol {

ErrorModelDraw::ErrorModelDraw( base::Simulation& sim, const data::Label& label,
		double probability )
:	draw_( sim, label + " draw", probability )
{}

ErrorModelDraw::~ErrorModelDraw()
{
}

ErrorModelPtr ErrorModelDraw::create( base::Simulation& sim, const data::Label& label,
		double probability )
{
	return ErrorModelPtr( new ErrorModelDraw( sim, label, probability ) );
}

bool ErrorModelDraw::apply( PduPtr p )
{
	return draw_.sample() == 1;
}

} } // namespace odemx::protocol
