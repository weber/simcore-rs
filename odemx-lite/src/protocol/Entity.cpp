//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Entity.cpp
 * @author Ronald Kluth
 * @date created at 2009/10/13
 * @brief Implementation of class odemx::protocol::Entity
 * @sa Entity.h
 * @since 3.0
 */

#include <odemx/protocol/Entity.h>
#include <odemx/protocol/Layer.h>

namespace odemx {
namespace protocol {

Entity::Entity( base::Simulation& sim, const data::Label& label )
:	ServiceProvider( sim, label )
{
}

Entity::~Entity()
{
}

void Entity::send( const std::string& lowerLayerSap, PduPtr p )
{
	Sap* sap = getLayer()->getLowerLayer()->getSap( lowerLayerSap );
	if( sap )
	{
		ODEMX_TRACE << log( "send" )
				.detail( "SAP", lowerLayerSap )
				.scope( typeid(Entity) );

		sap->write( p );
	}
	else
	{
		warning << log( "Entity::send(): lower layer does not offer SAP" )
				.detail( "name", lowerLayerSap )
				.scope( typeid(Entity) );
	}
}

void Entity::pass( const std::string& upperLayerSap, PduPtr p )
{
	Sap* sap = getLayer()->getUpperLayer()->getSap( upperLayerSap );
	if( sap )
	{
		ODEMX_TRACE << log( "pass" )
				.detail( "SAP", upperLayerSap )
				.scope( typeid(Entity) );

		sap->write( p );
	}
	else
	{
		warning << log( "Entity::pass(): upper layer does not offer SAP" )
				.detail( "name", upperLayerSap )
				.scope( typeid(Entity) );
	}
}

} } // namespace odemx::protocol
