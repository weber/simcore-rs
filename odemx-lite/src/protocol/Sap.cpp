//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Sap.cpp
 * @author Ronald Kluth
 * @date created at 2009/10/13
 * @brief Implementation of class odemx::protocol::Sap
 * @sa Sap.h
 * @since 3.0
 */

#include <odemx/protocol/Sap.h>
#include <odemx/protocol/ServiceProvider.h>

namespace odemx {
namespace protocol {

//------------------------------------------------------------buffer definitions

Sap::BufferHead::BufferHead( base::Simulation& sim, const data::Label& label, Sap& sap )
:	synchronization::PortHeadT< PduPtr >( sim, label + " " + sap.getName() + " buffer",
		synchronization::WAITING_MODE, 1000, 0 )
,	sap_( &sap )
{}

Sap& Sap::BufferHead::getSap() const
{
	return *sap_;
}

Sap::BufferHead::Ptr Sap::BufferHead::create( base::Simulation& sim,
		const data::Label& label, Sap& sap )
{
	Ptr rv( new BufferHead( sim, label, sap ) );
	rv->initPort();
	return rv;
}

//------------------------------------------------------construction/destruction

Sap::Sap( base::Simulation& sim, const data::Label& label, const std::string& name )
:	name_( name )
{
	queueHead_ = BufferHead::create( sim, label, *this );
	queueTail_ = queueHead_->getTail();
}

Sap::~Sap()
{
}

//-----------------------------------------------------------------public access

const std::string& Sap::getName() const
{
	return name_;
}

Sap::BufferHead* Sap::getBuffer() const
{
	return queueHead_.get();
}

void Sap::write( PduPtr p )
{
	queueTail_->put( p );
}

PduPtr Sap::read()
{
	std::unique_ptr< PduPtr > p = queueHead_->get();
	if( ! p.get() )
	{
		return PduPtr();
	}
	return *p;
}

} } // namespace odemx::protocol
