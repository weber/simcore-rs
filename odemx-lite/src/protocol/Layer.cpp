//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Layer.cpp
 * @author Ronald Kluth
 * @date created at 2009/10/13
 * @brief Implementation of class odemx::protocol::Layer
 * @sa Layer.h
 * @since 3.0
 */

#include <odemx/protocol/Layer.h>
#include <odemx/protocol/Sap.h>
#include <odemx/protocol/ServiceProvider.h>
#include <odemx/util/DeletePtr.h>

#include <algorithm> // for_each, find_if

namespace odemx {
namespace protocol {

Layer::Layer( base::Simulation& sim, const data::Label& label )
:	data::Producer( sim, label )
,	serviceProviders_()
,	saps_()
,	upperLayer_( 0 )
,	lowerLayer_( 0 )
{
	ODEMX_TRACE << log( "create" ).scope( typeid(Layer) );
}

Layer::~Layer()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(Layer) );

	std::for_each(
			serviceProviders_.begin(),
			serviceProviders_.end(),
			DeletePtr< ServiceProvider >() );
};

void Layer::addServiceProvider( ServiceProvider* sp )
{
	assert( sp );

	ODEMX_TRACE << log( "add service provider" )
					.detail( "label", sp->getLabel() )
					.scope( typeid(Layer) );

	sp->setLayer( this );
	for( ServiceProvider::SapVec::const_iterator iter = sp->getSaps().begin();
		iter != sp->getSaps().end(); ++iter )
	{
		addSap( *iter );
	}
	serviceProviders_.push_back( sp );
}

void Layer::addSap( Sap* sap )
{
	assert( sap );

	std::pair< SapMap::iterator, bool > result =
			saps_.insert( SapMap::value_type( sap->getName(), sap ) );

	if( result.second )
	{
		ODEMX_TRACE << log( "add SAP" )
						.detail( "name", sap->getName() )
						.scope( typeid(Layer) );
	}
	else
	{
		warning << log( "Layer::addSap(): SAP already in map, insertion failed")
				.detail( "name", sap->getName() )
				.scope( typeid(Layer) );
	}
}

bool Layer::removeSap( const std::string& sap )
{
	SapMap::iterator found = saps_.find( sap );

	if( found != saps_.end() )
	{
		ODEMX_TRACE << log( "remove SAP" )
						.detail( "name", sap )
						.scope( typeid(Layer) );

		saps_.erase( found );
		return true;
	}
	else
	{
		warning << log( "Layer::removeSap(): SAP not found")
				.detail( "name", sap )
				.scope( typeid(Layer) );
	}
	return false;
}

Sap* Layer::getSap( const std::string& sap ) const
{
	SapMap::const_iterator found = saps_.find( sap );

	if( found != saps_.end() )
	{
		return found->second;
	}
	return 0;
}

Layer::SapMap& Layer::getSaps()
{
	return saps_;
}

void Layer::setUpperLayer( Layer* layer )
{
	upperLayer_ = layer;
}

Layer* Layer::getUpperLayer() const
{
	return upperLayer_;
}

void Layer::setLowerLayer( Layer* layer )
{
	lowerLayer_ = layer;
}

Layer* Layer::getLowerLayer() const
{
	return lowerLayer_;
}

const Layer::ServiceProviderVec& Layer::getServiceProviders() const
{
	return serviceProviders_;
}

} } // namespace odemx::protocol
