//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file DefaultSimulation.cpp
 * @author Ronald Kluth
 * @date created at 2009/02/07
 * @brief Implementation of odemx::base::DefaultSimulation
 * @sa DefaultSimulation.h, Simulation.h, Simulation.cpp
 * @since 3.0
 */

#include <odemx/base/DefaultSimulation.h>

namespace odemx {
namespace base {

//------------------------------------------------------construction/destruction

DefaultSimulation::DefaultSimulation()
:	Simulation( "DefaultSimulation" )
{
}

DefaultSimulation::~DefaultSimulation()
{
}

//----------------------------------------------------------------initialization

void DefaultSimulation::initSimulation()
{
}

} // namespace base

//-----------------------------------------------------default simulation access

base::Simulation& getDefaultSimulation()
{
	static base::DefaultSimulation defaultSim;
	return defaultSim;
}

} // namespace odemx
