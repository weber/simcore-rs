//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Cell.cpp

 \author Sascha Qualitz

 \date created at 2010/01/05

 \brief Implementation of class Cell

 \sa Cell.h

 \since 3.0
 */
#include <exception>
#include <iostream>
#include <odemx/base/cellular_automaton/Cell.h>
#include <odemx/base/cellular_automaton/CellMonitor.h>
#include <odemx/base/cellular_automaton/Transition.h>
#include <odemx/base/cellular_automaton/CellVariablesContainer.h>

using namespace odemx::base;
using namespace odemx::base::cellular;
using namespace std;

//------------------------------------------------------------------------------construction/destruction
Cell::Cell(unsigned row, unsigned column, unsigned level, unsigned dimension, unsigned inputDimension, unsigned outputDimension, CellMonitor* monitor)
:	monitor(monitor)
,	stateDimension(dimension)
,	inputDimension(inputDimension)
,	outputDimension(outputDimension)
,	isBoundary(false)
,	pushed(false)
{
	cellPosition = new CellPosition(row, column, level, monitor->getRowCount(), monitor->getColumnCount(), monitor->getLevelCount());

	typeOfNeighborhood.neighborhoodType = MOORE;
	typeOfNeighborhood.radius = 1;
}

Cell::~Cell() {

}

void Cell::calculateNeighborhood() {

	switch (typeOfNeighborhood.neighborhoodType) {
		case MOORE:
			calculateMooreNeighborhood(typeOfNeighborhood.radius);
			break;
		case NEUMANN:
			calculateVonNeumannNeighborhood(typeOfNeighborhood.radius);
			break;
		case MOORETORUS:
			calculateMoore2DTorusNeighborhood(typeOfNeighborhood.radius);
			break;
		case NEUMANNTORUS:
			calculateNeumann2DTorusNeighborhood(typeOfNeighborhood.radius);
			break;
		case MOOREXTUBE:
			calculateMoore2DXAxisTubeNeighborhood(typeOfNeighborhood.radius);
			break;
		case NEUMANNXTUBE:
			calculateNeumann2DXAxisTubeNeighborhood(typeOfNeighborhood.radius);
			break;
		case MOOREYTUBE:
			calculateMoore2DYAxisTubeNeighborhood(typeOfNeighborhood.radius);
			break;
		case NEUMANNYTUBE:
			calculateNeumann2DYAxisTubeNeighborhood(typeOfNeighborhood.radius);
			break;
		default:
			calculateMooreNeighborhood(typeOfNeighborhood.radius);
			break;
	}
}

void Cell::pushValue() {
	this->pushed = true;
}

std::vector<double> Cell::pullValue(unsigned variableIndex) {

	std::vector<double> neigborhoodCellsValuesVector;
	try{
		for (std::map<CellPosition, Cell*, ComparePositions>::iterator iter = neighborhoodCellsMap.begin(); iter != neighborhoodCellsMap.end(); ++iter) {
			checkIndexBoundary(variableIndex, (iter->second->getDimension() - 1), "Cell::pullValue(unsigned variableIndex)");
			neigborhoodCellsValuesVector.push_back(monitor->cellVariablesContainer->getOutputValue(iter->second->getOutputBaseIndex(), variableIndex));
		}
	} catch(out_of_range e) {
		std::cerr << e.what() << std::endl;
	}

	return neigborhoodCellsValuesVector;

}

void Cell::setValue(unsigned variableIndex, double value) {
	try {
		checkIndexBoundary(variableIndex, (this->stateDimension - 1), "Cell::setValue(unsigned variableIndex, double value)");
		this->monitor->cellVariablesContainer->setStateValue(this->stateBaseIndex, variableIndex, value);
	} catch(out_of_range e) {
		std::cerr << e.what() << std::endl;
	}
}

double Cell::getValue(unsigned variableIndex) {
	try {
		checkIndexBoundary(variableIndex, (this->stateDimension - 1), "Cell::getValue(unsigned variableIndex)");
		return this->monitor->cellVariablesContainer->getStateValue(this->stateBaseIndex, variableIndex);
	} catch(out_of_range e) {
			std::cerr << e.what() << std::endl;
	}
}

void Cell::setOutputValue(unsigned variableIndex, double value) {
	try {
		checkIndexBoundary(variableIndex, (this->outputDimension -1), "Cell::setOutputValue(unsigned variableIndex, double value)");
		this->monitor->cellVariablesContainer->setOutputValue(this->outputBaseIndex, variableIndex, value);
	} catch(out_of_range e) {
			std::cerr << e.what() << std::endl;
	}
}

double Cell::getOutputValue(unsigned variableIndex) {
	try {
		checkIndexBoundary(variableIndex, (this->outputDimension - 1), "Cell::getOutputValue(unsigned variableIndex)");
		return this->monitor->cellVariablesContainer->getOutputValue(this->outputBaseIndex, variableIndex);
	} catch(out_of_range e) {
			std::cerr << e.what() << std::endl;
	}
}

void Cell::setInputValue(unsigned variableIndex, double value) {
	try {
		checkIndexBoundary(variableIndex, (this->inputDimension - 1), "Cell::setInputValue(unsigned variableIndex, double value)");
		this->monitor->cellVariablesContainer->setInputValue(this->inputBaseIndex, variableIndex, value);
	} catch(out_of_range e) {
			std::cerr << e.what() << std::endl;
	}
}

double Cell::getInputValue(unsigned variableIndex) {
	try {
		checkIndexBoundary(variableIndex, (this->inputDimension - 1), "Cell::getInputValue(unsigned variableIndex)");
		return this->monitor->cellVariablesContainer->getInputValue(this->inputBaseIndex, variableIndex);
	} catch(out_of_range e) {
			std::cerr << e.what() << std::endl;
	}
}

void Cell::setTransition(Transition* transition) {
	if(transition) {
		transition->setCell(this);
		this->transitions.push_back(transition);
	}
	else {
		throw new NotAssignedException("Transition", "Cell");
	}
}

void Cell::deleteTransition(Transition* transition) {

	if(transition) {
		list<Transition*>::iterator iter = find(this->transitions.begin(),
														this->transitions.end(), transition);
		//..and delete
		if (*iter == transition) {
			this->transitions.erase(iter);
		}
		transition->cell = 0;
	}
	else {
		throw new NotAssignedException("Transition", "Cell");
	}
}

unsigned Cell::getDimension() {
	return this->stateDimension;
}

unsigned Cell::getInputDimension() {
	return this->inputDimension;
}

unsigned Cell::getOutputDimension() {
	return this->outputDimension;
}

void Cell::setIsBoundary() {
	this->isBoundary = true;
	this->monitor->calculateNeighboorhood();
}

bool Cell::isPartOfBoundary() {
	return this->isBoundary;
}

void Cell::setPosition(unsigned row, unsigned column, unsigned level) {
	cellPosition->setPosition(row, column, level);
}

CellPosition* Cell::getPosition() {
	return this->cellPosition;
}

void Cell::setNeighborhoodSettings(unsigned neighborhoodType, unsigned radius, bool calcNeighborhood) {
	this->typeOfNeighborhood.neighborhoodType = neighborhoodType;
	this->typeOfNeighborhood.radius = radius;
	if(calcNeighborhood) calculateNeighborhood();
}

std::map<CellPosition, Cell*, ComparePositions> Cell::getNeighbors() {
	return neighborhoodCellsMap;
}

Cell* Cell::searchNeighbor(unsigned row, unsigned column, unsigned level) {
	CellPosition tempPosition(row, column, level, cellPosition->cellRows, cellPosition->cellColumns, cellPosition->cellLevel);
	map<CellPosition, Cell*, ComparePositions>::iterator iter = neighborhoodCellsMap.find(tempPosition);

	if(iter != neighborhoodCellsMap.end()) {
		return iter->second;
	}
	else {
		return 0;
	}
}

void Cell::calculateMooreNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();
	unsigned levelCount = monitor->getLevelCount();

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;
	unsigned levelNumberIndex = cellPosition->level;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, levelCount);

	for (int i = 0; i < rowCount; ++i) {
		for (int j = 0; j < columnCount; ++j) {
			for (int k = 0; k < levelCount; ++k) {
				if((std::abs(i-(int)rowNumberIndex) <= radius && std::abs(j-(int)columnNumberIndex) <= radius && (std::abs(k-(int)levelNumberIndex) <= radius)) && !monitor->getCell(i,j,k)->isPartOfBoundary()) {
					if(!(i == rowNumberIndex && j == columnNumberIndex && k == levelNumberIndex)) {
						neighborCellPosition.setPosition(i,j,k);
						neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(rowCount*(k*columnCount + j) + i);
					}
				}
			}
		}
	}
}

void Cell::calculateVonNeumannNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();
	unsigned levelCount = monitor->getLevelCount();

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;
	unsigned levelNumberIndex = cellPosition->level;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, levelCount);

	for (int i = 0; i < rowCount; ++i) {
		for (int j = 0; j < columnCount; ++j) {
			for (int k = 0; k < levelCount; ++k) {
				if((std::abs(i-(int)rowNumberIndex) + std::abs(j-(int)columnNumberIndex) + std::abs(k-(int)levelNumberIndex) <= radius) && !monitor->getCell(i,j,k)->isPartOfBoundary()) {
					if(!(i == rowNumberIndex && j == columnNumberIndex && k == levelNumberIndex))
						neighborCellPosition.setPosition(i,j,k);
						neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(rowCount*(k*columnCount + j) + i);
				}
			}
		}
	}
}

void Cell::calculateMoore2DTorusNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();

	unsigned actualI = 0, actualJ = 0;

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, 0);

	for (int i = -1*(int)radius; i < rowCount + radius; ++i) {
		for (int j = -1*(int)radius; j < columnCount + radius; ++j) {
			if((std::abs(i-(int)rowNumberIndex) <= radius) && (std::abs(j-(int)columnNumberIndex) <= radius) ) {//&& !monitor->getCell(i,j,0)->isPartOfBoundary()) {
				if(!(rowNumberIndex == i % rowCount && columnNumberIndex == j  % columnCount)) {
					if(i < 0) actualI = (i + rowCount);
					else if(i > rowCount) actualI = i % rowCount;
					else actualI= i;
					if(j < 0) actualJ = (j + rowCount);
					else if(j > columnCount) actualJ = j % columnCount;
					else actualJ = j;
					neighborCellPosition.setPosition(actualI,actualJ,0);
					neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(actualI*columnCount + actualJ);
				}
			}
		}
	}
}

void Cell::calculateMoore2DXAxisTubeNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();

	unsigned actualJ = 0;

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, 0);
	for (int i = 0; i < rowCount; ++i) {
		for (int j = -1*(int)radius; j < (int)columnCount + (int)radius; ++j) {
			if((std::abs(i-(int)rowNumberIndex) <= radius) && (std::abs(j-(int)columnNumberIndex) <= radius) ) {//&& !monitor->getCell(i,j,0)->isPartOfBoundary()) {
				if(!(rowNumberIndex == i && columnNumberIndex == j  % (int)columnCount)) {
					if(j < 0) actualJ = (j + columnCount);
					else if(j >= columnCount) actualJ = j % columnCount;
					else actualJ = j;
					neighborCellPosition.setPosition(i,actualJ,0);
					neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(i*columnCount + actualJ);
				}
			}
		}
	}
}

void Cell::calculateMoore2DYAxisTubeNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();

	unsigned actualI = 0;

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, 0);

	for (int i = -1*(int)radius; i < (int)rowCount + (int)radius; ++i) {
		for (int j = 0; j < columnCount; ++j) {
			if((std::abs(i-(int)rowNumberIndex) <= radius) && (std::abs(j-(int)columnNumberIndex) <= radius) ) {//&& !monitor->getCell(i,j,0)->isPartOfBoundary()) {
				if(!(rowNumberIndex == i % (int)rowCount && columnNumberIndex == j)) {
					if(i < 0) actualI = (i + rowCount);
					else if(i >= rowCount) actualI = i % rowCount;
					else actualI= i;
					neighborCellPosition.setPosition(actualI,j,0);
					neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(actualI*columnCount + j);
				}
			}
		}
	}
}


void Cell::calculateNeumann2DTorusNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();

	unsigned actualI = 0, actualJ = 0;

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, 0);

	for (int i = -1*(int)radius; i < rowCount + radius; ++i) {
		for (int j = -1*(int)radius; j < columnCount + radius; ++j) {
			if((std::abs(i-(int)rowNumberIndex) + std::abs(j-(int)columnNumberIndex) <= radius) ) {//&& !monitor->getCell(i,j,0)->isPartOfBoundary()) {
				if(!(rowNumberIndex == i % rowCount && columnNumberIndex == j  % columnCount)) {
					if(i < 0) actualI = (i + rowCount);
					else if(i > rowCount) actualI = i % rowCount;
					else actualI= i;
					if(j < 0) actualJ = (j + rowCount);
					else if(j > columnCount) actualJ = j % columnCount;
					else actualJ = j;
					neighborCellPosition.setPosition(actualI,actualJ,0);
					neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(actualI*columnCount + actualJ);
				}
			}
		}
	}
}

void Cell::calculateNeumann2DXAxisTubeNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();

	unsigned actualJ = 0;

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, 0);

	for (int i = 0; i < rowCount; ++i) {
		for (int j = -1*(int)radius; j < (int)columnCount + (int)radius; ++j) {
			if((std::abs(i-(int)rowNumberIndex) + std::abs(j-(int)columnNumberIndex) <= radius) ) {//&& !monitor->getCell(i,j,0)->isPartOfBoundary()) {
				if(!(rowNumberIndex == i && columnNumberIndex == j  % (int)columnCount)) {
					if(j < 0) actualJ = (j + columnCount);
					else if(j >= columnCount) actualJ = j % columnCount;
					else actualJ = j;
					neighborCellPosition.setPosition(i,actualJ,0);
					neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(i*columnCount + actualJ);
				}
			}
		}
	}
}

void Cell::calculateNeumann2DYAxisTubeNeighborhood(unsigned radius) {

	unsigned rowCount = monitor->getRowCount();
	unsigned columnCount = monitor->getColumnCount();

	unsigned actualI = 0;

	unsigned rowNumberIndex = cellPosition->row;
	unsigned columnNumberIndex = cellPosition->column;

	CellPosition neighborCellPosition(0, 0, 0, rowCount, columnCount, 0);

	for (int i = -1*(int)radius; i < (int)rowCount + (int)radius; ++i) {
		for (int j = 0; j < columnCount; ++j) {
			if((std::abs(i-(int)rowNumberIndex) + std::abs(j-(int)columnNumberIndex) <= radius) ) {//&& !monitor->getCell(i,j,0)->isPartOfBoundary()) {
				if(!(rowNumberIndex == i % (int)rowCount && columnNumberIndex == j)) {
					if(i < 0) actualI = (i + columnCount);
					else if(i >= rowCount) actualI = i % rowCount;
					else actualI= i;
					neighborCellPosition.setPosition(actualI,j,0);
					neighborhoodCellsMap[neighborCellPosition] = monitor->getCell_(actualI*columnCount + j);
				}
			}
		}
	}
}

void Cell::calculateAlternativeNeighborhood(unsigned radius) {
	int i = 0;
	for(list<Transition*>::iterator iter = this->transitions.begin(); iter != this->transitions.end(); ++iter) {
		neighborhoodCellsMap = (*iter)->calculateAlternativeNeighborhood(radius);
		i++;
	}
	if(i == 0)
		throw new NotAssignedException("Cell", "Transition");
}

void Cell::checkIndexBoundary(unsigned index, unsigned indexBoundary, std::string what) {
	if(index > indexBoundary) {
		throw std::out_of_range("The index in " + what + " is out of bounds.");
	}
}

void Cell::setBaseIndex(unsigned baseIndex) {
	this->stateBaseIndex = baseIndex;
}

void Cell::setInputBaseIndex(unsigned baseIndex) {
	this->inputBaseIndex = baseIndex;
}

void Cell::setOutputBaseIndex(unsigned baseIndex) {
	this->outputBaseIndex = baseIndex;
}

unsigned Cell::getBaseIndex() {
	return this->stateBaseIndex;
}

unsigned Cell::getInputBaseIndex() {
	return this->inputBaseIndex;
}

unsigned Cell::getOutputBaseIndex() {
	return this->outputBaseIndex;
}

void Cell::setMonitor(CellMonitor* monitor) {
	if(monitor) {
		this->monitor = monitor;
	}
	else {
		throw new NotAssignedException("Monitor", "Cell");
	}
}

CellMonitor* Cell::getMonitor() {
	return this->monitor;
}
