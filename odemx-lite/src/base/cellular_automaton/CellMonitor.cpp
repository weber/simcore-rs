//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file CellMonitor.cpp

	\author Sascha Qualitz

	\date created at 2010/01/05

	\brief Implementation of class CellMonitor

	\sa CellMonitor.h

	\since 3.0
*/
#include <iomanip>
#include <iostream>
#include <fstream>
#include <odemx/base/cellular_automaton/CellMonitor.h>
#include <odemx/base/cellular_automaton/CellVariablesContainer.h>
#include <odemx/base/cellular_automaton/Cell.h>
#include <odemx/base/Scheduler.h>
#include <odemx/base/Simulation.h>

using namespace odemx::base;
using namespace odemx::base::cellular;
using namespace std;

//----------------------------------------------------------------------------------construction/destruction
CellMonitor::CellMonitor(Simulation& sim, const data::Label& l, unsigned variablesInCount, unsigned variablesOutCount, unsigned cellRows, unsigned cellColumns, unsigned cellLevel, double timestep, unsigned cellDimension, ProcessObserver* o)
:	Process(sim, l, o)
, 	cellDimension(cellDimension)
,	variablesInCount(variablesInCount)
,	variablesOutCount(variablesOutCount)
,	cellRows(cellRows)
,	cellColumns(cellColumns)
,	cellLevel(cellLevel)
,	timestep(timestep)
,	timeLimit(0)
{
	typeOfNeighborhood.neighborhoodType = MOORE;
	typeOfNeighborhood.radius = 1;
	cellVariablesContainer = new CellVariablesContainer(cellRows, cellColumns, cellLevel, variablesInCount, variablesOutCount, cellDimension);
	generateCellularAutomaton(cellRows, cellColumns, cellLevel, cellDimension);
	calculateNeighboorhood();

	ODEMX_TRACE << log( "create" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, Create(this));
}

CellMonitor::CellMonitor(Simulation& sim, const data::Label& l, const char*const configFileName, ProcessObserver* o)
:	Process(sim, l, o)
,	timeLimit(0)
{
	setParameterFromConfigFile(configFileName);
	cellVariablesContainer = new CellVariablesContainer(cellRows, cellColumns, cellLevel, variablesInCount, variablesOutCount, cellDimension);
	generateCellularAutomaton(cellRows, cellColumns, cellLevel, cellDimension);
	calculateNeighboorhood();

	ODEMX_TRACE << log( "create with configuration file" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, Create(this));
}

CellMonitor::CellMonitor(Simulation& sim, const data::Label& l, const char*const configFileName, const char*const startDataFileName, ProcessObserver* o)
:	Process(sim, l, o)
,	timeLimit(0)
{
	setParameterFromConfigFile(configFileName);
	cellVariablesContainer = new CellVariablesContainer(cellRows, cellColumns, cellLevel, variablesInCount, variablesOutCount, cellDimension);
	generateCellularAutomaton(cellRows, cellColumns, cellLevel, cellDimension);
	setStartDataFromConfigFile(startDataFileName);
	calculateNeighboorhood();

	ODEMX_TRACE << log( "create with configuration file and start data file" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, Create(this));
}

CellMonitor::~CellMonitor(){
	if (cellVariablesContainer) {
		delete cellVariablesContainer;
	}
}

int CellMonitor::main() {

	//initialize arrays
	for(int i = 0; i < cellVector.size(); ++i){
		if(cellVector[i]->isPartOfBoundary()) continue;
		for(std::list<Transition*>::iterator iter = cellVector[i]->transitions.begin(); iter != cellVector[i]->transitions.end(); ++iter) {
			(*iter)->outputFunction();
		}
	}

	while (getTime() < timeLimit) {
		//notifyValidState();

		ODEMX_TRACE << log( "new valid state" ).scope( typeid(CellMonitor) );

		// observer
		ODEMX_OBS(CellMonitorObserver, NewValidState(this));

		ODEMX_TRACE << log( "begin calculation of state changes" ).scope( typeid(CellMonitor) );

		// observer
		ODEMX_OBS(CellMonitorObserver, BeginCalcStateChanges(this));

		for(int i = 0; i < cellVector.size(); ++i) {
			if(cellVector[i]->isPartOfBoundary()) continue;
			for(std::list<Transition*>::iterator iter = cellVector[i]->transitions.begin(); iter != cellVector[i]->transitions.end(); ++iter) {
					(*iter)->transitionFunction(getTime());
			}
		}

		for(int i = 0; i < cellVector.size(); ++i){
			if(cellVector[i]->isPartOfBoundary()) continue;
			for(std::list<Transition*>::iterator iter = cellVector[i]->transitions.begin(); iter != cellVector[i]->transitions.end(); ++iter) {
				(*iter)->outputFunction();
			}
		}

		ODEMX_TRACE << log( "end calculation of state changes" ).scope( typeid(CellMonitor) );

		// observer
		ODEMX_OBS(CellMonitorObserver, EndCalcStateChanges(this));

		holdFor(timestep);
	}
	return 0;
}

void CellMonitor::setTimeLimit(SimTime time) {
	timeLimit = time;

	ODEMX_TRACE << log( "set time limit" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, SetTimeLimit(this, timeLimit));

}

SimTime CellMonitor::getTimeLimit() {
	return timeLimit;
}

void CellMonitor::setTransitionToCells(Transition* transition) {

	for (int i = 0; i < this->cellVector.size(); ++i) {
		cellVector[i]->setTransition(transition);
	}

	ODEMX_TRACE << log( "sets transition to all cells" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, SetTransitionToCells(this));
}

//----------------------------------------------------------- temp function for printing values
double* CellMonitor::getValues() {
	return this->cellVariablesContainer->getOutputValues();
}

unsigned CellMonitor::getRowCount() {
	return cellRows;
}

unsigned CellMonitor::getColumnCount() {
	return cellColumns;
}

unsigned CellMonitor::getLevelCount() {
	return cellLevel;
}

void CellMonitor::setIsBoundaryCell(unsigned row, unsigned column, unsigned level) {
	this->cellVector[cellRows * (cellColumns * level + column) + row]->isPartOfBoundary();
}

void CellMonitor::setCellValue(unsigned row, unsigned column, unsigned level, unsigned variableIndex, double value) {
	this->cellVector[cellRows * (cellColumns * level + column) + row]->setValue(variableIndex, value);
}

double CellMonitor::getCellValue(unsigned row, unsigned column, unsigned level, unsigned variableIndex) {
	this->cellVector[cellRows * (cellColumns * level + column) + row]->getValue(variableIndex);
}

unsigned CellMonitor::getNeighborhoodType() {
	return this->typeOfNeighborhood.neighborhoodType;
}

unsigned CellMonitor::getNeighborhoodRadius() {
	return this->typeOfNeighborhood.radius;
}

Cell* CellMonitor::getCell(unsigned row, unsigned column, unsigned level) {
	return this->cellVector[cellRows * (cellColumns * level + column) + row];
}

Cell* CellMonitor::getCell_(unsigned cellIndex) {
	return this->cellVector[cellIndex];
}

void CellMonitor::setNeighborhoodSettings(unsigned neighborhoodType, unsigned radius) {
	for (int i = 0; i < this->cellVector.size(); ++i) {
		getCell_(i)->setNeighborhoodSettings(neighborhoodType, radius);
	}
	this->typeOfNeighborhood.neighborhoodType = neighborhoodType;
	this->typeOfNeighborhood.radius = radius;

	ODEMX_TRACE << log( "sets neighborhood" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, SetNeighborhoodSettings(this, neighborhoodType, radius));
}

void CellMonitor::setStartDataFromConfigFile(std::string fileName) {

	string line;
	ifstream is(fileName.c_str());

	if(!is) {
		cout << "Could not open file!" << endl;
		exit(1);
	}

	unsigned row = 0, column = 0, level = 0, statevariable_index = 0, outputvariable_index = 0, inputvariable_index = 0, radius = 0;

	double statevariable = 0, outputvariable = 0, inputvariable = 0;

	string tempstring = "";

	Cell* cell;

	string::size_type i;

	while(getline(is, line)) {

		while(line.find_first_not_of ( " \t\n\v" ) == string::npos) {
			getline(is, line);
		}

		statevariable = -1, outputvariable = -1, inputvariable = -1;

		if(line.find("<cell row-index=\"") != string::npos) {
			statevariable_index = 0, outputvariable_index = 0, inputvariable_index = 0;

			line.erase(0, line.find('\"')+1);
			tempstring = line.substr(0, line.find("\" column-index=\"")+1);
			istringstream row_tmp(tempstring);
			row_tmp >> row;

			line.erase(0, line.find('\"')+1);
			line.erase(0, line.find('\"')+1);
			tempstring = line.substr(0, line.find("\" level-index=\"")+1);
			istringstream column_tmp(tempstring);
			column_tmp >> column;

			line.erase(0, line.find('\"')+1);
			line.erase(0, line.find('\"')+1);
			tempstring = line.substr(0, line.find("\">")+1);
			istringstream level_tmp(tempstring);
			level_tmp >> level;
			cell = getCell(row, column, level);
		}

		while(line.find("<statevariable>") != string::npos || line.find("</statevariable>") != string::npos) {
			if(line.find("</statevariable>") != string::npos) {
				statevariable_index++;
				do {
					getline(is, line);
				} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
				continue;
			}
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream statevariable_tmp(line);
			statevariable_tmp >> statevariable;
			cell->setValue(statevariable_index, statevariable);
		}

		while(line.find("<outputvariable>") != string::npos || line.find("</outputvariable>") != string::npos) {
			if(line.find("</outputvariable>") != string::npos) {
				outputvariable_index++;
				do {
					getline(is, line);
				} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
				continue;
			}
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream outputvariable_tmp(line);
			outputvariable_tmp >> outputvariable;
			cell->setOutputValue(outputvariable_index, outputvariable);
		}

		while(line.find("<inputvariable>") != string::npos || line.find("</inputvariable>") != string::npos) {
			if(line.find("</inputvariable>") != string::npos) {
				inputvariable_index++;
				do {
					getline(is, line);
				} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
				continue;
			}
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream inputvariable_tmp(line);
			inputvariable_tmp >> inputvariable;
			cell->setInputValue(inputvariable_index, inputvariable);
		}

		if(line.find("<neighborhoodtype>") != string::npos || line.find("</inputvariable>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			transform(line.begin(), line.end(), line.begin(), (int(*)(int))toupper);
			if(line.find("MOORE") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 0;
			if(line.find("NEUMANN") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 1;
			if(line.find("MOORETORUS") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 2;
			if(line.find("NEUMANNTORUS") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 3;
			if(line.find("MOOREXTUBE") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 4;
			if(line.find("NEUMANNXTUBE") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 5;
			if(line.find("MOOREYTUBE") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 6;
			if(line.find("NEUMANNYTUBE") != string::npos) cell->typeOfNeighborhood.neighborhoodType = 7;

			ODEMX_TRACE << log( "sets neighborhood type for one cell" ).scope( typeid(CellMonitor) );

			// observer
			ODEMX_OBS(CellMonitorObserver, SetNeighborhoodType(this, typeOfNeighborhood.neighborhoodType, cell->getPosition()));
		}

		if(line.find("<radius>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> radius;
			cell->typeOfNeighborhood.radius = radius;

			ODEMX_TRACE << log( "sets neighborhood radius for one cell" ).scope( typeid(CellMonitor) );

			// observer
			ODEMX_OBS(CellMonitorObserver, SetNeighborhoodRadius(this, typeOfNeighborhood.radius, cell->getPosition()));
		}

		if(line.find("<boundary>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			transform(line.begin(), line.end(), line.begin(), (int(*)(int))toupper);
			if(line.find("TRUE") != string::npos) cell->setIsBoundary();
		}
	}
}

void CellMonitor::setParameterFromConfigFile(std::string fileName) {

	string line;
	ifstream is(fileName.c_str());
	int k = -1;

	bool settedNeighborhoodType = false, settedRadius = false;

	if(!is) {
		cout << "Could not open file!" << endl;
		exit(1);
	}

	while(getline(is, line)) {
		if(line.find("<rowcount>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			cellRows = k;
		}
		if(line.find("<columncount>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			cellColumns = k;
		}
		if(line.find("<levelcount>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			cellLevel = k;
		}
		if(line.find("<outputvariablesize>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			variablesOutCount = k;
		}
		if(line.find("<inputvariablesize>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			variablesInCount = k;
		}
		if(line.find("<celldimension>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			cellDimension = k;
		}
		if(line.find("<timestep>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			timestep = k;
		}
		if(line.find("<neighboorhoodtype>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			transform(line.begin(), line.end(), line.begin(), (int(*)(int))toupper);
			if(line.find("MOORE") != string::npos) typeOfNeighborhood.neighborhoodType = 0;
			if(line.find("NEUMANN") != string::npos) typeOfNeighborhood.neighborhoodType = 1;
			if(line.find("MOORETORUS") != string::npos) typeOfNeighborhood.neighborhoodType = 2;
			if(line.find("NEUMANNTORUS") != string::npos) typeOfNeighborhood.neighborhoodType = 3;
			if(line.find("MOOREXTUBE") != string::npos) typeOfNeighborhood.neighborhoodType = 4;
			if(line.find("NEUMANNXTUBE") != string::npos) typeOfNeighborhood.neighborhoodType = 5;
			if(line.find("MOOREYTUBE") != string::npos) typeOfNeighborhood.neighborhoodType = 6;
			if(line.find("NEUMANNYTUBE") != string::npos) typeOfNeighborhood.neighborhoodType = 7;

			settedNeighborhoodType = true;
		}
		if(line.find("<radius>") != string::npos) {
			do {
				getline(is, line);
			} while(line.find_first_not_of ( " \t\n\v" ) == string::npos);
			istringstream tmp(line);
			tmp >> k;
			typeOfNeighborhood.radius = k;

			settedRadius = true;
		}

		if(settedNeighborhoodType && settedRadius) {
			ODEMX_TRACE << log( "sets neighborhood" ).scope( typeid(CellMonitor) );

			// observer
			ODEMX_OBS(CellMonitorObserver, SetNeighborhoodSettings(this, typeOfNeighborhood.neighborhoodType, typeOfNeighborhood.radius));
		}
	}
}

//-----------------------------------------------------------generate cellular automaton
void CellMonitor::generateCellularAutomaton(unsigned rows, unsigned  columns, unsigned level, unsigned cellDimension){

	Cell* cell;
	unsigned variablesStateCounts = 0, variablesInCounts = 0, variablesOutCounts = 0;
	//generate cells and set the parameters
	for(int k = 0; k < level; ++k){
		for(int j = 0; j < columns; ++j) {
			for(int i = 0; i < rows; ++i) {
				cell = new Cell(i, j, k, this->variablesInCount, this->variablesOutCount, this->cellDimension, this);
				cell->setBaseIndex(variablesStateCounts);
				cell->setInputBaseIndex(variablesInCounts);
				cell->setOutputBaseIndex(variablesOutCounts);
				cell->setNeighborhoodSettings(typeOfNeighborhood.neighborhoodType, typeOfNeighborhood.radius, false);
				variablesStateCounts += cell->getDimension();
				variablesInCounts += cell->getInputDimension();
				variablesOutCounts += cell->getOutputDimension();
				this->cellVector.push_back(cell);
			}
		}
	}

	ODEMX_TRACE << log( "generate cellular automaton" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, GenerateCellularAutomaton(this));
}

void CellMonitor::calculateNeighboorhood() {
	for(int i = 0; i < cellColumns*cellRows*cellLevel; ++i) {
		this->cellVector[i]->calculateNeighborhood();
	}

	ODEMX_TRACE << log( "calculate neighborhood" ).scope( typeid(CellMonitor) );

	// observer
	ODEMX_OBS(CellMonitorObserver, CalculateNeighboorhood(this));
}

CellMonitorTrace::CellMonitorTrace(CellMonitor* cellMonitor, const string & fileName)
: out(0),
  firstTime(true),
  fileName(fileName),
  cellMonitor(cellMonitor)
{
#ifdef ODEMX_USE_OBSERVATION
	if( cellMonitor != 0 )
	{
		cellMonitor->data::Observable< CellMonitorObserver >::addObserver( this );
	}
#endif

	if( ! fileName.empty() )
	{
		openFile( fileName );
	}
	else if( cellMonitor != 0 )
	{
		std::string fn = cellMonitor->getLabel();
		fn += "_trace.xml";
		openFile( fn );
	}
}

CellMonitorTrace::~CellMonitorTrace()
{
	if( out == 0 || out == &std::cout )
	{
		return;
	}
	*out << "</tracefile>" << endl;

	static_cast< std::ofstream* >( out )->close();
	delete out;
}

void CellMonitorTrace::onNewValidState( CellMonitor* sender )
{
	using namespace std;

	assert( sender != 0 );

	if( firstTime )
	{
		if( out == 0 )
		{
			std::string fn = fileName;
			fn += "_trace.xml";
			openFile( fn );
		}

		*out << "<?xml version=\"1.0\"?>" << endl;
		*out << "<!DOCTYPE tracefile [" << endl;
		*out << "	<!ELEMENT tracefile (ca)+>" << endl;
		*out << "	<!ELEMENT ca (cell)+>" << endl;
		*out << endl;
		*out << "	<!ELEMENT cell (statevariable+, outputvariable+, inputvariable+)>" << endl;
		*out << endl;
		*out << "	<!ELEMENT statevariable (#PCDATA)>" << endl;
		*out << "	<!ELEMENT outputvariable (#PCDATA)>" << endl;
		*out << "	<!ELEMENT inputvariable (#PCDATA)>" << endl;
		*out << endl;
		*out << "	<!ATTLIST tracefile name CDATA #REQUIRED>" << endl;
		*out << "	<!ATTLIST ca time CDATA #REQUIRED>" << endl;
		*out << "	<!ATTLIST cell" << endl;
		*out << "		row-index CDATA #REQUIRED" << endl;
		*out << "		column-index CDATA #REQUIRED" << endl;
		*out << "		level-index CDATA #REQUIRED" << endl;
		*out << "	>" << endl;
		*out << "]>" << endl;

		*out << endl;
		*out << endl;
		*out << "<tracefile name=\"T R A C E   F I L E   F O R  C E L L U L A R   A U T O M A T O N " << sender->getLabel() << "\">" << endl;
		firstTime=false;
	}

	( *out ).setf( std::ios::showpoint );
	( *out ).setf( std::ios::fixed );

	*out << setprecision( 2 ) << "	<ca time=\"" << sender->getTime() << "\">" << endl;
	for (int k = 0; k < sender->getLevelCount(); ++k) {
		for (int i = 0; i < sender->getRowCount(); ++i) {
			for (int j = 0; j < sender->getColumnCount(); ++j) {
				if(sender->getCell(i,j,k)->isPartOfBoundary()) continue;
				*out << "		<cell row-index=\"" << sender->getCell(i, j, k)->getPosition()->getRowNumber() << "\""
						<< " column-index=\"" << sender->getCell(i, j, k)->getPosition()->getColumnNumber() << "\""
						<< " level-index=\"" << sender->getCell(i, j, k)->getPosition()->getLevelNumber() << "\""
						<< ">" << endl;

				for( int l = 0; l < sender->getCell(i, j, k)->getDimension(); ++l ) {
					*out << "			<statevariable>" << endl;
					*out << "				" << sender->getCell(i, j, k)->getValue(l) << endl;
					*out << "			</statevariable>" << endl;
				}
				for( int l = 0; l < sender->getCell(i, j, k)->getDimension(); ++l ) {
					*out << "			<outputvariable>" << endl;
					*out << "				" << sender->getCell(i, j, k)->getOutputValue(l) << endl;
					*out << "			</outputvariable>" << endl;
				}
				for( int l = 0; l < sender->getCell(i, j, k)->getDimension(); ++l ) {
					*out << "			<inputvariable>" << endl;
					*out << "				" << sender->getCell(i, j, k)->getValue(l) << endl;
					*out << "			</inputvariable>" << endl;
				}

				*out << "		</cell>" << endl;
			}
		}
	}
	*out << "	</ca>" << endl;
	*out << endl;
}

void CellMonitorTrace::openFile( const std::string& fileName )
{
	out = new std::ofstream( fileName.c_str() );
	if( out == 0 || !( *out ) )
	{
		// Error: cannot open file
		std::cerr << "CellMonitorTrace::openFile(): cannot open file; sending output to stdout." << std::endl;
		out = &std::cout;
	}
}
