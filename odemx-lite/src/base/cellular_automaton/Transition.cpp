//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Transition.cpp

 \author Sascha Qualitz

 \date created at 2010/01/05

 \brief Implementation of class Cell

 \sa Cell.h

 \since 3.0
 */

#include <odemx/base/cellular_automaton/Transition.h>

using namespace odemx::base;
using namespace odemx::base::cellular;
using namespace std;

//------------------------------------------------------------------------------construction/destruction
Transition::Transition() {

}

Transition::~Transition() {

}

void Transition::setCell(Cell* cell) {
	if(cell) {
		this->cell = cell;
	}
	else {
		throw new NotAssignedException("Cell", "Transition");
	}
}

void Transition::outputFunction() {
	// reimplement this
	for (int i = 0; i < this->cell->getDimension(); ++i) {
		cell->setOutputValue(i,cell->getValue(i));
	}
}

double Transition::getValue(unsigned variableIndex) {
	 if(cell) {
		 return cell->getValue(variableIndex);
	}
	else {
		throw new NotAssignedException("Cell", "Transition");
	}
}

void Transition::setValue(unsigned variableIndex, double value) {
	if(cell) {
		cell->setValue(variableIndex, value);
	}
	else {
		throw new NotAssignedException("Cell", "Transition");
	}
}

Cell* Transition::searchNeighbor(unsigned row, unsigned column, unsigned level) {
	return this->cell->searchNeighbor(row, column, level);
}

std::map<CellPosition, Cell*, ComparePositions> Transition::calculateAlternativeNeighborhood(unsigned radius) {
	// reimplement this if needed
	/**
	 	 Fill a map with cells which belongs to the neighborhood of the cell.
	 	 Example:

	 	 CellPosition position(this->cell->cellPosition->row -1, this->cell->cellPosition->column -3, this->cell->cellPosition->level -1, this->cell->monitor->getRowCount(), this->cell->monitor->getColumnCount(), this->cell->monitor->getLevelCount())
	 	 std::map<CellPosition, Cell*, ComparePositions> map;

	 	 map[position] = this->cell->monitor->getCell(position->row, position->column, position->level);

	 	 One have to check that the chosen cells really exists.
	 */
}

//------------------------------------------------------------------------------NotAssignedException
NotAssignedException::NotAssignedException(const char* missingObject, const char* object) {
	this->msg = "This ";
	this->msg += object;
	this->msg += "-Object has no ";
	this->msg += missingObject;
	this->msg += " assigned.";
}

NotAssignedException::~NotAssignedException() throw() {}

const char* NotAssignedException::what() const throw() {
	return this->msg.c_str();
}
