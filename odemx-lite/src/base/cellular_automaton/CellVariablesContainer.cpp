//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file CellVariablesContainer.cpp

 \author Sascha Qualitz

 \date created at 2010/01/05

 \brief Implementation of class CellVariablesContainer

 \sa CellVariablesContainer.h

 \since 3.0
 */

#include <odemx/base/cellular_automaton/CellVariablesContainer.h>

using namespace odemx::base;
using namespace odemx::base::cellular;
using namespace std;

CellVariablesContainer::CellVariablesContainer(unsigned cellRows, unsigned cellColumns, unsigned cellLevel, unsigned variablesInCount, unsigned variablesOutCount, unsigned cellDimension)
:  cellRows(cellRows)
,  cellColumns(cellColumns)
,  cellLevel(cellLevel)
,  variablesInCount(variablesInCount)
,  variablesOutCount(variablesOutCount)
,  cellDimension(cellDimension)
{
	unsigned inputSize = cellRows*cellColumns*cellLevel*variablesInCount;
	unsigned outputSize = cellRows*cellColumns*cellLevel*variablesOutCount;
	unsigned stateSize = cellRows*cellColumns*cellLevel*cellDimension;

	input_values = new double[inputSize];
	state_values = new double[stateSize];
	output_values = new double[outputSize];

	for (int i = 0; i < stateSize; ++i) {
		state_values[i] = 0;
	}
	for (int i = 0; i < inputSize; ++i) {
		input_values[i] = 0;
	}
	for (int i = 0; i < outputSize; ++i) {
		output_values[i] = 0;
	}
}

CellVariablesContainer::~CellVariablesContainer() {
	//nothing todo
}

void CellVariablesContainer::setOutputValue(unsigned baseIndex, unsigned variableIndex, double value) {
	this->output_values[baseIndex+variableIndex] = value;
}

double CellVariablesContainer::getOutputValue(unsigned baseIndex, unsigned variableIndex) {
	return this->output_values[baseIndex+variableIndex];
}

void CellVariablesContainer::setStateValue(unsigned baseIndex, unsigned variablesIndex, double value) {
	this->state_values[baseIndex + variablesIndex] = value;
}

double CellVariablesContainer::getStateValue(unsigned baseIndex, unsigned variablesIndex) {
	return this->state_values[baseIndex + variablesIndex];
}

void CellVariablesContainer::setInputValue(unsigned baseIndex, unsigned variableIndex, double value) {
	this->input_values[baseIndex+variableIndex] = value;
}

double CellVariablesContainer::getInputValue(unsigned baseIndex, unsigned variableIndex) {
	return this->input_values[baseIndex+variableIndex];
}

unsigned CellVariablesContainer::getNumberOfRows() {
	return this->cellRows;
}

unsigned CellVariablesContainer::getNumberOfColumns() {
	return this->cellColumns;
}

unsigned CellVariablesContainer::getNumberOfLevels() {
	return this->cellLevel;
}

//----------------------------------------------------------- temp function for printing values

double* CellVariablesContainer::getOutputValues() {
	return this->output_values;
}
