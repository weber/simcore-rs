//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Rate.cpp

	\author Sascha Qualitz

	 \date created at 2009/10/26

	 \brief Implementation of Rate

	\sa Rate.h

	\since 3.0
*/

#include <odemx/base/continuous/Rate.h>

#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/State.h>
#include <odemx/base/continuous/Continuous.h>
#include <odemx/base/continuous/Monitor.h>

using namespace odemx::base;
using namespace odemx::base::continuous;
using namespace std;

Rate::Rate()
:	continuous(0),
	index_(0)
{}

Rate::Rate(Continuous* continuous)
:	continuous(continuous),
	index_(0)
{}

Rate::~Rate() {
	// nothing todo!!
}

void Rate::setContinuous(Continuous* continuous) {
	if (continuous != 0) {
		this->continuous = continuous;
	} else
		throw new NotAssignedException("Continuous", "Rate");
}

void Rate::setValue(double value) {
	if (continuous != 0) {
		continuous->setDerivative(index_, value);
	} else
		throw new NotAssignedException("Continuous", "Rate");
}

double Rate::getValue() {
	if (continuous != 0)
		return continuous->getDerivative(index_);
	
	throw new NotAssignedException("Continuous", "Rate");
}

Rate& Rate::operator =(const double value) {
	setValue(value);
	return *this;
}

Rate& Rate::operator [](const unsigned i) {
	if (continuous != 0) {
		if(i > continuous->getDimension()-1)
			throw std::out_of_range("The index in an object of type Rate is out of bounds");
		index_ = i;

		return *this;
	} else
		throw new NotAssignedException("Continuous", "Rate");
}

#endif /* ODEMX_USE_CONTINUOUS */
