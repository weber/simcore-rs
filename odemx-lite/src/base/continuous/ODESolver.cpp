//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file ODESolver.cpp

	\author Michael Fiedler

	\date created at 2009/01/02

	\brief Implementation of class ODESolver

	\sa ODESolver.h

	\since 3.0
*/

#include <odemx/base/continuous/ODESolver.h>

#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/Monitor.h>

using namespace odemx::base;
using namespace odemx::base::continuous;
using namespace std;

//--------------------------------------------------------------------dConstruction
ODESolver::ODESolver(Simulation& sim) : Producer(sim, "ODESolver") {
	//nothing todo
}
//--------------------------------------------------------------------destruction
ODESolver::~ODESolver() {
	//nothing todo
}

//--------------------------------------------------------evaluate monitor system
//this is to pass access to these function to subclasses
void ODESolver::evalF(double time) {
	monitor->evalF(time);
}

void ODESolver::evalJacobian(double time) {
	monitor->evalJacobian(time);
}

//-------------------------------------------------------handle assigned monitor
void ODESolver::setMonitor(Monitor* monitor) {
	if (monitor) {
		monitor->setODESolver(this);
	}
}

Monitor* ODESolver::getMonitor() {
	return monitor;
}

//------------------------------------------------------get variables of monitor
VariableContainer* ODESolver::getVariableContainerOfMonitor() {
	return monitor->variables;
}

//--------------------------------------------------------integration Parameters
void ODESolver::setErrorLimit(double maxError) {
	if (maxError > 0) {
		errorLimit = maxError;
	}
}

double ODESolver::getErrorLimit() {
	return this->errorLimit;
}

void ODESolver::setErrorType(ErrorType type) {
	errorType = type;
}

void ODESolver::setStepLength(double min, double max) {
	if (min > 0) minStep = min;
	if (max > 0) maxStep = max;
	if (maxStep < minStep) maxStep = minStep;
}

double ODESolver::getStepLength() {
	return lastStep;
}

#endif /* ODEMX_USE_CONTINUOUS */
