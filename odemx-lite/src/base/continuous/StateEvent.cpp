//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file StateEvent.cpp
 
 \author Michael Fiedler
 
 \date created at 2009/01/02
 
 \brief Implementation of class StateEvent
 
 \sa StateEvent.h
 
 \since 3.0
 */

#include <odemx/base/continuous/StateEvent.h>

#ifdef ODEMX_USE_CONTINUOUS

using namespace odemx::base;
using namespace odemx::base::continuous;
using namespace std;

StateEvent::~StateEvent() {

}

//-----------------------------------------------------------handle assigned monitor
void StateEvent::setMonitor(Monitor *monitor) {
	monitor->addStateEvent(this);
}

Monitor* StateEvent::getMonitor() {
	return monitor;
}

#endif /* ODEMX_USE_CONTINUOUS */
