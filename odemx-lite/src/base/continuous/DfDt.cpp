//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file DfDt.cpp

	\author Sascha Qualitz

	 \date created at 2009/10/26

	 \brief Implementation of DfDt

	\sa DfDt.h

	\since 3.0
*/

#include <odemx/base/continuous/DfDt.h>

#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/Continuous.h>
#include <odemx/base/continuous/Monitor.h>

using namespace odemx::base;
using namespace odemx::base::continuous;
using namespace std;

DfDt::DfDt()
:	continuous(0),
	index_(0)
{}

DfDt::DfDt(Continuous* continuous)
:	continuous(continuous),
	index_(0)
{}

DfDt::~DfDt() {
	// nothing todo!
}

DfDt& DfDt::operator =(const double value) {
	setValue(value);
	return *this;
}

DfDt& DfDt::operator [](const unsigned i) {
	if (continuous != 0) {
		if(i > continuous->getDimension()-1)
			throw std::out_of_range("The index in an object of type DfDt is out of bounds");

		index_ = i;

		return *this;
	} else
		throw new NotAssignedException("Continuous", "DfDt");
}

void DfDt::setContinuous(Continuous* continuous) {
	if (continuous != 0) {
		this->continuous = continuous;
	} else
		throw new NotAssignedException("Continuous", "DfDt");
}

double DfDt::getValue() const{
	if (continuous != 0) {
		return continuous->getDfDt(index_);
	} else
		throw new NotAssignedException("Continuous", "DfDt");
}

void DfDt::setValue(double value) {
	if (continuous != 0) {
		continuous->setDfDt(index_, value);
	} else
		throw new NotAssignedException("Continuous", "DfDt");
}

#endif /* ODEMX_USE_CONTINUOUS */

