//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file JacobiMatrix.cpp

	\author Sascha Qualitz

	 \date created at 2009/10/26

	 \brief Implementation of JacobiMatrix

	\sa JacobiMatrix.h

	\since 3.0
*/

#include <odemx/base/continuous/JacobiMatrix.h>

#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/Continuous.h>
#include <odemx/base/continuous/Monitor.h>

using namespace odemx::base;
using namespace odemx::base::continuous;
using namespace std;

JacobiMatrix::JacobiMatrix()
:	continuous(0),
	otherContinuous(0),
	row_(0),
	column_(0)
{}

JacobiMatrix::JacobiMatrix(Continuous* continuous)
:	continuous(continuous),
	otherContinuous(0),
	row_(0),
	column_(0)
{}

JacobiMatrix::JacobiMatrix(Continuous* continuous, Continuous* otherContinuous)
:	continuous(continuous),
	otherContinuous(otherContinuous),
	row_(0),
	column_(0)
{}

JacobiMatrix::~JacobiMatrix() {
	// TODO Auto-generated destructor stub
}

double JacobiMatrix::getValue() const {
	if(continuous != 0 || otherContinuous != 0) {
		if(otherContinuous != 0)
			return continuous->getJacobi(row_, otherContinuous, column_);
		else
			return continuous->getJacobi(row_, column_);
	} else
		throw new NotAssignedException("Continuous", "JacobiMatrix");

}

void JacobiMatrix::setValue(double value) {
	if(continuous != 0 || otherContinuous != 0) {
		if(otherContinuous != 0)
			continuous->setJacobi(row_, otherContinuous, column_, value);
		else
			continuous->setJacobi(row_, column_, value);
	} else
		throw new NotAssignedException("Continuous", "JacobiMatrix");
}

JacobiMatrix& JacobiMatrix::operator =(const double value) {
	setValue(value);
	return *this;
}

JacobiMatrix& JacobiMatrix::operator ()(const unsigned row, const unsigned column) {
	if(continuous != 0 || otherContinuous != 0) {
		if(otherContinuous != 0) {
			if(row > continuous->getDimension()-1 || column > otherContinuous->getDimension()-1)
						throw std::out_of_range("An index in an object of type JacobiMatrix is out of bounds");
		} else {
			if(row > continuous->getDimension()-1 || column > continuous->getDimension()-1)
				throw std::out_of_range("An index in an object of type JacobiMatrix is out of bounds");
		}

		row_ = row;
		column_ = column;

		return *this;
	} else
		throw new NotAssignedException("Continuous", "JacobiMatrix");
}

void JacobiMatrix::setContinuous(Continuous* continuous) {
	if(continuous != 0) {
		this->continuous = continuous;
	} else
		throw new NotAssignedException("Continuous", "JacobiMatrix");
}

void JacobiMatrix::setContinuous(Continuous* continuous, Continuous* otherContinuous) {
	if(continuous != 0 || otherContinuous != 0) {
		this->continuous = continuous;
		this->otherContinuous = otherContinuous;
	} else
		throw new NotAssignedException("Continuous", "JacobiMatrix");
}


#endif /* ODEMX_USE_CONTINUOUS */
