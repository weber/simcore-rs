//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Comparators.cpp
 * @author Ronald Kluth
 * @date created at 2009/02/23
 * @brief Implementation of odemx::base::DefaultCmp and odemx::base::QPriorityCmp
 * @sa Comparators.h
 * @since 3.0
 */

#include <odemx/base/Comparators.h>
#include <odemx/base/Process.h>
#include <odemx/base/Sched.h> // Sched op<

namespace odemx {
namespace base {

bool DefaultCmp::operator()( const Sched* s1, const Sched* s2 ) const
{
	return s1->getPriority() > s2->getPriority();
}

bool QPriorityCmp::operator()( const Process* p1, const Process* p2 ) const
{
	return p1->getQueuePriority() > p2->getQueuePriority();
}

// comparison functor definition, needed for ExecutionList::inSort()
DefaultCmp defCmp;

// comparison functor definition, needed for ProcessQueue::inSort()
QPriorityCmp qPrioCmp;

} } // namespace odemx::base
