//------------------------------------------------------------------------------
//	Copyright (C) 2007, 2008, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Sched.cpp
 * @author Ronald Kluth
 * @date created at 2007/04/04
 * @brief Implementation of odemx::base::Sched
 * @sa Sched.h
 * @since 2.0
 */

#include <odemx/base/Sched.h>
#include <odemx/base/Simulation.h>

namespace odemx {
namespace base {

//------------------------------------------------------construction/destruction

Sched::Sched( Simulation& sim, const data::Label& label, SchedType st )
:	data::Producer( sim, label )
,	schedType_( st )
,	scheduled_( false )
{
}

Sched::~Sched()
{
}

//-------------------------------------------------------------------status info

Sched::SchedType Sched::getSchedType() const
{
	return schedType_;
}

bool Sched::isScheduled() const
{
	return scheduled_;
}

SimTime Sched::getTime() const
{
	return getSimulation().getTime();
}

} // namespace base

//--------------------------------------------------------------global operators

// determine order of processes and events in execution list
// by checking for the earlier (smaller) execution time and
// in case of equality comparing which has higher priority
bool operator<( const base::Sched& first, const base::Sched& second )
{
	return ( first.getExecutionTime() < second.getExecutionTime() )  ?  (true) :
 			 (( first.getExecutionTime() > second.getExecutionTime() )  ?  (false) :
 				(first.getPriority() > second.getPriority()));
}

} // namespace odemx
