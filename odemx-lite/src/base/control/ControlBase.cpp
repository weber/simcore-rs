#include <odemx/base/control/ControlBase.h>
#include <odemx/base/Process.h>
#include <iostream>


namespace odemx {
namespace base {

void ControlBase::signal()
{
	for(auto process: waitingProcesses_)
	{
		process->hold();
	}
}

synchronization::IMemory::Type ControlBase::getMemoryType() const
{
	return CONTROL;
}

bool ControlBase::isAvailable()
{
	return false;
}

void ControlBase::alert()
{}

bool ControlBase::remember(base::Sched* newObject)
{
	return false;
}

bool ControlBase::forget(base::Sched* rememberedObject)
{
	return false;
}

void ControlBase::eraseMemory()
{}

}
}
