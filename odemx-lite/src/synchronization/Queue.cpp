//------------------------------------------------------------------------------
//	Copyright (C) 2003, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Queue.cpp
 * @author Ralf Gerstenberger
 * @date created at 2003/06/28
 * @brief Implementation of odemx::sync::Queue
 * @sa Queue.h
 * @since 1.0
 */

#ifdef _MSC_VER
#pragma warning (disable : 4786)
#endif

#include <odemx/synchronization/Queue.h>
#include <odemx/base/Process.h>

#include <string>
#include <vector>

namespace odemx {
namespace synchronization {

//------------------------------------------------------construction/destruction

Queue::Queue( base::Simulation& sim, const data::Label& label )
:	data::Producer( sim, label )
,	ProcessQueue()
{
	ODEMX_TRACE << log( "create" ).scope( typeid(Queue) );
	statistics << update( "length", getLength() ).scope( typeid(Queue) );
}

Queue::~Queue()
{
	ODEMX_TRACE << log( "destroy" ).scope( typeid(Queue) );
}

//------------------------------------------------------------queue manipulation

void Queue::popQueue()
{
	base::Process* first = ProcessQueue::getTop();
	ProcessQueue::popQueue();

	if( first != 0 )
	{
		ODEMX_TRACE << log( "pop queue" )
				.detail( "process", first->getLabel() )
				.scope( typeid(Queue) );
	}
	else
	{
		warning << log( "Queue::popQueue(): called on empty queue" )
				.scope( typeid(Queue) );
	}

	statistics << update( "length", getLength() ).scope( typeid(Queue) );
}

void Queue::remove( base::Process* p )
{
	assert( p );

	ProcessQueue::remove( p );

	ODEMX_TRACE << log( "remove" )
			.detail( "process", p->getLabel() )
			.scope( typeid(Queue) );

	statistics << update( "length", getLength() ).scope( typeid(Queue) );
}

void Queue::inSort( base::Process* p,  bool fifo )
{
	assert( p );

	ProcessQueue::inSort( p, fifo );

	ODEMX_TRACE << log( "insert" )
			.detail( "process", p->getLabel() )
			.scope( typeid(Queue) );

	statistics << update( "length", getLength() ).scope( typeid(Queue) );
}

} } // namespace odemx::sync
