//------------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ProcessQueue.cpp
 * @author Ralf Gerstenberger
 * @date created at 2002/03/25
 * @brief Implementation of odemx::sync::ProcessQueue
 * @sa ProcessQueue.h
 * @since 1.0
 */

#include <odemx/synchronization/ProcessQueue.h>
#include <odemx/base/Comparators.h>
#include <odemx/base/Process.h>
#include <odemx/util/ListInSort.h>

#include <algorithm>

namespace odemx {
namespace synchronization {

//------------------------------------------------------construction/destruction

ProcessQueue::ProcessQueue()
:	processes_()
,	length_( 0 )
{
}

ProcessQueue::~ProcessQueue()
{
}

//------------------------------------------------------------------queue access

bool ProcessQueue::isEmpty() const
{
	return processes_.empty();
}

base::Process* ProcessQueue::getTop() const
{
	return processes_.empty() ? 0 : *processes_.begin();
}

const base::ProcessList& ProcessQueue::getList() const
{
	return processes_;
}

ProcessQueue::SizeType ProcessQueue::getLength() const
{
	return length_; // a lot faster than calling list::size()
}

ProcessQueue::SizeType ProcessQueue::getPosition( base::Process* p ) const
{
	assert( p != 0 );

	SizeType pos = 1;

	base::ProcessList::const_iterator iter = processes_.begin();

	// count positions until p is found
	while( iter != processes_.end() && ( *iter ) != p )
	{
		++pos;
		++iter;
	}

	// check if p was found
	if( iter != processes_.end() && ( *iter ) == p )
	{
		return pos;
	}

	return 0;
}

//------------------------------------------------------------queue manipulation

void ProcessQueue::popQueue()
{
	if( ! processes_.empty() )
	{
		ProcessQueue::remove( *processes_.begin() );
	}
}

void ProcessQueue::remove( base::Process* p )
{
	assert( p != 0 );

	// Process p forgets this queue
	p->dequeue( this );

	processes_.erase( p->queueListIter_ );
	--length_;
}

void ProcessQueue::inSort( base::Process* p,  bool fifo )
{
	assert( p != 0 );

	// if already in this queue, remove previous entry of Process* p
	if( p->queue_ == this )
	{
		processes_.erase( p->queueListIter_ );
		p->queue_ = 0; // queue_ != 0 would cause error in Process::enqueue()
	}
	// not yet in queue, increment size
	else
	{
		++length_;
	}

	// Process p has to remember this queue
	p->enqueue( this );

	// insert s into ordered list and store the location as iterator
	p->queueListIter_ =
		ListInSort< base::Process*, base::QPriorityCmp >(
				processes_, p, base::qPrioCmp, fifo );
}

//----------------------------------------------------------free queue functions

void awakeAll( ProcessQueue* q )
{
	if( q->isEmpty() )
	{
		return;
	}

	base::Sched* currentlyRunning = q->getTop()->getCurrentSched(); // may return 0 !

	base::ProcessList::const_iterator i;
	for( i = q->getList().begin(); i != q->getList().end(); ++i )
	{
		base::Process* p = *i;

		assert( static_cast< base::Sched* >( p ) != currentlyRunning );

		// cannot activate after current event, must use activate
		if( ! currentlyRunning || currentlyRunning->getSchedType() == base::Sched::EVENT )
		{
			p->activate();
		}
		else // a process is currently running
		{
			p->activateAfter( currentlyRunning );
		}
	}
}

void awakeFirst( ProcessQueue* q )
{
	if( q->isEmpty() )
	{
		return;
	}

	base::Sched* current = q->getTop()->getCurrentSched(); // may return 0 !

	// cannot activate after current event, must use activate
	if( ! current || current->getSchedType() == base::Sched::EVENT )
	{
		q->getTop()->activate();
	}
	else
	{
		if( q->getTop() != current )
		{
			q->getTop()->activateAfter( current );
		}
	}
}

void awakeNext( ProcessQueue* q, base::Process* p )
{
	if( q->isEmpty() )
	{
		return;
	}

	base::Sched* current = p->getCurrentSched(); // may return 0 !

	base::ProcessList::const_iterator iter = std::find( q->getList().begin(), q->getList().end(), p );

	// check if p was found
	if( iter != q->getList().end() )
	{
		++iter;

		// check if next position is valid
		if( iter != q->getList().end() )
		{
			base::Process* p = *iter;

			// cannot activate after current event, must use activate
			if( ! current || current->getSchedType() == base::Sched::EVENT )
			{
				p->activate();
			}
			else
			{
				if( p != current )
				{
					p->activateAfter( current );
				}
			}
		}
	}
}

} } // namespace odemx::sync
