//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Tab.cpp
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Implementation of odemx::statistics::Tab
 * @sa Tab.h
 * @since 3.0
 */

#include <odemx/statistics/Tab.h>
#include <odemx/base/DefaultSimulation.h>

namespace odemx {
namespace statistics {

//------------------------------------------------------construction/destruction

Tab::Tab( base::Simulation& sim, const data::Label& label )
:	ReportProducer( sim, label )
,	updateCount_( 0 )
,	resetTime_( sim.getTime() )
{
}

Tab::~Tab()
{
}

//---------------------------------------------------------------------modifiers

void Tab::update()
{
	++updateCount_;
}

void Tab::reset( base::SimTime time )
{
	resetTime_ = time;
	updateCount_ = 0;
}

//-----------------------------------------------------------------------getters

std::size_t Tab::getUpdateCount() const
{
	return updateCount_;
}

base::SimTime Tab::getResetTime() const
{
	return resetTime_;
}

} } // namespace odemx::statistics
