//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Tally.cpp
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Implementation of odemx::statistics::Tally
 * @sa Tally.h
 * @since 3.0
 */

#include <odemx/statistics/Tally.h>
#include <odemx/data/ReportTable.h>
#include <odemx/data/Report.h>

#include <cmath>

namespace odemx {
namespace statistics {

//------------------------------------------------------construction/destruction

Tally::Tally( base::Simulation& sim, const data::Label& label )
:	Tab( sim, label )
,	minValue_( 0.0 )
,	maxValue_( 0.0 )
,	sum_( 0.0 )
,	sumSq_( 0.0 )
{
}

Tally::~Tally()
{
}

//---------------------------------------------------------------------modifiers

void Tally::update( double value )
{
	Tab::update();

	sum_ += value;
	sumSq_ += std::pow( value, 2 );

	if( getUpdateCount() == 1 )
	{
		minValue_ = maxValue_ = value;
	}
	else if( value < minValue_ )
	{
		minValue_ = value;
	}
	else if( value > maxValue_ )
	{
		maxValue_ = value;
	}
}

void Tally::reset( base::SimTime time )
{
	Tab::reset( time );
	sum_ = sumSq_ = minValue_ = maxValue_ = 0.0;
}

//-----------------------------------------------------------------------getters

double Tally::getMin() const
{
	return minValue_;
}

double Tally::getMax() const
{
	return maxValue_;
}

double Tally::getMean() const
{
	return getUpdateCount() ? sum_ / getUpdateCount() : 0.0;
}

double Tally::getStandardDeviation() const
{
	if( getUpdateCount() <= 1 )
	{
		return 0.0;
	}

	double mean = getMean();
	double variance = std::fabs( sumSq_ / getUpdateCount() - std::pow( mean, 2 ) );
	return std::sqrt( variance );
}

//------------------------------------------------------------------------report

void Tally::report( data::Report& report )
{
	using namespace data;
	ReportTable::Definition def;
	def.addColumn( "Name", ReportTable::Column::STRING );
	def.addColumn( "Reset at", ReportTable::Column::SIMTIME );
	def.addColumn( "Uses", ReportTable::Column::INTEGER );
	def.addColumn( "Min", ReportTable::Column::REAL );
	def.addColumn( "Max", ReportTable::Column::REAL );
	def.addColumn( "Mean", ReportTable::Column::REAL );
	def.addColumn( "Standard Deviation", ReportTable::Column::REAL );
	ReportTable& table = report.getTable( "Tally Statistics", def );
	table
	<< getLabel()
	<< getResetTime()
	<< getUpdateCount()
	<< getMin()
	<< getMax()
	<< getMean()
	<< getStandardDeviation();
}

} } // namespace odemx::statistics
