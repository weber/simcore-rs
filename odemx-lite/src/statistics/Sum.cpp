//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Sum.cpp
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Implementation of odemx::statistics::Sum
 * @sa Sum.h
 * @since 3.0
 */

#include <odemx/statistics/Sum.h>
#include <odemx/data/ReportTable.h>
#include <odemx/data/Report.h>

namespace odemx {
namespace statistics {

//------------------------------------------------------construction/destruction

Sum::Sum( base::Simulation& sim, const data::Label& label )
:	Tab( sim, label )
,	sum_( 0.0 )
{
}

Sum::~Sum()
{
}

//---------------------------------------------------------------------modifiers

void Sum::update( double value )
{
	Tab::update();
	sum_ += value;
}

void Sum::reset( base::SimTime time )
{
	Tab::reset( time );
	sum_ = 0.0;
}

//-----------------------------------------------------------------------getters

double Sum::getValue() const
{
	return sum_;
}

//------------------------------------------------------------------------report

void Sum::report( data::Report& report )
{
	using namespace data;
	ReportTable::Definition def;
	def.addColumn( "Name", ReportTable::Column::STRING );
	def.addColumn( "Reset at", ReportTable::Column::SIMTIME );
	def.addColumn( "Uses", ReportTable::Column::INTEGER );
	def.addColumn( "Value", ReportTable::Column::REAL );
	ReportTable& table = report.getTable( "Sum Statistics", def );
	table
	<< getLabel()
	<< getResetTime()
	<< getUpdateCount()
	<< getValue();
}

} } // namespace odemx::statistics
