//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ErrorModelDraw.h
 * @author Ronald Kluth
 * @date created at 2010/04/05
 * @brief Declaration of class odemx::protocol::ErrorModelDraw
 * @sa ErrorModelDraw.cpp
 * @since 3.0
 */

#ifndef ODEMX_PROTOCOL_ERRORMODELDRAW_INCLUDED
#define ODEMX_PROTOCOL_ERRORMODELDRAW_INCLUDED

#include <odemx/protocol/ErrorModel.h>
#include <odemx/random/Draw.h>

namespace odemx {
namespace protocol {

/**
 * @brief Simle ErrorModel implementation
 * @ingroup protocol
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::protocol::ErrorModel odemx::random::Draw
 *
 * This class provides a random-number-based approach to model transmission
 * failures. It uses an odemx::random::Draw object in order to simulate
 * the loss of PDUs during transmission, meaning that PDUs are droppped with
 * a user-defined probability.
 */
class ErrorModelDraw
:	public ErrorModel
{
public:
	/// Creation via static method to enforce shared_ptr usage
	static ErrorModelPtr create( base::Simulation& sim, const data::Label& label,
			double probability );

	/// Destruction
	virtual ~ErrorModelDraw();

	/**
	 * @brief Implementation of the error model interface
	 * @param p The PDU that the error model is applied to
	 * @return @true if the PDU may be transmitted
	 *
	 * The implementation of the error model interface simply calls
	 * @c sample on the internal @c Draw object, which either returns 0 or 1.
	 * These value appear with the given probability and are used to
	 * decide whether PDUs may be received at their destination or not.
	 */
	virtual bool apply( PduPtr p );

private:
	/// Draw object for getting a stream of ones and zeros
	odemx::random::Draw draw_;

private:
	/// Construction private, use @c create instead
	ErrorModelDraw( base::Simulation& sim, const data::Label& label, double probability );
};

} } // namespace odemx::protocol

#endif /* ODEMX_PROTOCOL_ERRORMODELDRAW_INCLUDED */
