//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ErrorModel.h
 * @author Ronald Kluth
 * @date created at 2009/10/18
 * @brief Declaration of interface odemx::protocol::ErrorModel
 * @since 3.0
 */

#ifndef ODEMX_PROTOCOL_ERRORMODEL_INCLUDED
#define ODEMX_PROTOCOL_ERRORMODEL_INCLUDED

#include <odemx/protocol/Pdu.h>

namespace odemx {
namespace protocol {

/**
 * @brief Interface for error models used by classes Medium and Service
 * @ingroup protocol
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::protocol::Medium odemx::protocol::Service
 *
 * When modeling network simulations, it is also necessary to be able to
 * create different failure situations. In order to allow the definition of
 * any kind of failure that can occur during data transmission, the interface
 * ErrorModel is provided.
 */
class ErrorModel
{
public:
	/// Destruction
	virtual ~ErrorModel() {}

	/**
	 * @brief Process a transmitted message
	 * @param The PDU to be evaluated by the error model
	 * @retval @c true if the PDU may pass (even if it was altered)
	 * @retval @c false if the PDU shall not arrive at its destination
	 *
	 * The method may either drop the data unit completely, or it may
	 * merely change its contents, for example to model bit errors.
	 */
	virtual bool apply( PduPtr p ) = 0;
};

/// Smart pointer type to manage the lifetime of error model objects
typedef std::shared_ptr< ErrorModel > ErrorModelPtr;

} } // namespace odemx::protocol

#endif /* ODEMX_PROTOCOL_ERRORMODEL_INCLUDED */
