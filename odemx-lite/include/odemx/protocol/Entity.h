//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Entity.h
 * @author Ronald Kluth
 * @date created at 2009/10/13
 * @brief Declaration of class odemx::protocol::Entity
 * @sa Entity.cpp
 * @since 3.0
 */

#ifndef ODEMX_PROTOCOL_ENTITY_INCLUDED
#define ODEMX_PROTOCOL_ENTITY_INCLUDED

#include <odemx/protocol/ServiceProvider.h>
#include <odemx/protocol/Sap.h>

namespace odemx {
namespace protocol {

/**
 * @brief Base class for modeling all kinds of protocol entities
 * @ingroup protocol
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::protocol::Device odemx::protocol::Service
 *
 * An Entity extends the service provider functionality by the methods
 * @c send and @c pass, which are used for forwarding protocol data units
 * between adjacent layers of the protocol stack.
 */
class Entity
:	public ServiceProvider
{
public:

	/// Construction
	Entity( base::Simulation& sim, const data::Label& label );
	/// Destruction
	virtual ~Entity();

	/**
	 * @brief Forward a PDU to the lower layer via the given SAP
	 * @param lowerLayerSap The target SAP in the lower layer
	 * @param p The protocol data unit to be forwarded
	 *
	 * The function simply requests the given SAP from the lower layer
	 * and writes the PDU to it.
	 */
	void send( const std::string& lowerLayerSap, PduPtr p );

	/**
	 * @brief Forward a PDU to the upper layer via the given SAP
	 * @param upperLayerSap The target SAP in the upper layer
	 * @param p The protocol data unit to be forwarded
	 *
	 * The function simply requests the given SAP from the upper layer
	 * and writes the PDU to it.
	 */
	void pass( const std::string& upperLayerSap, PduPtr p );

	/// Service provider interface, must be implemented in subclasses
	virtual void handleInput( const std::string& sapName, PduPtr ) = 0;
};

} } // namespace odemx::protocol

#endif /* ODEMX_PROTOCOL_ENTITY_INCLUDED */
