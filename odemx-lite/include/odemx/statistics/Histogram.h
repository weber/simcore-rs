//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Histogram.h
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Declaration of odemx::statistics::Histogram
 * @sa Histogram.cpp
 * @since 3.0
 */

#ifndef ODEMX_STATS_HISTOGRAM_INCLUDED
#define ODEMX_STATS_HISTOGRAM_INCLUDED

#include <odemx/statistics/Tally.h>

#include <vector>

namespace odemx {
namespace statistics {

/** \class Histogram

	\ingroup statistics

	\author Ronald Kluth

	\brief Statistical analysis plus histogram

	Histogram computes a statistical analysis like Tally
	and also generates histogram data.

	\since 1.0
*/
class Histogram
:	public Tally
{
public:

	struct Cell
	{
		Cell(): count( 0 ), lowerBound( 0 ), upperBound( 0 ), percentage( 0 ) {}

		std::size_t count;
		double lowerBound;
		double upperBound;
		double percentage;
	};

	typedef std::vector< Cell > CellVec;

	/// Construction with user-defined simulation context
	Histogram( base::Simulation& sim, const data::Label& label,
			double lowerBound, double upperBound, unsigned int cellCount );
	/// Destruction
	virtual ~Histogram();

	/// Update statistics
	virtual void update( double value );
	/// Reset statistics
	virtual void reset( base::SimTime time );
	/// Get histogram data
	const CellVec& getData();
	/// Implementation of Reporter interface
	virtual void report( data::Report& report );

private:
	/// The lower bound for histogram values
	double lowerBound_;
	/// The upper bound for histogram values
	double upperBound_;
	/// The number of categories for values
	unsigned int cellCount_;
	/// The interval width of one value category
	double cellWidth_;
	/// The maximum index of the data vector
	int maxCellIndex_;
	/// The data vector
	CellVec cellData_;
};

} } // namespace odemx::statistics

#endif /* ODEMX_STATS_HISTOGRAM_INCLUDED */
