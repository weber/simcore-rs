//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Accumulate.h
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Declaration of odemx::statistics::Accumulate
 * @sa Accumulate.cpp
 * @since 3.0
 */

#ifndef ODEMX_STATS_ACCUMULATE_INCLUDED
#define ODEMX_STATS_ACCUMULATE_INCLUDED

#include <odemx/base/SimTime.h>
#include <odemx/statistics/Tab.h>

namespace odemx {
namespace statistics {

/** \class Accumulate

	\ingroup statistics

	\author Ralf Gerstenberger

	\brief Compute statistics about provided data

	Accum computes minimum, maximum, mean-value,
	standard deviation and number of samples (size) according to
	the simulation time (see Tally).

	Time weighted mean values are computed according to two
	different approximation stategies:

	\c Accum::stepwise: assumes constant values between samples

		A value of 1 held for 4 time units, followed by a
		value of 0 for 1 time unit will result in a mean
		value of (1*4 + 0*1)/5 = 0.8 .

	\c Accum::linear: assumes linear increase/decrease of values
	between samples

		A value of 1 held for 4 time units, followed by a
		value of 0 for 1 time unit will result in a mean
		value of (1*4/2 + 0*1)/5 = 0.4 .

	\note The approximation strategy can be set with the second constructor
	parameter, which by default is set to \c stepwise.

	\since 1.0
*/
class Accumulate
:	public Tab
{
public:
	/// Approximation strategy
	enum Approx
	{
		stepwise,
		linear
	};

	/// Construction
	Accumulate( base::Simulation& sim, const data::Label& label, Approx app = stepwise );
	/// Destruction
	virtual ~Accumulate();

	/// Update statistics (considering time)
	virtual void update( base::SimTime time, double value );
	/// Reset statistics
	virtual void reset( base::SimTime time );
	/// Get a count of zero-updates
	std::size_t getZeros() const;
	/// Get minimum
	double getMin() const;
	/// Get maximum
	double getMax() const;
	/// Get average
	double getMean() const;
	/// Get average (considering time)
	double getWeightedMean() const;
	/// Get deviation
	double getStandardDeviation() const;
	/// Get deviation (considering time)
	double getWeightedStandardDeviation() const;
	/// Implementation of ReportProducer interface
	virtual void report( data::Report& report );
protected:
	Approx approximation_;
	base::SimTime startTime_;
	base::SimTime lastTime_;
	double lastValue_;
	std::size_t zeros_;
	double minValue_;
	double maxValue_;
	double sum_;
	double sumSq_;
	double sumWeight_;
	double sumSqWeight_;
};

} } // namespace odemx::statistics

#endif /* ODEMX_STATS_ACCUMULATE_INCLUDED */
