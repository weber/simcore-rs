//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Regression.h
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Declaration of odemx::statistics::Regression
 * @sa Regression.cpp
 * @since 3.0
 */

#ifndef ODEMX_STATS_REGRESSION_INCLUDED
#define ODEMX_STATS_REGRESSION_INCLUDED

#include <odemx/statistics/Tab.h>

#include <vector>

namespace odemx {
namespace statistics {

/** \class Regression

	\ingroup statistics

	\author Ralf Gerstenberger

	\brief %Regression analysis

	\note Regression from ODEM
	\note Regression supports Report

	Regression computes the linear correlation between
	provided data X and Y by computing two mean values,
	whose intersection is the center of rotation, and a slope.

	\since 1.0
*/
class Regression
:	public Tab
{
public:
	/// Construction with user-defined simulation context
	Regression( base::Simulation& sim, const data::Label& label );
	/// Destruction
	virtual ~Regression();

	/// Update statistics
	void update( double valueX, double valueY );
	/// Reset statistics
	virtual void reset( base::SimTime time );
	/// Check if sufficient data for a regression analysis has been accumulated
	bool hasSufficientData() const;
	/// get mean of x update values
	double getXMean() const;
	/// get mean of y update values
	double getYMean() const;
	///
	double getDx() const;
	double getDy() const;

	/**
	 * @brief Get standard deviation of all residuals
	 *
	 * @note Residuals describe the distance of the measured value from
	 * the estimated linear regression function.
	 */
	double getResidualStandardDeviation() const;

	/**
	 * @brief Get the regression coefficient
	 *
	 * @note The regression coefficient describes the slope of the linear
	 * regression function.
	 */
	double getEstimatedRegressionCoefficient() const;

	/// intercept helper value ???
	double getIntercept() const;

	/**
	 * @brief Get standard deviation of the regression coefficient estimates
	 */
	double getRegressionCoefficientStandardDeviation() const;

	/**
	 * @brief Get the correlation coefficient
	 *
	 * The correlation coefficient is a measure for the relationship of
	 * x and y. \c 0 means no linear correlation exists, while \c 1 means
	 * there is a strong linear correlation between these variables.
	 */
	double getCorrelationCoefficient() const;
	/// Implementation of Reporter interface
	virtual void report( data::Report& report );

protected:
	double sumX_;
	double sumY_;
	double sumSqX_;
	double sumXMulY_;
	double sumSqY_;
};

} } // namespace odemx::statistics

#endif /* ODEMX_STATS_REGRESSION_INCLUDED */
