//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Tally.h
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Declaration of odemx::statistics::Tally
 * @sa Tally.cpp
 * @since 3.0
 */

#ifndef ODEMX_STATS_TALLY_INCLUDED
#define ODEMX_STATS_TALLY_INCLUDED

#include <odemx/statistics/Tab.h>

namespace odemx {
namespace statistics {

/** \class Tally

	\ingroup statistics

	\author Ralf Gerstenberger

	\brief Compute statistics about provided data

	Tally computes minimum, maximum, mean-value,
	standard deviation and number of samples (size)
	independent from simulation time (see Accum).

	The average value is computed as follows:
	A value of 1 for 4 time units together with a value
	of 0 for 1 time unit will result in a mean value of
	(1+0)/2 = 0.5.

	\since 1.0
*/
class Tally
:	public Tab
{
public:
	/// Construction with user-defined simulation context
	Tally( base::Simulation& sim, const data::Label& label );
	/// Destruction
	virtual ~Tally();

	/// Update statistics
	virtual void update( double value );
	/// Reset statistics
	virtual void reset( base::SimTime time );
	/// Get minimum
	double getMin() const;
	/// Get maximum
	double getMax() const;
	/// Get average
	double getMean() const;
	/// Get standard deviation
	double getStandardDeviation() const;
	/// Implementation of Reporter interface
	virtual void report( data::Report& report );

protected:
	/// The minimum of all update values
	double minValue_;
	/// The maximum of all update values
	double maxValue_;
	/// The sum of all update values since last reset
	double sum_;
	/// The sum of the squares of all update values since last reset
	double sumSq_;
};

} } // namespace odemx::statistics

#endif /* ODEMX_STATS_TALLY_INCLUDED */
