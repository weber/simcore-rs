//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Count.h
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Declaration of odemx::statistics::Count
 * @sa Count.cpp
 * @since 3.0
 */

#ifndef ODEMX_STATS_COUNT_INCLUDED
#define ODEMX_STATS_COUNT_INCLUDED

#include <odemx/statistics/Tab.h>

namespace odemx {
namespace statistics {

/** \class Count

	\ingroup statistics

	\author Ralf Gerstenberger

	\brief Counter

	Count is counting integers. It can be used also to
	calculate an integer sum of all provided numbers.
	To increase the counter use update().

	\since 1.0
*/
class Count
:	public Tab
{
public:
	/// Construction with user-defined simulation context
	Count( base::Simulation& sim, const data::Label& label );

	/// Destruction
	virtual ~Count();

	/// Update counter
	void update( int value = 1 );
	/// Reset counter and statistics
	virtual void reset( base::SimTime time );
	/// Get current counter value
	int getValue() const;
	/// Implementation of Reporter interface
	virtual void report( data::Report& report );

private:
	/// The counter value
	int count_;
};

} } // namespace odemx::statistics

#endif /* ODEMX_STATS_COUNT_INCLUDED */
