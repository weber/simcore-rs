//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Tab.h
 * @author Ronald Kluth
 * @date created at 2009/03/01
 * @brief Declaration of odemx::statistics::Tab
 * @sa Tab.cpp
 * @since 3.0
 */

#ifndef ODEMX_STATS_TAB_INCLUDED
#define ODEMX_STATS_TAB_INCLUDED

#include <odemx/base/SimTime.h>
#include <odemx/data/ReportProducer.h>

#include <cstddef> // size_t

namespace odemx {
namespace statistics {

/** \class Tab

	\ingroup statistics

	\author Ralf Gerstenberger

	\brief The base for statistics-computing classes.

	\since 1.0
*/
class Tab
:	public data::ReportProducer
{
public:
	/// Construction with user-defined simulation context
	Tab( base::Simulation& sim, const data::Label& label );
	/// Destruction
	virtual ~Tab();

	/// Update usage counter
	void update();
	/// Reset usage counter and store reset time
	virtual void reset( base::SimTime time );
	/// Get usage counter
	std::size_t getUpdateCount() const;
	/// Get last reset time
	base::SimTime getResetTime() const;

protected:
	/// The usage counter
	std::size_t updateCount_;
	/// The last reset time
	base::SimTime resetTime_;
};

} } // namespace odemx::statistics

#endif /*ODEMX_TAB_INCLUDED*/
