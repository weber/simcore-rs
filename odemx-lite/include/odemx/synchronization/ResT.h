//----------------------------------------------------------------------------
//	Copyright (C) 2002-2008 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Bin.h

	\author Ronald Kluth

	\date created at 2008/02/17

	\brief Forwarding header for inclusion of odemx::ResT, ResTChoice, and observers

	\since 2.1
*/

#ifndef ODEMX_RES_TEMPLATE_INCLUDED
#define ODEMX_RES_TEMPLATE_INCLUDED

// template ResTBase header with implementation
#include <odemx/synchronization/res/ResTBase.h>
#include <odemx/synchronization/res/ResTBase.cpp>
// template ResT header with implementation
#include <odemx/synchronization/res/ResT.h>
#include <odemx/synchronization/res/ResT.cpp>
// template ResTChoice header with implementation
#include <odemx/synchronization/res/ResTChoice.h>
#include <odemx/synchronization/res/ResTChoice.cpp>

#endif /* ODEMX_RES_TEMPLATE_INCLUDED */
