//----------------------------------------------------------------------------
//	Copyright (C) 2002-2008 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**
 * @file Port.h
 * @author Ronald Kluth
 * @date created at 2007/04/04
 * @brief Port forwarding header and declaration of odemx::PortHead, odemx::PortTail
 * @see PortImpl.h, PortHeadImpl.h, PortTailImpl.h
 * @since 2.0
 */

#ifndef ODEMX_SYNC_PORT_INCLUDED
#define ODEMX_SYNC_PORT_INCLUDED

// include port template headers with implementation
#include <odemx/synchronization/port/PortData.h>
#include <odemx/synchronization/port/PortHeadImpl.h>
#include <odemx/synchronization/port/PortTailImpl.h>

// type definitions for convenience
namespace odemx {
namespace synchronization {

/// Default PortHead supporting elements derived from PortData
typedef PortHeadT< PortData* > PortHead;
/// Default PortTail supporting elements derived from PortData
typedef PortTailT< PortData* > PortTail;

/// Default observer for PortHead
typedef PortHeadTObserver< PortData* > PortHeadObserver;
/// Default observer for PortTail
typedef PortTailTObserver< PortData* > PortTailObserver;

} } // namespace odemx::synchronization

#endif /* ODEMX_SYNC_PORT_INCLUDED */
