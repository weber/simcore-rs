//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file IMemory.h
 * @author Ronald Kluth
 * @date created at 2009/02/11
 * @brief Declaration of interface odemx::synchronization::IMemory
 * @since 3.0
 */

#ifndef ODEMX_IMEMORY_INCLUDED
#define ODEMX_IMEMORY_INCLUDED

#include <vector>

//-----------------------------------------------------------forward declaration

namespace odemx {

namespace base { class Sched; }

namespace synchronization {

//------------------------------------------------------------header declaration

/**
 * @interface IMemory
 * @brief Interface for process memory objects
 *
 * This interface declares the methods used for the implementation
 * of memory objects.
 */
class IMemory
{
public:
	/**
		\brief IMemory types

		%IMemory implementations can be different types of structures. When
		a pointer to an %IMemory object is returned, it can be useful to have
		a means of distinguishing them simply by asking for their type
		using \p getMemoryType(). Currently timers, ports and collision
		detection objects are supported by the library. However, users are
		encouraged to implement IMemory or derive from Memory and
		create a user-defined memory type.
	*/
	enum Type
	{
		TIMER,
		PORTHEAD,
		PORTTAIL,
		COLLISION_DETECTION,
		CONTROL,
		USER_DEFINED
	};

	// implicit Default-Constructor
	
	/// Destruction
	virtual ~IMemory();

	/// Get the memory type as stated by enum Type
	virtual Type getMemoryType() const = 0;
	/// Check if a memory object is available, if so, processes get alerted
	virtual bool isAvailable() = 0;
	/// Wake up the remembered processes
	virtual void alert() = 0;
	/// Remember a process
	virtual bool remember( base::Sched* newObject ) = 0;
	/// Forget a process
	virtual bool forget( base::Sched* rememberedObject ) = 0;
	/// Forget all processes
	virtual void eraseMemory() = 0;
};

/// Provided for convenience, used with Process::wait
typedef std::vector< IMemory* > IMemoryVector;

} } // namespace odemx::synchronization

#endif /*ODEMX_IMEMORY_INCLUDED*/
