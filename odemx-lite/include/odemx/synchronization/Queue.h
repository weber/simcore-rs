//------------------------------------------------------------------------------
//	Copyright (C) 2003, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Queue.h
 * @author Ralf Gerstenberger
 * @date created at 2003/06/28
 * @brief Declaration of odemx::synchronization::Queue
 * @sa Queue.cpp
 * @since 1.0
 */

#ifndef ODEMX_SYNC_QUEUE_INCLUDED
#define ODEMX_SYNC_QUEUE_INCLUDED

#include <odemx/data/Producer.h>
#include <odemx/synchronization/ProcessQueue.h>

namespace odemx {

// forward declarations
namespace base {
class Simulation;
}

namespace synchronization {

/** \class Queue

	\ingroup synch

	\author Ralf Gerstenberger

	\brief Wrapper for ProcessQueue with statistics support

	Queue is a wrapper for ProcessQueue. In addition to the
	functions of ProcessQueue it also provides statistics about
	the queue usage.

	\sa ProcessQueue

	\since 1.0
*/
class Queue
:	public data::Producer
,	public ProcessQueue
{
public:
	/// Same type as the size_type of the used STL container
	typedef ProcessQueue::SizeType SizeType;

	/// Contruction for user-defined Simulation
	Queue( base::Simulation& sim, const data::Label& label );

	/// Destruction
	virtual ~Queue();

	/**
	 * @name Manipulator methods
	 * @{
	 */
	/// Remove the first process from the queue
	virtual void popQueue();
	/// Remove the process @c p from the queue
	virtual void remove( base::Process* p );
	/// Insert process @c p into the queue, in fifo (default) or lifo mode
	virtual void inSort( base::Process* p,  bool fifo = true );
	//@}
};

} } // namespace odemx::synchronization

#endif /* ODEMX_SYNC_QUEUE_INCLUDED */
