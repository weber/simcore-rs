//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//------------------------------------------------------------------------------
/**
 * @file PortMode.h
 * @author Ronald Kluth
 * @date created at 2009/09/21
 * @brief Declaration of enum odemx::synchronization::PortMode
 * @see PortHeadImpl.h, PortTailImpl.h
 * @since 3.0
 */

#ifndef ODEMX_SYNC_PORTMODE_INCLUDED
#define ODEMX_SYNC_PORTMODE_INCLUDED

namespace odemx {
namespace synchronization {

/**
 * @ingroup synch
 * @author Ronald Kluth
 * @brief Port modes
 * @see PortHeadT PortTailT
 *
 * Since ports implement a limited buffer, a behavior needs to be
 * defined for when a port is full, but an element is to be inserted,
 * or when a port is empty and some process is trying to read from it.
 * Modes for port head and corresponding port tail can differ.
 */
enum PortMode
{
	ERROR_MODE,		///< error when full/empty
	WAITING_MODE,	///< context switch when full/empty
	ZERO_MODE		///< ignore caller when full/empty, return 0
};

inline std::string toString( const PortMode mode )
{
	switch( mode )
	{
	case ERROR_MODE: return "ERROR_MODE";
	case WAITING_MODE: return "WAITING_MODE";
	case ZERO_MODE: return "ZERO_MODE";
	}
	return "ERROR";
}

} } // namespace odemx::synchronization

#endif /* ODEMX_SYNC_PORTMODE_INCLUDED */
