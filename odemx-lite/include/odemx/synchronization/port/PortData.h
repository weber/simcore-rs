//----------------------------------------------------------------------------
//	Copyright (C) 2002-2008 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file PortData.h

	\author Ronald Kluth

	\date created at 2007/04/04

	\brief Declaration of odemx::synchronization::PortData

	\sa PortT.h, PortT.cpp, PortHeadT.cpp, PortTailT.cpp

	\since 2.0
*/

#ifndef ODEMX_PORTDATA_INCLUDED
#define ODEMX_PORTDATA_INCLUDED

namespace odemx {
namespace synchronization {

/** \class PortData

	\ingroup synch

	\author Ronald Kluth

	\brief %PortData is the default base class for port elements.

	\sa PortHeadT, PortTailT, PortT

	%PortData can be used as common base class for all objects that
	are handled by ports. With default ports, all class types that
	define elements such as messages to be passed through a port
	must be derived	from %PortData because default ports can only handle
	pointers to subclasses of this type.

	\since 2.0
*/
class PortData
{
protected:
	/// Construction
	PortData() {}
	/// Copy Construction
	PortData( const PortData& other ) {}

public:
	/// Destruction
	virtual ~PortData() {}
};

} } // namespace odemx::synchronization

#endif /* ODEMX_PORTDATA_INCLUDED */
