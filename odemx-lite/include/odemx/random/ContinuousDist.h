//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file ContinuousDist.h

	\author Ralf Gerstenberger

	\date created at 2003/05/09

	\brief Declaration of odemx::random::ContinuousDist

	\sa ContinuousDist.cpp

	Continuous random number generator.

	\since 1.0
*/

#ifndef ODEMX_RDIST_INCLUDED
#define ODEMX_RDIST_INCLUDED

#include <odemx/random/Dist.h>

namespace odemx {
namespace random {

/**
	\defgroup rdist Continuous distributions
	\ingroup random

	A group of random number generators which produce continuous (double)
	random numbers of different distributions.
*/

/** \interface ContinuousDist

	\ingroup rdist

	\author Ralf Gerstenberger

	\brief Interface for continuous distributions

	%ContinuousDist declares the 'double sample()' method for continuous
	distribution classes. All derived classes implement this
	method to provide the random numbers in different distributions.

	\since 1.0
*/
class ContinuousDist
:	public Dist
{
public:

	/**
		\brief Construction with user-defined DistContext (i.e. Simulation)
		\param label
			Label of the generator
		\param c
			pointer to DistContext object
	*/
	ContinuousDist( base::Simulation& sim, const data::Label& label );

	/// Destruction
	virtual ~ContinuousDist();

	/**
		\brief Get next random number

		This function returns the next random number.
		It is implemented in derived classes which generate
		random numbers in different distributions.
	*/
	virtual double sample() = 0;
};

} } // namespace odemx::random

#endif
