//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Draw.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::Draw
 */

#ifndef ODEMX_RANDOM_DRAW_INCLUDED
#define ODEMX_RANDOM_DRAW_INCLUDED

#include <odemx/random/DiscreteDist.h>

namespace odemx {
namespace random {

/** \class Draw

	\ingroup idist

	\author Ralf Gerstenberger

	\brief Random series of 0 and 1

	\note Draw from ODEM

	\note supports Report

	Draw provides a random series of 1 and 0. The
	probability of the 1 against a 0 is set in the
	constructor.

	\since 1.0
*/
class Draw
:	public DiscreteDist
{
public:

	/**
		\brief Construction for user-defined DistContext (i.e. Simulation)
		\param title
			Label of the generator
		\param c
			pointer to DistContext object
		\param da
			probability of a 1 against a 0

		The parameter \c da defines the probability
		of a 1 against a 0.
	*/
	Draw( base::Simulation& sim, const data::Label& label, double probability );

	/// Destruction
	virtual ~Draw();

	/// Get next random number
	virtual int sample();

	/// Get parameter \c da
	double getProbability();

protected:
	double probability_;
};

} } // namespace odemx::random

#endif /* ODEMX_RANDOM_DRAW_INCLUDED */
