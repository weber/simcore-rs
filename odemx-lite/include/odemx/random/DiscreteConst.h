//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file DiscreteConst.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::DiscreteConst
 */

#include <odemx/random/DiscreteDist.h>

namespace odemx {
namespace random {

/** \class DiscreteConst

	\ingroup idist

	\author Ralf Gerstenberger

	\brief Constant number generator

	\note Iconst from ODEM

	\note supports Report

	DiscreteConst provides a constant integer number through the
	DiscreteDist interface. The constant number is set in the
	constructor.

	\since 1.0
*/
class DiscreteConst
:	public DiscreteDist
{
public:
	/**
		\brief Construction for user-defined DistContext (i.e. Simulation)
		\param title
			Label of the generator
		\param c
			RNG Context
		\param i
			constant integer returned by sample()

		The parameter i defines the number returned
		by this pseudo generator.
	*/
	DiscreteConst( base::Simulation& sim, const data::Label& label, int value );

	/// Destruction
	virtual ~DiscreteConst();

	/// Get number
	virtual int sample();

	/// Get the constant value
	int getValue();

private:
	const int value_;
};

} } // namespace odemx::random
