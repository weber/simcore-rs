//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Erlang.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::Erlang
 */

#ifndef ODEMX_RANDOM_ERLANG_INCLUDED
#define ODEMX_RANDOM_ERLANG_INCLUDED

#include <odemx/random/ContinuousDist.h>

namespace odemx {
namespace random {

/** \class Erlang

	\ingroup rdist

	\author Ralf Gerstenberger

	\brief %Erlang distribution of random numbers

	\note Erlang from ODEM

	\note supports Report

	Erlang provides a series of Erlang-distributed
	random numbers. The parameters are set in the
	constructor.

	\since 1.0
*/
class Erlang
:	public ContinuousDist
{
public:

	/**
		\brief Construction with user-defined DistContext (i.e. Simulation)
		\param label
			Label of the generator
		\param c
			Pointer to DistContext object
		\param ea rate, the larger, the more spread out the distribution
		\param eb shape, affects the shape of the distribution
	*/
	Erlang( base::Simulation& sim, const data::Label& label, double rate, int shape );

	/// Destruction
	virtual ~Erlang();

	/// Get next random number
	virtual double sample();

	/// Get parameter a
	double getRate();
	/// Get parameter b
	int getShape();

protected:
	double rate_;
	int    shape_;
};

} } // namespace odemx::random

#endif /* ODEMX_RANDOM_ERLANG_INCLUDED */
