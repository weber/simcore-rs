//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Uniform.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::Uniform
 */

#ifndef ODEMX_RANDOM_UNIFORM_INCLUDED
#define ODEMX_RANDOM_UNIFORM_INCLUDED

#include <odemx/random/ContinuousDist.h>

namespace odemx {
namespace random {

/** \class Uniform

	\ingroup rdist

	\author Ralf Gerstenberger

	\brief %Uniform distribution of random numbers

	\note Uniform from ODEM

	\note supports Report

	Uniform provides a series of uniform
	distributed random numbers in the interval [a, b).
	The parameters a and b are set in the constructor.

	\since 1.0
*/
class Uniform
:	public ContinuousDist
{
public:

	/**
		\brief Construction with user-defined DistContext (i.e. Simulation)
		\param label
			Label of the generator
		\param c
			pointer to DistContext object
		\param ua
			lower bound a
		\param ub
			upper bound b

		The parameters \c ua and \c ub define the interval
		of the distribution.
	*/
	Uniform( base::Simulation& sim, const data::Label& label, double lowerBound,
			double upperBound );

	/// Destruction
	virtual ~Uniform();

	/// Get next random number
	virtual double sample();

	/// Get parameter a
	double getLowerBound();
	/// Get parameter b
	double getUpperBound();

private:
	double  zyqSpan_;

protected:
	double lowerBound_;
	double upperBound_;
};

} } // namespace odemx::random

#endif /* ODEMX_RANDOM_UNIFORM_INCLUDED */
