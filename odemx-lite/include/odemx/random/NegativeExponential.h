//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file NegativeExponential.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::NegativeExponential
 */

#ifndef ODEMX_RANDOM_NEGATIVEEXPONENTIAL_INCLUDED
#define ODEMX_RANDOM_NEGATIVEEXPONENTIAL_INCLUDED

#include <odemx/random/ContinuousDist.h>

namespace odemx {
namespace random {

/** \class NegativeExponential

	\ingroup rdist

	\author Ralf Gerstenberger

	\brief Negative exponential distribution of random numbers

	\note NegativeExponential from ODEM

	\note supports Report

	NegativeExponential provides a series of negative exponential
	distributed random numbers.

	\since 1.0
*/
class NegativeExponential
:	public ContinuousDist
{
public:

	/**
		\brief Construction with user-defined DistContext (i.e. Simulation)
		\param label
			Label of the generator
		\param c
			pointer to DistContext object
		\param na
			inverse mean (expected) value, meaning <tt>1 / na</tt> will be used
	*/
	NegativeExponential( base::Simulation& sim, const data::Label& label, double inverseMean );

	/// Destruction
	virtual ~NegativeExponential();

	/// Get next random number
	virtual double sample();

	/// Get parameter \c na
	double getInverseMean();

protected:
	double inverseMean_;
};

} } // namespace odemx::random

#endif /* ODEMX_RANDOM_NEGATIVEEXPONENTIAL_INCLUDED */
