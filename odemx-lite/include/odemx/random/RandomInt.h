//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file RandomInt.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::RandomInt
 */

#ifndef ODEMX_RANDOM_RANDOMINT_INCLUDED
#define ODEMX_RANDOM_RANDOMINT_INCLUDED

#include <odemx/random/DiscreteDist.h>

namespace odemx {
namespace random {

/** \class RandomInt

	\ingroup idist

	\author Ralf Gerstenberger

	\brief %Uniform distributed discrete random numbers

	\note RandomInt from ODEM

	\note supports Report

	RandomInt provides a series of uniformly distributed
	integer random numbers in the interval [a, b). The
	parameters a and b are set in the constructor.

	\since 1.0
*/
class RandomInt
:	public DiscreteDist
{
public:

	/**
		\brief Construction for user-defined DistContext (i.e. Simulation)
		\param title
			Label of the generator
		\param c
			pointer to DistContext object
		\param ra
			lower bound - a
		\param rb
			upper bound - b

		The parameters a and b define the interval [a, b)
		of the uniformly distributed random numbers generated
		by %RandomInt.
	*/
	RandomInt( base::Simulation& sim, const data::Label& label, int lowerBound, int upperBound );

	// Destruction
	virtual ~RandomInt();

	/// Get next random number
	virtual int sample();

	/// Get parameter a
	int getLowerBound();
	/// Get parameter b
	int getUpperBound();

protected:
	int lowerBound_;
	int upperBound_;

private:
	double zyqSpan_;
};

} } // namespace odemx::random

#endif /* ODEMX_RANDOM_RANDOMINT_INCLUDED */
