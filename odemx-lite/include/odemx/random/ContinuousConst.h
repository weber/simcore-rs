//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file ContinuousConst.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::ContinuousConst
 */

#ifndef CONTINUOUSCONST_H_
#define CONTINUOUSCONST_H_

#include <odemx/random/ContinuousDist.h>

namespace odemx {
namespace random {

/** \class ContinuousConst

	\ingroup rdist

	\author Ralf Gerstenberger

	\brief Constant number generator

	\note Rconst from ODEM

	\note supports Report

	ContinuousConst provides a constant number through the
	ContinuousDist interface.

	\since 1.0
*/
class ContinuousConst
:	public ContinuousDist
{
public:

	/**
		\brief Construction with user-defined DistContext (i.e. Simulation)
		\param label
			Label of the generator
		\param c
			Pointer to DistContext object
		\param d
			constant double returned by sample()
	*/
	ContinuousConst( base::Simulation& sim, const data::Label& label, double value );

	/// Destruction
	virtual ~ContinuousConst();

	/// Get number
	virtual double sample();

	/// Get parameter d
	double getValue();

private:
	const double value_;
};

} } // namespace odemx::random

#endif /* CONTINUOUSCONST_H_ */
