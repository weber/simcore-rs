//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/**
 * @file Normal.h
 * @date Feb 22, 2009
 * @author Ronald Kluth
 * @brief Declaration of odemx::random::Normal
 */

#ifndef ODEMX_RANDOM_NORMAL_INCLUDED
#define ODEMX_RANDOM_NORMAL_INCLUDED

#include <odemx/random/ContinuousDist.h>

namespace odemx {
namespace random {

/** \class Normal

	\ingroup rdist

	\author Ralf Gerstenberger

	\brief %Normal distribution of random numbers

	\note Normal from ODEM

	\note supports Report

	Normal provides a series of normal ('Gauss')
	distributed random numbers. The parameter mean
	and deviation are set in the constructor.

	\since 1.0
*/
class Normal
:	public ContinuousDist
{
public:

	/**
		\brief Construction with user-defined DistContext (i.e. Simulation)
		\param label
			Label of the generator
		\param c
			pointer to DistContext object
		\param na
			a mean-value
		\param nb
			b deviation

		The parameter \c na and \c nb define the mean-value
		and the deviation of the normal distribution generated
		by %Normal.
	*/
	Normal( base::Simulation& sim, const data::Label& label, double mean,
			double deviation );

	/// Destruction
	virtual ~Normal();

	/// Get next random number
	virtual double sample();

	/// Get parameter na
	double getMean();
	/// Get parameter nb
	double getDeviation();

private:
	double zyqu;
	double zyqv;
	bool zyqeven;

protected:
	double mean_;
	double deviation_;
};

} } // namespace odemx::random

#endif /* ODEMX_RANDOM_NORMAL_INCLUDED */
