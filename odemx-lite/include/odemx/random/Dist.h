//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Dist.h

	\author Ralf Gerstenberger

	\date created at 2003/05/09

	\brief Declaration of odemx::random::Dist

	\sa Dist.cpp

	\since 1.0
*/

#ifndef ODEMX_DIST_INCLUDED
#define ODEMX_DIST_INCLUDED

#include <odemx/data/Producer.h>

namespace odemx {
namespace random {

class DistContext;

/** \class Dist

	\ingroup random

	\author Ralf Gerstenberger

	\brief Linear Random Number Generator

	\note LRNG from ODEM

	%Dist is the base class for all random number generators of
	ODEMx. It provides an implementation of an integer random
	number generator using the linear congruential algorithm.
	This algorithm is known to produce sequences of numbers which
	are sufficiently random for simulations. \n
	This class is not used directly. Instead one of the derived
	classes is used, which provide transformations into sequences
	of random numbers with different distributions.

	\sa DiscreteDist and ContinuousDist

	\since 1.0
*/
class Dist
:	public data::Producer
{
protected:

	/**
		\brief Construction

		\param label
			Label of the object
	*/
	Dist( base::Simulation& sim, const data::Label& label = "" );
	/// Destruction
	virtual ~Dist();

public:
	/**
		\brief Set seed for the random number generators
		\param n
			new seed

		The new seed will be used for generating
		new random numbers.

		\warning The quality of the generated random numbers
		depends on the selected seed!
	*/
	virtual void setSeed( int n = 0 );

	/**
		\brief Get start seed

		This function returns the start seed of
		the random number generator.
	*/
	unsigned long getSeed();

protected:
	/**
		\brief Get new random number

		This function returns a new random number. It
		is used by derived classes.
	*/
	double getSample();

private:
	DistContext& context;
	unsigned long u;
	unsigned long ustart;
	unsigned int antithetics;
};

} } // namespace odemx::random

#endif
