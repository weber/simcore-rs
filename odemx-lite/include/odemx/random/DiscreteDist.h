//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file DiscreteDist.h

	\author Ralf Gerstenberger

	\date created at 2003/05/09

	\brief Declaration of odemx::random::DiscreteDist

	\sa DiscreteDist.cpp

	Discrete random number generators

	\since 1.0
*/

#ifndef ODEMX_IDIST_INCLUDED
#define ODEMX_IDIST_INCLUDED

#include <odemx/random/Dist.h>

namespace odemx {
namespace random {

/**
	\defgroup idist Integer distributions
	\ingroup random

	A group of random number generators which produce integer
	random numbers in different distributions.
*/

/** \interface DiscreteDist

	\ingroup idist

	\author Ralf Gerstenberger

	\brief Interface for integer distributions

	%DiscreteDist declares the 'int sample()' method for discrete
	distribution classes. All derived classes implement this
	method to provide the random numbers in different distributions.

	\since 1.0
*/
class DiscreteDist
:	public Dist
{
public:

	/**
		\brief Construction for user-defined DistContext (i.e. Simulation)
		\param title
			Label of the generator
		\param c
			pointer to a DistContext object
	*/
	DiscreteDist( base::Simulation& sim, const data::Label& label );

	/// Destruction
	virtual ~DiscreteDist();

	/**
		\brief Get next random number

		This function returns the next random number.
		It is implemented in derived classes which generate
		random numbers in different distributions.
	*/
	virtual int sample() = 0;
};

} } // namespace odemx::random

#endif
