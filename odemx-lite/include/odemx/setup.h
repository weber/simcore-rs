//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file odemx/setup.h
 * @author Ronald Kluth
 * @date created at 2009/08/02
 * @brief Contains preprocessor definitions controlling ODEMx build settings
 * @since 3.0
 */

#ifndef ODEMX_SETUP_INCLUDED
#define ODEMX_SETUP_INCLUDED

// NOTE: All defines in this file are guarded with #ifndef in order to avoid
// multiple definitions of the same macro. This can happen if the macro is
// given via compiler command line switch, for example.

/**
 * @name Build Parameters
 * @brief Default build parameters for ODEMx and dependent projects
 * @{
 */

/**
 * @def ODEMX_USE_CONTINUOUS
 * @brief Enables continuous processes and influences the type of SimTime
 *
 * The default type used for ODEMx @c SimTime is <tt>long long</tt>. However,
 * continuous processes require floating point precision. Therefore, enabling
 * this feature will switch the type used for @c SimTime to @c double.
 */
#ifndef ODEMX_USE_CONTINUOUS
#define ODEMX_USE_CONTINUOUS
#endif

/**
 * @def ODEMX_USE_TRACE
 * @brief Enables internal state logging of ODEMx classes
 *
 * This define allows users to get detailed logging information about all state
 * changes occurring within ODEMx classes. This can significantly ease the
 * debugging of simulation models. As soon as the simulator is working properly,
 * the logging output can be reduced to show only experiment-related data by
 * disabling internal traces. This also reduces runtime overhead.
 */
#ifndef ODEMX_USE_TRACE
//#define ODEMX_USE_TRACE
#endif

/**
 * @def ODEMX_USE_OBSERVATION
 * @brief Enables the observation feature of ODEMx
 *
 * Observation is an object-object relation with one object taking the role of
 * the observable and another taking the role of the observer. An observable
 * can inform multiple registered observers of its state changes. This define
 * mainly reduces runtime overhead when the feature is not needed.
 */
#ifndef ODEMX_USE_OBSERVATION
//#define ODEMX_USE_OBSERVATION
#endif

/**
 * @def ODEMX_USE_ODBC
 * @brief Enables logging to external databases via ODBC
 *
 * ODEMx has the capability to write log records to databases. The default
 * setup enables the use of SQLite database files. This define
 * enables the use of external databases by using an ODBC connector. However,
 * compiling ODEMx with this option requires ODBC development headers to be
 * installed on the host system.
 */
#ifndef ODEMX_USE_ODBC
//#define ODEMX_USE_ODBC
#endif

#ifndef ODEMX_USE_MACRO_COUNTER
//#define ODEMX_USE_MACRO_COUNTER
#endif
//@}

//------------------------------------------------------------------dependencies

// Continuous requires observation because ContuTrace is an observer type
#ifdef ODEMX_USE_CONTINUOUS
	#ifndef ODEMX_USE_OBSERVATION
	#define ODEMX_USE_OBSERVATION
	#endif
#endif /* ODEMX_USE_CONTINUOUS */

// Tracing checks the boolean value of ODEMX_TRACE_ENABLED in a condition
#ifdef ODEMX_USE_TRACE
	#define ODEMX_TRACE_ENABLED true
#else
	#define ODEMX_TRACE_ENABLED false
#endif /* ODEMX_USE_TRACE */

#endif /* ODEMX_SETUP_INCLUDED */
