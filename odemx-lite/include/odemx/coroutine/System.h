//------------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file System.h
 * @author Ralf Gerstenberger
 * @date created at 2002/02/04
 * @brief OS- and platform-specific includes, defines and types
 * @since 1.0
 */

#ifndef ODEMX_CORO_SYSTEM_INCLUDED
#define ODEMX_CORO_SYSTEM_INCLUDED

#include <odemx/setup.h>

#ifdef _WIN32
	#ifndef _WIN32_WINNT
		#define _WIN32_WINNT 0x0400
	#endif
	#ifndef WINVER
		#define WINVER 0x0400
	#endif
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#else
	#include <odemx/coroutine/ucFiber.h>
#endif

#endif /* ODEMX_CORO_SYSTEM_INCLUDED */
