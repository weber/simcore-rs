//------------------------------------------------------------------------------
//	Copyright (C) 2002 - 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ReportProducer.h
 * @author Ralf Gerstenberger
 * @date created at 2002/06/21
 * @brief Declaration of class odemx::data::ReportProducer
 * @sa ReportProducer.cpp
 * @since 1.0
 */

#ifndef ODEMX_DATA_REPORTER_INCLUDED
#define ODEMX_DATA_REPORTER_INCLUDED

#include <odemx/data/Label.h>
#include <CppLog/NamedElement.h>

//----------------------------------------------------------forward declarations

namespace odemx {

namespace base { class Simulation; }

namespace data {

class Report;

//-----------------------------------------------------------header declarations

/** \ingroup data

	\author Ralf Gerstenberger

	\brief A ReportProducer generates data in the form of tables.

	\sa Report ReportTable

	A ReportProducer implementation generates data for reports.
	It has to implement the pure virtual function report.

	\since 1.0
*/
class ReportProducer
:	public Log::NamedElement
{
public:
	/// Construction with user-defined simulation context
	ReportProducer( base::Simulation& sim, const Label& label );
	/// Destruction
	virtual ~ReportProducer();
	/// Get the label of the report producer for display in a table column
	const Label& getLabel() const;
	/**
		\brief report generation

		This function is called by a Report object during report generation.
		A ReportProducer implementation implements this method to
		provide its data. ReportProducer can be associated to one or
		more Report objects. A ReportProducer will contribute to each
		Report it is associated to.
	*/
	virtual void report( Report& report ) = 0;
};

} } // namespace odemx::data

#endif /* ODEMX_DATA_REPORTER_INCLUDED */
