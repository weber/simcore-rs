//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ManagedChannels.h
 * @author Ronald Kluth
 * @date created at 2009/09/05
 * @brief Declaration of ODEMx default log channels and various macros
 * @sa ManagedChannels.cpp
 * @since 3.0
 */

#ifndef ODEMX_DATA_MANAGEDCHANNELS_INCLUDED
#define ODEMX_DATA_MANAGEDCHANNELS_INCLUDED

#include <CppLog/DeclareChannel.h>

#ifdef ODEMX_USE_MACRO_COUNTER

/**
 * @def ODEMX_DECLARE_DATA_CHANNEL( PP_IdName )
 * @brief Declares a managed channel
 * @see odemx::data::LoggingManager, odemx::data::Producer
 *
 * This macro is used internally to create the log channels provided by ODEMx.
 * It ensures that each channel has a unique ID, and that subclasses of
 * @c odemx::data::Producer automatically get access to these channels. This
 * only works for the predefined channels, though.
 *
 * @note In case a compiler does not support the macro __COUNTER__ for ID
 * generation, there is also a manual variant of the macro called
 * ODEMX_DECLARE_DATA_CHANNEL_WITH_ID.
 */

// macro for log channel declaration
#define ODEMX_DECLARE_DATA_CHANNEL( PP_IdName )									\
	CPPLOG_DECLARE_CHANNEL( odemx_detail, PP_IdName, odemx::data::SimRecord );	\
	namespace odemx {															\
	namespace data {															\
	namespace channel_id {														\
	using odemx_detail::PP_IdName;												\
	} } } // namespace odemx::data::channel_id

#else

#define ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( PP_IdName, PP_IdValue )									\
	CPPLOG_DECLARE_CHANNEL_WITH_ID( odemx_detail, PP_IdName, odemx::data::SimRecord, PP_IdValue );	\
	namespace odemx {																				\
	namespace data {																				\
	namespace channel_id {																					\
	using odemx_detail::PP_IdName;																	\
	} } } // namespace odemx::data::id

#endif

//----------------------------------------------------------forward declarations

namespace odemx {
namespace data {
class SimRecord;
} }

//-----------------------------------------------------------header declarations

#ifdef ODEMX_USE_MACRO_COUNTER

ODEMX_DECLARE_DATA_CHANNEL( trace );
ODEMX_DECLARE_DATA_CHANNEL( debug );
ODEMX_DECLARE_DATA_CHANNEL( info );
ODEMX_DECLARE_DATA_CHANNEL( warning );
ODEMX_DECLARE_DATA_CHANNEL( error );
ODEMX_DECLARE_DATA_CHANNEL( fatal );
ODEMX_DECLARE_DATA_CHANNEL( statistics );

#else

ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( trace, 1000 );
ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( debug, 1001 );
ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( info, 1002 );
ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( warning, 1003 );
ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( error, 1004 );
ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( fatal, 1005 );
ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( statistics, 1006 );

#endif

/**
 * @def ODEMX_TRACE
 * @brief Macro to be used for trace logging in odemx classes
 *
 * This macro allows for ODEMx tracing to be turned on or off. It does so
 * by wrapping the trace call in a condition that is either always true
 * or always false. Modern compilers should completely optimize out
 * code that contains <tt>if( false ) {...}</tt>.
 */
#define ODEMX_TRACE \
	if( ODEMX_TRACE_ENABLED ) this->trace

namespace odemx {
namespace data {
namespace channel_id {

/**
 * @brief Get a string representation of a log channel name
 * @param id The ID of the log channel
 * @return The name of the log channel as string
 *
 * If other IDs than those know to ODEMx are given as argument, then
 * the return value will simply state "user-defined".
 */
extern const std::string toString( const Log::ChannelId id );

} } } // namespace odemx::data::channel_id

#endif /* ODEMX_DATA_MANAGEDCHANNELS_INCLUDED */
