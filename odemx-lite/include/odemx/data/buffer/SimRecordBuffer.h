//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file SimRecordBuffer.h
 * @author Ronald Kluth
 * @date created at 2009/09/14
 * @brief Declaration of class odemx::data::SimRecordBuffer
 * @sa SimRecordBuffer.cpp
 * @since 3.0
 */

#ifndef ODEMX_DATA_BUFFER_SIMRECORDBUFFER_INCLUDED
#define ODEMX_DATA_BUFFER_SIMRECORDBUFFER_INCLUDED

#include <odemx/data/SimRecord.h>
#include <CppLog/ChannelId.h>
#include <CppLog/Record.h>
#include <deque>

namespace odemx {
namespace data {

// info types needed when buffering records
// used to prevent dangling pointers and provide additional info
CPPLOG_DECLARE_INFO_TYPE( ChannelInfo, "channel", std::string );
CPPLOG_DECLARE_INFO_TYPE( ClassScopeInfo, "class_scope", std::string );
CPPLOG_DECLARE_INFO_TYPE( SenderLabelInfo, "sender_label", Label );
CPPLOG_DECLARE_INFO_TYPE( SenderTypeInfo, "sender_type", std::string );
CPPLOG_DECLARE_INFO_TYPE( StringTimeInfo, "sim_time", std::string );
CPPLOG_DECLARE_INFO_TYPE( SimIdInfo, "sim_id", TableKey );

namespace buffer {

/**
 * @brief Buffer for storing SimRecord information to allow deferred handling
 * @ingroup data
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::data::SimRecord odemx::data::output::DatabaseWriter
 *
 * As soon as a log consumer needs to buffer data, as is the case with class
 * odemx::data::output::DatabaseWriter, volatile information must be copied
 * in order to be stored. Otherwise, there is a possibility for dangling
 * pointers involved because SimRecords do not copy data for efficiency.
 * Therefore, ODEMx offers this class in order to support the buffering of
 * log records.
 */
class SimRecordBuffer
{
public:
	/// Record type used by the buffer for storing arbitrary information
	struct StoredRecord: public Log::Record
	{
		/// Construction with record text, and details
		StoredRecord( const Log::StringLiteral& text, std::shared_ptr< SimRecord::DetailVec > details )
		:	text_( text )
		,	details_( details )
		{}

		/// Access the string literal holding the record's text message
		const Log::StringLiteral& getText() const
		{
			return text_;
		}

		/// Check whether the record has details
		bool hasDetails() const
		{
			return !! details_;
		}

		/**
		 * @brief  Get read access the detail vector
		 * @note This call will crash if the detail vector pointer is invalid.
		 * Always check @c hasDetails first.
		 */
		const SimRecord::DetailVec& getDetails() const
		{
			return *details_;
		}
	private:
		/// A copy of the string literal that was sent as record text
		Log::StringLiteral text_;
		/// A copy of the shared_ptr to the detail vector created by SimRecord
		std::shared_ptr< SimRecord::DetailVec > details_;
	};

	/// Vector type for storing simulation records
	typedef std::deque< StoredRecord > StorageType;
	/// Size type used for this buffer
	typedef StorageType::size_type SizeType;

	/**
	 * @brief Construction
	 * @param limit Maximum number of records to be stored by the buffer
	 *
	 * @note The limit provides a means to check whether the buffer is full.
	 * However, it does not stop users from putting more data into the
	 * buffer, if necessary.
	 */
	SimRecordBuffer( SizeType limit );

	/**
	 * @brief Add another record to the buffer
	 * @param channelId The channel over which the record was sent
	 * @param simRecord The actual record to extract the data from
	 * @param simId An ID representing the simulaiton context of the record
	 * @param timeString A string representation of the current SimTime
	 *
	 * This method copies all relevant information from the SimRecord
	 * and keeps it in a StoredRecord object.
	 */
	void put( const Log::ChannelId channelId, const SimRecord& simRecord,
			const TableKey simId, const std::string& timeString );

	/// Empty the buffer and reset size
	void clear();

	/// Check whether the buffer contains any records
	bool isEmpty() const;

	/// Check whether the buffer has reached its limit
	bool isFull() const;

	/// Get the current size of the buffer
	SizeType getSize() const;

	/// Get the maximum size of the buffer
	SizeType getLimit() const;

	/// Get a reference to the buffer's record vector
	const StorageType& getStorage() const;

private:
	StorageType storage_; ///< Container for storing log records
	SizeType size_; ///< Size counter to avoid use of size() method
	SizeType limit_; ///< Maximum number of records the buffer can hold

	SimRecordBuffer( const SimRecordBuffer& ); ///< Non-copyable
	SimRecordBuffer& operator=( const SimRecordBuffer& ); ///< Non-assignable
};

} } } // namespace odemx::data::buffer

#endif /* ODEMX_DATA_BUFFER_SIMRECORDBUFFER_INCLUDED */
