//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Label.h
 * @author Ronald Kluth
 * @date created at 2009/09/19
 * @brief Declaration of type odemx::data::Label
 * @since 3.0
 */

#ifndef ODEMX_DATA_LABEL_INCLUDED
#define ODEMX_DATA_LABEL_INCLUDED

#include <string>

namespace odemx {
namespace data {

/**
 * @brief String type used for object labels in ODEMx
 *
 * Every model element class that is derived from class @c odemx::data::Producer
 * provides unique names for its objects. This is ensured by the simulation context,
 * which must always be provided. Access to the unique name is given with the method
 * getLabel().
 *
 * @see odemx::data::Producer
 */
typedef std::string Label;

} } // namespace odemx::data

#endif /* ODEMX_DATA_LABEL_INCLUDED */
