//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ErrorWriter.h
 * @author Ronald Kluth
 * @date created at 2009/09/30
 * @brief Declaration of class odemx::data::output::ErrorWriter
 * @sa ErrorWriter.cpp
 * @since 3.0
 */

#ifndef ODEMX_DATA_OUTPUT_ERRORWRITER_INCLUDED
#define ODEMX_DATA_OUTPUT_ERRORWRITER_INCLUDED

#include <odemx/data/SimRecord.h>
#include <CppLog/Consumer.h>

#ifdef _MSC_VER
#include <memory>
#else
#include <memory>
#endif

//----------------------------------------------------------forward declarations

namespace odemx {
namespace data {
namespace output {

class TimeFormat;

//-----------------------------------------------------------header declarations

/**
 * @brief A log consumer specifically intended for warnings, errors, and fatal errors
 * @ingroup data
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::data::Producer
 *
 * This class outputs error messages in a similar way as previous ODEMx
 * versions. However, with the use of class SimRecord as record type,
 * much more detailed information can be given about the error condition.
 *
 * @note This consumer type is meant to be registered only with the
 * channels warning, error, and fatal. Otherwise, an error message will
 * be generated.
 */
class ErrorWriter
:	public Log::Consumer< SimRecord >
{
public:
	/**
	 * @brief Creation by static method
	 * @return A shared pointer that manages the consumer object.
	 *
	 * This method forces the use of reference-counting pointers to data
	 * consumer objects in order to avoid manual memory management of these
	 * resources. ODEMx log channels maintain a set of shared pointers to
	 * their consumers, which guarantees that consumer objects stay alive while
	 * they are registered with a channel.
	 */
	static std::shared_ptr< ErrorWriter > create();

	/// Destruction
	virtual ~ErrorWriter();

	/**
	 * @brief Implementation of the consumer interface, writes records to std::cerr
	 * @param channelId ID of the forwarding log channel
	 * @param record A log record describing the detected failure
	 *
	 * The output format contains the type of failure (i.e. the channel),
	 * the message, the SimTime, the sender and its type. Details and class
	 * scope are optional data.
	 */
	virtual void consume( const Log::ChannelId channelId, const SimRecord& record );

	/**
	 * @brief Set a new time formatter
	 * @param timeFormat Pointer to a dynamically allocated time formatter
	 * @see odemx::data::output::GermanTime odemx::data::output::Iso8601Time
	 *
	 * The default formatter simply outputs SimTime values, i.e. numbers.
	 * With this method, the format can be changed. The function takes
	 * ownership of the pointer and deletes it automatically when the writer is
	 * destroyed or the format is changed.
	 */
	void setTimeFormat( TimeFormat* timeFormat );

private:
	/// Pointer to the time formatter, if set
	std::unique_ptr< TimeFormat > format_;

private:
	/// Construction private, use create instead
	ErrorWriter();
	/// Non-copyable
	ErrorWriter( const ErrorWriter& );
	/// Non-assignable
	ErrorWriter& operator=( const ErrorWriter& );
};

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_ERRORWRITER_INCLUDED */
