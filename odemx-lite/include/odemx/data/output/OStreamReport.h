//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file OStreamReport.h
 * @author Ronald Kluth
 * @date created at 2009/02/28
 * @brief Declaration of class odemx::data::output::OStreamReport
 * @sa OStreamReport.cpp
 * @since 3.0
 */

#ifndef ODEMX_DATA_OUTPUT_OSTREAMREPORT_INCLUDED
#define ODEMX_DATA_OUTPUT_OSTREAMREPORT_INCLUDED

#include <odemx/data/Report.h>
#include <ostream>

namespace odemx {
namespace data {
namespace output {

/**
 * @brief Simple Report implementation that prints statistics tables to a stream
 * @ingroup data
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::data::Report odemx::data::ReportProducer odemx::data::output::XmlReport
 *
 * ODEMx can collect statistical data in accumulated form. This is either
 * done by statistics computing components provided by module statistics,
 * or with the help of the log consumer odemx::data::buffer::StatisticsBuffer
 * that collects data from channel statistics.
 *
 * This class is mainly used to print accumulated statistical data to the
 * console by providing std::cout as target stream. However, it is also
 * possible to use file streams or other subclasses of std::ostream.
 */
class OStreamReport
:	public Report
{
public:
	/// Construction with @c ostream
	OStreamReport( std::ostream& os );
	/// Destruction
	virtual ~OStreamReport();

protected:
	/// Redefined to output a headline before showing the tables
	virtual void startProcessing();
	/// Prints formatted tables from ReportProducers to the stream
	virtual void processTables();
	/// Does nothing
	virtual void endProcessing();

private:
	/// Reference to the @c ostream the report will be written to
	std::ostream& os_;
};

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_OSTREAMREPORT_INCLUDED */
