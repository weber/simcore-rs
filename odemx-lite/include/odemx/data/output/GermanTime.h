//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file GermanTime.h
 * @author Ronald Kluth
 * @date created at 2009/04/03
 * @brief Declaration of class odemx::data::output::GermanTime
 * @sa GermanTime.cpp
 * @since 2.1
 */

#ifndef ODEMX_DATA_OUTPUT_GERMANTIME_INCLUDED
#define ODEMX_DATA_OUTPUT_GERMANTIME_INCLUDED

#include <odemx/data/output/TimeFormat.h>

#include <string>

//----------------------------------------------------------forward declarations

namespace odemx {
namespace data {
namespace output {

struct TimeBase;

//-----------------------------------------------------------header declarations

/**
 * @brief Implementation of the German time format
 * @ingroup data
 * @author Ronald Kluth
 * @since 2.1
 * @see odemx::data::output::TimeFormat odemx::data::output::Iso8601Time
 *
 * This time formatter converts SimTime to the German time format
 * <tt>YYYY-MM-DD / hh:mm:ss.f</tt>. It uses the predefined computation
 * function from TimeFormat and merely provides a string conversion
 * for the computed values.
 *
 * The default precision for the generated strings is seconds.
 * The millisecond part will only be displayed if the given TimeBase
 * uses the TimeUnit milliseconds.
 */
class GermanTime
:	public TimeFormat
{
public:
	/// Default construction
	GermanTime();
	/// Construction with given time base (SimTime 0 represents this time)
	GermanTime( const TimeBase& base );

protected:
	// uses predefined computation function from Format()

	/**
	 * @brief Transforms the computed values to the output format
	 * @return A formatted time string
	 *
	 * This method is called by TimeFormat::timeToString() after the
	 * values for date and day time have been computed. It merely
	 * transforms that data into an std::string.
	 */
	virtual const std::string makeString() const;
};

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_GERMANTIME_INCLUDED */
