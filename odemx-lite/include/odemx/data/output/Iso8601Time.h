//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Iso8601Time.h
 * @author Ronald Kluth
 * @date created at 2009/04/03
 * @brief Declaration of class odemx::data::output::Iso8601Time
 * @sa Iso8601Time.cpp
 * @since 2.1
 */

#ifndef ODEMX_DATA_OUTPUT_ISO8601TIME_INCLUDED
#define ODEMX_DATA_OUTPUT_ISO8601TIME_INCLUDED

#include <odemx/data/output/TimeFormat.h>

//----------------------------------------------------------forward declarations

namespace odemx {
namespace data {
namespace output {

struct TimeBase;

//-----------------------------------------------------------header declarations

/**
 * @brief Implementation of the ISO-8601 time format
 * @ingroup data
 * @author Ronald Kluth
 * @since 2.1
 * @see odemx::data::output::TimeFormat odemx::data::output::Iso8601Time
 *
 * This time formatter converts SimTime to the ISO 8601 time format
 * <tt>YYYY-MM-DDThh:mm:ss.f&plusmn;HH:mm</tt>. It uses the predefined
 * computation function from TimeFormat and merely provides a string
 * conversion for the computed values.
 *
 * This implementation features a subset, as proposed by the W3C, of the
 * whole ISO-8601 standard for outputting international dates and time.
 * It enables output of different time precisions ranging from minutes over
 * seconds up to milliseconds.
 *
 * The default precision for the generated strings is seconds.
 * The millisecond part will only be displayed if the given TimeBase
 * uses the unit milliseconds.
 */
class Iso8601Time
:	public TimeFormat
{
public:
	/// Structure that describes the Offset ofa time zone from UTC time
	struct UtcOffset
	{
		/// Construction
		UtcOffset( short h, unsigned short m );

		short hour; ///< time zone hour offset from UTC
		unsigned short minute; ///< time zone minute offset from UTC
	};

	/**
	 * @brief Construction with TieBase
	 * @param base The point in time that represents SimTime 0
	 * @param utcOffsetHour UTC offset hour part
	 * @param utcOffsetMin UTC offset minute part
	 *
	 * The UTC offset is limited to narrow ranges of values.
	 * The hour part ranges from -12 though 14, and the minute part may
	 * only contain the values 0, 15, 30, or 45.
	 */
	explicit Iso8601Time( const TimeBase& base, short utcOffsetHour = 0,
			unsigned short utcOffsetMin = 0 );

	// uses predefined computation function from Format()

	/// defines the output format of the time string
	virtual const std::string makeString() const;

	/// Change to UTC time offset
	void setUtcOffset( short hour, short minute );
	/// Get the offset from UTC time
	const UtcOffset& getUtcOffset() const;

private:
	UtcOffset utcOffset_; ///< time zone to be added
};

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_ISO8601TIME_INCLUDED */
