//------------------------------------------------------------------------------
//	Copyright (C) 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file DefaultLoggingType.h
 * @author Ronald Kluth
 * @date created at 2010/04/12
 * @brief Declaration of enum odemx::data::output::Type
 * @since 3.0
 */

#ifndef ODEMX_DATA_OUTPUT_DEFAULTLOGGINGTYPE_INCLUDED
#define ODEMX_DATA_OUTPUT_DEFAULTLOGGINGTYPE_INCLUDED

namespace odemx {
namespace data {
namespace output {

/**
 * @brief Determines which output location should be used for default logging
 * @ingroup data
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::data::LoggingManager
 *
 * ODEMx supports default logging output. This functionality can be activated
 * by calling @c enableDefaultLogging on the Simulation object, which is a
 * subclass of the LoggingManager. One required paramerter is the output type,
 * i.e. one of the values of this enumeration.
 */
enum Type
{
	DATABASE = 100000,///< Enable database output, needs connection string
	STDOUT,           ///< Enable console output
	XML               ///< Enable XML file output, requires filename prefix
};

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_DEFAULTLOGGINGTYPE_INCLUDED */
