//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file data/output/OStreamWriter.h
 * @author Ronald Kluth
 * @date created at 2009/02/09
 * @brief Declaration of class odemx::data::output::OStreamWriter
 * @sa OStreamWriter.cpp
 * @since 3.0
 */

#ifndef ODEMX_DATA_OUTPUT_OSTREAMWRITER_INCLUDED
#define ODEMX_DATA_OUTPUT_OSTREAMWRITER_INCLUDED

#include <odemx/data/SimRecord.h>
#include <CppLog/Consumer.h>

#include <ostream>
#ifdef _MSC_VER
#include <memory>
#else
#include <memory>
#endif

//----------------------------------------------------------forward declarations

namespace odemx {
namespace data {
namespace output {

class TimeFormat;

//-----------------------------------------------------------header declarations

/**
 * @brief A simple log consumer implementation for writing plain text
 * @ingroup data
 * @author Ronald Kluth
 * @since 3.0
 * @see odemx::data::Producer
 *
 * This data consumer accepts a reference to any \c std::ostream object and
 * immediately writes records to it when \c consume() is called. The time
 * format of the record output is changeable.
 */
class OStreamWriter
:	public Log::Consumer< SimRecord >
{
public:
	/**
	 * @brief Creation by static method
	 * @param os The \c std::ostream to which output will be written
	 * @return A shared_ptr-managed OStreamWriter object
	 *
	 * This method enforces the use of reference-counting pointers to data
	 * consumer objects in order to avoid manual memory management of these
	 * resources. ODEMx log channels maintain a set of shared pointers to
	 * their consumers, which guarantees that consumer objects stay alive while
	 * they are registered with a channel.
	 */
	static std::shared_ptr< OStreamWriter > create( std::ostream& os );

	/// Destruction
	virtual ~OStreamWriter();

	/**
	 * @brief Implementation of the consumer interface, writes records to a stream
	 * @param channelId ID of the forwarding log channel
	 * @param record A log record describing the current simulation event
	 *
	 * Each record is printed on a single line. The output format contains
	 * the formatted simulation time, the channel name, the producer label,
	 * and the message text. Additionally, details and scope may be printed,
	 * if the record contains any.
	 */
	virtual void consume( const Log::ChannelId channelId, const SimRecord& record );

	/**
	 * @brief Set a new time formatter
	 * @param timeFormat Pointer to a dynamically allocated time formatter
	 * @see odemx::data::output::GermanTime odemx::data::output::Iso8601Time
	 *
	 * The default formatter simply outputs SimTime values. The function takes
	 * ownership of the pointer and deletes it automatically when the writer is
	 * destroyed or the format is changed.
	 */
	void setTimeFormat( TimeFormat* timeFormat );

private:
	/// Internal reference to the consuming ostream object.
	std::ostream& os_;
	/// Pointer to the time formatter, if set
	std::unique_ptr< TimeFormat > format_;

	/// Construction only via create()
	OStreamWriter( std::ostream& os );
	/// Non-copyable
	OStreamWriter( const OStreamWriter& );
	/// Non-assignable
	OStreamWriter& operator=( const OStreamWriter& );
};

/// Smart pointer type to manage OStreamWriter objects
typedef std::shared_ptr< OStreamWriter > OStreamWriterPtr;

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_OSTREAMWRITER_INCLUDED */
