//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file DefaultTimeFormat.h
 * @author Ronald Kluth
 * @date created at 2009/04/03
 * @brief Declaration of class odemx::data::output::DefaultTimeFormat
 * @since 2.1
 */

#ifndef ODEMX_DATA_OUTPUT_DEFAULTTIMEFORMAT_INCLUDED
#define ODEMX_DATA_OUTPUT_DEFAULTTIMEFORMAT_INCLUDED

#include <odemx/data/output/TimeFormat.h>
#include <odemx/util/StringConversion.h>

namespace odemx {
namespace data {
namespace output {

/**
 * @brief Simple default implementation of a TimeFormat
 * @ingroup data
 * @author Ronald Kluth
 * @since 2.1
 * @see odemx::data::output::TimeFormat odemx::data::output::GermanTime odemx::data::output::Iso8601Time
 *
 * The default time formatter is provided for log consumers that convert
 * their SimTime values to strings. It can be used like any other TimeFormat,
 * thus making them exchangeable for log consumers. All ODEMx log consumers
 * have the capability to format SimTime.
 */
class DefaultTimeFormat
:	public TimeFormat
{
public:
	/// Redefines computation function to avoid overhead, simply stores @c t in a member
	virtual void computeTimeValues( base::SimTime t );
	/// Converts normal SimTime number to string without computing time units
	virtual const std::string makeString() const;
};

//-----------------------------------------------------------------------inlines

inline void DefaultTimeFormat::computeTimeValues( base::SimTime t )
{
	simTime = t;
}

inline const std::string DefaultTimeFormat::makeString() const
{
	return toString( simTime );
}

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_DEFAULTTIMEFORMAT_INCLUDED */
