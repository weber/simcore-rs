//------------------------------------------------------------------------------
//	Copyright (C) 2008, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file TimeUnit.h
 * @author Ronald Kluth
 * @date created at 2008/03/14
 * @brief Declaration of odemx::data::output::TimeUnit
 * @sa TimeUnit.cpp
 * @since 2.1
 */

#ifndef ODEMX_DATA_OUTPUT_TIMEUNIT_INCLUDED
#define ODEMX_DATA_OUTPUT_TIMEUNIT_INCLUDED

namespace odemx {
namespace data {
namespace output {

/**
 * @brief SimTime always represents one of these time units
 * @ingroup data
 * @author Ronald Kluth
 * @since 2.1
 * @see odemx::data::output::TimeBase odemx::data::output::TimeFormat
 *
 * The computation of real dates from SimTime requires a starting point,
 * and a time unit. The latter are represented by values of this enumeration.
 */
struct TimeUnit
{
	enum Type
	{
		none,
		milliseconds,
		seconds,
		minutes,
		hours
	};
};

} } } // namspace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_TIMEUNIT_INCLUDED */
