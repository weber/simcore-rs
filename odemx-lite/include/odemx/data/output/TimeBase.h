//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file TimeBase.h
 * @author Ronald Kluth
 * @date created at 2009/04/03
 * @brief Declaration of class odemx::data::output::TimeBase
 * @sa TimeBase.cpp
 * @since 2.1
 */

#ifndef ODEMX_DATA_OUTPUT_TIMEBASE_INCLUDED
#define ODEMX_DATA_OUTPUT_TIMEBASE_INCLUDED

#include <odemx/data/output/TimeUnit.h>

namespace odemx {
namespace data {
namespace output {

/**
 * @brief Describes the time base in day, month, year, day time offset and unit
 * @ingroup data
 * @author Ronald Kluth
 * @since 2.1
 * @see odemx::data::output::GermanTime odemx::data::output::Iso8601Time
 *
 * SimTime is an abstraction from real time. It always starts at 0, and the
 * user interprets which time units are used. In order to output formatted
 * SimTime values, two pieces of information are required: the starting point
 * in time, and the unit to be used. This class provides both.
 */
struct TimeBase
{
	/**
	 * @brief Construction, initializes all members at once
	 * @param year The year of the start date
	 * @param month The month of the start date
	 * @param day The day of the start date
	 * @param dayTimeOffset The daytime offset on the start date in units
	 * @param unit The unit to be used when converting SimTime
	 *
	 *
	 */
	TimeBase( unsigned short year, unsigned short month, unsigned short day,
			base::SimTime dayTimeOffset, TimeUnit::Type unit );

	unsigned short year; ///< time base year
	unsigned short month; ///< time base month
	unsigned short day; ///< time base day
	base::SimTime offset; ///< time offset on the given day, must respect unit
	TimeUnit::Type unit; ///< time base unit
};

//-----------------------------------------------------------------------inlines

inline TimeBase::TimeBase( unsigned short year, unsigned short month,
		unsigned short day,	base::SimTime dayTimeOffset, TimeUnit::Type unit )
:	year( year )
,	month( month )
,	day( day )
,	offset( dayTimeOffset )
,	unit( unit )
{
}

} } } // namespace odemx::data::output

#endif /* ODEMX_DATA_OUTPUT_TIMEBASE_INCLUDED */
