//------------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2007, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file odemx.h
 * @author Ralf Gerstenberger
 * @date created at 2002/08/05
 * @brief Includes all ODEMx header files
 * @since 1.0
 */

#ifndef ODEMX_ODEMX_INCLUDED
#define ODEMX_ODEMX_INCLUDED

// base package
#include <odemx/base/Comparators.h>
#include <odemx/base/Continuous.h>
#include <odemx/base/DefaultSimulation.h>
#include <odemx/base/Event.h>
#include <odemx/base/ExecutionList.h>
#include <odemx/base/Process.h>
#include <odemx/base/Sched.h>
#include <odemx/base/Scheduler.h>
#include <odemx/base/SimTime.h>
#include <odemx/base/Simulation.h>
#include <odemx/base/TypeDefs.h>

// control package
#include <odemx/base/control/ControlBase.h>
#include <odemx/base/control/Control.h>

// continuous package
// removed in odemx-lite

// cellular automaton package
#include <odemx/base/cellular_automaton/CellMonitor.h>
#include <odemx/base/cellular_automaton/Cell.h>
#include <odemx/base/cellular_automaton/CellVariablesContainer.h>

// coroutine package
#include <odemx/coroutine/Coroutine.h>
#include <odemx/coroutine/CoroutineContext.h>
#include <odemx/coroutine/System.h>

// data package
#include <odemx/data/LoggingManager.h>
#include <odemx/data/Label.h>
#include <odemx/data/ManagedChannels.h>
#include <odemx/data/Observable.h>
#include <odemx/data/Producer.h>
#include <odemx/data/SimRecord.h>
#include <odemx/data/SimRecordFilter.h>
#include <odemx/data/TableKey.h>
#include <odemx/data/TypeInfo.h>

// buffer sub-package
#include <odemx/data/buffer/SimRecordBuffer.h>
#include <odemx/data/buffer/StatisticsBuffer.h>

// output sub-package
// removed in odemx-lite
// #include <odemx/data/output/DatabaseWriter.h>
#include <odemx/data/output/DefaultLoggingType.h>
#include <odemx/data/output/DefaultTimeFormat.h>
#include <odemx/data/output/ErrorWriter.h>
#include <odemx/data/output/GermanTime.h>
#include <odemx/data/output/Iso8601Time.h>
#include <odemx/data/output/OStreamReport.h>
#include <odemx/data/output/OStreamWriter.h>
#include <odemx/data/output/TimeBase.h>
#include <odemx/data/output/TimeFormat.h>
#include <odemx/data/output/TimeUnit.h>
// removed in odemx-lite
// #include <odemx/data/output/XmlReport.h>
// #include <odemx/data/output/XmlWriter.h>

// protocol package
#include <odemx/protocol/Device.h>
#include <odemx/protocol/Entity.h>
#include <odemx/protocol/ErrorModel.h>
#include <odemx/protocol/Layer.h>
#include <odemx/protocol/Medium.h>
#include <odemx/protocol/Pdu.h>
#include <odemx/protocol/Sap.h>
#include <odemx/protocol/Service.h>
#include <odemx/protocol/ServiceProvider.h>
#include <odemx/protocol/Stack.h>

// random package
#include <odemx/random/ContinuousConst.h>
#include <odemx/random/ContinuousDist.h>
#include <odemx/random/DiscreteConst.h>
#include <odemx/random/DiscreteDist.h>
#include <odemx/random/Dist.h>
#include <odemx/random/DistContext.h>
#include <odemx/random/Draw.h>
#include <odemx/random/Erlang.h>
#include <odemx/random/NegativeExponential.h>
#include <odemx/random/Normal.h>
#include <odemx/random/Poisson.h>
#include <odemx/random/RandomInt.h>
#include <odemx/random/Uniform.h>

// statistics package
#include <odemx/statistics/Accumulate.h>
#include <odemx/statistics/Count.h>
#include <odemx/statistics/Histogram.h>
#include <odemx/statistics/Regression.h>
#include <odemx/statistics/Sum.h>
#include <odemx/statistics/Tab.h>
#include <odemx/statistics/Tally.h>

// synchronization package
#include <odemx/synchronization/Bin.h>
#include <odemx/synchronization/BinT.h>
#include <odemx/synchronization/CondQ.h>
#include <odemx/synchronization/IMemory.h>
#include <odemx/synchronization/Memory.h>
#include <odemx/synchronization/Port.h>
#include <odemx/synchronization/ProcessQueue.h>
#include <odemx/synchronization/Queue.h>
#include <odemx/synchronization/Res.h>
#include <odemx/synchronization/ResT.h>
#include <odemx/synchronization/Timer.h>
#include <odemx/synchronization/Wait.h>
#include <odemx/synchronization/WaitQ.h>

// util package
#include <odemx/util/DeletePtr.h>
#include <odemx/util/Exceptions.h>
#include <odemx/util/ListInSort.h>
#include <odemx/util/StringConversion.h>
#include <odemx/util/TypeNames.h>
#include <odemx/util/TypeToString.h>
#include <odemx/util/Version.h>

// simml package
// removed in odemx-lite

#endif
