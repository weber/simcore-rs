//------------------------------------------------------------------------------
//	Copyright (C) 2003, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file ControlBase.h
 * @author Jonathan Schlue
 * @date created at 2016/10/06
 * @brief Header declaration of a base a class for all Control variables, signaling informed lambda expressions on change.
 * @sa Control.h
 * @sa IMemory.h
 * @since 3.1
 */

#ifndef ODEMX_BASE_CONTROLBASE_INCLUDED
#define ODEMX_BASE_CONTROLBASE_INCLUDED

#include <map>

#include <odemx/base/TypeDefs.h>
#include <odemx/synchronization/IMemory.h>


namespace odemx {
namespace base {


/** \class ControlBase
	
	\ingroup base
	
	\author Jonathan Schlue
	
	\brief ControlBase acts as base class for all control variable types.
	
	\note ControlBase supports Process alertion.
	
	ControlBase implements the IMemory interface to act as an alerter for remembered, waiting processes.
 	Control variables are subclass of ControlBase and alert waiting processes on change.
 	ControlBases are not instantiable, only Control variables are.
	
	\since 3.1
*/
class ControlBase : public odemx::synchronization::IMemory
{
public:

	/// Destruction
	virtual ~ControlBase()
	{}

	/// Get the Control variable memory type: "CONTROL"
	virtual Type getMemoryType() const override;
	friend class Process;

protected:

	/// Protected copy constructor to forbid user instantiation
	ControlBase(const ControlBase&) = default;

	/// Protected move constructor to forbid user movement
	ControlBase(ControlBase&&) = default;

	/// Protected compiler generated default constructor
	ControlBase() = default;

	/// Wake up all waiting processes using FIFO strategy by calling their hold() method
	void signal();

private:
	virtual bool isAvailable() override;
	virtual void alert() override;
	virtual bool remember(base::Sched* newObject) override;
	virtual bool forget(base::Sched* rememberedObject) override;
	virtual void eraseMemory() override;

	/// Private copy assignment to forbid usage
	ControlBase& operator =(const ControlBase&) = default;

	/// Private move assignment to forbid usage
	ControlBase& operator =(ControlBase&&) = default;

	/**
	 * \brief List of all registered, waiting processes
	 *
	 * Processe are registed, whenever they wait until an arbitrary condition comes true using their waitUntil() method.
	 * All involved and explicitely listed control variables then register these processes and alert them on change to eventually
	 * check their condition again.
	 */
	ProcessList waitingProcesses_;
};

}
}

#endif //ODEMX_BASE_CONTROLBASE_INCLUDED
