//------------------------------------------------------------------------------
//	Copyright (C) 2003, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Control.h
 * @author Jonathan Schlue
 * @date created at 2016/10/06
 * @brief Header declaration and header-only definitions of all class templates for Control Variables for built-in types.
 * @sa ControlBase.h
 * @sa ControlBase.cpp
 * @since 3.1
 */

#ifndef ODEMX_BASE_CONTROL_INCLUDED
#define ODEMX_BASE_CONTROL_INCLUDED

#include <type_traits>

#include <odemx/base/control/ControlBase.h>


namespace odemx {
namespace base {

/** \class Control
	
	\ingroup base
	
	\author Jonathan Schlue
	
	\brief %Control represents and acts as a general interface for control variables of built-in types.
	
	%Control variables wrap variables of built-in types. All increment, decrement and binary assignment operators are overloaded, so that
 	changes to the variable are recognized and registered processes are alerted using ControlBase's signal() method. Apart from that,
 	control variables act exactly as their wrapped built-in type. Use "Control<T>" as a type for variable declarations, where T may be any build-in type.
	
 	\note Waiting processes are alerted only if the variable value changes.
	
 	\note All operators are overloaded in a canonical manner. For implementation details please refer to the source code directly.
	
	\since 3.1
*/
template <typename T>
class Control: public ControlBase
{
	static_assert(
		std::is_integral<T>::value,
		"Only integral build-in types may be used as control variables."
	);
	
public:
	Control() = default;
	
	Control(const T& val)
	: val_(val)
	{}
	
	operator T() const
	{
		return val_;
	}
	
	// ASSIGNMENT
	
	Control<T>& operator=(const T& other)
	{
		if (val_ != other)
		{
			val_ = other;
			signal();
		}
		return *this;
	}
	
	// BINARY ASSIGNMENT ARITHMETICS
	
	Control<T>& operator+=(const T& other)
	{
		if (other != 0)
		{
			val_ += other;
			signal();
		}
		return *this;
	}
	
	Control<T>& operator-=(const T& other)
	{
		if (other != 0)
		{
			val_ -= other;
			signal();
		}
		return *this;
	}
	
	Control<T>& operator*=(const T& other)
	{
		if (val_ != 0 && other != 1)
		{
			val_ *= other;
			signal();
		}
		return *this;
	}
	
	Control<T>& operator/=(const T& other)
	{
		if (val_ != 0 && other != 1)
		{
			val_ /= other;
			signal();
		}
		return *this;
	}
	
	// BINARY ASSIGNMENT MODULO
	
	std::enable_if<std::is_integral<T>::value, Control<T>&> operator%=(const T& other)
	{
		T res = val_ % other;
		if (res != val_)
		{
			val_ = res;
			signal();
		}
		return *this;
	}
	
	// BINARY ASSIGNMENT BITWISE
	
	std::enable_if<std::is_integral<T>::value, Control<T>&> operator^=(const T& other)
	{
		T res = val_ ^ other;
		if (val_ != res)
		{
			val_ = res;
			signal();
		}
		return *this;
	}
	
	std::enable_if<std::is_integral<T>::value, Control<T>&> operator&=(const T& other)
	{
		T res = val_ & other;
		if (val_ != res)
		{
			val_ = res;
			signal();
		}
		return *this;
	}
	
	std::enable_if<std::is_integral<T>::value, Control<T>&> operator|=(const T& other)
	{
		T res = val_ | other;
		if (val_ != res)
		{
			val_ = res;
			signal();
		}
		return *this;
	}
	
	std::enable_if<std::is_integral<T>::value, Control<T>&> operator<<=(unsigned shift)
	{
		if (shift != 0)
		{
			val_ <<= shift;
			signal();
		}
		return *this;
	}
	
	std::enable_if<std::is_integral<T>::value, Control<T>&> operator>>=(unsigned shift)
	{
		if (shift != 0)
		{
			val_ >>= shift;
			signal();
		}
		return *this;
	}
	
	// INCREMENT
	
	std::enable_if<!std::is_same<T,bool>::value, Control<T> > operator++(int)
	{
		Control<T> temp = *this;
		++*this;
		return temp;
	}
	
	std::enable_if<!std::is_same<T,bool>::value, Control<T>&> operator++()
	{
		++val_;
		signal();
		return *this;
	}
	
	// DECREMENT
	
	std::enable_if<!std::is_same<T,bool>::value, Control<T> > operator--(int)
	{
		Control<T> temp = *this;
		--*this;
		return temp;
	}
	
	std::enable_if<!std::is_same<T,bool>::value, Control<T>&> operator--()
	{
		--val_;
		signal();
		return *this;
	}
	
private:
	T val_;
};

} // END namespace base
} // END namespace odemx

#endif //ODEMX_BASE_CONTROL_INCLUDED
