//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file VariableContainer.h

 \author Michael Fiedler

 \date created at 2008/11/21

 \brief Declaration of VariableContainer

 \since 3.0
 */

#ifndef ODEMX_VARIABLECONTAINER_INCLUDED
#define ODEMX_VARIABLECONTAINER_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/Monitor.h>

namespace odemx {
	namespace base {
		namespace continuous {

			/** \class VariableContainer

				\ingroup base

				\brief Abstract class for implementing memory handling of a solver.

				\sa ODESolver, Monitor

				\since 3.0
			 */
			class VariableContainer {
			public:
				/// destruction
				virtual ~VariableContainer();

				/** \brief Adapts the numbers of variables which can be stored in the container
					\param newSize
						new size of container

					\note Old content is preserved as long as it fits in \p newSize, everything over will be truncated.
				 */
				virtual void adaptSizeTo(int newSize) = 0;

				/** \name Variables

				 	 Variables are for holding values, while derivatives, jacobi and DfDt are for calculating.

				 @{
				 */
				/** \brief Sets all variables to 0.0
				 */
				virtual void nullVariables() = 0;

				/** \brief Sets value of one variable
					\param num
						index of variable on which \p value is set
				 */
				virtual void setVariable(int num, double value) = 0;

				/** \brief Returns the value of one variable
					\param num
						index of returned variable
				 */
				virtual double getVariable(int num) = 0;
				//@}

				/** \name temporary values

					These values are used during the calculation of the intermediate steps.

				 @{
				 */
				/** \brief Sets all temporary variables to 0.0
				 */
				virtual void nullValues() = 0;

				/** \brief Copies values of temporary variables to the corresponding variables
				 */
				virtual void copyVariablesToValues() = 0;

				/** \brief Sets value of one temporary variable
					\param num
						index of variable on which \p value is set
				 */
				virtual void setValue(int num, double value) = 0;

				/** \brief Returns value of one temporary variable
					\param num
						index of returned variable
				 */
				virtual double getValue(int num) = 0;
				//@}

				/** \name derivatives

					@{
				 */
				/** \brief Sets all derivative values to 0.0
				 */
				virtual void nullDerivatives() = 0;

				/** \brief Sets value of one derivative variable
					\param num
						index of variable on which \p value is set
				 */
				virtual void setDerivative(int num, double value) = 0;

				/** \brief Adds value to one derivative variable
					\param num
						index of variable to which \p value will be added
				 */
				virtual void addToDerivative(int num, double value) = 0;

				/** \brief Returns value of one derivative variable
					\param num
						index of returned variable
				 */
				virtual double getDerivative(int num) = 0;
				//@}

				/** \name jacobi

					@{
				 */
				/** \brief Sets all jacobian values to 0.0
				 */
				virtual void nullJacobi() = 0;

				/** \brief Sets value of one jacobian variable

						Sets variable(\p i ,\p j ) to \p value.
				 */
				virtual void setJacobi(int i, int j, double value) = 0;

				/**  \brief Sets value of one jacobian variable

						Adds \p value to variable(\p i ,\p j ).
				 */
				virtual void addToJacobi(int i, int j, double value) = 0;

				/** \brief Returns value of one jacobian variable

					\return variable of index (\p i ,\p j )
				 */
				virtual double getJacobi(int i, int j) = 0;
				//@}

				/** \name DfDt

					right hand side derived by time

				 @{
				 */
				/** \brief Sets all derived components to 0.0
				 */
				virtual void nullDfDt() = 0;

				/** \brief Sets value of one derived component
					\param num
						index of derived component on which \p value is set
				 */
				virtual void setDfDt(int num, double value) = 0;

				/** \brief Adds value to one derived component
					\param num
						index of derived component to which \p value will be added
				 */
				virtual void addToDfDt(int num, double value) = 0;

				/** \brief Returns value of one derived component
					\param num
						index of returned derived component
				 */
				virtual double getDfDt(int num) = 0;
				//@}
			};
		}
	}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_VARIABLECONTAINER_INCLUDED */
