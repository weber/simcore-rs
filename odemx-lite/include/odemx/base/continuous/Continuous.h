//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004, 2007, 2008, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Continuous.h

 \author Michael Fiedler

 \date created at 2008/11/21

 \brief Declaration of odemx::base::Continuous

 \sa Continuous.cpp

 \since 3.0
 */

#ifndef ODEMX_CONTINUOUS_INCLUDED
#define ODEMX_CONTINUOUS_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/ODEObject.h>
#include <odemx/base/continuous/VariableContainer.h>
#include <odemx/data/Producer.h>
#include <odemx/data/Observable.h>

#include <string>
#include <vector>
#include <list>

namespace odemx {
	namespace base {
		namespace continuous {

			// forward declarations
			class Monitor;

			class ODEObject;

			class VariableContainer;

			class ContinuousObserver;

			/** \class Continuous

			 \ingroup base

			 \author Michael Fiedler

			 \brief %Continuous is the class for describing values of a continuous process in the system

			 \note since 3.0 the values and the equations of a continuous process are split in Continuous and ODEObject

			 \note Continuous supports Observation

			 \note Continuous supports Trace

			 \note Continuous supports Logging

			 \sa ODEObject, Monitor, StateEvent, ODESolver

			 The user creates an %Continuous instance for each continuous object in a simulation.
			 This instance holds dimension continuous values.

			 \since 3.0

			 */
			class Continuous : public data::Producer, public data::Observable<ContinuousObserver> {
			public:
				/** \brief Construction
				 	\param sim reference to one simulation-object
				 	\param l label of this object
				 	\param dimension count of continuous values for the instance (dedicated)
				 */
				Continuous(Simulation& sim, const data::Label& label, int dimension);

				/** \brief Construction
				 \param sim reference to one simulation-object
				 \param l label of this object
				 \param monitor the monitor were the continuous object belongs to
				 \param dimension number of continuous values for the instance (dedicaated)
				 \param equation first equation in the life cycle of the object this can changed and other equations can be added via addODE(...).
				 */
				Continuous(Simulation& sim, const data::Label& label, Monitor* monitor, int dimension, ODEObject* equation = 0);

				/** \brief Construction
				 \param sim reference to one simulation-object
				 \param l label of this object
				 \param monitor the monitor were the continuous object belongs to
				 \param dimension number of continuous values for the instance (dedicaated)
				 \param equationList list of equations of the continuous object this can changed and other equations can be added via addODE(...).
				 */
				Continuous(Simulation& sim, const data::Label& label, Monitor* monitor, int dimension, std::list<ODEObject*> equationList);

				/// Destruction
				~Continuous();

				/** \name Equation handling

					These functions manage which equations are currently influencing the continuous values.

					@{
				 */
				/** \brief Adds an equation
					\param equation
						pointer to an ODEObject describing an equation on the values
					\note All equations which are added will be used for integration simultaneously and summed up.
					\note This results in a change of the associated monitor.
				 */
				void addODE(ODEObject *equation);

				/** \brief Adds a list of equations
					\param equation
						list of pointer to an ODEObject describing the equations on the values
					\note All equations which are added will be used for integration simultaneously and summed up.
					\note This results in a change of the associated monitor.
				 */
				void addODEList(std::list<ODEObject*> equation);

				/** \brief Remove an equation
					\param
						pointer to the ODEObject to be removed
					\note The equation is only removed from calculation. The object will not be destroyed.
					\note This results in a change of the associated monitor.
				 */
				void removeODE(ODEObject *equation);

				//@}

				/** \name Monitor handling

					These functions are setting and returning the %Monitor, which calculates the evolution of the
					continuous values of this instance described by the added equations.

					@{
				 */
				/** \brief Sets the Monitor in which the Continuous %Process will be integrated
					\param monitor
						pointer to the monitor which manages this instance
					\note The instance will automatically be transferred from another %Monitor, if one was assigned.
					\note To remove the Continuous object from a %Monitor pass NULL as parameter.
					\note This results in a change to the involved monitors.
				 */
				void setMonitor(Monitor *monitor);

				/** \brief Returns the Monitor which manages this instance
				 */
				Monitor* getMonitor();
				//@}

				/** \brief Returns the dimension of the Continuous %Process

					\note That means the number of variables the %Process has.
				 */
				int getDimension();

				/** \name Access to values during integration

					Values should only be changed, when the responsible Monitor is not integrating.

					@{
				 */
				/** \brief Sets the value of one variable
					\param i
						index of the variable to be set (range 0..(Dimension-1))
					\param value
						the value which the variable will be set to
					\note Should only be used, when responsible %Monitor is not running (that means is not integrating).
					\note This results in a change of the associated monitor.
				 */
				void setValue(int i, double value);

				/** \brief Returns the value of one variable
					\param i
						index of the returned variable (range 0..(Dimension-1))
					\note Should only be used when responsible %Monitor is not running (that means is not integrating)
					\return the value of the variable accessed by index \p i
				 */
				double getValue(int i);
				//@}

				/**	\name Access to Values in Integration

					These functions are for the evaluation of the differential equations.
					That means they are used in implementations of the functions derivates and jacobi of the class ODEObject.
					The user has to use the member function getValueForDerivative to access the value of one variable in such a implementation.

					\note These functions should only be used while integration.

					@{
				 */
				/** \brief Returns the value of one variable while integrating
					\param i
						index of the returned variable (range 0..(Dimension-1))
					\return the value of the i-th variable at a possible intermediate step

					\note The user has to use this function to access the value of a variable in an implementation of derivates or
					jacobi in a derived class of ODEObject
				 */
				double getValueForDerivative(int i);

				/** \brief Sets the value for y' = f(y) in the i-th component
					\param i
						index of the variable to be set (range 0..(Dimension-1))
					\param value
						set y'[i] = \p value

					\note The values of multiple calls will be summed up.
				 */
				void setDerivative(int i, double value);

				/** \brief Returns the value of y' = f(y) in the i-th component
					\param i
						index of the variable to get (range 0..(Dimension-1))

					\note That means y'[i] will be returned
				 */
				double getDerivative(int i);

				/** \brief Sets the value for Df exactly f_i \partial y[j] = value
					\param i
						index of the component to differentiate (range 0..(Dimension-1))
					\param j
						index of the variable by which will be differentiated (range 0..(Dimension-1))

					For an equation given as y' = f(y) set the result \p value for differentiating
					the i-th component of f by the j-th component of y.

					\note The values of multiple calls will be summed up.
				 */
				void setJacobi(int i, int j, double value);

				/** \brief Sets the value for Df exactly f_i \partial continuous->y[j] = value
					\param i
						index of the component to differentiate (range 0..(Dimension-1))
					\param continuous
						pointer to a continuous instance which influences the right hand side
					\param j
						index of the variable by which will be differentiated (range 0..(continuous->getDimension()-1))

					For an equation given as y' = f(y, x) set the result \p value for differentiating
					the i-th component of f by the j-th component of x.

					\note The values of multiple calls will be summed up.
					\note To work correctly this instance and \p continuous have to be managed by the same %Monitor
				 */
				void setJacobi(int i, Continuous* continuous, int j, double value);

				/** \brief Returns the value of Df returns f_i \partial y[j]
					\param i
						index of the component to get (range 0..(Dimension-1))
					\param j
						index of the variable by which will be differentiated (range 0..(Dimension-1))

					For an equation given as y' = f(y) gives the intermediate result for differentiating
					the i-th component of f by the j-th component of y. The value of result is determined
					by previous calls to setJacobi.
				 */
				double getJacobi(int i, int j);

				/** \brief Returns the value of Df returns f_i \partial continuous->y[j]
					\param i
						index of the component to differentiate (range 0..(Dimension-1))
					\param continuous
						pointer to a Continuous instance which influences the right hand side
					\param j
						index of the variable by which will be differentiated (range 0..(continuous->getDimension()-1))

					For an equation given as y' = f(y, x) set the result \p value for differentiating
					the i-th component of f by the j-th component of x. The value of result is determined
					by previous calls to setJacobi.

					\note To work correctly this instance and \p continuous have to be managed by the same %Monitor.
				 */
				double getJacobi(int i, Continuous* continuous, int j);

				/** \brief Sets the value for DfDt exactly f_i \partial t = value
					\param i
						index of the component to set (range 0..(Dimension-1))

					For an equation given as y' = f(y) set the result \p value for differentiating
					the i-th component of f by time.

					\note The values of multiple calls will be summed up.
				 */
				void setDfDt(int i, double value);

				/** \brief gets the value for DfDt returns f_i \partial t = value
					\param i
						index of the component to get (range 0..(Dimension-1))

					For an equation given as y' = f(y) set the result \p value for differentiating
					the i-th component of f by time. The value of result is determined
					by previous calls to setDfDt.
				 */
				double getDfDt(int i);
				//@}

				/** \brief Sets changed on true, so that the monitor will call solver->init() before next calculation
				 */
				void setChanged();

				/** \brief Triggers the observer to write the state variables
				 */
				void notifyValidState();

			private:
				// List all equations which belong to this instance
				std::list<ODEObject*> equations;

				// Monitor in which this instance will be integrated
				Monitor *monitor;

				// Dimension of the instance
				int dimension;

				// Base index for variables of this continuous object in the variables container of the monitor
				int baseIndex;

				friend class Monitor;
			};

			//forward declaration
			class Monitor;

			/** \interface ContinuousObserver

				\author Sascha Qualitz

				\brief Observer for Continuous specific events

				\sa Monitor

				\since 3.0
			*/
			class ContinuousObserver:
				public ProcessObserver
			{
			public:
				virtual void onCreate( Continuous* sender ) {} ///< Construction

				virtual void onNewValidState( Continuous* sender ) {} ///< New valid state

				virtual void onSetMonitor( Continuous* sender, Monitor* monitor ) {} ///< new Monitor set

				virtual void onAddODE(Continuous* sender, ODEObject* ode) {} ///< new ordinary differential equation added
				virtual void onRemoveODE(Continuous* sender, ODEObject* ode) {} ///< old ordinary differential equation removed
			};

			/** \class ContinuousTrace

				\author Sascha Qualitz

				\brief Trace for Continuous state changes of continuous objects controlled by the monitor

				\sa Continuous MonitorObserver

				ContinuousTrace logs the state changes of the Continuous objects
				into a text file. The text file is automatically opened and closed.

				\since 3.0
			*/
			class ContinuousTrace
			:	public ContinuousObserver
			{
				public:
					/// Construction
				ContinuousTrace( Continuous* contu = 0, const std::string& fileName = "" );
					/// Destruction
					virtual ~ContinuousTrace();

					/// Follow state changes
					virtual void onNewValidState( Continuous* sender );

				private:
					// Implementation
					std::ostream* out;
					bool firstTime;
					const std::string& fileName;

					Continuous* continuous;

					void openFile( const std::string& fileName );
			};
		}
	}
}
#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_CONTINUOUS_INCLUDED */
