//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file ODEObject.h

 \author Michael Fiedler

 \date created at 2008/11/21

 \brief abstract class to derive a representation of an ordinary differential equation
 \sa Continuous, Monitor, StateEvent
 \since 3.0
 */

#ifndef ODEMX_ODEOBJECT_INCLUDED
#define ODEMX_ODEOBJECT_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/SimTime.h>

#include <odemx/base/TypeDefs.h>

#include <string>
#include <exception>
#include <vector>
#include <list>


namespace odemx {
namespace base {
namespace continuous {

//class Monitor; //TODO: really useful?

class Continuous;


/** Exception for methods which fails, because there is missingObject assigned for this object
 */
class NotAssignedException: public std::exception
{
public:
	/// Constructor
	NotAssignedException(const char* missingObject, const char* object);
	/// Destructor
	~NotAssignedException() throw();
	/// give message for exception
	const char* what() const throw();

private:
	// contains the Exception message
	std::string msg;
};


/** \class ODEObject

	\ingroup base

	\author Michael Fiedler

	\brief Object for handling an equation

	To describe an ordinary differential equation the user has to implement derivates and jacobi.

	\note In such implementations the user has to use getValueForDerivative to access the variables of
	the corresponding Continuous instance.
 */
class ODEObject
{
public:
	/** \brief Construction
	 */
	ODEObject(const Derivative& derivatives, const Derivative& jacobi) :derivatives_{derivatives}, jacobi_{jacobi}, continuous{0}
	{}

	/** \brief Construction
	 */
	ODEObject(Derivative&& derivatives, Derivative&& jacobi) :derivatives_{derivatives}, jacobi_{jacobi}, continuous{0}
	{}

	/// destruction
//	virtual ~ODEObject();

	/** \name Handling of corresponding Continuous instance

		These are the functions to set/get the Continuous instance for this equation to work on.
		If the user wants to have an equation which works on more than one Continuous instance he
		has to extend this functionality for the extra instances.

		@{
	 */
	/** \brief Adds this ODEObject-instance to a Continuous-Object
		\param continuous
			pointer to Continuous instance where this equation will be added
	 */
	void setContinuous(Continuous* continuous); //TODO: is never used!

	/** \brief Returns the corresponding Continuous instance
		\note If no corresponding Continuous instance is set, the result will be NULL
	 */
	Continuous* getContinuous();
	//@}

	/** \brief implementation of equation in form y' = f(y)

	 the following is an example for implementation of this function

	 v'[0] = v[1]

	 v'[1] = v[0]

	 in code:

	 DerivatesElement rate(continuous);

	 DerivatesElement state(continuous);

	 rate[0] = state[1];

	 rate[1] = -1 * state[0];

	 \sa Rate, State
	 */
	void derivates(SimTime time);
	/** \brief derivates of implemented equation in form y' = f(y)

	 the following is an example for implementation of this function (fßx means derive f by x):

	 in code:

	 JacobiElement jacobi(continuous);

	 DfDtElement dfdt(continuous);

	 jacobi(0,0) = 0;

	 jacobi(0,1) = 1;

	 jacobi(1,0) = -1;

	 jacobi(1,1) = 0;

	 dfdt[0] = 0;

	 dfdt[1] = 0;
	 */
	void jacobi(SimTime time);

//			protected:
private:
	// stores the Continuous-Object where equation belongs to
	Continuous* continuous;
	Derivative derivatives_;
	Derivative jacobi_;

//			protected:
public:
				friend class Continuous; // FIXME needed?

};
}
}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_ODEOBJECT_INCLUDED */
