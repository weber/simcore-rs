//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/** \file State.h

 	\author Sascha Qualitz

 	\date created at 2010/2/26

 	\brief Declaration of class odemx::base::continuous::State

 	\sa State.cpp

    \since 3.0
 */

#ifndef ODEMX_STATE_INCLUDED
#define ODEMX_STATE_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

namespace odemx {
	namespace base {
		namespace continuous {

			//Forward declaration
			class Continuous;

			class ODEObject;

			/** \class State

				\ingroup base

				\author Sascha Qualitz

				\brief Object for handling an equation-element of the right hand side of an equation.
					   Used in the derivates function in a derived class of ODEObject.

				To describe ordinary differential equations the user has to use this class.

				\sa Rate
				\sa JacobiMatrix
				\sa DfDt
			 */
			class State {
			public:
				/**
					\brief Construction

					\note The variables continuous and index_ will be set to 0.
				*/
				State();
				/**
					\brief Construction
					\param continuous
						pointer to the continuous object where the equation-element belongs to

					\note The variable index_ will be set to 0.
				*/
				State(Continuous* continuous);

				/// destruction
				virtual ~State();

				/**
					\brief Overrides the double converting-operator, to convert implicit an object of type state to type double.

					\note If this operator is called, the double value is the value that is returned by the method
					getValueForDerivative(index_) of the corresponding continuous-object. The variable index_ was
					set to an index between 0 and continuous->getDimension-1 before. To set the variables index_
					see also the method	operator [].
				*/
				operator double() {
					return this->getValue();
				}

				/**
					\brief Overrides the index operator to set the internal variable index_
					\param index
						index of the variable to get/set (range 0..(Dimension-1))
				*/
				State& operator [](const unsigned i);

				/**
					\brief Sets the continuous variable
					\param continuous
						pointer to the continuous object where the equation-element belongs to
				*/
				void setContinuous(Continuous* continuous);

				/**
					\brief Overrides the assignment operator to store the value of type double in the variable container
					\param value
						value to be set

					\note Values must not be changed while the responsible monitor is integrating.
				*/
				State& operator =(const double value);

			private:
				/**
					\brief Returns the value of type double with given index index_

					getValue returns the value from the correct index, because index_ was set by the overwritten operator[] before.
				*/
				double getValue() const;

				/**
					\brief Sets the value of type double at index_
					\param value
						value to be set

					\note setValue stores the value at the correct index, because index_ was set by the overwritten operator[] before.
					\note Values must not be changed while the responsible monitor is integrating.
				*/
				void setValue(double value);


				// pointer to the continuous object where the equation-element belongs to
				Continuous* continuous;

				// index of the variable to get/set (range 0..(Dimension-1))
				unsigned index_;

				friend class Rate;
			};
		}
	}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_STATE_INCLUDED */
