//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Monitor.h

 \author Michael Fiedler

 \date created at 2008/11/21

 \brief Declaration of Monitor

 \since 3.0
 */

#ifndef ODEMX_MONITOR_INCLUDED
#define ODEMX_MONITOR_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/Process.h>
#include <odemx/base/continuous/ODESolver.h>
#include <odemx/base/continuous/Continuous.h>
#include <odemx/base/continuous/StateEvent.h>
#include <odemx/base/continuous/VariableContainer.h>
#include <list>
#include <fstream>
#include <iostream>
#include <odemx/setup.h>
#include <odemx/data/Observable.h>

namespace odemx {
	namespace base {
		namespace continuous {

			/** Choice if integration is done by interval or by steps
			 */
			enum WorkingModes {
				stepwise, intervall
			};

			/** Choice how to search for StateEvents. That means if to work on a linear approximation or
				direct on the solution.
			 */
			enum SearchModes {
				linearization, direct
			};

			//forward declaration
			class ODESolver;

			class VariableContainer;

			class Continuous;

			class StateEvent;

			class MonitorObserver;

			/** \class Monitor

				\ingroup base

				\brief A monitor is used to integrate several continuous processes simultaneously,
				but independent from continuous processes in other monitors of the simulation.

				\sa Continuous, ODEObject, ODESolver, StateEvent, Process

				\since 3.0
			 */
			class Monitor : public Process, public data::Observable<MonitorObserver> {
			public:

				/** \brief Construction
				 \param variablesArraySize
					 initial size of data structs
				 \param s
					pointer to the Simulation object
				 \param label
					label of this object
				 \param solver
					the solver for this monitor
				 \param o
					initial observer
				 \note If the data structs are to small on runtime, they will be enlarged automatically at cost of speed
				 */
				Monitor(Simulation& s, const data::Label& label, int variablesArraySize, ODESolver* solver, MonitorObserver* o = 0);

				/// destruction
				~Monitor();

				/** \brief sets how monitor schedules integration
					\param mode
						If value is 'stepwise' the %Monitor will be scheduled after every step (as in older version).
						If value is 'interval' the %Monitor will be scheduled only on known synchronization points.

					\note The synchronization point is the earliest time to which one of the processes in the list wait list is scheduled.
				 */
				void setWorkingMode(WorkingModes mode);

				/** \brief Sets the method how to search for StateEvents
					\param mode
						value is direct or linearization

					Sets if monitor searches for StateEvents directly on the solution (slower but more accurate) or
					on a linearization (faster)
				 */
				void setSearchMode(SearchModes mode);

				/** \brief Synchronizes this instance to another monitor

					That means the other monitor will be synchronized to the time of this monitor

					\note not implemented now
				 */
				void synchronizeToMonitor(Monitor *otherMonitor);

				/** \name handle used solver

					These functions are to control which solving method is used for integration

					@{
				 */
				/** \brief Sets the solver for this monitor
				 */
				void setODESolver(ODESolver *solver);

				/** \brief Returns the actual solver of this monitor
				 */
				ODESolver* getODESolver();
				//@}

				/** \name Process synchronization

					These functions control with which processes the monitor synchronizes.
					This means the monitor stops integration at times when one of these processes is scheduled.
					The integration will be continued after this process has been executed.

					\note This is necessary for time consistent exchange of values between processes and monitors.

					@{
				 */
				/** \brief Adds a discrete process to which the monitor will be synchronized

					That means the monitor will only integrate to the next event of one discrete process.
					The monitor waits till this event is computed.
				 */
				void addProcessToWaitList(Process *process);

				/** \brief Removes a discrete process from the monitor to synchronize
				 */
				void removeProcessFromWaitList(Process *process);
				//@}

				/** \name continuous processes and StateEvents

					This functions control which continuous processes are calculated by this monitor and
					which StateEvents will be checked during integration.

					@{
				 */
				/** \brief Adds a continuous process to the monitor

					This process will be integrated by the monitor simultaneously
					to other continuous processes of the monitor.

					\note This results in a change of the monitor.
				 */
				void addContinuous(Continuous *continuous);

				/** \brief Removes a continuous process from the monitor

					\note This results in a change of the monitor.
				 */
				void removeContinuous(Continuous *continuous);

				/** \brief Adds a StateEvent to the monitor to watch for

					That means after every step the monitor checks for all StateEvents.
					If condition() of one StateEvent evaluates to true the point of switching
					from false to true will be approximated and at this point action of the StateEvent
					will be executed. The StateEvent will be removed from the monitor afterwards

					\note If condition() evaluates to true while added, then action() is immediately called.
				 */
				void addStateEvent(StateEvent *stateEvent);

				/** \brief Removes a StateEvent from the monitor
				 */
				void removeStateEvent(StateEvent *stateEvent);
				//@}

				/** \brief Calculates the evolution of the continuous processes

					\note This will be called by Simulation.
				 */
				int main();

				/** \name stop monitor

					These functions are to set the time limit for the main method to work or give a stop command.

					\note The monitor will only integrate, if there is a time limit set or processes to synchronize with or StateEvents to check.
					\note If there are only StateEvents to check be sure that at least one will evaluate at some point to true or you will have an infinite loop.

					@{
				 */
				/** \brief Sets a time limit when the monitor will finish the main method
					\param time
						time to stop monitor
				 */
				void setTimeLimit(SimTime time);

				/** \brief Returns the time limit
					\param time
						the set time limit for this instance, which is only valid if returned true
				 */
				bool getTimeLimit(SimTime* time);

				/** \brief Remove time limit
				 */
				void removeTimeLimit();

				/** \brief Returns the execution time of the next synchronized process
				 */
				SimTime getStepTimeLimit();

				/** \brief Forces the monitor to finish the main method when next time limit is reached
				 */
				void stop();
				//@}

				/** \name internal Time

					To make ODEMx time representation independent of the time representation in the solver
					(numerical framework) the monitor has an internal timing for conversion.
					This means the monitor has an internal reference time and an actual time,
					which is after the reference time.

					@{
				 */
				/** \brief Sets internal reference time
				 	\param time
						the time limit to be set

					\note That means a change for the calculation and triggers a new initialization for the solver.
				 */
				void setInternalTime(SimTime time);

				/** \brief Returns internal reference time
				 */
				SimTime getInternalTime();

				/** \brief Returns actual time of monitor
				 */
				SimTime getActualInternalTime();

				/** \brief Sets internal reference time to actual time

					Sets internal reference time in a way that actual time and internal
					reference time are equal for this monitor.

					\note That means a change for the calculation and triggers a new initialization for the solver
				 */
				void adaptInternalTime();
				//@}

				/** \brief Sets the monitor changed

					If the continuous system has a discrete change, the solver has to be newly initialized
					to guarantee consistency. So it causes a call of solver->init() before next calculation.

					\note As long a user access only objects by supplied functions there is no need to call this.
				 */
				void setChanged();

			protected:

				// the solver used by the monitor
				ODESolver *solver;

				// the working mode in which the monitor runs
				WorkingModes workingMode;

				// the search mode used by the monitor to search for the appearance of state events
				SearchModes searchMode;

				// list of the discrete processes which the monitor synchronizes to
				std::list<Process*> waitForProcesses;

				// list of the continuous processes managed by the monitor
				std::list<Continuous*> continuousList;

				// list of the StateEvents the monitor watches for
				std::list<StateEvent*> stateEvents;

				// actual size of the variable structs
				int variablesArraySize;

				// actual number of variables used
				int variablesCount;

				// variable container used by monitor for managing variables
				VariableContainer *variables;

				// the internal reference time value
				SimTime internalTime;

				// the time limit if set
				SimTime timeLimit;

				// indicates if a time limit is set
				bool timeLimitSet;

				// indicates if stop() was called
				bool hasToStop;

				// actual time of monitor is internalTime + diffTime
				double diffTime;

				// indicates if the system has changed since the calculation of the last step
				bool changed;

				/** \brief Integration to \p time
				 	\param time
						the time limit to which will be integrated

					\note internal function
				 */
				void integrateUntil(SimTime time);

				/** \brief Integration till state event occurs

					\note internal function
				 */
				void integrate();

				/** \brief Searches the time when a state event occurs.
				 	When a known condition is false at internalTime + old_diffTime with values old_variable_values (for all
				 	state events) and in the actual state there is a state event of the monitor with condition true,
				 	then the monitor triggers this event (calls the method action()).
				 	\param old_diffTime
						initial left bound of the search interval
					\param old_variable_values
						initial values for the search
				 */
				void searchStateEvent(double old_diffTime, double *old_variable_values);

				/** \brief Evaluates all equations of all continuous processes as one function.
				 	It will be evaluated as sum of partial functions at time internalTime + time.
				 	\param time
						the time limit
				 */
				void evalF(double time);

				/** \brief Evaluates all jacobian of all continuous processes as one matrix.
				 	It will be evaluated as sum of partial matrices at time internalTime + time.
				 	\param time
						the time limit
				 */
				void evalJacobian(double time);

				friend class Continuous;
				friend class ODESolver;
				friend class MonitorTrace;
			};

			//forward declaration
			class Monitor;

			class Continuous;

			/** \interface MonitorObserver

				\author Sascha Qualitz

				\brief Observer for monitor specific events

				\sa Continuous

				\since 3.0
			*/
			class MonitorObserver:
				public ProcessObserver
			{
			public:
				virtual ~MonitorObserver(){};

				virtual void onCreate( Monitor* sender ){} ///< Construction

				virtual void onAddProcessToWaitList( Monitor* sender, Process* peer ){} ///< Add peer
				virtual void onRemoveProcessFromWaitList( Monitor* sender, Process* peer ){} ///< Remove peer

				virtual void onBeginIntegrate( Monitor* sender ){} ///< Begin integrate
				virtual void onEndIntegrate( Monitor* sender){} ///< End integrate

				virtual void onBeginIntegrateUntil( Monitor* sender ){} ///< Begin integrate
				virtual void onEndIntegrateUntil( Monitor* sender){} ///< End integrate

				virtual void onNewValidState( Monitor* sender ){} ///< New valid state

				virtual void onAddContinuous( Monitor* sender, Continuous* continuous ){} ///< new continuous object added
				virtual void onRemoveContinuous( Monitor* sender, Continuous* continuous ){} ///< continuous object removed

				virtual void onAddStateEvent( Monitor* sender, StateEvent* event ){} ///< new state event added
				virtual void onRemoveStateEvent( Monitor* sender, StateEvent* event ){} ///< state event removed

				virtual void onStop( Monitor* sender){} ///< Monitor stops

				virtual void onSetODESolver( Monitor* sender, ODESolver* solver){} ///< set ODESolver

				virtual void onSetChanged( Monitor* sender){} ///< system changed

				virtual void onSetInternalTime(Monitor* sender, SimTime time){} ///< internal time changed

				virtual void onAdaptInternalTime(Monitor* sender, SimTime time){} ///< adapted internal time
			};
		}
	}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_MONITOR_INCLUDED */

