//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------

/** \file JacobiMatrix.h

 	\author Sascha Qualitz

 	\date created at 2010/2/26

 	\brief Declaration of class odemx::base::continuous::JacobiMatrix

 	\sa JacobiMatrix.cpp

    \since 3.0
 */

#ifndef ODEMX_JACOBIMATRIX_INCLUDED
#define ODEMX_JACOBIMATRIX_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

namespace odemx {
	namespace base {
		namespace continuous {

			//Forward declaration
			class Continuous;

			class ODEObject;
			/** \class JacobiMatrix

				\ingroup base

				\author Sascha Qualitz

				\brief Object for handling the jacobi-matrix in the class ODEObject.

				To describe the jacobi-matrix of ordinary differential equations the user has to use this class.
				Used in the jacobi function in a derived class of ODEObject.

				\sa Rate
				\sa State
				\sa DfDt
			 */

			class JacobiMatrix {
			public:
				/**
					\brief Construction

					\note The variables continuous, otherContinuous, row_ and column_ will be set to 0.
				*/
				JacobiMatrix();

				/**
					\brief Construction
					\param continuous
						pointer to the continuous object where the jacobi-matrix belongs to

					\note The variables row_, column_ and otherContinuous will be set to 0.
				*/
				JacobiMatrix(Continuous* continuous);

				/**
					\brief Construction
					\param continuous
						pointer to the continuous object where the jacobi-matrix belongs to
					\param otherContinuous
						pointer to a continuous instance which influences the right hand side of the equation

					\note The variables row_ and column_ will be set to 0
				*/
				JacobiMatrix(Continuous* continuous, Continuous* otherContinuous);

				/// destruction
				~JacobiMatrix();

				/**
					\brief Overrides the assignment operator to store the value of type double in the variable container.
					\param value
						value to be set
				*/
				JacobiMatrix& operator =(const double value);

				/**
					\brief Overrides the ()-operator to set the values of row_ and column_.
					\param row
						row number of the matrix
					\param column
						column number of the matrix
				*/
				JacobiMatrix& operator ()(const unsigned row, const unsigned column);

				/**
					\brief Sets the continuous variable
					\param continuous
						pointer to the continuous object where the jacobi-matrix belongs to
				*/
				void setContinuous(Continuous* continuous);

				/**
					\brief Sets the variables continuous and otherContinuous
					\param continuous
						pointer to the continuous object where the jacobi-matrix belongs to
					\param otherContinuous
						pointer to a continuous instance which influences the right hand side of the equation
				*/
				void setContinuous(Continuous* continuous, Continuous* otherContinuous);

			private:
				/**
					\brief Returns value of the jacobi-matrix, at the given indexes row_ and column_

					getValue returns the correct value, because row_ and column_ was set by the overwritten ()-operator before.
				*/
				double getValue() const;

				/**
					\brief Sets the value of the jacobi-matrix at the indexes row_ and column_
					\param value
						value to be set
				*/
				void setValue(double value);

				// pointer to the continuous object where the jacobi-matrix belongs to
				Continuous* continuous;

				// pointer to a continuous instance which influences the right hand side of the equation
				Continuous* otherContinuous;

				// row number of the matrix
				unsigned row_;

				// column number of the matrix
				unsigned column_;
			};
		}
	}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif/* ODEMX_JACOBIMATRIX_INCLUDED */
