//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file ODESolver.h

 \author Michael Fiedler

 \date created at 2008/11/21

 \brief Declaration of ODESolver

 \since 3.0
 */

#ifndef ODEMX_ODESOLVER_INCLUDED
#define ODEMX_ODESOLVER_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/VariableContainer.h>
#include <odemx/data/Producer.h>
#include <odemx/data/Observable.h>

namespace odemx {
	namespace base {
		namespace continuous {

			/** Choice which error is checked while integration
			 */
			enum ErrorType {
				absolute, relative
			};

			class Monitor;

			class VariableContainer;

			class ODESolverObserver;

			/** \class ODESolver

				\ingroup base

				\brief Abstract class for implementing a solver

				This class specifies the interface of a solver as seen by the monitor.

				\note ODESolver supports Observation

			    \note ODESolver supports Trace

			    \note ODESolver supports Logging

				\sa Monitor, GSLSolver

				\since 3.0
			 */
			class ODESolver : public data::Producer, public data::Observable<ODESolverObserver> {
			public:
				/** \brief Construction
					\param sim
						reference to one simulation-object
				 */
				ODESolver(Simulation& sim);
				/// destruction
				virtual ~ODESolver();

				/** \brief Returns a variable container suitable for this Solver
					\param size
						initial size of arrays in the variable container
				 */
				virtual VariableContainer* getVariableContainer(int size) = 0;

				/** \brief Makes one integration step for monitor
					\param time
						internal time of assigned monitor.

					\note \p time is the time from which to do the step.
				 */
				virtual void makeStep(double time) = 0;

				/** \brief Initialize the solver

					\note This is also called if the monitor has changed.
				 */
				virtual void init() = 0;

				/** \name Monitor handling

					@{
				 */
				/** \brief Sets the assigned Monitor
				 	\param monitor
						pointer to the monitor which manages this instance
				 */
				void setMonitor(Monitor* monitor);

				/** \brief Returns the assigned Monitor
				 */
				Monitor* getMonitor();
				//@}

				/** \name Solving parameters

					@{
				 */
				/** \brief Sets the error limit for integration
				 */
				void setErrorLimit(double maxError);

				/** \brief Returns the error limit for integration
				 */
				double getErrorLimit();

				/** \brief Sets the type of error which will be checked
					\param type
						is 'absolute' or 'relative'
				 */
				void setErrorType(ErrorType type);

				/** \brief Sets the interval for the step length of one integration step
					\param min
						lower bound of step length
					\param max
						upper bound of step length
				 */
				void setStepLength(double min, double max);

				/** \brief gives the size of the last step done
				 */
				double getStepLength();
				//@}

			protected:
				// pointer to the Monitor which holds the Continuous objects which will be integrated
				Monitor* monitor;

				/** \brief Calls all derivates functions of all ODEObject objects assigned to Continuous objects which are managed by the monitor

					\note This function evaluates all equations of all continuous processes as one function
					   	  as sum of partial functions at time internalTime + time.
					\note This function is passed to the GSL Library for solving the ordinary differential equations.
				 */
				void evalF(double time);

				/** \brief Calls all jacbian functions of all ODEObject objects assigned to Continuous objects which are managed by the monitor

					\note This function evaluates all jacobian functions of all continuous processes as one matrix
						  as sum of partial matrices at time internalTime + time. Also it evaluates the function for differentiating
						  the i-th component of f by time
					\note This function is passed to the GSL Library for solving the ordinary differential equations, if a solving algorithm for
						  stiff equations is used.
				 */
				void evalJacobian(double time);

				/** \brief Returns the variables container of the assigned Monitor
				 */
				VariableContainer* getVariableContainerOfMonitor();

				// error type which will be checked after each step to optimize step length
				ErrorType errorType;

				//error limit which will be checked after each step to optimize step length
				double errorLimit;

				// minimum step length allowed by user
				double minStep;

				// maximum step length allowed by user
				double maxStep;

				// step length of the last step
				double lastStep;

				friend class Monitor;
			};

			/** \interface ODESolverObserver

				\author Sascha Qualitz

				\brief Observer for ODESolver specific events

				\sa Continuous

				\since 3.0
			*/
			class ODESolverObserver:
				public ProcessObserver
			{
			public:
				virtual void onCreate( ODESolver* sender ) {} ///< Construction
				virtual void onDestroy( ODESolver* sender ) {} ///< Destruction

				virtual void onSetMonitor( ODESolver* sender, Monitor* monitor ) {} ///< new Monitor set

				virtual void onGetVariableContainer() {} ///< create new VariableContainer and return a pointer to monitor
			};

		}
	}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_ODESOLVER_INCLUDED */

