//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file DfDt.h

 \author Sascha Qualitz

 \date created at 2009/10/26

 \brief Declaration of class DfDt

 \since 3.0
 */

#ifndef ODEMX_DFDT_INCLUDED
#define ODEMX_DFDT_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

namespace odemx {
	namespace base {
		namespace continuous {

			//Forward declaration
			class Continuous;

			class ODEObject;

			/** \class DfDt

				\ingroup base

				\author Sascha Qualitz

				\brief Object for handling a derived by time equation-element in the class ODEObject
					   Used in the jacobi function in a derived class of ODEObject.

				To describe ordinary derived by time equations the user has to use this class.

				\sa JacobiMatrix
				\sa State
				\sa Rate
			 */
			class DfDt {
			public:
				/**
					\brief Construction
					\param continuous
						pointer to the continuous object where the dfdt-element belongs to

					\note The variables continuous and index_ will be set to 0
				*/
				DfDt();

				/**
					\brief Construction
					\param continuous
						pointer to the continuous object where the dfdt-element belongs to

					\note The variable index_ will be set to 0
				*/
				DfDt(Continuous* continuous);

				/// destruction
				virtual ~DfDt();

				/**
					\brief Overrides the converting operator double to convert implicit an object of type DfDt to double

					\note If this operator is called, the double value is the value that is returned by the method
					getDfDt(index_) of the corresponding continuous-object. The variable index_ is set to an index
					between 0 and continuous->getDimension before. To set the variables index_ see also the methods
					operator [].
				*/
				operator double() {
					return this->getValue();
				}

				/**
					\brief Overrides the assignment operator to store the value of type double in the variable container
					\param value
						value to be set
				*/
				DfDt& operator =(const double value);

				/**
					\brief Overrides the index operator to set the internal variable index_
					\param index
						index of the variable to get/set (range 0..(Dimension-1))
				*/
				DfDt& operator [](const unsigned i);

				/**
					\brief Sets the variable continuous
					\param continuous
						pointer to the continuous object where the equation-element belongs to
				*/
				void setContinuous(Continuous* continuous);

			private:

				/**
					\brief Returns the value of the dfdt-element with given index index_

					getValue returns the value from the correct index position, because index_ was set by the overwritten operator[] before.
				*/
				double getValue() const;

				/**
					\brief Sets the value of type double at the index index_
					\param value
						value to be set

					\note setValue stores the value at the correct index, because index_ was set by the overwritten operator[] before.
				*/
				void setValue(double value);

				// pointer to the continuous object where the dfdt-element belongs to
				Continuous* continuous;

				// index of the variable to get/set (range 0..(Dimension-1))
				unsigned index_;
			};
		}
	}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_DFDT_INCLUDED */
