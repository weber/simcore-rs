//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file StateEvent.h

 \author Michael Fiedler

 \date created at 2008/11/21

 \brief Declaration of StateEvent

 \sa Monitor

 \since 3.0
 */

#ifndef ODEMX_STATEEVENT_INCLUDED
#define ODEMX_STATEEVENT_INCLUDED

#include <odemx/setup.h>

// usage of this class requires SimTime type double,
// which can be switched at compile time by defining
// ODEMX_USE_CONTINUOUS in file odemx/setup.h
#ifdef ODEMX_USE_CONTINUOUS

#include <odemx/base/continuous/ODESolver.h>
#include <odemx/data/Producer.h>
#include <odemx/data/Observable.h>

namespace odemx {
	namespace base {
		namespace continuous {

			//forward declaration
			class StateEventObserver;

			/** \class StateEvent

				\ingroup base

				\author Michael Fiedler

				\brief Check for conditions while integration and specify action

				The class StateEvent is for checking if continuous processes arrives a special state
				specified by condition and than triggers an event specified by action

				\since 3.0
			 */
			class StateEvent {
			public:
				/// destruction
				virtual ~StateEvent();

				/** \name Handle assigned Monitor

					@{
				 */
				/** \brief Sets the Monitor where the StateEvent will be checked
					\param monitor
						pointer to the Monitor where it will be checked

					\note If this instance is already assigned to a Monitor it will automatically be transferred.
					\note If \p monitor is NULL the StateEvent will be removed from an assigned Monitor.
				 */
				void setMonitor(Monitor *monitor);

				/** \brief Returns the Monitor where this StateEvent belongs to
				 */
				Monitor* getMonitor();
				//@}

				/** \brief Condition which will be checked

					This is to be implemented by the user. When condition returns true the action will be triggered by the monitor.

					\note If condition is true when added to the Monitor, then the action will be triggered immediately.
				 */
				virtual bool condition(SimTime time) = 0;

				/** \brief Action which will be executed when condition occurs

					This is to be implemented by the user. When condition returns true this action will be triggered by the monitor.

					\note If condition is true when added to the Monitor, then action will be triggered immediately.
				 */
				virtual void action() = 0;

			private:
				// Monitor where the StateEvent will be checked
				Monitor *monitor;

				friend class Monitor;
			};
		}
	}
}

#endif /* ODEMX_USE_CONTINUOUS */

#endif /* ODEMX_STATEEVENT_INCLUDED */
