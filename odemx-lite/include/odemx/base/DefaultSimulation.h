//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file DefaultSimulation.h
 * @author Ronald Kluth
 * @date created at 2009/02/07
 * @brief Declaration of class DefaultSimulation and getDefaultSimulation
 * @sa DefaultSimulation.cpp, Simulation.h, Simulation.cpp
 * @since 3.0
 */

#ifndef ODEMX_DEFAULTSIMULATION_INCLUDED
#define ODEMX_DEFAULTSIMULATION_INCLUDED

#include <odemx/base/Simulation.h>

namespace odemx {

/**
	@brief Get the DefaultSimulation
	@return	Reference to the DefaultSimulation object

	The first time getDefaultSimulation() is called, an
	object of type DefaultSimulation is created and returned.
	All following calls to getDefaultSimulation() also return
	this object.

	@note The reference returned by this function refers to a static object.
	DefaultSimulation access is basically implemented like a Meyers singleton.

	@see DefaultSimulation
*/
extern base::Simulation& getDefaultSimulation();

namespace base {

/** \class DefaultSimulation

	\author Ralf Gerstenberger

	\brief A default implementation of Simulation provided for convenience.

	The %DefaultSimulation is provided for convenience. You can
	use this simulation if you don't want to define your own. Use
	getDefaultSimulation() to get a pointer to a %DefaultSimulation
	object. Instead of \p init(), the \p main function of your simulation
	program will be required to initialize all necessary objects for
	the start of the simulation.

	\since 1.0
*/
class DefaultSimulation
:	public Simulation
{
private:

	friend Simulation& odemx::getDefaultSimulation();

	/**
		\brief Construction

		Use odemx::getDefaultSimulation() to get an object of %DefaultSimulation.

		\sa odemx::getDefaultSimulation()
	*/
	DefaultSimulation();

	/// private Destructor to prevent delete calls
	~DefaultSimulation();

protected:
	virtual void initSimulation();
};

} } // namespace odemx::base

#endif /*ODEMX_DEFAULTSIMULATION_INCLUDED*/
