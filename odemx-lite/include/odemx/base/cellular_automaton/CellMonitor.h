//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file CellMonitor.h

 \author Sascha Qualitz

 \date created at 2010/01/05

 \brief Declaration of CellMonitor

 \since 3.0
 */

#ifndef ODEMX_CELLMONITOR_INCLUDED
#define ODEMX_CELLMONITOR_INCLUDED

#include <list>
#include <odemx/base/Process.h>
#include <odemx/base/cellular_automaton/Transition.h>

// Use these macros to define a neighborhood type.
#define MOORE 0
#define NEUMANN 1
#define MOORETORUS 2
#define NEUMANNTORUS 3
#define MOOREXTUBE 4
#define NEUMANNXTUBE 5
#define MOOREYTUBE 6
#define NEUMANNYTUBE 7
#define ALTERNATIVENEIGHBORHOOD 8

#define ROWSPAN(FROM, TO) CellIndexSpan(FROM, TO)
#define COLUMNSPAN(FROM, TO) CellIndexSpan(FROM, TO)
#define LEVELSPAN(FROM, TO) CellIndexSpan(FROM, TO)

// Use this macro to set one transition object to the cells given by the from and to parameters of the macro.
#define _setTransitionToCells(rowIndexSpan, columnIndexSpan, levelIndexSpan, monitorName, transitionType) { \
	if(rowIndexSpan.getIndexTo() > monitorName->getRowCount()-1) {\
		throw std::out_of_range("The right side of the row index span is greater than the number of rows");\
	}\
	if(rowIndexSpan.getIndexFrom() < 0) {\
		throw std::out_of_range("The left side of the row index span is less than 0");\
	}\
	if(columnIndexSpan.getIndexTo() > monitorName->getColumnCount()-1) {\
		throw std::out_of_range("The right side the column index span is greater than the number of columns");\
	}\
	if(columnIndexSpan.getIndexFrom() < 0) {\
		throw std::out_of_range("The left side the column index span is less than 0");\
	}\
	if(levelIndexSpan.getIndexTo() > monitorName->getLevelCount()-1) {\
		throw std::out_of_range("The right side the level index span is greater than the number of levels");\
	}\
	if(levelIndexSpan.getIndexFrom() < 0) {\
		throw std::out_of_range("The left side the level index span is less than 0");\
	}\
	for (int i = rowIndexSpan.getIndexFrom(); i <= rowIndexSpan.getIndexTo(); i++) { \
		for (int j = columnIndexSpan.getIndexFrom(); j <= columnIndexSpan.getIndexTo(); j++) { \
			for (int k = levelIndexSpan.getIndexFrom(); k <= levelIndexSpan.getIndexTo(); k++) { \
				monitorName->getCell(monitorName->getLevelCount() * (monitorName->getColumnCount() * i + j) + k)->setTransition(new transitionType());\
			} \
		} \
	} \
}

namespace odemx {
	namespace base {
		namespace cellular {
		/** \class CellPosition

			\ingroup base

			\author Sascha Qualitz

			\brief Object for handling the index span of row, column and level span in the cellular automaton.

			\sa CellMonitor
		 */
		class CellIndexSpan {
		public:
			/** \brief Construction
				\param indexFrom
					start-index of the span
				\param indexTo
					end-index of the span
			 */
			CellIndexSpan(unsigned indexFrom, unsigned indexTo)
			: indexFrom(indexFrom)
			, indexTo(indexTo)
			{}

			/** \brief Returns the first value of the index span.
			 */
			unsigned getIndexFrom() {
				return this->indexFrom;
			}
			/** \brief Returns the second value of the index span.
			 */
			unsigned getIndexTo() {
				return this->indexTo;
			}

		private:
			/* row of the cell
			 */
			unsigned indexFrom;

			/* column of the cell
			 */
			unsigned indexTo;
		};

		//forward declaration
		class CellVariablesContainer;

		class Cell;

		class Transition;

		class CellMonitorObserver;

		/** \class CellMonitor

			\ingroup base

			\author Sascha Qualitz

			\note %CellMonitor supports Observation.
			\note %CellMonitor supports Trace.
			\note %CellMonitor supports Logging.

			\brief A CellMonitor is used to generate a cellular automaton and manage it during a simulation.

			%One have to use the getter-methods to get one cell object to make changes on it.

			\sa CellularAutomaton, CellVariablesContainer, Cell, Transition, Process

			\since 3.0
		 */
		class CellMonitor : public Process, public data::Observable<CellMonitorObserver> {
		public:
			/** \brief Construction
				\param sim
					reference to one simulation-object
				\param l
					label of this object
				\param variablesInCount
					number of the input variables of the cells
				\param variablesOutCount
					number of the output variables of the cells
				\param cellRows
					number of rows of the grid
				\param cellColumns
					number of columns of the grid
				\param cellLevel
					number of levels of the grid
				\param timestep
					size of the time step
				\param cellDimension
					number of the state variables of the cells
				\param o
					initial observer
			 */
			CellMonitor(Simulation& sim, const data::Label& l, unsigned variablesInCount, unsigned variablesOutCount, unsigned cellRows, unsigned cellColumns, unsigned cellLevel, double timestep, unsigned cellDimension, ProcessObserver* o = 0);

			/** \brief Construction used with configuration file
				\param sim
					reference to one simulation-object
				\param l
					label of this object
				\param fileName
					name of the configuration file which contains settings like grid size of the cellular automaton
				\param o
					initial observer
			 */
			CellMonitor(Simulation& sim, const data::Label& l, const char*const configFileName, ProcessObserver* o = 0);

			/** \brief Construction used with configuration file
				\param sim
					reference to one simulation-object
				\param l
					label of this object
				\param configFileName
					name of the configuration file which contains settings like grid size of the cellular automaton
				\param startDataFileName
					name of the configuration file which contains settings like the start values or individual neighborhood settings of all cells
				\param o
					initial observer
			 */
			CellMonitor(Simulation& sim, const data::Label& l, const char*const configFileName, const char*const startDataFileName, ProcessObserver* o = 0);

			/// destruction
			~CellMonitor();

			/** \brief Calculating the evolution of the cellular automaton.
				\note This will be called by the Simulation-object.
			 */
			int main();

			/** \brief Sets the time limit when the monitor has to finish the main method.
				\param time
					time to stop monitor
			 */
			void setTimeLimit(SimTime time);

			/** \brief Returns the time limit.
			 */
			SimTime getTimeLimit();

			/** \brief Sets one transition object to all Cell-objects.
			 */
			void setTransitionToCells(Transition* transition);

			/** \brief Function for printing values.
			 */
			double* getValues();

			/** \brief Returns the number of rows that belong to the grid of the cellular automaton.
			*/
			unsigned getRowCount();

			/** \brief Returns the number of columns that belong to the grid of the cellular automaton.
			*/
			unsigned getColumnCount();

			/** \brief Returns the number of levels that belong to the grid of the cellular automaton.
			*/
			unsigned getLevelCount();

			/** \brief Adds one cell with given coordinates (row, column, level) to the boundary.
				\param row
					row that belongs to the cell
				\param column
					column that belongs to the cell
				\param level
					level that belongs to the cell
			 */
			void setIsBoundaryCell(unsigned row, unsigned column, unsigned level);

			/** \brief Sets the state value of one cell with given coordinates (row, column, level).
				\param row
					row that belongs to the cell
				\param column
					column that belongs to the cell
				\param level
					level that belongs to the cell
				\param variableIndex
					index of the variable with a range of 0 to dimension-1
				\param value
					the value to be set
			 */
			void setCellValue(unsigned row, unsigned column, unsigned level, unsigned variableIndex, double value);

			/** \brief Returns the output value of one cell with given coordinates (row, column, level) and given variable index.
				\param row
					row that belongs to the cell
				\param column
					column that belongs to the cell
				\param level
					level that belongs to the cell
				\param variableIndex
					index of the variable with a range of 0 to dimension-1
			 */
			double getCellValue(unsigned row, unsigned column, unsigned level, unsigned variableIndex);
			
			/** \brief Returns the global neighborhood type.
			 	\note If different cells have different neighborhood types don't use this function.
			 */
			unsigned getNeighborhoodType();

			/** \brief Returns the radius of the neighborhood.
			 	\note If different cells have different neighborhood radiuses don't use this function.
			 */
			unsigned getNeighborhoodRadius();

			/** \brief Returns a pointer to one cell-object with given coordinates (row, column, level).
				\param row
					row that belongs to the cell
				\param column
					column that belongs to the cell
				\param level
					level that belongs to the cell
			 */
			Cell* getCell(unsigned row, unsigned column = 0, unsigned level = 0);

		private:
			/** \brief Returns a pointer to one cell with given index in the cell vector.
				\param cellIndex
					index of one cell in the cell vector (cellIndex = cellRows * (cellColumns * level + column) + row)

				\note This method is for internal use only.
			 */
			Cell* getCell_(unsigned cellIndex);

			/** \brief Sets the settings for the neighborhood.
				\param neighborhoodType
					type of the neighborhood
				\param radius
					radius of the neighborhood (1,....)

				\note For possible values of neighborhoodType take a look in CellMonitor.h
					  The default values are neighborhoodType = MOORE and radius = 1.
			 */
			void setNeighborhoodSettings(unsigned neighborhoodType, unsigned radius);

			/** \brief Sets parameters used by CellMonitor to create the cellular automaton with given file name.
				\param fileName
					name or rather path of the configuration file
			 */
			void setParameterFromConfigFile(const std::string fileName = "cellmonitor_config.xml");

			/** \brief Sets parameters of the cellular automaton like start values and neighborhood settings with given file name.
				\param fileName
					name or rather path of the configuration file
			 */
			void setStartDataFromConfigFile(std::string fileName = "ca_start_data.xml");

			/** \brief Creates a cellular automaton with the given numbers of rows, columns and levels.
			 	\param rows
					number of rows that belong to the grid of the cellular automaton
				\param columns
					number of columns that belong to the grid of the cellular automaton
				\param level
					number of levels that belong to the grid of the cellular automaton

				TODO cellDimension entfernen!!!!
			 */
			void generateCellularAutomaton(unsigned rows, unsigned  columns, unsigned level, unsigned cellDimension);

			/** \brief Calculates the neighborhood of all cells.
			 */
			void calculateNeighboorhood();

		private:
			/** \brief This structure saves the settings for the global neighborhood.
				\param
					radius of the neighborhood (1,....)
				\param neighborhoodType
			   		type of the neighborhood

			   	\note For possible values of neighborhoodType take a look in CellMonitor.h
					  The default values are neighborhoodType = MOORE and radius = 1.
			 */
			struct neighborhoodSettings
			{
				unsigned neighborhoodType;
				unsigned radius;
			} typeOfNeighborhood;

			// Number of input, output and state variables which belong to a cell of the cellular automaton, stored in the values arrays.
			unsigned cellDimension;
			unsigned variablesInCount;
			unsigned variablesOutCount;

			// Number of rows, columns and levels that belong to the grid of the cellular automaton.
			unsigned cellRows;
			unsigned cellColumns;
			unsigned cellLevel;

			// Variables container used by CellMonitor for managing variables of the cells.
			CellVariablesContainer* cellVariablesContainer;

			// Vector of all cell objects.
			std::vector<Cell*> cellVector;

			// Size of every time step.
			double timestep;

			// The time limit.
			SimTime timeLimit;

			/** \brief Triggers the observer to write the input, output and state variables.
			 */
			void notifyValidState();

			friend class Cell;

			friend class CellMonitorTrace;
		};

		//forward declaration
		class CellPosition;

		/** \interface CellMonitorObserver

			\author Sascha Qualitz

			\brief Observer for CellMonitor specific events

			\since 3.0
		*/
		class CellMonitorObserver:
			public ProcessObserver
		{
		public:
			// destruction
			virtual ~CellMonitorObserver(){};

//			virtual void onCreate( CellMonitor* sender ){} ///< Construction

			virtual void onNewValidState( CellMonitor* sender ){} ///< New valid state

			virtual void onBeginCalcStateChanges( CellMonitor* sender ){} ///< Monitor calculates the state changes
			virtual void onEndCalcStateChanges( CellMonitor* sender ){} ///< Monitor ended one calculation step

			virtual void onSetTimeLimit(CellMonitor* sender, SimTime time){} ///< time changed

			virtual void onSetTransitionToCells(CellMonitor* sender){} ///< one transition was set to all cells
			virtual void onSetNeighborhoodSettings(CellMonitor* sender, unsigned neighborhoodType, unsigned radius){} ///< neighborhood settings
			virtual void onSetNeighborhoodType(CellMonitor* sender, unsigned neighborhoodType, CellPosition* position){} ///< neighborhood type
			virtual void onSetNeighborhoodRadius(CellMonitor* sender, unsigned neighborhoodRadius, CellPosition* position){} ///< neighborhood radius

			virtual void onGenerateCellularAutomaton(CellMonitor* sender){} ///< generate cellular automaton
			virtual void onCalculateNeighboorhood(CellMonitor* sender){} ///< calculate neighborhood
		};

		/** \class CellMonitorTrace

			\author Sascha Qualitz

			\brief Trace for cell state changes of the cellular automaton controlled by the monitor.

			\sa Cell, CellMonitorObserver, CellMonitor

			%CellMonitorTrace logs the state changes of the cells in the cellular automaton
			into a xml file. The xml file is automatically opened and closed.

			\since 3.0
		*/
		class CellMonitorTrace
		:	public CellMonitorObserver
		{
			public:
				/// Construction
			CellMonitorTrace( CellMonitor* cellMonitor = 0, const std::string& fileName = "" );
				/// Destruction
				virtual ~CellMonitorTrace();

				/// Follow state changes
				virtual void onNewValidState( CellMonitor* sender );

			private:
				// Implementation
				std::ostream* out;
				bool firstTime;
				const std::string& fileName;

				CellMonitor* cellMonitor;

				void openFile( const std::string& fileName );
		};

	}
}
}

#endif

