//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file CellVariablesContainer.h

 \author Sascha Qualitz

 \date created at 2010/01/05

 \brief Declaration of CellVariablesContainer

 \since 3.0
 */

#ifndef ODEMX_CELLVARIABLESCONTAINER_INCLUDED
#define ODEMX_CELLVARIABLESCONTAINER_INCLUDED

namespace odemx {
	namespace base {
		namespace cellular {

		/** \class CellVariablesContainer

			\ingroup base

			\brief Abstract class for implementing memory handling of the cells in a cellular automaton.

			\sa Cell, CellMonitor, Transition

			\since 3.0
		 */
		class CellVariablesContainer {
		private:
			/** \brief Construction
				\param cellRows
					number of rows that belong to the grid of the cellular automaton
				\param cellColumns
					number of columns that belong to the grid of the cellular automaton
				\param cellLevel
					number of levels that belong to the grid of the cellular automaton
				\param variablesInCount
					number of the input variables of the cells
				\param variablesOutCount
					number of the output variables of the cells
				\param cellDimension
					number of the state variables of the cells
			*/
			CellVariablesContainer(unsigned cellRows, unsigned cellColumns, unsigned cellLevel, unsigned variablesInCount, unsigned variablesOutCount, unsigned cellDimension);

			/// destruction
			virtual ~CellVariablesContainer();

			/** \brief Sets output value of one variable.
				\param baseIndex
					base index for variables of cell objects
				\param variableIndex
					index of the variable to be set to the \p value
				\param value
					the value to be set
			 */
			virtual void setOutputValue(unsigned baseIndex, unsigned variableIndex, double value);

			/** \brief Sets state value of one variable.
				\param baseIndex
					base index for variables of cell objects
				\param variableIndex
					index of the variable to be set to the \p value
				\param value
					the value to be set
			 */
			virtual void setStateValue(unsigned baseIndex, unsigned variableIndex, double value);

			/** \brief Sets input value of one variable.
				\param baseIndex
					base index for variables of cell objects
				\param variableIndex
					index of the variable to be set to the \p value
				\param value
					the value to be set
			 */
			virtual void setInputValue(unsigned baseIndex, unsigned variableIndex, double value);

			/** \brief Returns output value of one variable.
				\param baseIndex
					base index for variables of cell objects
				\param variableIndex
					index of the returned variable
			 */
			virtual double getOutputValue(unsigned baseIndex, unsigned variableIndex);

			/** \brief Returns state value of one variable.
				\param baseIndex
					base index for variables of cell objects
				\param variableIndex
					index of the returned variable
			 */
			virtual double getStateValue(unsigned baseIndex, unsigned variablesIndex);

			/** \brief Returns input value of one variable.
				\param baseIndex
					base index for variables of cell objects
				\param variableIndex
					index of the returned variable
			 */
			virtual double getInputValue(unsigned baseIndex, unsigned variableIndex);

			/** \brief Returns the number of rows that belong to the grid of the cellular automaton.
			 */
			unsigned getNumberOfRows();

			/** \brief Returns the number of columns that belong to the grid of the cellular automaton.
			 */
			unsigned getNumberOfColumns();

			/** \brief Returns the number of level that belong to the grid of the cellular automaton.
			 */
			unsigned getNumberOfLevels();

			/**
			 * \brief Function for printing values.
			 */
			double* getOutputValues();

		private:
			// Number of rows, columns and levels that belong to the grid of the cellular automaton.
			unsigned cellRows;
			unsigned cellColumns;
			unsigned cellLevel;

			// Number of input, output and state variables which belong to a cell of the cellular automaton, stored in the values arrays.
			unsigned variablesInCount;
			unsigned variablesOutCount;
			unsigned cellDimension;

			// Arrays which stores the input, output and state variables which belong to a cell of the cellular automaton.
			double* input_values;
			double* state_values;
			double* output_values;

			friend class CellMonitor;

			friend class Cell;
		};

	}
}
}
#endif
