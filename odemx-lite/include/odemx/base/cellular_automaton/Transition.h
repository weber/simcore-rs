//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Transition.h

 \author Sascha Qualitz

 \date created at 2010/01/05

 \brief Abstract class to derive a representation of one transition of a cell in the cellular automaton.
 \sa Cell, CellMonitor
 \since 3.0
 */

#ifndef ODEMX_TRANSITION_INCLUDED
#define ODEMX_TRANSITION_INCLUDED

#include <string>
#include <exception>
#include <map>
#include <odemx/base/SimTime.h>
#include <odemx/base/TypeDefs.h>
#include <odemx/base/cellular_automaton/Cell.h>

using namespace odemx::base;

namespace odemx {
	namespace base {
		namespace cellular {

		//forward declaration
		class CellMonitor;

		class Cell;

		struct typeOfNeighborhood;

		/** \class NotAssignedException

			\ingroup base

			\author Sascha Qualitz

			\brief Exception for methods which fails, because there is missingObject assigned for this object

			\since 3.0
		 */
		class NotAssignedException : public std::exception {
		public:
			/// Constructor
			NotAssignedException(const char* missingObject, const char* object);

			/// Destructor
			~NotAssignedException() throw();

			/// give message for exception
			const char* what() const throw();

		private:
			std::string msg;	//saves the Exception message
		};

			/** \class Transition

				\ingroup base

				\author Sascha Qualitz

				\brief Object for handling a transition function

				%Transition is a base class for all user defined transition functions.
				It provides the functionality to compute state changes for one cell.
				In addition one can define an output function here. It configures which
				state variables other cells can use to compute their state changes.

				\sa CellMonitor, Cell

				\since 3.0
			 */
			class Transition
			{
			public:
				/** \brief Construction
				 */
				Transition();

				/// destruction
				virtual ~Transition();
			protected:
				/** \brief Calculates the transition function based on the own values and influences of other cells.
					\param time
						time
				 */
				virtual void transitionFunction(SimTime time) = 0;

				/** \brief Configures which state variables other cells can use to compute their state changes.
				  	\note Optional one can rewrite this method. Default of this method is copying the state values to the output array.
				 */
				virtual void outputFunction();

				/** \brief Calculates an user-defined neighborhood.
				 	\param radius
						radius of the neighborhood
				 */
				virtual std::map<CellPosition, Cell*, ComparePositions> calculateAlternativeNeighborhood(unsigned radius = 1);

				/** \brief Sets the cell-object.
					\param cell
						the cell to set
				 */
				void setCell(Cell* cell);

				/** \brief Returns state variable of the cell belonging to this transition object.
					\param variableIndex
						index of the variable to get \p value
				 */
				double getValue(unsigned variableIndex = 0);

				/** \brief Sets value of one variable of the cell belonging to the transition object
					\param variableIndex
						index of the variable to be set to the \p value
					\param value
						the value to be set
				 */
				void setValue(unsigned variableIndex, double value);

				/** \brief Returns a cell-object-pointer if the cell with given position is in the neighborhood. Otherwise it returns 0.
					\param row
							row-position of the cell
					\param column
							column-position of the cell
					\param level
							level-position of the cell
				 */
				Cell* searchNeighbor(unsigned row, unsigned column = 0, unsigned level = 0);

			protected:

				// Cell-object, where the transition function belongs to.
				Cell* cell;

				friend class Cell;

				friend class CellMonitor;
			};
		}
	}
}
#endif
