//----------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//----------------------------------------------------------------------------
/**	\file Cell.h

 \author Sascha Qualitz

 \date created at 2010/01/05

 \brief Declaration of Cell
 \sa CellContainer, CellMonitor, Transition
 \since 3.0
 */

#ifndef ODEMX_CELL_INCLUDED
#define ODEMX_CELL_INCLUDED

#include <vector>
#include <list>
#include <map>
#include <string>

namespace odemx {
	namespace base {
		namespace cellular {

		class Cell;

		class ComparePositions;

	/** \class CellPosition

		\ingroup base

		\author Sascha Qualitz

		\brief Object for handling the position of one cell in the cellular automaton.

		\sa Cell
	 */
	class CellPosition {
	public:
		/** \brief Construction
			\param row
				row-position of the cell
			\param column
				column-position of the cell
			\param level
				level-position of the cell
			\param cellRows
				number of rows that belong to the grid of the cellular automaton
			\param cellColumns
				number of columns that belong to the grid of the cellular automaton
			\param cellLevel
				number of levels that belong to the grid of the cellular automaton
		 */
		CellPosition(unsigned row, unsigned column, unsigned level, unsigned cellRows, unsigned cellColumns, unsigned cellLevel)
		: row(row)
		, column(column)
		, level(level)
		, cellRows(cellRows)
		, cellColumns(cellColumns)
		, cellLevel(cellLevel)
		{}

		/** \brief Construction
		 */
		CellPosition()
		: row(0)
		, column(0)
		, level(0)
		, cellRows(0)
		, cellColumns(0)
		, cellLevel(0)
		{}

		/** \brief Sets the position of the cell.
			\param row
				row that belongs to the cell
			\param column
				column that belongs to the cell
			\param level
				level that belongs to the cell
		 */
		void setPosition(unsigned row, unsigned column = 0, unsigned level = 0) {
			this->row = row;
			this->column = column;
			this->level = level;
		}

		/** \brief Sets the grid size of the cellular automaton.
			\param cellRows
				number of rows that belong to the grid of the cellular automaton
			\param cellColumns
				number of columns that belong to the grid of the cellular automaton
			\param cellLevel
				number of levels that belong to the grid of the cellular automaton
		 */
		void setGridSize(unsigned cellRows, unsigned cellColumns, unsigned cellLevel) {
			this->cellRows = cellRows;
			this->cellColumns = cellColumns;
			this->cellLevel = cellLevel;
		}

		/** \brief Returns the row number that belongs to the cell.
		 */
		unsigned getRowNumber() {
			return this->row;
		}
		/** \brief Returns the column number that belongs to the cell.
		 */
		unsigned getColumnNumber() {
			return this->column;
		}
		/** \brief Returns the level number that belongs to the cell.
		 */
		unsigned getLevelNumber() {
			return this->level;
		}
		/** \brief Returns the number of rows that belong to the grid of the cellular automaton.
		 */
		unsigned getNumberOfCellRows() {
			return cellRows;
		}
		/** \brief Returns number of columns that belong to the grid of the cellular automaton.
		 */
		unsigned getNumberOfCellColumns() {
			return cellColumns;
		}
		/** \brief Returns number of levels that belong to the grid of the cellular automaton.
		 */
		unsigned getNumberOfCellLevels() {
			return cellLevel;
		}
	private:
		/* row of the cell
		 */
		unsigned row;

		/* column of the cell
		 */
		unsigned column;

		/* level of the cell
		 */
		unsigned level;

		/* number of rows of the grid
		 */
		unsigned cellRows;

		/* number of columns of the grid
		 */
		unsigned cellColumns;

		/* number of levels of the grid
		 */
		unsigned cellLevel;

		friend class Cell;
		friend class ComparePositions;
	};

	/** \class ComparePositions

		\ingroup base

		\author Sascha Qualitz

		\brief Object for handling comparisons between to CellPosition-objects.

		\sa Cell
	 */
	class ComparePositions {
	public:
		/** \brief Overrides the ()-operator to compare two CellPosition-objects.
			\param cellPosition1
				first object to compare
			\param cellPosition2
				second object to compare
		 */
		bool operator()(const CellPosition& cellPosition1, const CellPosition& cellPosition2) {
			return (cellPosition1.cellRows*(cellPosition1.level*cellPosition1.cellColumns + cellPosition1.column) + cellPosition1.row) <
						(cellPosition2.cellRows*(cellPosition2.level*cellPosition2.cellColumns + cellPosition2.column) + cellPosition2.row);
		}
	};

	//forward declaration
	class CellVariablesContainer;
	class CellMonitor;
	class Transition;

	struct typeOfNeighborhood;

		/** \class Cell

			\ingroup base

			\author Sascha Qualitz

			\brief Object for handling one cell in the grid of the cellular automaton.

			\sa CellMonitor, CellVariablesContainer, Transition
		 */
		class Cell {
		public:
			/** \brief Construction
				\param row
					row-position of the cell
				\param column
					column-position of the cell
				\param level
					level-position of the cell
				\param dimension
					number of the state variables of this cell
				\param inputDimension
					number of the input variables of this cell
				\param outputDimension
					number of the output variables of this cell
				\param monitor
					the monitor which manages this cell
			 */
			Cell(unsigned row, unsigned column, unsigned level, unsigned dimension, unsigned inputDimension, unsigned outputDimension, CellMonitor* monitor);

			/// destruction
			virtual ~Cell();

			/** \brief Calculates the cells in the neighborhood of one cell. Different neighborhood types can be used.
			   	\note See CellMonitor.h for possible neighborhood types.
			 */
			void calculateNeighborhood();

			/** \brief Pushes the values for neighbor cells.
			 */
			void pushValue();

			/** \brief Returns the output values from neighbor cells.
			    \param variableIndex
					index of the variables (range 0...(outputDimension-1))
			 */
			std::vector<double> pullValue(unsigned variableIndex);

			/** \brief Sets the value of one state variable.
				\param variableIndex
					index of the variable to be set to the \p value (range 0...(stateDimension-1))
				\param value
					the value to be set
			 */
			void setValue(unsigned variableIndex, double value);

			/** \brief Returns the value of one state variable.
				\param variableIndex
					index of the returned variable (range 0...(stateDimension-1))
			 */
			double getValue(unsigned variableIndex);

			/** \brief Sets the value of one output variable.
				\param variableIndex
					index of the variable to be set to the \p value (range 0...(outputDimension-1))
				\param value
					the value to be set
			 */
			void setOutputValue(unsigned variableIndex, double value);

			/** \brief Returns the value of one output variable.
				\param variableIndex
					index of the returned variable (range 0...(outputDimension-1))
			 */
			double getOutputValue(unsigned variableIndex);

			/** \brief Sets the value of one input variable.
				\param variableIndex
					index of the variable to be set to the \p value (range 0...(inputDimension-1))
				\param value
					the value to be set
			 */
			void setInputValue(unsigned variableIndex, double value);

			/** \brief Returns the value of one input variable.
				\param variableIndex
					index of the returned variable (range 0...(inputDimension-1))
			 */
			double getInputValue(unsigned variableIndex);

			/** \brief Sets the transition object which contains the transition and output function of the cell.
				\param transition
					transition and output function of the cell
			 */
			void setTransition(Transition* transition);

			/** \brief Deletes transition object which contains the transition and output function of the cell.
				\param transition
					transition and output function of the cell
			 */
			void deleteTransition(Transition* transition);

			/** \brief Returns the dimension of the state variables.
			 */
			unsigned getDimension();

			/** \brief Returns the dimension of the input variables.
			 */
			unsigned getInputDimension();

			/** \brief Returns the dimension of the output variables.
			 */
			unsigned getOutputDimension();

			/** \brief Makes the cell part of the boundary.
			 */
			void setIsBoundary();

			/** \brief Is showing if the cell is part of the boundary or not (true means is part of the boundary and false means regular cell).
			 */
			bool isPartOfBoundary();

			/** \brief Sets the position of the cell.
			    \param row
					row that belongs to the cell
				\param column
					column that belongs to the cell
				\param level
					level that belongs to the cell
			 */
			void setPosition(unsigned row, unsigned column = 0, unsigned level = 0);

			/** \brief Returns a CellPosition-object-pointer with the position of the cell.
			 */
			CellPosition* getPosition();

			/** \brief Sets the settings for the neighborhood.
				\param neighborhoodType
					type of the neighborhood
				\param radius
					radius of the neighborhood (1,....)
				\param calcNeighborhood
					if true the neighborhood must be calculated again after setting

				\note For possible values of neighborhoodType take a look in CellMonitor.h
					  The default values are neighborhoodType = MOORE and radius = 1.
			 */
			void setNeighborhoodSettings(unsigned neighborhoodType, unsigned radius, bool calcNeighborhood = true);

			/** \brief Returns a map with pointers to the cells in the neighborhood of one cell characterized by the position (saved in the key of the map).
			*/
			std::map<CellPosition, Cell*, ComparePositions> getNeighbors();

			/** \brief It returns a cell-object-pointer if the cell with given position is in the neighborhood. Otherwise it returns 0.
				\param row
						row-position of the cell
				\param column
						column-position of the cell
				\param level
						level-position of the cell
			 */
			Cell* searchNeighbor(unsigned row, unsigned column = 0, unsigned level = 0);

		private:
			/**
			   \brief Calculates the Moore neighborhood of the cell.
			   		  N_(i,j)={(k,l) \in L | |k-i| <= r and |l-j| <= r }
			   \param radius
			   		radius of the neighborhood
			 */
			void calculateMooreNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the Von-Neumann-neighborhood of the cell.
			   		  N_(i,j)={(k,l) \in L | |k-i| + |l-j| <= r }
			   \param radius
			   		radius of the neighborhood
			 */
			void calculateVonNeumannNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the Moore neighborhood of the cell. The grid of the cellular automaton is a 2-d-grid which forms a torus.
			   	   	  That means along the x-axis and the y-axis the grid has periodic boundaries.
					  N_(i,j)={(k,l) \in L | |k-i| <= r and |l-j| <= r }
			   \param radius
					radius of the neighborhood
			 */
			void calculateMoore2DTorusNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the Moore neighborhood of the cell. The grid of the cellular automaton is a 2-d-grid which forms a tube.
			   	   	  That means along the x-axis the grid has periodic boundaries.
					  N_(i,j)={(k,l) \in L | |k-i| <= r and |l-j| <= r }
			   \param radius
					radius of the neighborhood
			 */
			void calculateMoore2DXAxisTubeNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the Moore neighborhood of the cell. The grid of the cellular automaton is a 2-d-grid which forms a tube.
			   	   	  That means along the y-axis the grid has periodic boundaries.
					  N_(i,j)={(k,l) \in L | |k-i| <= r and |l-j| <= r }
			   \param radius
					radius of the neighborhood
			 */
			void calculateMoore2DYAxisTubeNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the Von-Neumann-neighborhood of the cell. The grid of the cellular automaton is a 2-d-grid which forms a torus.
			   	   	  That means along the x-axis and the y-axis the grid has periodic boundaries.
					  N_(i,j)={(k,l) \in L | |k-i| + |l-j| <= r }
			   \param radius
					radius of the neighborhood
			 */
			void calculateNeumann2DTorusNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the Von-Neumann-neighborhood of the cell. The grid of the cellular automaton is a 2-d-grid which forms a tube.
			   	   	  That means along the x-axis the grid has periodic boundaries.
					  N_(i,j)={(k,l) \in L | |k-i| + |l-j| <= r }
			   \param radius
					radius of the neighborhood
			 */
			void calculateNeumann2DXAxisTubeNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the Von-Neumann-neighborhood of the cell. The grid of the cellular automaton is a 2-d-grid which forms a tube.
			   	   	  That means along the y-axis the grid has periodic boundaries.
					  N_(i,j)={(k,l) \in L | |k-i| + |l-j| <= r }
			   \param radius
					radius of the neighborhood
			 */
			void calculateNeumann2DYAxisTubeNeighborhood(unsigned radius = 1);

			/**
			   \brief Calculates the user-defined-neighborhood of the cell. The user has to define this neighborhood in a class, which is derived from the class Transition.
			   \param radius
					radius of the neighborhood
			 */
			void calculateAlternativeNeighborhood(unsigned radius = 1);

			/** \brief Checks if an index exceeds the corresponding index boundary and throws an index out of bounds exception.
			 	\param index
					the index will be checked using the variable indexBoundary
				\param indexBoundary
					the value of the index boundary
				\param std::string what
					which index is out of boundary
			*/
			void checkIndexBoundary(unsigned index, unsigned indexBoundary, std::string what);

			/** \brief Sets the base index of the state variables of the cell
				\param baseIndex
					 state base index of the cell
			 */
			void setBaseIndex(unsigned baseIndex);

			/** \brief Sets the input base index of the input variables of the cell.
				\param baseIndex
					input base index of the cell
			 */
			void setInputBaseIndex(unsigned baseIndex);

			/** \brief Sets the output base index of the output variables of the cell.
				\param baseIndex
					output base index of the cell
			 */
			void setOutputBaseIndex(unsigned baseIndex);

			/** \brief Returns the base index of the state variables of the cell.
			 */
			unsigned getBaseIndex();

			/** \brief Returns the base index of the input variables of the cell.
			 */
			unsigned getInputBaseIndex();

			/** \brief Returns the base index of the output variables of the cell.
			 */
			unsigned getOutputBaseIndex();

			/** \brief Sets the monitor to whom the Cell-object belongs.
				\param monitor
					monitor of the cellular automaton
			 */
			void setMonitor(CellMonitor* monitor);

			/** \brief Returns the monitor to whom the Cell-object belongs.
			 */
			CellMonitor* getMonitor();

		private:

			// Monitor to whom the cell belongs.
			CellMonitor* monitor;

			// This object saves the indexes of the cell and the grid size of the cellular automaton.
			CellPosition* cellPosition;

		   /** \brief This structure saves the settings for the neighborhood of the cell.
				\param
					radius of the neighborhood (1,....)
				\param neighborhoodType
					type of the neighborhood

				\note For possible values of neighborhoodType take a look in CellMonitor.h
					  The default values are neighborhoodType = MOORE and radius = 1.
			 */
			struct neighborhoodSettings
			{
				unsigned neighborhoodType;
				unsigned radius;
			} typeOfNeighborhood;

			// Base indexes of the input, output and state variables of the cell.
			unsigned int stateBaseIndex;
			unsigned int inputBaseIndex;
			unsigned int outputBaseIndex;

			// Dimensions of the input, output and state variables of the cell.
			unsigned int stateDimension;
			unsigned int inputDimension;
			unsigned int outputDimension;

			// Map of the Cell-object-pointers in the neighborhood.
			std::map<CellPosition, Cell*, ComparePositions> neighborhoodCellsMap;

			// List of all transition functions which belong to this instance.
			std::list<Transition*> transitions;

			/** \brief Transition object to compute state changes and handle alternative neighborhoods.
			*/
			//Transition* transition;TODO!!

			// Is showing if the cell is part of the boundary or not.
			bool isBoundary;

			// Is showing if the cell received values from neighborhood cells.
			bool pushed;

			friend class CellMonitor;
		};
	}
}
}
#endif
