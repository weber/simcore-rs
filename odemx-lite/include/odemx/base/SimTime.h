//------------------------------------------------------------------------------
//	Copyright (C) 2002, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file SimTime.h
 * @author Ralf Gerstenberger
 * @date created at 2002/01/24
 * @brief TypeDef of odemx::base::SimTime
 *
 * The simulation time is an abstraction from real time. It is also referred to
 * as model time.
 *
 * @since 1.0
 */

#ifndef ODEMX_BASE_SIMTIME_INCLUDED
#define ODEMX_BASE_SIMTIME_INCLUDED

#include <odemx/setup.h>

namespace odemx {
namespace base {

#ifdef ODEMX_USE_CONTINUOUS
	typedef double SimTime; ///< Simulation time type
#else
	// changed in odemx-lite, was Poco::UInt64 (this is a platform dependend type.)
	typedef long long SimTime; ///< Simulation time type
#endif

} } // namespace odemx::base

#endif /* ODEMX_BASE_SIMTIME_INCLUDED */
