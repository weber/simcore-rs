//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Scheduler.h
 * @author Ronald Kluth
 * @date created at 2009/03/12
 * @brief Declaration of odemx::base::Scheduler
 * @sa Scheduler.cpp
 * @since 3.0
 */

#include <odemx/base/ExecutionList.h>
#include <odemx/base/SimTime.h>
#include <odemx/data/Producer.h>

#ifndef ODEMX_BASE_SCHEDULER_INCLUDED
#define ODEMX_BASE_SCHEDULER_INCLUDED

namespace odemx {
namespace base {

class Simulation;

/**
 * @brief Internal class used by the simulation context to manage scheduling and execution
 */
class Scheduler
:	public data::Producer
{
public:
	Scheduler( Simulation& sim, const data::Label& label );
	~Scheduler();

	void run();
	void runUntil( SimTime stopTime );
	void step();

	void addSched( Sched* s );
	void insertSched( Sched* s );
	void insertSchedBefore( Sched* s, Sched* partner );
	void insertSchedAfter( Sched* s, Sched* partner );
	void removeSched( Sched* s );

	const ExecutionList& getExecutionList() const;

private:
	Simulation& sim_;
	ExecutionList executionList_;

protected:
	// allow Sched-derived classes access to private members
	friend class Process;
	friend class Event;

	void inSort( Sched* s );
	void executeNextSched();
};

} } // namespace odemx::base

#endif /* ODEMX_BASE_SCHEDULER_INCLUDED */
