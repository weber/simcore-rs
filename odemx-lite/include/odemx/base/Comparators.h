//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Comparators.h
 * @author Ronald Kluth
 * @date created at 2009/02/23
 * @brief Declaration of odemx::base::DefaultCmp and odemx::base::QPriorityCmp
 * @sa Comparators.cpp
 * @since 3.0
 */

#ifndef ODEMX_BASE_COMPARATORS_INCLUDED
#define ODEMX_BASE_COMPARATORS_INCLUDED

namespace odemx {
namespace base {

// forward declarations
class Sched;
class Process;

/** \struct DefaultCmp

	\author Ronald Kluth

	\brief DefaultCmp is used for ordering objects in std::list<>

	\sa ListInSort, QPriorityCmp
*/
struct DefaultCmp
{
	bool operator()( const Sched* s1, const Sched* s2 ) const;
};

/** 
	\var defCmp
	\author Ronald Kluth
	\brief defCmp is a callable object of type DefaultCmp.
 */
extern DefaultCmp defCmp;

/** \struct QPriorityCmp

	\author Ronald Kluth

	\brief QPriorityCmp is used for ordering objects in std::list<>

	\sa ListInSort, DefaultCmp
*/
struct QPriorityCmp
{
	bool operator()( const Process* p1, const Process* p2 ) const;
};


/** 
	\var qPrioCmp
	\author Ronald Kluth
	\brief qPrioCmp is a callable object of type QPriorityCmp.
 */
extern QPriorityCmp qPrioCmp;

} } // namespace odemx::base

#endif /* ODEMX_BASE_COMPARATORS_INCLUDED */
