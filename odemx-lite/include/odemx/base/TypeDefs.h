//------------------------------------------------------------------------------
//	Copyright (C) 2009, 2010 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file TypeDefs.h
 * @author Ronald Kluth
 * @date created at 2009/02/13
 * @brief Declaration of types SchedList, ProcessList, Priority, SimulationId
 * @since 3.0
 */

#ifndef ODEMX_BASE_TYPEDEFS_INCLUDED
#define ODEMX_BASE_TYPEDEFS_INCLUDED

#include <list>
#include <functional>

#include <odemx/base/SimTime.h>

//----------------------------------------------------------forward declarations

namespace odemx {
namespace base {

class Process;
class Sched;

//-----------------------------------------------------------header declarations

/// List type to hold scheduled objects in order
typedef std::list< Sched* > SchedList;

/// List type to store process objects
typedef std::list< Process* > ProcessList;

/// ID type to provide simulation objects with unique IDs
typedef int SimulationId; // changed in odemx-lite from Poco::UUID

/**
 * @brief Numeric type to express scheduling and queueing priority
 *
 * The type Priority is used to set a priority for processes and events, which
 * allows users to influence scheduling or queueing order.
 */
typedef double Priority;


/**
	\brief Generic function type for coding conditions
 	\param subject
		pointer to process upon which the condition is applied.

	This generic function type is used for coding
	conditions. The condition-function should return true
	if the condition is fulfilled.

 	Usally use the this pointer when a Process itself passes some condition in form of lambda expression.
*/
typedef std::function<bool(Process* subject)> Condition;

/**
	\brief Generic function type for coding selections
	\param master
		pointer to master to select
	\param slave
		pointer to slave to select

	This Generic function type is used for coding
	selections. The condition-function should return true
	if the \p partner process fits the selection concerning \p master.
*/
typedef std::function<bool(Process* master, Process* slave)> Selection;

/**
	\brief Generic function type for coding Process weight functions
	\param master
		pointer to master to weight
	\param slave
		pointer to slave to weight

	This generic function type is used for coding
	a weight function to determine the importance of the partner process.
	The weight function should return a higher number for a higher degree
	of importance. This type of function is used in the implementation of
	WaitQ to synchronize processes according to their highest weight in
	relation to each other.
*/
typedef std::function<double(Process* master, Process* slave)> Weight;

/**
	\brief Generic function type for coding Process weight functions
	\param master
		pointer to master to weight
	\param slave
		pointer to slave to weight

	This generic function type is used for coding
	a weight function to determine the importance of the partner process.
	The weight function should return a higher number for a higher degree
	of importance. This type of function is used in the implementation of
	WaitQ to synchronize processes according to their highest weight in
	relation to each other.
*/
typedef std::function<void(odemx::base::SimTime t)> Derivative;

} } // namespace odemx::base

#endif /* ODEMX_BASE_TYPEDEFS_INCLUDED */
