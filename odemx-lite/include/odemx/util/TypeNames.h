//------------------------------------------------------------------------------
//	Copyright (C) 2008 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file TypeNames.h
 * @author Ronald Kluth
 * @date created at 2008/02/12
 * @brief Typedefs to retain backwards-compatibility after changing type names
 * @since 2.1
 */

#ifndef ODEMX_TYPENAMES_INCLUDED
#define ODEMX_TYPENAMES_INCLUDED

#include <odemx/odemx.h>

// for this file to work without inclusion of <odemx/odemx.h>
// the necessary library headers must be included here:

/*
#include <odemx/protocol/ProtocolStack.h>
#include <odemx/random/DiscreteDist.h>
#include <odemx/random/ContinuousDist.h>
#include <odemx/synchronization/CondQ.h>
#include <odemx/synchronization/Memory.h>
#include <odemx/synchronization/WaitQ.h>
#include <odemx/util/Table.h>
*/

namespace odemx
{
	typedef random::DiscreteDist Idist;
	typedef random::DiscreteConst Iconst;
	typedef random::ContinuousDist Rdist;
	typedef random::ContinuousConst Rconst;
	typedef random::NegativeExponential Negexp;
	typedef random::RandomInt Randint;
	typedef synchronization::CondQ Condq;
	typedef synchronization::WaitQ Waitq;
	typedef synchronization::Memory Memo;
}

#endif /* ODEMX_TYPENAMES_INCLUDED */
