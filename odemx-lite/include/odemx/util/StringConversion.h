//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file StringConversion.h
 * @author Ronald Kluth
 * @date created at 2009/03/29
 * @brief Declaration and implementation of string helpers
 * @since 3.0
 */

#ifndef ODEMX_UTIL_STRINGCONVERSION_INCLUDED
#define ODEMX_UTIL_STRINGCONVERSION_INCLUDED

#include <sstream>
#include <string>

namespace odemx {

/**
 * @brief Get a string representation of the given value
 * @tparam T The type of the given value, automatically deduced by the compiler
 * @param value Reference to the given value
 * @return The converted string
 *
 * This function converts any streamable C++-type into a string. It uses an
 * std::ostringstream to do the work. To make this function work with
 * user-defined types, operator<<() must be implemented for the type.
 */
template < typename T >
inline std::string toString( const T& value )
{
	std::ostringstream convert;
	convert << value;
	return convert.str();
}

} // namespace odemx::util

#endif /* ODEMX_UTIL_STRINGCONVERSION_INCLUDED */
