//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file DeletePtr.h
 * @author Ronald Kluth
 * @date created at 2009/03/29
 * @brief Declaration and implementation of functor odemx::DeletePtr
 * @since 3.0
 */

#ifndef ODEMX_UTIL_DELETEPTR_INCLUDED
#define ODEMX_UTIL_DELETEPTR_INCLUDED

namespace odemx {

/**
 * @brief Delete pointers of any type
 * @tparam PointerT type of the objects pointed to
 *
 * This functor can call delete on any kind of pointer. It is useful in
 * for_each loops that delete containers with pointers to owned resources.
 * Each pointer is checked for @c 0 before delete is called on it.
 */
template< typename PointerT >
struct DeletePtr
{
	void operator()( PointerT* p )
	{
		if( p ) delete p;
	}
};

} // namespace odemx

#endif /* ODEMX_UTIL_DELETEPTR_INCLUDED */
