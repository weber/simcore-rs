//------------------------------------------------------------------------------
//	Copyright (C) 2002, 2003, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Version.h
 * @author Ralf Gerstenberger
 * @date created at 2003/06/10
 * @brief Declaration and Implementation of odemx::Version
 * @sa Version.cpp
 * @since 1.0
 */

#ifndef ODEMX_UTIL_VERSION_INCLUDED
#define ODEMX_UTIL_VERSION_INCLUDED

#include <sstream>
#include <string>

namespace odemx {

/**
 * @class Version
 * @ingroup util
 * @author Ralf Gerstenberger
 * @brief ODEMx version information
 * @since 1.0
 */
class Version
{
private:
	/// The major version number
	static const unsigned short major = 3;
	/// The minor version number
	static const unsigned short minor = 1;

public:
	/// Get major version number
	static unsigned short getMajor() { return major; }
	/// Get minor version number
	static unsigned short getMinor() { return minor; }
	/// Get version string
	static const std::string getString()
	{
		static std::string version;

		if( version.empty() )
		{
			std::ostringstream stream;
			stream << getMajor() << '.' << getMinor();
			version = stream.str();
		}
		return version;
	}
};

} // namespace odemx

#endif /* ODEMX_UTIL_VERSION_INCLUDED */
