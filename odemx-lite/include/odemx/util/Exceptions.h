//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Exceptions.h
 * @author Ronald Kluth
 * @date created at 2009/03/29
 * @brief Declaration and implementation of ODEMx exceptions
 * @since 3.0
 */

#ifndef ODEMX_EXCEPTIONS_INCLUDED
#define ODEMX_EXCEPTIONS_INCLUDED

#include <stdexcept>
#include <string>

namespace odemx {

/** @brief Exception type for the scheduler
 * @ingroup util
 */
class SchedulingException
:	public std::runtime_error
{
public:
	SchedulingException( const std::string& description );
};

/** @brief Exception type for reporting queueing problems
 * @ingroup util
 */
class QueueingException
:	public std::runtime_error
{
public:
	QueueingException( const std::string& description );
};

/** @brief Exception type for data module problems
 * @ingroup util
 */
class DataException
:	public std::runtime_error
{
public:
	DataException( const std::string& description );
};

/** @brief Exception type for problem of output components
 * @ingroup util
 */
class DataOutputException
:	public std::runtime_error
{
public:
	DataOutputException( const std::string& description );
};

//-----------------------------------------------------------------------inlines

inline SchedulingException::SchedulingException( const std::string& description )
:	runtime_error( description )
{
}

inline QueueingException::QueueingException( const std::string& description )
:	runtime_error( description )
{
}

inline DataException::DataException( const std::string& description )
:	runtime_error( description )
{
}

inline DataOutputException::DataOutputException( const std::string& description )
:	runtime_error( description )
{
}

} // namespace odemx

#endif /* ODEMX_EXCEPTIONS_INCLUDED */
