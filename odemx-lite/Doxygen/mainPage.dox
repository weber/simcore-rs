/**
	\mainpage

	\section Introduction
	Welcome to ODEMx. ODEMx is a library for discrete event simulation in C++.
	\ref toc "(Table of Contents)"

	ODEMx is a C++ library for process and event simulation, which
	is a branch of computer simulation. As such it uses processes and
	events to model real-world or fictional systems.
	A process in this context is a continuous or discrete
	sequence of actions which are somehow closely related to each
	other, for instance they are all 'done' by one agent. The
	sequence can be divided into branches, and broken off by idle
	periods or synchronisation. An event on the other hand describes
	only one atomic action in this context.

	\section installation Installation
	There is no installation procedure for ODEMx, at least not in
	this version. To use ODEMx in your projects you need to
	build the library, include its header files in your project, and 
	finally link your object files against the compiled library. 

	As ODEMx uses many modern C++ components, it must be compiled with a
	GCC version higher than 4.0 because starting with that version,
	GCC includes the C++ standard library extensions from the
	Technical Report 1, which adds new containers such as hash maps, 
	tuples and arrays, as well as smart pointers and function object 
	generators. All of these components are currently in use in ODEMx.

	Building ODEMx on Windows operating systems requires MinGW
	and MSYS to be installed. The current version of ODEMx does not 
	support Visual Studio because there are several known bugs in the
	implementation of the TR1 components. Even though the library and 
	its dependencies have been successfully built with VS 2008 and
	VS 2010, the resulting code did not run reliably.  
	
	\subsection	mingw_setup Setting up MinGW and MSYS
	The procedure is fairly simple, as it only requires downloading
	the minimal MinGW component set, plus the g++ compiler
	(see http://mingw.org/wiki/Getting_Started). All of the downloaded 
	archives are simply unzipped to the same directory of the user's choice.
	After that,	the MinGW/bin path needs to be added to the Windows PATH
	variable, and then gcc should be available from the command line.
	MSYS provides a collection of GNU utilities such as bash, make and rm.
	ODEMx itself does not require this, but the POCO libraries'
	build system for MinGW depends on the execution of various
	build scripts, besides make. MSYS can be set up by getting the
	installer for version 1.0.11 (http://downloads.sourceforge.net/mingw/MSYS-1.0.11.exe) 
	
	\section firststeps First Steps
	At first you will have to build ODEMx - see \ref installation for details.
	Afterwards try the examples included. That should give you a good start.
	Apart from that you can use them to check whether ODEMx is working properly on your
	computer. \n

	ODEMx includes an online documentation as well. You will need Doxygen
	(http://www.doxygen.org) to generate the documentation from the source files.
	To do so, change to \c odemx/Doxygen directory and run \c doxygen.

	\section contributions Contributions
	ODEMx is based on ODEM (http://odem.sf.net). That's why all contributors of
	ODEM could be listed here, too. Instead only those who put their
	hands on this code directly will be mentioned. Anyway, we encourage
	you to take a look at ODEM and its contributors list as well.

	\li Ralf Gerstenberger <gerstenb@users.sourceforge.net>
	\li Ronald Kluth (since Version 2.0)
	\li Toralf Niebuhr <niebuhr@niebuhrt.de> (since Version 2.0)
	\li Magnus Mueller <evnu@users.sf.net> (since Version 3.0)

	\section toc Table of Contents
	<ol>
	<li>\ref overview
		<ol>
		<li>\ref module_base</li>
		<li>\ref module_coroutine</li>
		<li>\ref module_data</li>
		<li>\ref module_protocol</li>
		<li>\ref module_random</li>
		<li>\ref module_statistics</li>
		<li>\ref module_synchronization</li>
		<li>\ref module_test</li>
		<li>\ref module_util</li>
		</ol>
	</li>
	<li>\ref otoo
	</li>
	</ol>

	\section copyright Copyright and License
	ODEMx is protected by the GNU LESSER GENERAL PUBLIC LICENSE as
	described in the file COPYING, which has to be present in
	the ODEMx package; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.

*/
