//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Example_Custom_Channel.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/16
 * @brief Example demonstrating how to create and use a new log channel
 * @since 3.0
 */

/**
 * @example Example_Custom_Channel.cpp
 * The usage of ODEMx channels is demonstrated.
 *
 * ODEMx provides 7 separate log channels in order to represent relevant
 * logging categories: trace, debug, info, warning, error, fatal, and
 * statistics. However, ODEMx users are not limited to these logging
 * categories but can instead create their own. This is demonstrated
 * by first creating a new channel id and then a consumer class that makes
 * use of the channel.
 */

#include <odemx/odemx.h>
//
// First we use a macro to create a new channel ID. This value will be
// created in namespace odemx::data::channel_id.
//
ODEMX_DECLARE_DATA_CHANNEL_WITH_ID( clock, 99 );
//
// Next, we define a simple producer class that has a shared_ptr member
// for the ODEMx log channel type. During construction, this pointer
// gets initialized be requesting a new channel pointer from the simulation
// context. The method @c useChannel then demonstrates how this channel
// can be put to use.
//
class CustomProducer: public odemx::data::Producer {
public:
	CustomProducer( odemx::base::Simulation& sim, const odemx::data::Label& label )
	:	Producer( sim, label )
	{
		clock = sim.getChannel( odemx::data::channel_id::clock );
	}
	void useChannel() {
		clock << log( "using my own channel" );
	}
private:
	std::shared_ptr< Log::Channel< odemx::data::SimRecord > > clock;
};

int main()
{
	odemx::base::Simulation& sim = odemx::getDefaultSimulation();
	//
	// After creating a new producer object, we enable the default logging
	// functionality of ODEMx an choose to log output to the console.
	// Then, the method @c useChannel is called to produce some
	// text output on the console.
	//
	CustomProducer p( sim, "Custom Producer" );
	sim.enableDefaultLogging( odemx::data::output::STDOUT );
	p.useChannel();
}
