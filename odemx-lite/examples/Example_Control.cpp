//------------------------------------------------------------------------------
//	Copyright (C) 2003, 2004, 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Example_Control.cpp
 * @author Jonathan Schlue
 * @date created at 2016/10/06
 * @brief Example for using Control variables to wait until an arbitrary condition turns true.
 * @since 3.1
 */

/**
 * @example Example_Control.cpp
 * The basic usage of Control variables to wait until an arbitrary condition turns true.
 *
 * There are two processes: a Ping process, and a Pong process.
 * ping says "ping" from time to time, by switching pong's controlled bool variable "pinged_".
 * pong is waiting for changes to exactly this variable, and whenever it is set to true, pong prints an update about the current
 * recognized ping to the console.
 */

#include <iostream>

#include <odemx/odemx.h>

struct Pong: Process
{
	Control<bool> pinged_;

	Pong() :Process(odemx::getDefaultSimulation(), "Pong"), pinged_{false}
	{}

protected:
	virtual int main() override
	{

		int times_pinged = 0;

		for(;;)
		{
			waitUntil([&](Process*) { // <-- NOTE the capture default by reference "[&]", to actually always get the current value of "pinged_".
				return pinged_;
			}, "waiting for a ping", {pinged_});

			++times_pinged;

			std::cout << "#" << times_pinged << ": \tgot pinged at time " << getTime() << std::endl;
			pinged_ = false;
		}
		return 0;
	}
};

struct Ping: Process
{
	Pong* pong_;

	Ping(Pong* pong) :Process(odemx::getDefaultSimulation(), "Ping"), pong_{pong}
	{}

protected:
	virtual int main() override
	{

		for(int interval = 1;; ++interval)
		{
			// ping once in a while
			holdFor(interval);
			pong_->pinged_ = true;
		}
		return 0;
	}
};



int main()
{
	Pong pong;
	Ping ping{&pong};
	ping.hold();
	pong.hold();
	odemx::getDefaultSimulation().runUntil(101);
	return 0;
}
