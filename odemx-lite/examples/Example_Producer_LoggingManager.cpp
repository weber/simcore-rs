//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Example_Producer_LoggingManager.cpp
 * @author Ronald Kluth
 * @date created at 2009/12/28
 * @brief Example showing the use of a producer class with ODEMx's default logging
 * @since 3.0
 */

/**
 * @example Example_Producer_LoggingManager.cpp
 * The usage of ODEMx default logging is shown.
 *
 * The LoggingManager is responsible for managing default logging components.
 * A simple call to @c enableDefaultLogging suffices to activate it. The
 * parameters can be STDOUT, XML, or DATABASE. The latter two would then
 * require another string to specify the location.
 */

#include "Example_Producer_StopWatch.h"

int main() {
	odemx::base::Simulation& sim = odemx::getDefaultSimulation();
	sim.enableDefaultLogging( odemx::data::output::STDOUT );

	StopWatch watch( sim, "Stop Watch" );
	watch.start();
	sim.setCurrentTime( 10 );
	watch.stop();
	//
	// Even though, logging is disbled for the watch object, it will still
	// produce warning messages because the failure-related channels cannot
	// be deactivated.
	//
	watch.disableLogging();
	watch.stop();
}
