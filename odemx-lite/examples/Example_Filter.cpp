//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file examples/Example_Filter.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/24
 * @brief Example demonstrating how to use a SimRecordFilter
 * @since 3.0
 */

/**
 * @example Example_Filter.cpp
 * The usage of the ODEMx log record filter class is shown.
 *
 * All four methods of filtering simulation records are shown in the
 * main function. Filtering can be done by record text, record scope,
 * producer label, or producer type.
 */

#include "Example_Producer_DebugStopWatch.h"
#include <iostream>
using namespace odemx;

int main() {
	typedef std::shared_ptr< data::SimRecordFilter > FilterPtr;
	FilterPtr filter = data::SimRecordFilter::create();

	base::Simulation& sim = getDefaultSimulation();
	sim.enableDefaultLogging( data::output::STDOUT );
	sim.setFilter( filter );

	//
	// The construction of the DebugStopWatch is filtered here.
	//
	filter->addRecordText() << "construction";
	DebugStopWatch watch( sim, "Stop Watch" );

	//
	// The scope filters all output from a class in hierarchy, in this case
	// the records sent by class DebugStopWatch. Therefore, only the base class
	// records from StopWatch may pass here.
	//
	filter->addRecordScope() << typeid( DebugStopWatch );
	watch.start();
	watch.stop();

	//
	// Filtering by label means blocking everything that is sent by this object.
	// So, no output happens when calling @c start.
	//
	filter->addProducerLabel() << "Stop Watch";
	watch.start();

	//
	// Resetting the filter removes all previously added values. However,
	// adding the producer type is different than using a scope because
	// this will now also filter all records, but not only from one object
	// but all objects of class DebugStopWatch.
	//
	filter->resetFilter();
	filter->addProducerType() << typeid( DebugStopWatch );
	watch.stop();
}
