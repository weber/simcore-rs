//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Example_Producer_DebugStopWatch.h
 * @author Ronald Kluth
 * @date created at 2010/04/13
 * @brief Example demonstrating the use of ODEMx's debug channel within a producer class
 * @since 3.0
 */

/**
 * @example Example_Producer_DebugStopWatch.h
 * The usage of ODEMx log channel @c debug is shown.
 *
 * For this purpose, a new log data producer is derived from the class
 * @c StopWatch. It extends this class by hiding its methods @c start and @c stop
 * in order to add debug statements to the calls.
 */

#ifndef EXAMPLE_PRODUCER_DEBUGSTOPWATCH_INCLUDED
#define EXAMPLE_PRODUCER_DEBUGSTOPWATCH_INCLUDED

#include "Example_Producer_StopWatch.h"

class DebugStopWatch: public StopWatch {
public:
	DebugStopWatch( odemx::base::Simulation& sim, const odemx::data::Label& label )
	:	StopWatch( sim, label ) {
		debug << log( "construction" ).scope( typeid( DebugStopWatch ) );
	}
	~DebugStopWatch() {
		debug << log( "destruction" ).scope( typeid( DebugStopWatch ) );
	}
	void start() {
		debug << log( "started" ).scope( typeid( DebugStopWatch ) );
		StopWatch::start();
	}
	void stop(){
		debug << log( "stopped" ).scope( typeid( DebugStopWatch ) );
		StopWatch::stop();
	}
};

#endif /* EXAMPLE_PRODUCER_DEBUGSTOPWATCH_INCLUDED */
