//------------------------------------------------------------------------------
//	Copyright (C) 2009 Humboldt-Universitaet zu Berlin
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

/**
 * @file Example_Producer_StopWatch.h
 * @author Ronald Kluth
 * @date created at 2010/04/13
 * @brief Example showing the use of ODEMx log channels within a producer class
 * @since 3.0
*/

/**
 * @example Example_Producer_StopWatch.h
 * The typical usage of ODEMx logging is shown.
 *
 * Since most ODEMx components are derived from odemx::data::Producer,
 * it is actually quite easy to use the log channels provided by that
 * base class. This example shows the definition of a log producing
 * class that makes use of the channels @c info and @c error.
 */

#ifndef EXAMPLE_PRODUCER_STOPWATCH_H_
#define EXAMPLE_PRODUCER_STOPWATCH_H_

#include <odemx/odemx.h>

class StopWatch: public odemx::data::Producer {
public:
	StopWatch( odemx::base::Simulation& sim, const odemx::data::Label& label )
	:	Producer( sim, label ), running( false )
	{}
	void start() {
		running = true;
		startTime = getSimulation().getTime();
		info << log( "started" );
	}
	void stop() {
		if( running ) {
			running = false;
			info << log( "stopped" ).detail( "duration", getDuration() );
		} else {
			error << log( "StopWatch::stop(): stop watch is not running" );
		}
	}
	odemx::base::SimTime getDuration() { return getSimulation().getTime()-startTime; }
private:
	bool running;
	odemx::base::SimTime startTime;
};

#endif /* EXAMPLE_PRODUCER_STOPWATCH_H_ */
