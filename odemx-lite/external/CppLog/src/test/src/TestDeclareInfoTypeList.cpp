//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestDeclareInfoTypeList.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/27
 * @brief Unit tests for macro CPPLOG_DECLARE_INFO_TYPE_LIST
 * @since 0.1
 */

#include "global.h"
#include <string>
#include <vector>
#include <type_traits>

CPPLOG_DECLARE_INFO_TYPE_LIST( TestInfoTypeList, std::string, int, std::vector< bool > );

SUITE( CppLogUnitTest )
{
	TEST( TestDeclareInfoTypeListTypes )
	{
		bool isSame = std::is_same<
						std::string,
						Log::Detail::TypeOf< TestInfoTypeList, 0 >::type
						>::value;
		CHECK( isSame );

		isSame = std::is_same<
						int,
						Log::Detail::TypeOf< TestInfoTypeList, 1 >::type
						>::value;
		CHECK( isSame );

		isSame = std::is_same<
						std::vector< bool >,
						Log::Detail::TypeOf< TestInfoTypeList, 2 >::type
						>::value;
		CHECK( isSame );
	}
}
