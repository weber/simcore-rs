//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestProducer.cpp
 * @author Ronald Kluth
 * @date created at 2009/05/17
 * @brief Unit tests for class template DataProducer
 * @since 0.1
 */

#include "global.h"
#include <string>

CPPLOG_DECLARE_CHANNEL_WITH_ID( test, channel_one, int, 0 );
CPPLOG_DECLARE_CHANNEL_WITH_ID( test, channel_two, int, 1 );

typedef Log::Producer< Log::Enable< test::channel_one, test::channel_two > > ProducerType;

SUITE( CppLogUnitTest )
{
	struct Producer: ProducerType
	{
		typedef std::shared_ptr< Log::Channel< int > > ChannelPtr;
		
		Producer( Log::NameScope& scope, std::string const& name )
		:	ProducerType( scope, name )
		{}
		Producer( Log::ChannelManager< int >& scope, std::string const& name )
		:	ProducerType( scope, name )
		{}
		ChannelPtr getChannelOne()
		{
			return channel_one;
		}
		ChannelPtr getChannelTwo()
		{
			return channel_two;
		}
	};
	
	TEST( ProducerName )
	{
		Log::NameScope scope;
		Log::ChannelManager< int > manager;
		std::string name( "Test Producer" );
		Producer producer1( scope, name );
		Producer producer2( manager, name );
		CHECK_EQUAL( name, producer1.getName() );
		CHECK_EQUAL( name, producer2.getName() );
	}

	TEST( ProducerChannelMemberInit )
	{
		Log::ChannelManager< int > manager;
		Producer producer( manager, "Test Producer" );
		CHECK( producer.getChannelOne() );
		CHECK( producer.getChannelTwo() );
	}
}
