//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestNameScope.cpp
 * @author Ronald Kluth
 * @date created at 2009/05/17
 * @brief Unit tests for class NameScope
 * @since 0.1
 */

#include "global.h"
#include <string>

SUITE( CppLogUnitTest )
{
	TEST( NameScopeDefaults )
	{
		std::string defaultName = "No label given";
		char defaultSpacer = ' ';
		Log::NameScope scope( defaultName, defaultSpacer );
		std::string emptyName = scope.createUniqueName( "" );
		std::string emptyName1 = scope.createUniqueName( "" );

		CHECK_EQUAL( defaultName, emptyName );
		CHECK_EQUAL( defaultSpacer, emptyName1[ defaultName.size() ] );
	}

	TEST( NameScopeUniqueness )
	{
		Log::NameScope scope;
		std::set< std::string > names;
		int i, iterations;
		i = iterations = 10000;
		while( i-- )
		{
			names.insert( scope.createUniqueName( "UniqueName" ) );
		}

		CHECK_EQUAL( iterations, names.size() );
	}

	TEST( NameScopeNameCreation )
	{
		Log::NameScope scope;
		std::string testName = "Name Scope Test Name";

		CHECK( scope.createUniqueName( testName ).find( testName ) != std::string::npos );
		CHECK( scope.createUniqueName( testName ).find( testName ) != std::string::npos );
		CHECK( scope.createUniqueName( testName ).find( testName ) != std::string::npos );
	}
}
