//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestOutputXmlStreamWriter.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/28
 * @brief Unit tests for class template XmlStreamWriter
 * @since 0.1
 */

#include "global.h"

SUITE( CppLogUnitTest )
{
	typedef Log::XmlStreamWriter< Infos > XmlStreamWriterType;
	typedef std::shared_ptr< XmlStreamWriterType > XmlStreamWriterPtr;

	TEST( TestXmlStreamWriterConsume )
	{
		std::ostringstream os;
		XmlStreamWriterPtr xmlWriter = XmlStreamWriterType::create( os, 2 );

		Point3d p3d = { 4, 5, 6 };
		xmlWriter->consume( 0,
				Log::Record( "test_xml_stream_writer_consume" )
				+ StringInfo( "test_string_info" )
				+ PointInfo( p3d ) );

		CHECK( find( os.str(), "  <record>" ) );
		CHECK( find( os.str(), "    <text>test_xml_stream_writer_consume</text>" ) );
		CHECK( find( os.str(), "    <test_string_info>test_string_info</test_string_info>" ) );
		CHECK( find( os.str(), "    <test_point_info>(4 5 6)</test_point_info>" ) );
		CHECK( find( os.str(), "  </record>" ) );
	}
}
