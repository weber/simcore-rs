//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestConsumer.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/27
 * @brief Unit tests for class template Consumer
 * @since 0.1
 */

#include "global.h"
#include <string>

SUITE( CppLogUnitTest )
{
	struct TestConsumer: public Log::Consumer< std::string >
	{
		TestConsumer(): lastId( 0 ), lastRecord( "" ) {}
		virtual void consume( Log::ChannelId const channelId, std::string const& record )
		{
			lastId = channelId;
			lastRecord = record;
		}
		Log::ChannelId lastId;
		std::string lastRecord;
	};

	TEST( TestConsumerInterfaceCall )
	{
		std::string testRecord( "test record" );
		Log::ChannelId id = 666;
		Log::Channel< std::string > channel( id );
		std::shared_ptr< TestConsumer > testConsumer( new TestConsumer() );
		channel.addConsumer( testConsumer );
		channel << testRecord;
		CHECK_EQUAL( channel.getId(), testConsumer->lastId );
		CHECK_EQUAL( testRecord, testConsumer->lastRecord );
	}
}
