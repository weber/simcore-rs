//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file global.h
 * @date created at 2010/04/28
 * @author Ronald Kluth
 * @brief Declaration of global components used for testing
 */

#ifndef CPPLOGTEST_GLOBAL_INCLUDED
#define CPPLOGTEST_GLOBAL_INCLUDED

#include <UnitTest++.h>
#include <CppLog.h>

namespace SuiteCppLogUnitTest {

struct Point3d
{
	float x, y, z;
};

extern std::ostream& operator<<( std::ostream& os, Point3d const& p );

CPPLOG_DECLARE_INFO_TYPE( StringInfo, "test_string_info", std::string );
CPPLOG_DECLARE_INFO_TYPE( PointInfo, "test_point_info", Point3d );
CPPLOG_DECLARE_INFO_TYPE_LIST( Infos, StringInfo, PointInfo );

extern bool find( std::string const& str, std::string const& seq );

} // namespace CppLogUnitTest

#endif /* CPPLOGTEST_GLOBAL_INCLUDED */
