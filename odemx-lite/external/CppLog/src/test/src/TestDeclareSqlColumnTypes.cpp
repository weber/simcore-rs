//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestDeclareSqlColumnTypes.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/27
 * @brief Unit tests for macro CPPLOG_DECLARE_SQL_COLUMN_TYPES
 * @since 0.1
 */

#include "global.h"
#include <string>
#include <vector>
#include <type_traits>

CPPLOG_DECLARE_INFO_TYPE_LIST( TestInfoTypeList, std::string, int, std::vector< bool > );
CPPLOG_DECLARE_SQL_COLUMN_TYPES( TestInfoTypeList, testSqlTypes, "VARCHAR", "INTEGER", "BLOB" );

SUITE( CppLogUnitTest )
{
	TEST( TestDeclareSqlColumnTypesValues )
	{
		CHECK_EQUAL( "VARCHAR", testSqlTypes[ 0 ] );
		CHECK_EQUAL( "INTEGER", testSqlTypes[ 1 ] );
		CHECK_EQUAL( "BLOB", testSqlTypes[ 2 ] );
	}
}
