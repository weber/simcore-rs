//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file global.cpp
 * @date created at 2010/04/28
 * @author Ronald Kluth
 * @brief Implementation of global components used for testing
 */

#include "global.h"

namespace SuiteCppLogUnitTest {

std::ostream& operator<<( std::ostream& os, Point3d const& p )
{
	os << "(" << p.x << " " << p.y << " " << p.z << ")";
	return os;
}

bool find( std::string const& str, std::string const& seq )
{
	std::size_t found = str.find( seq );
	return found != std::string::npos;
}

}
