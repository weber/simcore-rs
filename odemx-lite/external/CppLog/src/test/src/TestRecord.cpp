//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestRecord.cpp
 * @author Ronald Kluth
 * @date created at 2009/05/29
 * @brief Unit tests for class Record
 * @since 0.1
 */

#include "global.h"
#include <string>
#include <iostream>

CPPLOG_DECLARE_INFO_TYPE( TestInfo, "test info", char );

SUITE( CppLogUnitTest )
{
	TEST( RecordText )
	{
		Log::StringLiteral message = "Log record test message";
		Log::Record rec( message );
		CHECK_EQUAL( message, rec.getText() );
	}

	TEST( RecordInfo )
	{
		Log::Record rec;
		Log::StringLiteral f = __FILE__;
		int l = __LINE__;
		Log::StringLiteral s = "TestRecordInfo";
		
		rec.file( f ).line( l ).scope( s );
		CHECK( rec.get< Log::FileInfo >() );
		CHECK( rec.get< Log::LineInfo >() );
		CHECK( rec.get< Log::ScopeInfo >() );
		CHECK_EQUAL( f, *rec.get< Log::FileInfo >() );
		CHECK_EQUAL( l, *rec.get< Log::LineInfo >() );
		CHECK_EQUAL( s, *rec.get< Log::ScopeInfo >() );
		CHECK_EQUAL( (void*)0, rec.get< TestInfo >() );
/*		
		char c = 'X';
		rec + TestInfo( c );
		CHECK( rec.get< TestInfo >() );
		CHECK_EQUAL( c, *rec.get< TestInfo >() );*/
	}
}
