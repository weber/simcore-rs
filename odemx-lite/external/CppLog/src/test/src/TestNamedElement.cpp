//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestNamedElement.cpp
 * @author Ronald Kluth
 * @date created at 2009/05/18
 * @brief Unit tests for class NamedElement
 * @since 0.1
 */

#include "global.h"
#include <string>

SUITE( CppLogUnitTest )
{
	TEST( NamedElementLabel )
	{
		std::string label = "Label Test";
		Log::NameScope scope;
		Log::NamedElement obj( scope, label );

		CHECK_EQUAL( label , obj.getName() );
	}
}
