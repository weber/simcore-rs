//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestDeclareInfoType.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/27
 * @brief Unit tests for macro CPPLOG_DECLARE_INFO_TYPE
 * @since 0.1
 */

#include "global.h"
#include <string>

CPPLOG_DECLARE_INFO_TYPE( PointerInfo, "pointer info", std::string* );
CPPLOG_DECLARE_INFO_TYPE( CopyInfo, "copy info", std::string );
CPPLOG_DECLARE_INFO_TYPE( ReferenceInfo, "reference info", std::string& );

SUITE( CppLogUnitTest )
{
	TEST( TestDeclareInfoTypeStringName )
	{
		CHECK_EQUAL( "pointer info", PointerInfo::getName().c_str() );
	}

	TEST( TestDeclareInfoTypePointer )
	{
		std::string testString( "test string" );
		CHECK_EQUAL( &testString, PointerInfo( &testString ).value() );
	}

	TEST( TestDeclareInfoTypeCopy )
	{
		std::string testString( "test string" );
		CHECK_EQUAL( "test string", CopyInfo( testString ).value()->c_str() );
	}

	TEST( TestDeclareInfoTypeReference )
	{
		std::string testString( "test string" );
		CHECK_EQUAL( &testString, ReferenceInfo( testString ).value() );
	}
}
