//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestDeclareChannel.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/27
 * @brief Unit tests for macro CPPLOG_DECLARE_CHANNEL
 * @since 0.1
 */

#include "global.h"
#include <string>

static const int testChannelId = 666;
typedef unsigned long TestRecordType;

CPPLOG_DECLARE_CHANNEL_WITH_ID( Channel, testChannel , TestRecordType, testChannelId );

SUITE( CppLogUnitTest )
{
	TEST( TestDeclareChannelId )
	{
		CHECK_EQUAL( testChannelId, (int)Channel::testChannel );
	}

	struct TestDeclareChannelProducer: Log::Detail::ChannelField< Channel::testChannel >
	{
		using Log::Detail::ChannelField< Channel::testChannel >::testChannel;
	};
	struct TestDeclareChannelConsumer: Log::Consumer< TestRecordType >
	{
		virtual void consume( Log::ChannelId const channelId, TestRecordType const& record )
		{
			lastRecord = record;
		}
		TestRecordType lastRecord;
	};

	TEST( TestDeclareChannelMember )
	{
		TestRecordType testRecord( 42 );
		TestDeclareChannelProducer p;
		std::shared_ptr< TestDeclareChannelConsumer > testConsumer(
				new TestDeclareChannelConsumer() );

		p.testChannel.reset( new Log::Channel< TestRecordType >( Channel::testChannel ) );
		p.testChannel->addConsumer( testConsumer );
		p.testChannel << testRecord;

		CHECK_EQUAL( testRecord, testConsumer->lastRecord );
	}
}
