//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file global.cpp
 * @date created at 2010/04/28
 * @author Ronald Kluth
 * @brief Implementation of global components used for testing
 */

#include <UnitTest++.h>

/**
 * @brief This is the main function for the test runner application
 *
 * The function automatically runs all registered test suites.
 * and returns the value of UnitTest::RunAllTests(), which states
 * how many tests have failed during execution.
 */
int main()
{
	return UnitTest::RunAllTests();
}
