//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestOutputXmlWriter.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/28
 * @brief Unit tests for class XmlWriter
 * @since 0.1
 */

#include "global.h"
#include <string>

SUITE( CppLogUnitTest )
{
	TEST( TestXmlWriterStartElement )
	{
		std::ostringstream os;
		Log::XmlWriter::startElement( os, 4, "test_start_element" );
		CHECK_EQUAL( "    <test_start_element>", os.str() );
	}

	TEST( TestXmlWriterCloseElement )
	{
		std::ostringstream os;
		Log::XmlWriter::closeElement( os, 4, "test_close_element" );
		CHECK_EQUAL( "    </test_close_element>\n", os.str() );
	}

	TEST( TestXmlWriterEmptyElement )
	{
		std::ostringstream os;
		Log::XmlWriter::emptyElement( os, 4, "test_empty_element" );
		CHECK_EQUAL( "    <test_empty_element/>\n", os.str() );
	}

	TEST( TestXmlWriterWriteElement )
	{
		std::ostringstream os;
		Log::XmlWriter::writeElement( os, 4, "test_write_element", 999 );
		CHECK_EQUAL( "    <test_write_element>999</test_write_element>\n", os.str() );
	}

	TEST( TestXmlWriterWriteHeader )
	{
		std::ostringstream os;
		Log::XmlWriter::writeHeader( os );
		CHECK_EQUAL( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", os.str() );
	}

	struct DummyXmlWriter: Log::XmlWriter
	{
		using Log::XmlWriter::StreamInfoElements;
	};

	TEST( TestXmlWriterStreamInfoElements )
	{
		std::ostringstream os;
		DummyXmlWriter::StreamInfoElements writer( os, 2 );
		writer( "test_stream_info_elements_value", 111, true );
		writer( "test_stream_info_elements_empty", 111, false );
		CHECK_EQUAL( "  <test_stream_info_elements_value>111</test_stream_info_elements_value>\n"
				"  <test_stream_info_elements_empty/>\n", os.str() );
	}
}
