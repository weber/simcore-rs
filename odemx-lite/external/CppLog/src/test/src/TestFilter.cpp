//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestFilter.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/27
 * @brief Unit tests for class template specialization Filter< Record >
 * @since 0.1
 */

#include "global.h"
#include <iostream>

CPPLOG_DECLARE_INFO_TYPE( TestFilterInfo, "test filter info", std::string )

SUITE( CppLogUnitTest )
{
	TEST( TestFilterRecordText )
	{
		std::shared_ptr< Log::Filter<> > filter = Log::Filter<>::create();
		Log::Record rec( "text filter test" );
		filter->addRecordText() << "text filter test";
		CHECK( ! filter->pass( rec ) );
	}

	TEST( TestFilterRecordInfo )
	{
		std::shared_ptr< Log::Filter<> > filter = Log::Filter<>::create();
		Log::Record rec;
		rec + TestFilterInfo( "info filter test" );
		filter->add< TestFilterInfo >() << "info filter test";
		CHECK( ! filter->pass( rec ) );
	}

	TEST( TestFilterRecordMode )
	{
		std::shared_ptr< Log::Filter<> > filter = Log::Filter<>::create();
		Log::Record rec( "mode filter test" );
		rec + TestFilterInfo( "mode filter test" );
		filter->addRecordText() << "mode filter test";
		filter->add< TestFilterInfo >() << "mode filter test";
		filter->passNone();
		CHECK( filter->pass( rec ) );
		filter->passAll();
		CHECK( ! filter->pass( rec ) );
	}

	TEST( TestFilterRecordReset )
	{
		std::shared_ptr< Log::Filter<> > filter = Log::Filter<>::create();
		Log::Record rec;
		rec + TestFilterInfo( "reset filter test" );
		filter->add< TestFilterInfo >() << "reset filter test";
		filter->resetFilter();
		CHECK( filter->pass( rec ) );
	}
}
