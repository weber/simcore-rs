//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestOutputOStreamWriter.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/28
 * @brief Unit tests for class template OStreamWriter
 * @since 0.1
 */

#include "global.h"

SUITE( CppLogUnitTest )
{
	typedef Log::OStreamWriter< Infos > OStreamWriterType;
	typedef std::shared_ptr< OStreamWriterType > OStreamWriterPtr;

	TEST( TestOStreamWriterConsume )
	{
		std::ostringstream os;
		OStreamWriterPtr osWriter = OStreamWriterType::create( os );

		Point3d p3d = { 7, 8, 9 };
		osWriter->consume( 0,
				Log::Record( "test_ostream_writer_consume" )
				+ StringInfo( "test_string_info" )
				+ PointInfo( p3d ) );

		CHECK( find( os.str(), "test_ostream_writer_consume" ) );
		CHECK( find( os.str(), "test_string_info=test_string_info" ) );
		CHECK( find( os.str(), "test_point_info=(7 8 9)" ) );
	}
}
