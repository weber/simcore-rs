//
// Copyright (c) 2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file TestEnable.cpp
 * @author Ronald Kluth
 * @date created at 2010/04/27
 * @brief Unit tests for class template Enable
 * @since 0.1
 */

#include "global.h"
#include <string>
#include <vector>
#include <type_traits>

SUITE( CppLogUnitTest )
{
	enum
	{
		id1,
		id2
	};

	TEST( TestEnableChannelFields )
	{
		typedef Log::Producer< Log::Enable< id1, id2 > > TestEnableProducer;
		bool inheritedField1 = std::is_base_of<
							Log::Detail::ChannelField< id1 >,
							TestEnableProducer
							>::value;
		bool inheritedField2 = std::is_base_of<
							Log::Detail::ChannelField< id2 >,
							TestEnableProducer
							>::value;
		CHECK( inheritedField1 );
		CHECK( inheritedField2 );
	}
}
