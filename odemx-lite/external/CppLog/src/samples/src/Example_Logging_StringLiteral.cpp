//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file Example_Logging_StringLiteral.cpp
 * @author Ronald Kluth
 * @date 2009/04/11
 * @brief Example displaying use of CppLog with normal C-strings
 * @since 0.1
 */

/**
 * @example Example_Logging_StringLiteral.cpp
 *
 * This example illustrates how to log data with the simplest and fastest
 * kind of strings - constant string literals. In order to do this we
 * define Log::StringLiteral as the record type. To be able to use simple
 * C-strings without constructor calls to Log::StringLiteral, another
 * overload of @c operator<< must be provided that transforms const char[]
 * to a Log::StringLiteral and writes it to the channel.
 *
 */
#include <CppLog.h>
#include <iostream>
//
//------------------------------------------------------------------------global
//
// To log strings, the simplest log records are sufficient.
//
typedef Log::StringLiteral Rec;
//
// In order to create log records from string literals, we must overload the
// insertion operator for fixed-length character arrays.
//
template < std::size_t LenV >
const Log::Channel< Rec >&
operator<<( const Log::Channel< Rec >& channel, const char (&recordText)[LenV] )
{
	//
	// The record text is used to create a simple StringLiteral record for
	// each logged string, which is then sent through the channel via
	// Log::operator<<.
	//
	channel << Rec( recordText );
	return channel;
}
//
// For simplicity, we use a global log channel object.
//
Log::Channel< Rec > logChannel;
//
//-----------------------------------------------------------------------classes
//
// To receive and handle log records, we subclass the DataConsumer interface
// with the appropriate record type and implement the method 'consume()'
// In this example, we print formatted records to the console.
//
class ConsoleWriter
:	public Log::Consumer< Rec >
{
public:
	//
	// Override the pure virtual method 'consume()'. The second parameter
	// uses a typedef from the base class DataConsumer to free us from
	// using possibly complicated record type names.
	//
	virtual void consume( const Log::ChannelId channelId, const Rec& record )
	{
		std::cout << record << std::flush;
	}
};
//
//--------------------------------------------------------------------------main
//
int main( int argc, char* argv[] )
{
	//
	// We register an instance of ConsoleWriter with the channel.
	//
	logChannel.addConsumer( std::shared_ptr< ConsoleWriter >( new ConsoleWriter() ) );
	//
	// Now, we can start logging right away by sending strings to the channel,
	// which basically forwards them to stdout.
	//
	logChannel << "a string literal followed by some value: " << "2" << "\n";
	return 0;
}
//
//----------------------------------------------------------------program output
//
//a string record displaying some value: 2
//
