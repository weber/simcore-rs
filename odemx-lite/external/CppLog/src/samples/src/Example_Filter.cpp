//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file src/Example_Filter.cpp
 * @author Ronald Kluth
 * @date 2009/11/29
 * @brief Example introducing basic filter usage
 * @since 0.1
 */

/**
 * @example src/Example_Filter.cpp
 *
 * This example illustrates the basic usage of record filters. Since @c Filter
 * is only an interface, we use the filter specialization for the library's
 * default record type. Filtering of @c Record objects is based on filter sets
 * for the record text and each info type.
 * 
 * Filter entries are added in a stream-like manner
 * with @c operator<< to filter sets retrieved by the method template @c add.
 * Filtering happens when a log record passes the channel inserter 
 * (@c operator<<), which checks the channel for a filter and, if one is set, 
 * whether the record may pass. In this example we filter records
 * by the @c ScopeInfo value "foo()", which means that all records originating
 * from the function @c foo are blocked.
 */

#include <CppLog.h>
#include <iostream>
using namespace Log;

CPPLOG_DECLARE_INFO_TYPE_LIST( Infos, ScopeInfo );
Channel<> channel;

void foo() {
	channel << Record( "Logging filter example (foo)" ) + ScopeInfo( "foo()" );
}

int main() {
	std::shared_ptr< Filter<> > filter = Filter<>::create();
	filter->add< ScopeInfo >() << "foo()";
	channel.setFilter( filter );
	channel.addConsumer( OStreamWriter< Infos >::create( std::cout ) );

	channel << Record( "Logging filter example (main)" ) + ScopeInfo( "main()" );
	foo();
}
