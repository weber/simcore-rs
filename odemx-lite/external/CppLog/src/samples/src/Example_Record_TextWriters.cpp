//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file Example_Record_TextWriters.cpp
 * @author Ronald Kluth
 * @date 2009/11/23
 * @brief Example introducing default text writers
 * @since 0.1
 */

/**
 * @example Example_Record_TextWriters.cpp
 *
 * This example demonstrates the use of the library's default text writing
 * components. To illustrate how they can be used with any class that provides
 * a stream inserter overload (@c operator<<), we first define a structure
 * @c Point3d and two info types @c PointInfo and @c DoubleInfo. These are then
 * added to the list @c Infos, which we plan to extract automatically from the
 * log records.
 * 
 * In the main function we create a channel object an register two consumers:
 * an @c OStreamWriter which is connected to the console, and an @c XmlFileWriter
 * which will store all records in XML files. The second parameter sets a record
 * limit per file. When the limit is reached, a new XML file is created 
 * automatically. The output of the log record in the last statement will appear
 * on the console and in a file called @c Example_0.xml. The added @c Point3d
 * is converted into a string by means of @c operator<<.
 */

#include <CppLog.h>
#include <iostream>

struct Point3d { float x, y, z; };

std::ostream& operator<<( std::ostream& os, Point3d const& p ) {
	os << "(" << p.x << " " << p.y << " " << p.z << ")";
	return os;
}

CPPLOG_DECLARE_INFO_TYPE( PointInfo, "point", Point3d );
CPPLOG_DECLARE_INFO_TYPE( DoubleInfo, "value", double& );
CPPLOG_DECLARE_INFO_TYPE_LIST( Infos, PointInfo, DoubleInfo );

int main() {
	Log::Channel<> channel;
	channel.addConsumer( Log::OStreamWriter< Infos >::create( std::cout ) );
	channel.addConsumer( Log::XmlStreamWriter< Infos >::create( std::cout ) );
	channel.addConsumer( Log::XmlFileWriter< Infos >::create( "Example", 1000 ) );

	Point3d p = { 1, 2, 3 };
	channel << Log::Record() + PointInfo( p ) + DoubleInfo( 123.45 );
}
