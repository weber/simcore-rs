//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file CppLog.h
 * @author Ronald Kluth
 * @date 2009/05/17
 * @brief Forwarding header for all CppLog classes and macros
 * @since 0.1
 */

#ifndef CPPLOG_INCLUDED
#define CPPLOG_INCLUDED

// include library setup, enables/disables database connectivity
#include "setup.h"

// include core components
#include "CppLog/Channel.h"
#include "CppLog/ChannelId.h"
#include "CppLog/ChannelManager.h"
#include "CppLog/Consumer.h"
#include "CppLog/DeclareChannel.h"
#include "CppLog/DeclareInfoType.h"
#include "CppLog/DeclareInfoTypeList.h"
#include "CppLog/DeclareSqlColumnTypes.h"
#include "CppLog/Enable.h"
#include "CppLog/Filter.h"
#include "CppLog/NamedElement.h"
#include "CppLog/NameScope.h"
#include "CppLog/Producer.h"
#include "CppLog/Record.h"
#include "CppLog/StringLiteral.h"

// include output components
#include "CppLog/output/OStreamWriter.h"
#include "CppLog/output/XmlFileWriter.h"
#include "CppLog/output/XmlStreamWriter.h"
#include "CppLog/output/XmlWriter.h"
#include "CppLog/output/DatabaseAccessor.h"

#endif /* CPPLOG_INCLUDED */
