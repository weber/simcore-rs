//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file StringLiteral.h
 * @author Ronald Kluth
 * @date created at 2009/11/19
 * @brief Declaration of type StringLiteral
 * @since 0.1
 */

#ifndef LOG_STRINGLITERAL_INCLUDED
#define LOG_STRINGLITERAL_INCLUDED

#include "detail/string_literal.hpp"

namespace Log {

/**
 * @typedef StringLiteral
 * @brief Thin wrapper around string literals, borrowed from boost::log
 *
 * This class provides a thin wrapper around string literals (i.e. "strings like this")
 * with the const method interface of @c std::string. All member data are
 * allocated on the stack, making it significantly faster than @c std::string.
 * It can be contructed with or without a string literal, and the class offers
 * assignment, clearing, and comparison operators. Explicit conversion to std::string
 * and C-strings ist provided as well. Use in containers is possible.
 */
typedef boost::log::string_literal StringLiteral;

} // namespace Log

#endif /* LOG_STRINGLITERAL_INCLUDED */
