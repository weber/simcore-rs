//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file NamedElement.h
 * @author Ronald Kluth
 * @date created at 2009/02/08
 * @brief Declaration and implementation of class NamedElement
 * @since 0.1
 */

#ifndef LOG_NAMEDELEMENT_INCLUDED
#define LOG_NAMEDELEMENT_INCLUDED

#include "NameScope.h"
#include <string>

namespace Log {

/**
 * @class NamedElement
 * @brief The base class for all classes whose instances require unique names
 * within a name scope
 */
class NamedElement
{
public:
	/**
	 * @brief Construction with scope and name initialization
	 *
	 * The constructor requests the use of @c name from the scope object. The
	 * value of @c name provides the base for the actual name of the object
	 * because the scope object may need to append a suffix to ensure the
	 * uniqueness of names.
	 */
	NamedElement( NameScope& scope, const std::string& name );

	/// Destruction
	virtual ~NamedElement();

	/// Get the object's name
	const std::string& getName() const;

private:
	/// The name of an object
	std::string name_;
};

//------------------------------------------------------construction/destruction

inline NamedElement::NamedElement( NameScope& scope, const std::string& name )
{
	name_ = scope.createUniqueName( name );
}

inline NamedElement::~NamedElement()
{
}

//-----------------------------------------------------------------------getters

inline const std::string& NamedElement::getName() const
{
	return name_;
}

} // namespace Log

#endif /* LOG_NAMEDELEMENT_INCLUDED */
