//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file DeclareSqlColumnTypes.h
 * @author Ronald Kluth
 * @date created at 2009/06/19
 * @brief Declaration and implementation of macro CPPLOG_DECLARE_SQL_COLUMN_TYPES
 * @since 0.1
 */

#ifndef LOG_DECLARESQLCOLUMNTYPES_INCLUDED
#define LOG_DECLARESQLCOLUMNTYPES_INCLUDED

#ifdef _MSC_VER
#include <tuple>
#include <array>
#else
#include <tuple>
#include <array>
#endif


/**
 * @def CPPLOG_DECLARE_SQL_COLUMN_TYPES
 * @brief This macro creates an array of SQL types matching an info type list
 * @param PP_InfoListName Type name of the info type list
 * @param PP_ArrayName Name of the array of SQL data types to be created
 *
 * The automatic extraction of info data from @c Record objects is based on
 * iterating over info type lists. Writing into a database requires, however,
 * that table columns have suitable SQL types that can stores the data
 * transported by an info type. This macro is provided for the comfortable
 * creation of SQL table definitions simply by iterating over the info type
 * list and an array of matching column types, which is created by this macro.
 *
 * @see CPPLOG_DECLARE_INFO_TYPE_LIST
 */
#define CPPLOG_DECLARE_SQL_COLUMN_TYPES( PP_InfoListName, PP_ArrayName, ... ) \
	std::array< std::string, std::tuple_size< PP_InfoListName >::value > \
	PP_ArrayName = {{ __VA_ARGS__ }};

#endif /* LOG_DECLARESQLCOLUMNTYPES_INCLUDED */
