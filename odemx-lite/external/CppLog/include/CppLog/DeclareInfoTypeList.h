//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file DeclareInfoTypeList.h
 * @author Ronald Kluth
 * @date created at 2009/06/09
 * @brief Declaration and implementation of macro CPPLOG_DECLARE_INFO_TYPE_LIST
 * @since 0.1
 */

#ifndef LOG_DECLAREINFOTYPELIST_INCLUDED
#define LOG_DECLAREINFOTYPELIST_INCLUDED

#ifdef _MSC_VER
#include <tuple>
#else
#include <tuple>
#endif

/**
 * @def CPPLOG_DECLARE_INFO_TYPE_LIST( PP_InfoListName, ... )
 * @brief This macro allows the creation of info type lists
 * @param PP_InfoListName Type name of the list of info types
 *
 * Lists of info types are used in the automatic extraction of the data
 * transported by Record objects. The first macro parameter specifies the
 * type name of the list type, followed by a variable number of parameters,
 * all of which must be info types declared with @c CPPLOG_DECLARE_INFO_TYPE.
 * The implementation of these type lists is provided by the standard library
 * class template @c tuple (added in TR1). The two helper templates
 * @c tuple_size and @c tuple_element are used to iterate over the list at
 * compile time.
 *
 * @see CPPLOG_DECLARE_INFO_TYPE
 */
#define CPPLOG_DECLARE_INFO_TYPE_LIST( PP_InfoListName, ... ) \
	typedef std::tuple < __VA_ARGS__ > PP_InfoListName;

#endif /* LOG_DECLAREINFOTYPELIST_INCLUDED */
