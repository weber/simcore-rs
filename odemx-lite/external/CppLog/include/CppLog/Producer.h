//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file CppLog/Producer.h
 * @author Ronald Kluth
 * @date created at 2009/06/06
 * @brief Declaration and implementation of class template Producer
 * @since 0.1
 */

#ifndef LOG_PRODUCER_INCLUDED
#define LOG_PRODUCER_INCLUDED

#include "ChannelManager.h"
#include "Enable.h"
#include "NamedElement.h"

namespace Log {

/**
 * @brief Producer template type, ensures that all producers have unique names
 * @tparam EnableT The type of channels to be enabled for the producer
 *
 * The producer will automatically contain pointers to enabled channels, but
 * these are not initialized. This is the responsibility of the subclass.
 */
template < typename EnableT >
class Producer
:	public NamedElement
,	public EnableT::Type
{
public:
	/**
	 * @brief Construction
	 * @param scope Reference to the name scope which ensures unique names
	 * @param name Name of the log producer object, unique within the given scope
	 */
	Producer( NameScope& scope, std::string const& name )
	:	NamedElement( scope, name )
	{}

	/**
	 * @brief Construction with channel manager
	 * @param manager Reference to a channel manager
	 * @param name Name of the log producer object, unique within manager's scope
	 * @note This constructor can only be used if all of the producer's channels
	 * use the same record type, because the enabled channels are automatically
	 * initialized during construction
	 */
	template < typename RecordT >
	Producer( ChannelManager< RecordT >& manager, std::string const& name )
	:	NamedElement( manager, name )
	,	EnableT::Type( manager )
	{}

	/// Destruction
	virtual ~Producer() {}

private:
	/// Non-copyable
	Producer( Producer const& );
	/// Non-assignable
	Producer& operator=( Producer const& );
};

} // namespace Log

#endif /* LOG_PRODUCER_INCLUDED */
