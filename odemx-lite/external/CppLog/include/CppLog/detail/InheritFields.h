//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file InheritFields.h
 * @author Ronald Kluth
 * @date created at 2009/12/17
 * @brief Declaration and implementation of class template InheritFields
 * @since 0.1
 */

#ifndef LOG_DETAIL_INHERITFIELDS
#define LOG_DETAIL_INHERITFIELDS

namespace Log {
namespace Detail {

/**
 * @brief Helper class template to inherit from a maximum of 10 ChannelField specializations
 */
template <
	typename T0,
	typename T1,
	typename T2,
	typename T3,
	typename T4,
	typename T5,
	typename T6,
	typename T7,
	typename T8,
	typename T9
	>
struct InheritFields
:	public T0
,	public T1
,	public T2
,	public T3
,	public T4
,	public T5
,	public T6
,	public T7
,	public T8
,	public T9
{
	InheritFields(){}
	template< typename ManagerT >
	InheritFields( ManagerT& manager )
	:	T0( manager )
	,	T1( manager )
	,	T2( manager )
	,	T3( manager )
	,	T4( manager )
	,	T5( manager )
	,	T6( manager )
	,	T7( manager )
	,	T8( manager )
	,	T9( manager )
	{}
};

} } // namespace Log::Detail

#endif /* LOG_DETAIL_INHERITFIELDS */
