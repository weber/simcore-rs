//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file Bindings.h
 * @author Ronald Kluth
 * @date 2009/11/28
 * @brief Implementation of template specialization Poco::Data::Binding<boost::log::string_literal>
 * @since 0.1
 */

#ifndef LOG_DETAIL_BINDINGS_INCLUDED
#define LOG_DETAIL_BINDINGS_INCLUDED

#include "string_literal.hpp"

#include <Poco/Data/Binding.h>

namespace Poco {
namespace Data {

/**
 * @brief Template specialization for database insertion of boost::log::string_literal
 */
template <>
class Binding< boost::log::string_literal >: public AbstractBinding
	/// Binding const char* specialization wraps char pointer into string.
{
public:
	explicit Binding( const boost::log::string_literal& pVal,
		const std::string& name = "",
		Direction direction = PD_IN):
		AbstractBinding(name, direction),
		_val( pVal.c_str() ),
		_bound(false)
		/// Creates the Binding by copying the passed string.
	{
	}

	~Binding()
		/// Destroys the Binding.
	{
	}

	std::size_t numOfColumnsHandled() const
	{
		return 1u;
	}

	std::size_t numOfRowsHandled() const
	{
		return 1u;
	}

	bool canBind() const
	{
		return !_bound;
	}

	void bind(std::size_t pos)
	{
		poco_assert_dbg(getBinder() != 0);
		TypeHandler<std::string>::bind(pos, _val, getBinder(), getDirection());
		_bound = true;
	}

	void reset ()
	{
		_bound = false;
		AbstractBinder* pBinder = getBinder();
		poco_check_ptr (pBinder);
		pBinder->reset();
	}

private:
	std::string _val;
	bool        _bound;
};

/**
 * @brief Template specialization for database insertion of boost::log::string_literal
 */
template <>
class CopyBinding< boost::log::string_literal >: public AbstractBinding
	/// Binding const char* specialization wraps char pointer into string.
{
public:
	explicit CopyBinding( const boost::log::string_literal& pVal,
		const std::string& name = "",
		Direction direction = PD_IN):
		AbstractBinding(name, direction),
		_val( pVal.c_str() ),
		_bound(false)
		/// Creates the Binding by copying the passed string.
	{
	}

	~CopyBinding()
		/// Destroys the Binding.
	{
	}

	std::size_t numOfColumnsHandled() const
	{
		return 1u;
	}

	std::size_t numOfRowsHandled() const
	{
		return 1u;
	}

	bool canBind() const
	{
		return !_bound;
	}

	void bind(std::size_t pos)
	{
		poco_assert_dbg(getBinder() != 0);
		TypeHandler<std::string>::bind(pos, _val, getBinder(), getDirection());
		_bound = true;
	}

	void reset ()
	{
		_bound = false;
		AbstractBinder* pBinder = getBinder();
		poco_check_ptr (pBinder);
		pBinder->reset();
	}

private:
	std::string _val;
	bool        _bound;
};

} } // namespace Poco::Data

#endif /* LOG_DETAIL_BINDINGS_INCLUDED */
