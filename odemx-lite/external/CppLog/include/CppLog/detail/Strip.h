//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file Strip.h
 * @author Ronald Kluth
 * @date created at 2009/09/06
 * @brief Declaration and implementation of class template Strip
 * @since 0.1
 */

#ifndef LOG_DETAIL_STRIP_INCLUDED
#define LOG_DETAIL_STRIP_INCLUDED

namespace Log {
namespace Detail {

/**
 * @brief Strips pointer or reference types to enable use of the real type
 *
 * This is a helper class for templates that need to acquire the basic type
 * of a parameter type, even if a pointer or reference was given. Used to
 * implement a specialization of InfoType so it can hold pointer types.
 *
 * @see InfoType
 */
template < typename T >
struct Strip
{
	typedef T Type;
	typedef T* Ptr;
	typedef T& Ref;
};

#ifndef DOXYGEN_SKIP
/// Specialization for pointer types
#endif
template < typename T >
struct Strip< T* >
{
	typedef T Type;
	typedef T* Ptr;
	typedef T& Ref;
};

#ifndef DOXYGEN_SKIP
/// Specialization for reference types
#endif
template < typename T >
struct Strip< T& >
{
	typedef T Type;
	typedef T* Ptr;
	typedef T& Ref;
};

} } // namespace Log::Detail

#endif /* LOG_DETAIL_STRIP_INCLUDED */
