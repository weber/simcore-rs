//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file ChannelField.h
 * @author Ronald Kluth
 * @date created at 2009/11/05
 * @brief Declaration of class template ChannelField
 * @since 0.1
 */

#ifndef LOG_CHANNELFIELD_INCLUDED
#define LOG_CHANNELFIELD_INCLUDED

#include "../ChannelManager.h"

namespace Log {
namespace Detail {

/**
 * @brief Class template for adding producer channel members, default is empty
 *
 * This template is used by the macro @c CPPLOG_DECLARE_CHANNEL, which defines
 * specializations containing a named channel pointer member. These
 * specializations are then inherited to add channel members to producers.
 */
template < int ID >
struct ChannelField
{
	ChannelField(){}
	template< typename ManagerT >
	ChannelField( ManagerT& ignored ){}
};

} } // namespace Log::Detail

#endif /* LOG_CHANNELFIELD_INCLUDED */
