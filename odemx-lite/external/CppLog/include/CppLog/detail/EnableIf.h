//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file EnableIf.h
 * @author Ronald Kluth
 * @date created at 2009/11/25
 * @brief Declaration and implementation of class template EnableIf
 * @since 0.1
 */

#ifndef LOG_DETAIL_ENABLEIF_INCLUDED
#define LOG_DETAIL_ENABLEIF_INCLUDED

namespace Log {
namespace Detail {

/**
 * @brief EnableIf implementation, matches @c true, has @c Type definition
 */
template < bool B, typename T = void >
struct EnableIfImpl
{
	typedef T Type;
};

/**
 * @brief EnableIf implementation, @c false specialization, has no @c Type definition
 */
template < typename T >
struct EnableIfImpl< false, T >
{
};

/**
 * @brief Enables templates by checking a condition (e.g. using type traits)
 * @note This technique is based on boost::enable_if
 * @see InfoType
 *
 * This template is used to select template specializations, for example by
 * checking type traits such as @c is_pointer or @c is_reference.
 */
template < typename Cond, typename T = void >
struct EnableIf: public EnableIfImpl< Cond::value, T >
{
};

} } // namespace Log::Detail

#endif /* LOG_DETAIL_ENABLEIF_INCLUDED */
