//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file DebugOut.h
 * @author Ronald Kluth
 * @date created at 2009/11/25
 * @brief Declaration and implementation of class DebugOut and stream inserter
 * @since 0.1
 */

#ifndef LOG_DETAIL_DEBUGOUT_INCLUDED
#define LOG_DETAIL_DEBUGOUT_INCLUDED

#include <iostream>
#include <string>

namespace Log {
namespace Detail {

/**
 * @brief Helper struct to quickly add scoped stdout debugging
 *
 * When an object of this type is declared with its scope given as string,
 * it will print 'entering scope' upon construction and 'leaving scope' upon
 * destruction. During its lifetime, the object can then be used like an
 * @c std::ostream to print arbitrary data to stdout.
 */
struct DebugOut
{
	DebugOut( const std::string& scope ): scope_( scope )
	{
		std::cout << "entering " << scope_ << std::endl;
	}
	~DebugOut()
	{
		std::cout << "leaving " << scope_ << std::endl;
	}
	std::string scope_;
};

/**
 * @brief Overload of the stream inserter op<< for DebugOut and arbitrary values
 * @tparam T Type of the value to be printed to @c stdout
 * @param out Reference to the DebugOut object
 * @param t Value to be printed to @c stdout
 * @return @c out to enable chaining
 */
template < typename T >
const DebugOut& operator<<( const DebugOut& out, const Strip<T>::Ref t )
{
	// shortcut to cout
	std::cout << t << std::flush;
	return out;
}

} } // namespace Log::Detail

#endif /* LOG_DETAIL_DEBUGOUT_INCLUDED */
