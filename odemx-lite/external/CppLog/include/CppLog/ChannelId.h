//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file ChannelId.h
 * @author Ronald Kluth
 * @date created at 2009/03/11
 * @brief Declaration of type ChannelId
 * @since 0.1
 */

#ifndef LOG_CHANNELID_INCLUDED
#define LOG_CHANNELID_INCLUDED

namespace Log {

/**
 * @typedef ChannelId
 * @brief An alias to make the use of channel IDs more obvious in the code.
 *
 * Every @c Channel object can be constructed with an ID. The default is -1,
 * meaning that it is not set. Channel IDs can be used by consumers to identify
 * the channel that forwarded a log record via call to @c consume. The consumer
 * interface ensures that the @c ChannelId of the caller is given.
 *
 * @see Channel, Consumer
 */
typedef int ChannelId;

} // namespace Log

#endif /* LOG_CHANNELID_INCLUDED */
