//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file Enable.h
 * @author Ronald Kluth
 * @date 2010/04/24
 * @brief Declaration and implementation of class template Enable
 * @since 0.1
 */

#ifndef LOG_ENABLE_INCLUDED
#define LOG_ENABLE_INCLUDED

#include "detail/ChannelField.h"
#include "detail/InheritFields.h"

namespace Log {

namespace Detail { enum { UNUSED = -100 }; }

/**
 * @brief Allows enabling of Producer channel members by providing declared channel IDs
 * @note The unused IDs will match the general template of ChannelField, which
 * becomes an empty class. Only IDs declared with CPPLOG_DECLARE_CHANNEL
 * provide a specialization of ChannelField to be derived from.
 * @note Declared IDs should not be negative numbers. The numbers for unused
 * channels must be different so we do not derive from the same class twice.
 */
template <
	int ID0,
	int ID1 = Detail::UNUSED-1,
	int ID2 = Detail::UNUSED-2,
	int ID3 = Detail::UNUSED-3,
	int ID4 = Detail::UNUSED-4,
	int ID5 = Detail::UNUSED-5,
	int ID6 = Detail::UNUSED-6,
	int ID7 = Detail::UNUSED-7,
	int ID8 = Detail::UNUSED-8,
	int ID9 = Detail::UNUSED-9
	>
struct Enable
{
	typedef Detail::InheritFields<
		Detail::ChannelField< ID0 >,
		Detail::ChannelField< ID1 >,
		Detail::ChannelField< ID2 >,
		Detail::ChannelField< ID3 >,
		Detail::ChannelField< ID4 >,
		Detail::ChannelField< ID5 >,
		Detail::ChannelField< ID6 >,
		Detail::ChannelField< ID7 >,
		Detail::ChannelField< ID8 >,
		Detail::ChannelField< ID9 >
		>
	Type;
};

} // namespace Log

#endif /* LOG_ENABLE_INCLUDED */
