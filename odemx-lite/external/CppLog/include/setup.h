//
// Copyright (c) 2009-2010 Ronald Kluth
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

/**
 * @file include/setup.h
 * @author Ronald Kluth
 * @date 2010/04/24
 * @brief Preprocessor macros for compiling CppLog with different settings
 * @since 0.1
 */

#ifndef CPPLOG_SETUP_INCLUDED
#define CPPLOG_SETUP_INCLUDED

/**
 * @def CPPLOG_USE_DATABASE
 * @brief Enables the component DatabaseAccessor
 *
 * The database component requires the library POCO Data. Hence, this
 * define should only be activated when POCO is present in the compiler's
 * include path.
 */
//#define CPPLOG_USE_DATABASE

/**
 * @def CPPLOG_USE_SQLITE
 * @brief Enables the SQLite database connectivity
 *
 * The default database management system is the embedded library SQLite
 * that comes with POCO Data. It is enabled by using this define.
 */
//#define CPPLOG_USE_SQLITE

/**
 * @def CPPLOG_USE_ODBC
 * @brief Enables the ODBC database connectivity
 *
 * POCO's ODBC-Connector can be used by enabling this define. Some Methods
 * of class DatabaseAccessor will need other defines to be enabled, which
 * specify the external database.
 *
 * @see CPPLOG_USE_SQLITE, CPPLOG_USE_POSTGRESQL
 */
//#define CPPLOG_USE_ODBC

/**
 * @def CPPLOG_USE_POSTGRESQL
 * @brief Enables ODBC and some PostgreSQL-specific SQL syntax
 *
 * Since all DBMS use different SQL dialects it became necessary to make the
 * distinction between specific databases in order to enable some concurrency
 * functionality.
 *
 * @see CPPLOG_USE_SQLITE, CPPLOG_USE_ODBC
 */
//#define CPPLOG_USE_POSTGRESQL

//------------------------------------------------------------------dependencies

// if any of the database defines is enabled, compile DatabaseAccessor
#ifdef CPPLOG_USE_SQLITE
	#define CPPLOG_USE_DATABASE
#endif

// if PostgreSQL is enabled, enable DatabaseAccessor and ODBC
#ifdef CPPLOG_USE_POSTGRESQL
	#define CPPLOG_USE_DATABASE
	#define CPPLOG_USE_ODBC
#endif

#endif /* CPPLOG_SETUP_INCLUDED */
