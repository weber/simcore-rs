# A Minimalistic Simulation Framework in Rust

## Introduction

This Rust project provides a framework for creating and running simulations using asynchronous processes. By leveraging Rust's concurrency features, it allows you to simulate various scenarios, making it a valuable tool for studying the mechanics of simulation libraries in projects involving complex, process-driven models.

## Features

The project includes:

- **Core Library (`lib.rs`)**: Defines reusable components for building simulators.
- **Utility Modules (`util.rs`)**: Provides support for common simulation tasks.
- **Example Models**: Demonstrates different aspects of the framework through three examples:
  - `barbershop`
  - `ferry`
  - `philosophers`
- **SLX Code (`slx`)**: Contains SLX code for the same examples, including benchmarking provisions with real-time measurements.
- **C++ Code (`cpp`)**: Contains three subprojects for the examples using ODEMx.
- **C Code (`c`)**: Demonstrates the transformation of an ordinary routine into a coroutine for a decompressor-tokenizer example.
- **ODEMx (`odemx-lite`)**: A clean version without dependencies, compiled using a C++14-conforming compiler to run benchmarks.

## Getting Started

### Prerequisites

- **Rust Compiler**: Latest stable version is recommended. [Install Rust][Rust-compiler]
- **C++14-Conforming Compiler**: Required for the ODEMx benchmarks. Popular compilers like MSVC, Clang, MinGW, and GCC should work out of the box.
- **SLX Compiler**: Required for the SLX benchmarks (both student and full versions work).

#### Note on SLX Compiler

Currently, obtaining the SLX compiler may be challenging because the creator, James O. Henriksen, passed away in 2020, and the company's original website is no longer available. If you wish to obtain the free version of SLX for benchmarking, please contact me directly.

### Building the Project

To build the project, run:

```sh
cargo build
```

### Running the Examples

The examples are located in the `examples` directory and demonstrate different types of simulations:

- **Barbershop**: A discrete event simulation of a barbershop where customers arrive randomly, wait if the barber is busy, receive service, and depart. It tracks customer wait times and provides statistical analysis over the simulated period. This is a minimalistic model containing processes and time- and state-based synchronization.

- **Ferry**: Simulates a car-ferry system where cars arrive at multiple harbors and wait to be transported by ferries to other harbors. It tracks statistics like car wait times, ferry cargo lengths, and ferry load times. This example demonstrates complex synchronization, featuring channels and speculative execution.

- **Philosophers**: Simulates the classic Dining Philosophers problem, where philosophers alternate between thinking and eating, requiring coordination to avoid deadlocks. It tracks the time until a deadlock occurs and collects statistical data over multiple simulation runs. This example highlights parallel execution of multiple simulations and concurrent resource access.

To run an example, use:

```sh
cargo run --example <example_name>
```

For example, to run the barbershop simulation:

```sh
cargo run --example barbershop
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Acknowledgments

Special thanks to the contributors of the Rust community for creating powerful tools that make projects like this
possible. Extra-special thanks to Lukas Markeffsky for helping me to refine this library both through fruitful
discussions and by resolving its soundness problems through a refactoring of the code.

[Rust-compiler]: https://rust.sh
