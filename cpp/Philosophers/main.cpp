#include <odemx/odemx.h>
#include <iostream>
#include <vector>
#include <memory>

const int PHILOSOPHER_COUNT =   5;
const int EXPERIMENT_COUNT  = 500;

using odemx::base::Simulation;
using odemx::base::Process;
using odemx::synchronization::Bin;
using odemx::statistics::Tally;
using odemx::data::output::OStreamReport;

namespace Philosophers {
	///@brief Passive class used to model the table that the philosophers are
	/// seated around
	class Table {
		///@brief A vector for the forks.
		std::vector<std::unique_ptr<Bin>> forks;
		
	public:
		///@brief Constructor initializing the fork-vector.
		Table(Simulation& sim, unsigned int forks) {
			for (int i = 0; i < forks; ++i) {
				this->forks.push_back(std::make_unique<Bin>(sim, "Fork", 1));
			}
		}
		
		///@brief Blocks until the requested fork is available and acquires it.
		void acquire_fork(unsigned int i) {
			i %= forks.size();
			forks[i]->take(1);
		}
		
		///@brief Returns the requested fork (presumably wiped clean) to the 
		/// table.
		void release_fork(unsigned int i) {
			i %= forks.size();
			forks[i]->give(1);
		}
	};
	
	///@brief An active philosopher, philosophising and eating at the table.
	class Philosopher: public Process {
		///@brief The table.
		Table& table;
		///@brief The seat number of this particular philosopher.
		unsigned int seat;
		
	public:
		///@brief Constructor initializing table and seat.
		Philosopher(Simulation& sim, Table& table, unsigned int seat)
		:	Process(sim, "Philosopher"),
			table(table),
			seat(seat)
		{}
		
		///@brief Method containing the lifecycle of the philosopher.
		int main() override {
			using namespace odemx::random;
			NegativeExponential thinking_duration(getSimulation(), "thinking", 1.0);
			Uniform artificial_delay(getSimulation(), "delay", 0.1, 0.2);
			Normal eating_duration(getSimulation(), "eating", 0.5, 0.2);
			
			while (true) {
				double duration;
				
				// spend some time pondering the nature of things
				holdFor(thinking_duration.sample());
				
				// acquire the first fork
				table.acquire_fork(seat);
				
				// introduce an artificial delay to leave room for deadlocks
				holdFor(artificial_delay.sample());
				
				// acquire the second fork
				table.acquire_fork(seat + 1);
				
				// spend some time eating
				do duration = eating_duration.sample();
				while (duration < 0.0);
				holdFor(duration);
				
				// release the forks
				table.release_fork(seat + 1);
				table.release_fork(seat);
			}
			
			return 0;
		}
	};
	
	///@brief A dining-philosopher-specific simulation class.
	class Sim: public Simulation {
		///@brief The table.
		Table table;
		///@brief A vector storing all of the philosophers.
		std::vector<std::unique_ptr<Philosopher>> philo;
		
	public:
		///@brief Constructor populating the philosopher vector based on the
		/// requested entry-count.
		Sim(unsigned int count)
		:	Simulation("Dining Philosophers"),
			table(*this, count)
		{
			// create the philosopher-processes
			for (int i = 0; i < count; ++i) {
				philo.push_back(std::make_unique<Philosopher>(*this, table, i));
			}
		}
		
		///@brief Runs multiple simulations in quick succession and returns
		/// statistical information about the model-times recorded before a
		/// deadlock occurred.
		static Tally main(unsigned int count, unsigned int reruns) {
			Tally sim_duration(odemx::getDefaultSimulation(), "Sim Duration");
			unsigned long seed = 907;
			
			// run the simulation a number of times
			for (unsigned int i = 0; i < reruns; ++i) {
				// create a new simulation context
				Sim sim(count);
				
				// set the seed for the simulation's rng
				sim.setSeed(seed);
				
				// run until no more processes can be scheduled;
				// this would indicate a deadlock-situation
				sim.run();
				
				// tabulate the latest simulation time
				sim_duration.update(sim.getTime());
				
				// extract the seed for the next simulation
				seed = sim.getNextSeed();
			}
			
			return sim_duration;
		}
		
	protected:
		///@brief Automatically called method activating the philosophers.
		void initSimulation() override {
			// activate the philosopher-processes
			for (auto& p: philo) { p->hold(); }
		}
	};
}

#ifdef RUST_FFI
///@brief Entry point for the benchmarking function.
extern "C" void philosophers(unsigned int count, unsigned int reruns) {
	Philosophers::Sim::main(count, reruns);
}
#else
int main() {
	Tally sim_duration =
		Philosophers::Sim::main(PHILOSOPHER_COUNT, EXPERIMENT_COUNT);
	
	// generate the report
	OStreamReport report(std::cout);
	report.addReportProducer(sim_duration);
	report.generateReport();
	return 0;
}
#endif
