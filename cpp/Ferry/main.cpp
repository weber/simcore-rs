
#include <odemx/odemx.h>
#include <vector>
#include <memory>
#include <iostream>
#include <utility>

using odemx::base::SimTime;
using odemx::base::Process;
using odemx::base::Simulation;
using odemx::synchronization::PortHeadT;
using odemx::synchronization::PortTailT;
using odemx::synchronization::Timer;
using odemx::synchronization::IMemory;
using odemx::random::NegativeExponential;
using odemx::random::Normal;
using odemx::statistics::Tally;

const SimTime      SIM_DURATION    = 24.0*60.0*7.0;
const SimTime      HARBOR_DISTANCE = 10.0;
const SimTime      FERRY_TIMEOUT   =  5.0;
const unsigned int FERRY_CAPACITY  =  5;
const unsigned int FERRY_COUNT     =  2;
const unsigned int HARBOR_COUNT    =  4;

namespace Ferry {
	// forward declarations
	struct Car;
	class Pier;
	class Ferry;
	
	// These are just ODEMx-flavored sender/receiver-pairs for the same channel.
	typedef odemx::synchronization::PortTailT<Car> PortTail;
	typedef odemx::synchronization::PortHeadT<Car> PortHead;
	
	///@brief A car-ferry-specific simulation class.
	class Sim: public Simulation {
		///@brief A vector for all of the harbors.
		std::vector<std::unique_ptr<Pier>> piers;
		///@brief A vector for all of the ferries.
		std::vector<std::unique_ptr<Ferry>> ferries;
		
	public:
		///@brief Constructor setting up the number of harbors and ferries.
		Sim(unsigned int ferries, unsigned int harbors);
		
		///@brief Cleans up the model by accounting for all of the remaining
		/// cars of the harbors.
		void finalize();
		
		///@brief Statistical aggregations.
		Tally ferry_cargo_len;
		Tally ferry_load_time;
		Tally car_wait_time;
		
	protected:
		///@brief Automatically called method activating all of the ferries and
		/// harbors.
		void initSimulation() override;
	};
	
	///@brief A passive structure for cars.
	struct Car {
		SimTime arrival_time;
		SimTime load_duration;
	};
	
	///@brief The process modeling a harbor with arriving cars.
	class Pier: public Process {
		///@brief Writing end for the queue with arriving cars.
		PortTail::Ptr landing_site;
		
		///@brief Returns an upcasted version of the basic simulation.
		Sim& getSimulation() {
			return static_cast<Sim&>(Process::getSimulation());
		}
		
	public:
		///@brief The constructor initializing the car queue.
		Pier(Sim& sim)
		:	Process(sim, "Harbor"),
			landing_site(PortTail::create(sim, "Harbor Port"))
		{}
		
		///@brief Cleans up the harbor by accounting for all of the cars that
		/// are still waiting for a ferry.
		void finalize() {
			unsigned int count = landing_site->count();
			PortHead::Ptr head = landing_site->getHead();
			Sim& sim = getSimulation();
		
			// take cars into account that weren't picked up by a ferry
			for (unsigned int i = 0; i < count; ++i) {
				sim.car_wait_time.update(sim.getTime() - head->get()->arrival_time);
			}
		}
		
		///@brief Method providing access to the reading end of the car queue.
		inline PortHead::Ptr getHead() {
			return landing_site->getHead();
		}
		
		///@brief Models the lifecycle of the harbor.
		int main() override {
			NegativeExponential arrival_delay(getSimulation(), "Arrival Delay", 0.1);
			Normal loading_delay(getSimulation(), "Loading Delay", 0.5, 0.2);
		
			while (true) {
				double load_duration;
			
				holdFor(arrival_delay.sample());
			
				do load_duration = loading_delay.sample();
				while (load_duration < 0.0);
			
				landing_site->put(Car {
					getTime(), load_duration
				});
			}
		
			return 0;
		}
	};
	
	///@brief The process modeling an individual ferry.
	class Ferry: public Process {
		///@brief Space for the cars.
		std::vector<Car> cargo;
		///@brief Maximum car capacity.
		unsigned int capacity;
		///@brief Time to wait for another car before leaving before filling all
		/// of the available cargo space.
		SimTime timeout;
		///@brief Travel time between (any) two harbors.
		SimTime travel_time;
		///@brief Vector of reading ends for all of the harbors.
		std::vector<PortHead::Ptr> piers;
		
		///@brief Returns an upcasted version of the basic simulation.
		Sim& getSimulation() {
			return static_cast<Sim&>(Process::getSimulation());
		}
		
	public:
		///@brief Constructor.
		Ferry(Sim& sim, unsigned int capacity,
		      SimTime timeout, SimTime travel_time,
			  std::vector<PortHead::Ptr>&& piers)
	  	:	Process(sim, "Ferry"),
	  		capacity(capacity),
	  		timeout(timeout),
	  		travel_time(travel_time),
	  		piers(piers)
	  	{}
		
		///@brief Models the lifecycle of the ferry.
		int main() override {
			Timer timer(getSimulation(), "FerryTimer");
			IMemory* notifier;
			Car car;
		
			while (true) {
				for (auto& pier: piers) {
					// unload the cars
					for (auto& car: cargo) {
						holdFor(car.load_duration);
					}
					cargo.clear();
				
					SimTime begin_loading = getTime();
				
					// wait until new cars arrive or a timeout occurs
					while (cargo.size() < capacity) {
						timer.setIn(timeout);
						notifier = wait(&timer, pier.get());
					
						if (notifier != &timer) {
							// a car arrived in time
							car = *pier->get();
							timer.stop();
							getSimulation()
								.car_wait_time
								.update(getTime() - car.arrival_time);
							holdFor(car.load_duration);
							cargo.push_back(car);
						} else {
							// the timeout has been triggered
							break;
						}
					}
				
					getSimulation()
						.ferry_load_time
						.update(getTime() - begin_loading);
					getSimulation()
						.ferry_cargo_len
						.update(cargo.size());
				
					// travel to the next harbor
					holdFor(travel_time);
				}
			}
		
			return 0;
		}
	};
	
	// Implementation of the simulation class.
	
	Sim::Sim(unsigned int ferries, unsigned int harbors)
	:	Simulation("Ferry"),
		ferry_cargo_len(*this, "Ferry Cargo Len"),
		ferry_load_time(*this, "Ferry Load Time"),
		car_wait_time(*this, "Car Wait Time")
	{
		// create all of the harbors
		for (unsigned int i = 0; i < harbors; ++i) {
			piers.push_back(std::make_unique<Pier>(*this));
		}
		
		// create all of the ferries
		for (unsigned int i = 0; i < ferries; ++i) {
			std::vector<PortHead::Ptr> ports;
	
			for (unsigned int j = i; j < harbors; ++j) {
				ports.push_back(piers[j]->getHead());
			}
			
			for (unsigned int j = 0; j < i; ++j) {
				ports.push_back(piers[j]->getHead());
			}
			
			this->ferries.push_back(std::make_unique<Ferry>(
				*this, FERRY_CAPACITY, FERRY_TIMEOUT, HARBOR_DISTANCE,
				std::move(ports)
			));
		}
	}
	
	void Sim::finalize() {
		for (auto& pier: piers) {
			pier->finalize();
		}
	}
	
	void Sim::initSimulation() {
		// activate the harbors
		for (auto& pier: piers) {
			pier->activate();
		}

		// activate the ferries
		for (auto& ferry: ferries) {
			ferry->activate();
		}
	}
}

#ifdef RUST_FFI
///@brief Entry point for the function used to benchmark this scenario.
extern "C" void ferry(SimTime duration, unsigned int ferries, unsigned int harbors) {
	// set up the simulation model
	Ferry::Sim sim(ferries, harbors);
	sim.runUntil(duration);
	sim.finalize();
	
	// no report
}
#else
int main() {
	// set up the simulation model
	Ferry::Sim sim(FERRY_COUNT, HARBOR_COUNT);
	sim.runUntil(SIM_DURATION);
	sim.finalize();
	
	// generate the report
	odemx::data::output::OStreamReport report(std::cout);
	report.addReportProducer(sim.ferry_cargo_len);
	report.addReportProducer(sim.ferry_load_time);
	report.addReportProducer(sim.car_wait_time);
	report.generateReport();
	
	return 0;
}
#endif
