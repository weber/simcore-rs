#include <odemx/odemx.h>
#include <vector>
#include <memory>
#include <iostream>

using odemx::base::SimTime;
using odemx::base::Process;
using odemx::base::Simulation;
using odemx::synchronization::Bin;
using odemx::random::Uniform;
using odemx::statistics::Tally;
using odemx::data::output::OStreamReport;

// helper constants
const SimTime SIM_DURATION = 24.0*60.0*7.0*3.0;

/**
 * @brief The simulation context for a barbershop.
 * 
 * This class simulates a barbershop where customers arrive and are served by
 * a single barber. It includes random number distributions for the time between
 * customers and the time for each customer, a barber that can only serve one
 * customer at a time, and statistics collection for the time customers spend
 * waiting.
 */
struct Barbershop: Simulation {
	///@brief The random number distribution for the time between customers.
	Uniform dist_a;
	///@brief The random number distribution for the time for each customer.
	Uniform dist_s;
	///@brief The barber that can only serve one customer concurrently.
	Bin joe;
	///@brief Statistics collection for the time customers spend waiting.
	Tally wait_time;
	
	///@brief Pointer to the main process.
	std::unique_ptr<Process> main;
	
	///@brief Initializes the main process and the other attributes.
	Barbershop(SimTime duration);
	
protected:
	///@brief Automatically called method activating the main process.
	void initSimulation() override {
		main->activate();
	}
};

///@brief Active customer trying to get a haircut from Joe.
struct Customer: Process {
	///@brief Constructor.
	Customer(Barbershop& sim):	Process(sim, "Customer") {}
	
	///@brief Method containing the lifecycle of the customer.
	int main() override {
		auto& sim = static_cast<Barbershop&>(getSimulation());
		
		// access the barber and record the time for the report
		SimTime arrival_time = getTime();
		sim.joe.take(1);
		sim.wait_time.update(getTime() - arrival_time);
		
		// spend time
		holdFor(sim.dist_s.sample());
		// release the barber
		sim.joe.give(1);
		
		return 0;
	}
};

///@brief The process responsible for generating customers.
struct Generator: Process {
	///@brief The constructor.
	Generator(Barbershop& sim): Process(sim, "Generator") {}
	
	///@brief Generates customers .
	int main() override {
		auto& sim = static_cast<Barbershop&>(getSimulation());
		std::vector<std::unique_ptr<Customer>> customers;
		
		while (true) {
			holdFor(sim.dist_a.sample());
			customers.push_back(std::make_unique<Customer>(sim));
			customers.back()->activate();
		}
	}
};

///@brief The main process.
class SimMain: public Process {
	///@brief Time between opening and closing the shop.
	SimTime duration;
	///@brief Internal customer generator process.
	Generator generator;
	
public:
	///@brief The constructor.
	SimMain(Barbershop& sim, SimTime duration)
	:	Process(sim, "Main Process"),
		duration(duration),
		generator(sim)
	{}
	
	///@brief The method containing the lifecycle of the main process.
	int main() override {
		// activate a process to generate the customers
		generator.activate();
		
		// wait until the store closes
		holdFor(duration);
		generator.cancel();
		
		// finish processing the queue (no more customers arrive)
		return 0;
	}
};

// Simulation constructor moved down here to prevent backwards references.
Barbershop::Barbershop(SimTime duration)
:	Simulation("Barbershop"),
	dist_a(*this, "Time between customers", 12.0, 24.0),
	dist_s(*this, "Time for each customer", 12.0, 18.0),
	joe(*this, "Joe", 1),
	wait_time(*this, "Wait Time"),
	main(std::make_unique<SimMain>(*this, duration))
{}

#ifdef RUST_FFI
///@brief Entry point for the function used to benchmark this scenario.
extern "C" void barbershop(SimTime duration) {
	Barbershop(duration).run();
}
#else
int main() {
	// set up the simulation model
	Barbershop sim(SIM_DURATION);
	
	// run to completion
	sim.run();
	
	// generate the report
	OStreamReport report(std::cout);
	report.addReportProducer(sim.wait_time);
	report.generateReport();
	
	return 0;
}
#endif
