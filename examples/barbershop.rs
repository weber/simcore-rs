//! A discrete event simulation of a barbershop using `simcore_rs`, the
//! simulator core developed as part of my (Dorian Weber) dissertation.
//!
//! This module models a simple barbershop scenario where customers arrive at
//! random intervals, wait if the barber is busy, receive service, and then
//! depart. The simulation tracks customer wait times and provides statistical
//! analysis over the simulated period.
//!
//! # Notes
//! - The simulation assumes a continuous operation of the barbershop over the
//!   specified duration.
//! - Customers arriving after the shop closes are not admitted; however, the
//!   barber will finish servicing any remaining customers.

use rand::{rngs::SmallRng, Rng};
use simcore_rs::{
	util::{Facility, RandomVariable},
	Process, Sim, Time,
};
use std::cell::RefCell;

// Helper constants for random number generator seeds.
const SEED_A: u64 = 100_000;
const SEED_S: u64 = 200_000;

/// Globally shared data for the barbershop simulation.
struct Barbershop {
	/// Random number generator for customer arrival times.
	rng_a: RefCell<SmallRng>,
	/// Random number generator for service times.
	rng_s: RefCell<SmallRng>,
	/// Facility representing the barber (Joe).
	joe: Facility,
	/// Statistical accumulator for customer wait times.
	wait_time: RandomVariable,
}

/// Represents a customer in the barbershop simulation.
struct Customer;

impl Customer {
	/// Defines the actions performed by a customer in the simulation.
	///
	/// The customer arrives at the barbershop, waits for the barber to be
	/// available, gets a haircut (spends time being serviced), and then leaves.
	pub async fn actions(self, sim: Sim<'_, Barbershop>) {
		// Record the arrival time for wait time calculation.
		let arrival_time = sim.now();

		// Seize the barber (wait if not available).
		sim.global().joe.seize().await;

		// Calculate and record the customer's wait time.
		sim.global().wait_time.tabulate(sim.now() - arrival_time);

		// Simulate the time taken for the haircut.
		let cut = sim.global().rng_s.borrow_mut().gen_range(12.0..18.0);
		sim.advance(cut).await;

		// Release the barber for the next customer.
		sim.global().joe.release();
	}
}

/// The main simulation function.
///
/// This function initializes the customer arrival process, runs the simulation
/// for the specified duration, and ensures that all customers are serviced
/// before the simulation ends.
async fn sim_main(sim: Sim<'_, Barbershop>, duration: Time) {
	// Activate a process to generate customers at random intervals.
	sim.activate(async move {
		loop {
			// Wait for a random time before the next customer arrives.
			let wait_time = sim.global().rng_a.borrow_mut().gen_range(12.0..24.0);
			sim.advance(wait_time).await;

			// If the simulation time exceeds the duration, stop generating customers.
			if sim.now() >= duration {
				return;
			}

			// Activate a new customer process.
			sim.activate(Customer.actions(sim));
		}
	});

	// Run the simulation until the store closes.
	sim.advance(duration).await;

	// Ensure the barber finishes servicing any remaining customers.
	sim.global().joe.seize().await;
}

/// Entry point for the barbershop simulation.
///
/// Initializes the simulation environment and runs the simulation for a
/// specified duration.
#[cfg(not(test))]
fn main() {
	use rand::SeedableRng;

	// Run the simulation and collect the result.
	let result = simcore_rs::simulation(
		// Initialize the global data for the simulation.
		Barbershop {
			rng_a: RefCell::new(SmallRng::seed_from_u64(SEED_A)),
			rng_s: RefCell::new(SmallRng::seed_from_u64(SEED_S)),
			joe: Facility::new(),
			wait_time: RandomVariable::new(),
		},
		// Simulation entry point.
		|sim| Process::new(sim, sim_main(sim, 3.0 * 7.0 * 24.0 * 60.0)),
	);

	// Print statistics after the simulation ends.
	println!("wait_time: {:#.3?}", result.wait_time);
}

#[cfg(test)]
criterion::criterion_main!(bench::benches);

#[cfg(all(test, windows))]
mod slx;

/// Benchmark module for the barbershop simulation.
///
/// Uses the Criterion crate to benchmark the simulation with different
/// implementations and simulation durations.
#[cfg(test)]
mod bench {
	use super::*;
	use criterion::{
		criterion_group, AxisScale, BatchSize, BenchmarkId, Criterion, PlotConfiguration,
	};

	#[cfg(odemx)]
	mod odemx {
		use std::os::raw::c_double;

		#[link(name = "odemx", kind = "static")]
		extern "C" {
			pub fn barbershop(duration: c_double);
		}
	}

	#[cfg(windows)]
	const SLX_PATH: &'static str = "C:\\Wolverine\\SLX";
	const RANGE: u32 = 10;
	const STEP: Time = 1000.0;

	/// Benchmarks the barbershop simulation using different implementations and
	/// simulation durations.
	fn barbershop_bench(c: &mut Criterion) {
		use rand::{rngs::SmallRng, SeedableRng};

		let mut group = c.benchmark_group("Barbershop");

		// Set up the benchmark parameters.
		group.confidence_level(0.99);
		group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));

		#[cfg(windows)]
		let slx_path = {
			let path = slx::slx_version(SLX_PATH);

			if path.is_none() {
				println!("SLX not found, skipping SLX benchmarks!");
			} else {
				println!("Using SLX program at {:?}", path.as_ref().unwrap());
			}

			path
		};

		// Vary the length of the simulation run.
		for sim_duration in (0..RANGE).map(|c| Time::from(1 << c) * STEP) {
			#[cfg(windows)]
			if let Some(path) = slx_path.as_ref() {
				let duration = sim_duration.to_string();
				let args = [
					"/silent",
					"/stdout",
					"/noicon",
					"/nowarn",
					"/noxwarn",
					"/#BENCH",
					"slx\\barbershop.slx",
					duration.as_str(),
				];

				// Benchmark the SLX implementation.
				group.bench_function(BenchmarkId::new("SLX", sim_duration), |b| {
					b.iter_custom(|iters| {
						slx::slx_bench(path.as_os_str(), &args, iters as usize)
							.expect("couldn't benchmark the SLX program")
					})
				});
			}

			// Benchmark the C++ implementation.
			#[cfg(odemx)]
			group.bench_function(BenchmarkId::new("ODEMx", sim_duration), |b| {
				b.iter(|| unsafe { odemx::barbershop(sim_duration as _) })
			});

			// Benchmark the Rust implementation.
			group.bench_function(BenchmarkId::new("Rust", sim_duration), |b| {
				b.iter_batched(
					|| Barbershop {
						rng_a: RefCell::new(SmallRng::seed_from_u64(SEED_A)),
						rng_s: RefCell::new(SmallRng::seed_from_u64(SEED_S)),
						joe: Facility::new(),
						wait_time: RandomVariable::new(),
					},
					|shared| {
						simcore_rs::simulation(shared, |sim| {
							Process::new(sim, sim_main(sim, sim_duration))
						})
					},
					BatchSize::SmallInput,
				)
			});
		}

		group.finish();
	}

	criterion_group!(benches, barbershop_bench);
}
