//! A discrete event simulation of a car-ferry system using `simcore_rs`, the
//! simulator core developed as part of my (Dorian Weber) dissertation.
//!
//! This module models a ferry system where cars arrive at multiple harbors and
//! wait to be transported by ferries to other harbors. The simulation tracks
//! various statistics such as car wait times, ferry cargo lengths, and ferry
//! load times over the simulated period.
//!
//! # Notes
//!
//! - The simulation assumes continuous operation over the specified duration.
//! - Cars that are not picked up by the end of the simulation are accounted for
//!   in the wait time statistics.
//! - The ferry routes are set up in such a way that each ferry starts at a
//!   different harbor and cycles through all harbors.

use rand::{rngs::SmallRng, Rng, SeedableRng};
use rand_distr::{Distribution, Exp, Normal};
use simcore_rs::{
	util::{channel, select, RandomVariable, Receiver, Sender},
	Process, Sim, Time,
};
use std::cell::RefCell;

// Constants for simulation parameters.
const SEED: u64 = 100_000;
const FERRY_COUNT: usize = 2;
const HARBOR_COUNT: usize = 4;
const HARBOR_DISTANCE: Time = 10.0;
const FERRY_TIMEOUT: Time = 5.0;
const FERRY_CAPACITY: usize = 5;

/// Shared global data for the ferry simulation.
struct Ferries {
	/// Master random number generator for seeding.
	master_rng: RefCell<SmallRng>,
	/// Statistical accumulator for ferry cargo lengths.
	ferry_cargo_len: RandomVariable,
	/// Statistical accumulator for ferry load times.
	ferry_load_time: RandomVariable,
	/// Statistical accumulator for car wait times.
	car_wait_time: RandomVariable,
}

/// Represents a car in the simulation.
#[derive(Debug)]
struct Car {
	/// Time when the car arrives at the pier.
	arrival_time: Time,
	/// Duration required to load the car onto the ferry.
	load_duration: Time,
}

/// Represents a pier (harbor) where cars arrive and wait for ferries.
struct Pier {
	/// Random number generator specific to this pier.
	rng: SmallRng,
	/// Channel to send cars to the ferry.
	landing_site: Sender<Car>,
}

/// Represents a ferry that transports cars between harbors.
struct Ferry {
	/// Cargo hold containing the cars on the ferry.
	cargo: Vec<Car>,
	/// Timeout duration for ferry departure.
	timeout: Time,
	/// Time taken to travel between harbors.
	travel_time: Time,
	/// Receivers from piers to accept arriving cars.
	piers: Vec<Receiver<Car>>,
}

impl Pier {
	/// Simulates the actions of a pier in the simulation.
	///
	/// Cars arrive at the pier following an exponential distribution
	/// and are sent to the ferry when it arrives.
	async fn actions(mut self, sim: Sim<'_, Ferries>) {
		// Arrival and loading time distributions.
		let arrival_delay = Exp::new(0.1).unwrap();
		let loading_delay = Normal::new(0.5, 0.2).unwrap();

		loop {
			// Wait for the next car to arrive.
			sim.advance(arrival_delay.sample(&mut self.rng)).await;

			// Create a new car with arrival and load times.
			self.landing_site
				.send(Car {
					arrival_time: sim.now(),
					load_duration: loading_delay
						.sample_iter(&mut self.rng)
						.find(|&val| val >= 0.0)
						.unwrap(),
				})
				.await
				.expect("No ferries in the simulation");
		}
	}
}

impl Ferry {
	/// Simulates the actions of a ferry in the simulation.
	///
	/// The ferry travels between harbors, loads cars, waits for a timeout or
	/// until it reaches capacity, and then moves to the next harbor.
	async fn actions(mut self, sim: Sim<'_, Ferries>) {
		loop {
			for pier in self.piers.iter() {
				// Unload the cars at the current pier.
				for car in self.cargo.drain(..) {
					sim.advance(car.load_duration).await;
				}

				let begin_loading = sim.now();

				// Load cars until capacity is reached or timeout occurs.
				while self.cargo.len() < self.cargo.capacity() {
					match select(sim, pier.recv(), async {
						sim.advance(self.timeout).await;
						None
					})
					.await
					{
						// A car arrived before timeout.
						Some(car) => {
							sim.global()
								.car_wait_time
								.tabulate(sim.now() - car.arrival_time);
							sim.advance(car.load_duration).await;
							self.cargo.push(car);
						}
						// Timeout occurred; depart to next harbor.
						None => break,
					}
				}

				// Record ferry loading statistics.
				sim.global()
					.ferry_load_time
					.tabulate(sim.now() - begin_loading);
				sim.global()
					.ferry_cargo_len
					.tabulate(self.cargo.len() as f64);

				// Travel to the next harbor.
				sim.advance(self.travel_time).await;
			}
		}
	}
}

/// The main simulation function.
///
/// Sets up the piers and ferries, activates their processes, and runs the simulation
/// for the specified duration.
async fn sim_main(sim: Sim<'_, Ferries>, duration: Time, ferries: usize, harbors: usize) {
	let mut ports = Vec::with_capacity(harbors);

	// Create all the harbors (piers).
	for _ in 0..harbors {
		let (sx, rx) = channel();
		let harbor = Pier {
			rng: SmallRng::from_seed(sim.global().master_rng.borrow_mut().gen()),
			landing_site: sx,
		};
		sim.activate(harbor.actions(sim));
		ports.push(rx);
	}

	// Create all the ferries.
	for i in 0..ferries {
		let ferry = Ferry {
			cargo: Vec::with_capacity(FERRY_CAPACITY),
			timeout: FERRY_TIMEOUT,
			travel_time: HARBOR_DISTANCE,
			piers: ports
				.iter()
				.skip(i)
				.chain(ports.iter().take(i))
				.cloned()
				.collect(),
		};
		sim.activate(ferry.actions(sim));
	}

	// Run the simulation for the specified duration.
	sim.advance(duration).await;

	// Handle any cars that weren't picked up by a ferry.
	for port in ports {
		for _ in 0..port.len() {
			let car = port.recv().await.unwrap();
			sim.global()
				.car_wait_time
				.tabulate(sim.now() - car.arrival_time);
		}
	}
}

/// Entry point for the ferry simulation.
///
/// Initializes the simulation environment and runs the simulation for one week.
#[cfg(not(test))]
fn main() {
	let result = simcore_rs::simulation(
		// Global shared data.
		Ferries {
			master_rng: RefCell::new(SmallRng::seed_from_u64(SEED)),
			ferry_cargo_len: RandomVariable::new(),
			ferry_load_time: RandomVariable::new(),
			car_wait_time: RandomVariable::new(),
		},
		// Simulation entry point.
		|sim| {
			Process::new(
				sim,
				sim_main(sim, 24.0 * 60.0 * 365.0, FERRY_COUNT, HARBOR_COUNT),
			)
		},
	);

	// Output simulation statistics.
	println!("Number of harbors: {}", HARBOR_COUNT);
	println!("Number of ferries: {}", FERRY_COUNT);
	println!("Car wait time: {:#.3?}", result.car_wait_time);
	println!("Ferry cargo len: {:#.3?}", result.ferry_cargo_len);
	println!("Ferry load time: {:#.3?}", result.ferry_load_time);
}

#[cfg(test)]
criterion::criterion_main!(bench::benches);

#[cfg(all(test, windows))]
mod slx;

/// Benchmark module for the ferry simulation.
///
/// Uses the Criterion crate to benchmark the simulation with different
/// implementations and simulation durations.
#[cfg(test)]
mod bench {
	use super::*;
	use criterion::{
		criterion_group, AxisScale, BatchSize, BenchmarkId, Criterion, PlotConfiguration,
	};

	#[cfg(feature = "odemx")]
	mod odemx {
		use std::os::raw::{c_double, c_uint};

		#[link(name = "odemx", kind = "static")]
		extern "C" {
			pub fn ferry(duration: c_double, ferries: c_uint, harbors: c_uint);
		}
	}

	#[cfg(windows)]
	const SLX_PATH: &'static str = "C:\\Wolverine\\SLX";
	const RANGE: u32 = 10;
	const STEP: Time = 1000.0;

	/// Benchmarks the ferry simulation using different implementations and
	/// simulation durations.
	fn ferry_bench(c: &mut Criterion) {
		let mut group = c.benchmark_group("Ferry");

		// Set up the benchmark parameters.
		group.confidence_level(0.99);
		group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));

		#[cfg(windows)]
		let slx_path = {
			let path = slx::slx_version(SLX_PATH);

			if path.is_none() {
				println!("SLX not found, skipping SLX benchmarks!");
			} else {
				println!("Using SLX program at {:?}", path.as_ref().unwrap());
			}

			path
		};

		// Vary the length of the simulation run.
		for (i, sim_duration) in (0..RANGE).map(|c| Time::from(1 << c) * STEP).enumerate() {
			#[cfg(windows)]
			if let Some(path) = slx_path.as_ref() {
				let duration = sim_duration.to_string();
				let args = [
					"/silent",
					"/stdout",
					"/noicon",
					"/nowarn",
					"/noxwarn",
					"/#BENCH",
					"slx\\ferry.slx",
					duration.as_str(),
				];

				// Benchmark the SLX implementation.
				group.bench_function(BenchmarkId::new("SLX", sim_duration), |b| {
					b.iter_custom(|iters| {
						slx::slx_bench(path.as_os_str(), &args, iters as usize)
							.expect("Couldn't benchmark the SLX program")
					})
				});
			}

			// Benchmark the C++ implementation.
			#[cfg(feature = "odemx")]
			group.bench_function(BenchmarkId::new("ODEMx", sim_duration), |b| {
				b.iter(|| unsafe {
					odemx::ferry(sim_duration as _, FERRY_COUNT as _, HARBOR_COUNT as _)
				})
			});

			// Benchmark the Rust implementation.
			group.bench_function(BenchmarkId::new("Rust", sim_duration), |b| {
				b.iter_batched(
					|| Ferries {
						master_rng: RefCell::new(SmallRng::seed_from_u64(SEED * ((i + 1) as u64))),
						ferry_cargo_len: RandomVariable::new(),
						ferry_load_time: RandomVariable::new(),
						car_wait_time: RandomVariable::new(),
					},
					|shared| {
						simcore_rs::simulation(shared, |sim| {
							Process::new(
								sim,
								sim_main(sim, sim_duration, FERRY_COUNT, HARBOR_COUNT),
							)
						})
					},
					BatchSize::SmallInput,
				)
			});
		}

		group.finish();
	}

	criterion_group!(benches, ferry_bench);
}
