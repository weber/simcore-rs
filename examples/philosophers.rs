//! A discrete event simulation of the Dining Philosophers problem using
//! `simcore_rs`, the simulator core developed as part of my (Dorian Weber)
//! dissertation.
//!
//! This module models the classic Dining Philosophers problem, where multiple
//! philosophers sit around a table and alternate between thinking and eating.
//! They need two forks to eat and must coordinate to avoid deadlocks. The
//! simulation tracks the time until a deadlock occurs and collects statistical
//! data over multiple runs.
//!
//! # Notes
//!
//! - The simulation detects deadlocks when all philosophers are waiting for
//!   forks and none can proceed.
//! - The statistical data collected includes the time until deadlock over
//!   multiple simulation runs.

use rand::{rngs::SmallRng, Rng, SeedableRng};
use rand_distr::{Distribution, Exp, Normal, Uniform};
use rayon::prelude::*;
use simcore_rs::{
	util::{until, Control, RandomVariable},
	Process, Sim, Time,
};
use std::{
	cell::{Cell, RefCell},
	rc::Rc,
};

// Constants for simulation parameters.
const PHILOSOPHER_COUNT: usize = 5;
const SEED: u64 = 100_000;

/// Shared global data for the simulation.
struct Philosophers {
	/// Master random number generator for seeding.
	master_rng: RefCell<SmallRng>,
	/// Cell to store the simulation duration until deadlock.
	sim_duration: Cell<Time>,
}

/// Represents the shared table with forks for the philosophers.
struct Table {
	/// Controls for each fork, indicating whether it is held.
	forks: Vec<Control<bool>>,
	/// Counter for the number of forks currently held.
	forks_held: Control<usize>,
	/// Counter for the number of forks currently awaited.
	forks_awaited: Control<usize>,
}

impl Table {
	/// Creates a new table with a specified number of forks.
	fn new(forks: usize) -> Self {
		Table {
			forks: (0..forks).map(|_| Control::new(false)).collect(),
			forks_held: Control::default(),
			forks_awaited: Control::default(),
		}
	}

	/// Acquires a fork at a given position, blocking until it is available.
	async fn acquire_fork(&self, i: usize) {
		let num = i % self.forks.len();
		self.forks_awaited.set(self.forks_awaited.get() + 1);
		until(&self.forks[num], |fork| !fork.get()).await;
		self.forks[num].set(true);
		self.forks_awaited.set(self.forks_awaited.get() - 1);
		self.forks_held.set(self.forks_held.get() + 1);
	}

	/// Releases a fork at a given position back to the table.
	fn release_fork(&self, i: usize) {
		self.forks[i % self.forks.len()].set(false);
		self.forks_held.set(self.forks_held.get() - 1);
	}
}

/// Represents a philosopher in the simulation.
struct Philosopher {
	/// Reference to the shared table.
	table: Rc<Table>,
	/// The seat index of the philosopher at the table.
	seat: usize,
	/// Random number generator for this philosopher.
	rng: SmallRng,
}

impl Philosopher {
	/// Simulates the actions of a philosopher.
	///
	/// The philosopher alternates between thinking and eating, and tries to
	/// acquire forks.
	async fn actions(mut self, sim: Sim<'_, Philosophers>) {
		let thinking_duration = Exp::new(1.0).unwrap();
		let artificial_delay = Uniform::new(0.1, 0.2);
		let eating_duration = Normal::new(0.5, 0.2).unwrap();

		loop {
			// Spend some time pondering the nature of things.
			sim.advance(thinking_duration.sample(&mut self.rng)).await;

			// Acquire the first fork.
			self.table.acquire_fork(self.seat).await;

			// Introduce an artificial delay to leave room for deadlocks.
			sim.advance(artificial_delay.sample(&mut self.rng)).await;

			// Acquire the second fork.
			self.table.acquire_fork(self.seat + 1).await;

			// Spend some time eating.
			sim.advance(
				eating_duration
					.sample_iter(&mut self.rng)
					.find(|&val| val >= 0.0)
					.unwrap(),
			)
			.await;

			// Release the forks.
			self.table.release_fork(self.seat + 1);
			self.table.release_fork(self.seat);
		}
	}
}

/// Runs a single simulation instance of the Dining Philosophers problem.
///
/// Initializes the table and philosophers, and waits until a deadlock occurs.
async fn sim_main(sim: Sim<'_, Philosophers>, count: usize) {
	let table = Rc::new(Table::new(count));

	// Create the philosopher processes and seat them.
	for i in 0..count {
		sim.activate(
			Philosopher {
				table: table.clone(),
				seat: i,
				rng: SmallRng::from_seed(sim.global().master_rng.borrow_mut().gen()),
			}
			.actions(sim),
		);
	}

	// Wait for the precise configuration indicating a deadlock.
	until(
		(&table.forks_held, &table.forks_awaited),
		|(held, awaited)| held.get() == count && awaited.get() == count,
	)
	.await;

	// Record the current simulation time.
	sim.global().sim_duration.set(sim.now());
}

/// Runs multiple simulation instances in parallel and collects statistics.
///
/// # Arguments
///
/// * `count` - The number of philosophers (and forks) in each simulation.
/// * `reruns` - The number of simulation runs to perform.
///
/// # Returns
///
/// A `RandomVar` containing statistical data of simulation durations until
/// deadlock.
fn philosophers(count: usize, reruns: usize) -> RandomVariable {
	// Use thread-based parallelism to concurrently run simulation models.
	(1..=reruns)
		.into_par_iter()
		.map(|i| {
			simcore_rs::simulation(
				// Global data.
				Philosophers {
					master_rng: RefCell::new(SmallRng::seed_from_u64(i as u64 * SEED)),
					sim_duration: Cell::default(),
				},
				// Simulation entry point.
				|sim| Process::new(sim, sim_main(sim, count)),
			)
			.sim_duration
			.get()
		})
		.fold(RandomVariable::new, |var, duration| {
			var.tabulate(duration);
			var
		})
		.reduce(RandomVariable::new, |var_a, var_b| {
			var_a.merge(&var_b);
			var_a
		})
}

/// Entry point for the Dining Philosophers simulation.
///
/// Runs multiple simulations and prints out the statistical results.
#[cfg(not(test))]
fn main() {
	const EXPERIMENT_COUNT: usize = 500;

	let sim_duration = philosophers(PHILOSOPHER_COUNT, EXPERIMENT_COUNT);
	println!("Simulation duration until deadlock: {:#?}", sim_duration);
}

#[cfg(test)]
criterion::criterion_main!(bench::benches);

#[cfg(all(test, windows))]
mod slx;

/// Benchmark module for the Dining Philosophers simulation.
///
/// Uses the Criterion crate to benchmark the simulation with different numbers
/// of reruns.
#[cfg(test)]
mod bench {
	use super::*;
	use criterion::{criterion_group, AxisScale, BenchmarkId, Criterion, PlotConfiguration};

	#[cfg(feature = "odemx")]
	mod odemx {
		use std::os::raw::c_uint;

		#[link(name = "odemx", kind = "static")]
		extern "C" {
			pub fn philosophers(count: c_uint, reruns: c_uint);
		}
	}

	#[cfg(windows)]
	const SLX_PATH: &'static str = "C:\\Wolverine\\SLX";
	const RANGE: u32 = 10;
	const STEP: usize = 3;

	/// Benchmarks the Dining Philosophers simulation using different numbers of
	/// reruns.
	fn philosopher_bench(c: &mut Criterion) {
		let mut group = c.benchmark_group("Philosophers");

		// Set up the benchmark parameters.
		group.confidence_level(0.99);
		group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));

		#[cfg(windows)]
		let slx_path = {
			let path = slx::slx_version(SLX_PATH);

			if path.is_none() {
				println!("SLX not found, skipping SLX benchmarks!");
			} else {
				println!("Using SLX program at {:?}", path.as_ref().unwrap());
			}

			path
		};

		// Vary the number of performed reruns in each experiment.
		for experiment_count in (0..RANGE).map(|c| (1 << c) * STEP) {
			#[cfg(windows)]
			if let Some(path) = slx_path.as_ref() {
				let count = experiment_count.to_string();
				let args = [
					"/silent",
					"/stdout",
					"/noicon",
					"/nowarn",
					"/noxwarn",
					"/#BENCH",
					"slx\\philosophers.slx",
					count.as_str(),
				];

				// Benchmark the SLX implementation.
				group.bench_function(BenchmarkId::new("SLX", experiment_count), |b| {
					b.iter_custom(|iters| {
						slx::slx_bench(path.as_os_str(), &args, iters as usize)
							.expect("Couldn't benchmark the SLX program")
					})
				});
			}

			// Benchmark the C++ implementation.
			#[cfg(feature = "odemx")]
			group.bench_function(BenchmarkId::new("ODEMx", experiment_count), |b| {
				b.iter(|| unsafe {
					odemx::philosophers(PHILOSOPHER_COUNT as _, experiment_count as _)
				})
			});

			// Benchmark the Rust implementation.
			group.bench_function(BenchmarkId::new("Rust", experiment_count), |b| {
				b.iter(|| philosophers(PHILOSOPHER_COUNT, experiment_count))
			});
		}

		group.finish();
	}

	criterion_group!(benches, philosopher_bench);
}
