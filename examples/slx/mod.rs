use itertools::Itertools;
use std::ffi::{OsStr, OsString};
use std::{iter::once, path::Path, process::Command, time::Duration};

/// Runs the SLX runtime environment once for an SLX program that measures its
/// own duration and returns it on the standard output.
///
/// Errors during this execution are returned back to the caller.
fn slx_run_once(
	program: &OsStr,
	arguments: &[&str],
	iterations: usize,
) -> Result<Duration, (i32, String)> {
	// start the SLX runtime and wait for its return
	let rc = Command::new(program)
		.args(arguments)
		.arg(iterations.to_string())
		.output()
		.map_err(|err| (-1, format!("Failed to run the SLX program: {}", err)))?;

	if !rc.status.success() {
		// check the error code and add a description to it
		Err(rc.status.code().map_or_else(
			|| (-10000, "Unknown error code".to_string()),
			|code| {
				(
					code,
					match code {
						-10001 => "Unable to find a security key with SLX permission".to_string(),
						-10002 => "The command line includes an invalid option".to_string(),
						-10003 => "The source file cannot be found".to_string(),
						-10004 => "The specified/implied output file cannot be written".to_string(),
						-10005 => "The program contains compile-time errors".to_string(),
						-10006 => "The program has terminated with a run-time error".to_string(),
						-10007 => "The /genrts option failed".to_string(),
						_ => format!("Unknown error code: {}", code),
					},
				)
			},
		))
	} else {
		Ok(Duration::from_secs_f64(
			(&String::from_utf8_lossy(&rc.stdout))
				.trim()
				.parse()
				.unwrap(),
		))
	}
}

/// Repeats runs of an SLX program for a specified number of times and collects
/// the total (real-time) duration of those combined runs.
///
/// The SLX program in question has to report its own real-time duration using
/// the standard output.
pub fn slx_bench(
	program: &OsStr,
	arguments: &[&str],
	iterations: usize,
) -> Result<Duration, String> {
	// try to complete the iterations in a single run of the SLX program
	slx_run_once(program, arguments, iterations).or_else(|(code, desc)| {
		if code == -10006 {
			// Runtime error: this happens when the free version of SLX
			// exceeds its total instance budget, either through a
			// complicated scenario or too many iterations in one call.
			// We can still make this work by running SLX multiple times
			// with lower iteration counts and combining the timings.
			// This effectively trades benchmark speed with simulation model
			// size.
			(1..)
				.map(|i| iterations >> i)
				.take_while(|&chunk_size| chunk_size > 0)
				// .inspect(|chunk_size| println!("reducing the chunk size to {}", chunk_size))
				.map(|chunk_size| {
					(0..(iterations - 1) / chunk_size)
						.map(|_| chunk_size)
						.chain(once((iterations - 1) % chunk_size + 1))
						.map(|size| slx_run_once(program, arguments, size))
						.fold_ok(Duration::default(), |duration, run| duration + run)
				})
				.find_map(|result| result.ok())
				.ok_or(desc)
		} else {
			Err(desc)
		}
	})
}

/// Attempts to find the best SLX version for the benchmarks, falling back on
/// the free version if no SLX license can be found.
pub fn slx_version(base: impl AsRef<Path>) -> Option<OsString> {
	if base.as_ref().exists() {
		let programs = ["se64.exe", "se32.exe", "sse.exe"];
		let args = ["/silent", "/noicon", "/nowarn", "/noxwarn", "bad_file.slx"];

		for path in programs.iter().map(|p| base.as_ref().join(p)) {
			match Command::new(&path).args(&args).status() {
				Err(_) => continue,
				Ok(rc) => {
					if let Some(code) = rc.code() {
						if code == -10003 {
							return Some(path.into_os_string());
						}
					}
				}
			}
		}
	}

	None
}
