// compiles the ODEMx-library and the C++ examples to link against
fn main() {
	// only rerun this script if a source file in odemx has changed
	println!("cargo:rerun-if-changed=odemx-lite/src");
	println!("cargo:rerun-if-changed=cpp");
	
	// attempt to translate the library using the native platform's compiler
	let result = cc::Build::new()
		.cpp(true)
		.define("RUST_FFI", None)
		.opt_level(3)
		.includes(&["odemx-lite/include", "odemx-lite/external/CppLog/include"])
		.flag_if_supported("-std=c++17")
		.flag_if_supported("-std:c++17")
		.flag_if_supported("-Wno-potentially-evaluated-expression")
		.flag_if_supported("-Wno-deprecated-declarations")
		.flag_if_supported("-flto")
		.warnings(false)
		.debug(false)
		.files(&[
			"odemx-lite/src/base/Comparators.cpp",
			"odemx-lite/src/base/Continuous.cpp",
			"odemx-lite/src/base/DefaultSimulation.cpp",
			"odemx-lite/src/base/Event.cpp",
			"odemx-lite/src/base/ExecutionList.cpp",
			"odemx-lite/src/base/Process.cpp",
			"odemx-lite/src/base/Sched.cpp",
			"odemx-lite/src/base/Scheduler.cpp",
			"odemx-lite/src/base/Simulation.cpp",
			"odemx-lite/src/base/control/ControlBase.cpp",
			"odemx-lite/src/coroutine/Coroutine.cpp",
			"odemx-lite/src/coroutine/CoroutineContext.cpp",
			"odemx-lite/src/coroutine/ucFiber.cpp",
			"odemx-lite/src/data/buffer/SimRecordBuffer.cpp",
			"odemx-lite/src/data/buffer/StatisticsBuffer.cpp",
			"odemx-lite/src/data/LoggingManager.cpp",
			"odemx-lite/src/data/ManagedChannels.cpp",
			"odemx-lite/src/data/output/ErrorWriter.cpp",
			"odemx-lite/src/data/output/GermanTime.cpp",
			"odemx-lite/src/data/output/Iso8601Time.cpp",
			"odemx-lite/src/data/output/OStreamReport.cpp",
			"odemx-lite/src/data/output/OStreamWriter.cpp",
			"odemx-lite/src/data/output/TimeFormat.cpp",
			"odemx-lite/src/data/Producer.cpp",
			"odemx-lite/src/data/Report.cpp",
			"odemx-lite/src/data/ReportProducer.cpp",
			"odemx-lite/src/data/ReportTable.cpp",
			"odemx-lite/src/data/SimRecord.cpp",
			"odemx-lite/src/data/SimRecordFilter.cpp",
			"odemx-lite/src/protocol/Device.cpp",
			"odemx-lite/src/protocol/Entity.cpp",
			"odemx-lite/src/protocol/ErrorModelDraw.cpp",
			"odemx-lite/src/protocol/Layer.cpp",
			"odemx-lite/src/protocol/Medium.cpp",
			"odemx-lite/src/protocol/Sap.cpp",
			"odemx-lite/src/protocol/Service.cpp",
			"odemx-lite/src/protocol/ServiceProvider.cpp",
			"odemx-lite/src/protocol/Stack.cpp",
			"odemx-lite/src/random/ContinuousConst.cpp",
			"odemx-lite/src/random/ContinuousDist.cpp",
			"odemx-lite/src/random/DiscreteConst.cpp",
			"odemx-lite/src/random/DiscreteDist.cpp",
			"odemx-lite/src/random/Dist.cpp",
			"odemx-lite/src/random/DistContext.cpp",
			"odemx-lite/src/random/Draw.cpp",
			"odemx-lite/src/random/Erlang.cpp",
			"odemx-lite/src/random/NegativeExponential.cpp",
			"odemx-lite/src/random/Normal.cpp",
			"odemx-lite/src/random/Poisson.cpp",
			"odemx-lite/src/random/RandomInt.cpp",
			"odemx-lite/src/random/Uniform.cpp",
			"odemx-lite/src/statistics/Accumulate.cpp",
			"odemx-lite/src/statistics/Count.cpp",
			"odemx-lite/src/statistics/Histogram.cpp",
			"odemx-lite/src/statistics/Regression.cpp",
			"odemx-lite/src/statistics/Sum.cpp",
			"odemx-lite/src/statistics/Tab.cpp",
			"odemx-lite/src/statistics/Tally.cpp",
			"odemx-lite/src/synchronization/Bin.cpp",
			"odemx-lite/src/synchronization/CondQ.cpp",
			"odemx-lite/src/synchronization/IMemory.cpp",
			"odemx-lite/src/synchronization/Memory.cpp",
			"odemx-lite/src/synchronization/ProcessQueue.cpp",
			"odemx-lite/src/synchronization/Queue.cpp",
			"odemx-lite/src/synchronization/Res.cpp",
			"odemx-lite/src/synchronization/Timer.cpp",
			"odemx-lite/src/synchronization/Wait.cpp",
			"odemx-lite/src/synchronization/WaitQ.cpp",
			// example scenarios
			"cpp/Barbershop/main.cpp",
			"cpp/Ferry/main.cpp",
			"cpp/Philosophers/main.cpp",
		])
		.try_compile("odemx");

	// there is no need to stop the build altogether if the compilation failed
	if let Err(msg) = result {
		println!("cargo::warning=Compiling ODEMx-lite has failed.");
		println!(
			"cargo::warning=This library will still work, \
	          the C++ benchmarks will not!"
		);
		println!("cargo::warning={}", msg);
	} else {
		println!("cargo::rustc-cfg=odemx");
	}

	println!("cargo::rustc-check-cfg=cfg(odemx)");
}
