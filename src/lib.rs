//! A minimal discrete-event simulation framework using Rust's async/await.
//!
//! This module provides a simple discrete-event simulation environment where
//! processes are represented as asynchronous tasks (`Future`s). The simulation
//! advances in discrete time steps, executing processes scheduled at specific
//! simulation times.
//!
//! Key components:
//!
//! - **`simulation`**: Runs a single simulation given a shared global state
//!   and a main process function. It sets up the scheduler (`Calendar`),
//!   initializes a handle to the simulation context (`Sim`), and manages the
//!   event loop until the main process terminates.
//!
//! - **`Simulator`**: The simulation context, containing the event calendar,
//!   an owning registry for all processes, and a reference to the shared data.
//!
//! - **`Sim`**: A lightweight handle to the simulation context, providing
//!   methods for processes to interact with the simulation. Processes can use
//!   it to schedule themselves or other processes, advance simulation time,
//!   retrieve the current model-time, and access shared global data.
//!
//! - **`Process`**: Wraps a `Future` to represent a process in the simulation.
//!   It can be scheduled to run at specific model times and can also act as a
//!   waker to resume suspended processes.
//!
//! - **`Calendar`**: The scheduler that maintains the current model time and a
//!   priority queue of processes scheduled to run at future times.
//!
//! - **Utility functions**:
//!   - `sleep()`: A future that processes can await to suspend execution until
//!     reactivated.
//!   - `waker()`: A future that returns the current waker, allowing processes
//!     to interact with the task system.
//!
//! **Example usage:**
//!
//! ```rust
//! use simcore_rs::*;
//!
//! async fn sim_main(sim: Sim<'_>) {
//!     // Main Process code here
//!     println!("Process started at time {}", sim.now());
//!     sim.advance(5.0).await; // Suspend for 5 units of time
//!     println!("Process resumed at time {}", sim.now());
//! }
//!
//! // Run the simulation with empty shared data
//! let shared_data = simulation((), |sim| Process::new(sim, sim_main(sim)));
//! ```
//!
//! **Notes:**
//!
//! - The simulation is designed for single-threaded execution and is not `Send`
//!   or `Sync` safe.
//! - Processes manage their own scheduling and can be reactivated by the
//!   simulation context.
//! - Unsafe code is used internally, but safety is maintained as long as
//!   strongly owned processes do not escape the closure passed to `simulation`.
//! - Wakers are customized to integrate with the simulation's process
//!   scheduling.
//!
//! This framework is intended for educational purposes as part of my (Dorian
//! Weber) dissertation to illustrate how asynchronous Rust can be used to
//! implement a discrete-event simulation framework. It may not cover all edge
//! cases or be suitable for production use.

use std::{
	cell::{Cell, RefCell},
	cmp::Ordering,
	collections::BinaryHeap,
	future::{poll_fn, Future},
	hash::{Hash, Hasher},
	mem::ManuallyDrop,
	pin::Pin,
	rc::{Rc, Weak},
	task::{self, Context, Poll},
};

pub mod util;

// simple time type
pub type Time = f64;

/// Performs a single simulation run.
///
/// Input is a function that takes a simulation context and returns the first
/// process.
pub fn simulation<G, F>(shared: G, main: F) -> G
where
	F: for<'s> FnOnce(Sim<'s, G>) -> Process<'s, G>,
{
	let simulator;
	let registry = Rc::default();

	// create a fresh simulation context and a handle to it
	simulator = Simulator::new(&shared, Rc::downgrade(&registry));
	let sim = Sim { handle: &simulator };

	// construct a custom context to pass into the poll()-method
	let event_waker = StateEventWaker::new(sim);
	let waker = unsafe { event_waker.as_waker() };
	let mut cx = Context::from_waker(&waker);

	// evaluate the passed function for the main process and schedule it
	let root = main(sim);
	simulator.schedule(root.clone());

	// pop processes until empty or the main process terminates
	while let Some(process) = simulator.next_process() {
		if process.poll(&mut cx).is_ready() && process == root {
			break;
		}

		// drop the active process if only the weak reference of the
		// `active` field in the event calendar remains
		if Rc::weak_count(&process.0) <= 1 {
			registry.borrow_mut().swap_remove(&process);
		}
	}

	// Here we drop `registry` first and `simulator` after. Since `registry`
	// is an `IndexSet` (and not a `HashSet`), all processes are guaranteed
	// to be dropped, even if a destructor of a future unwinds. This ensures
	// that any weak process wakers leaked out of the `simulation` function
	// can no longer be upgraded.
	drop(registry);

	// return the global data
	shared
}

/// Private simulation context that holds a reference to the shared data, the
/// event calendar, and the owning container for all processes.
struct Simulator<'s, G> {
	/// A reference to the globally shared data.
	shared: &'s G,

	/// The scheduler for processes.
	calendar: RefCell<Calendar<'s, G>>,

	/// The container that has strong ownership of the processes during a
	/// simulation run.
	registry: Weak<RefCell<indexmap::IndexSet<StrongProcess<'s, G>>>>,
}

impl<'s, G> Simulator<'s, G> {
	/// Create a new simulator instance referencing shared data and using the
	/// registry as owner of the processes.
	fn new(
		shared: &'s G,
		registry: Weak<RefCell<indexmap::IndexSet<StrongProcess<'s, G>>>>,
	) -> Self {
		Simulator {
			shared,
			calendar: RefCell::new(Calendar::default()),
			registry,
		}
	}

	/// Returns a (reference-counted) copy of the currently active process.
	fn active(&self) -> Process<'s, G> {
		self.calendar
			.borrow()
			.active
			.as_ref()
			.expect("no active process")
			.clone()
	}

	/// Returns the current simulation time.
	fn now(&self) -> Time {
		self.calendar.borrow().now
	}

	/// Schedules a process at the current simulation time.
	fn schedule(&self, process: Process<'s, G>) {
		self.schedule_in(Time::default(), process);
	}

	/// Schedules a process at a later simulation time.
	fn schedule_in(&self, dt: Time, process: Process<'s, G>) {
		self.calendar.borrow_mut().schedule_in(dt, process);
	}

	/// Returns the next process according to the event calendar.
	fn next_process(&self) -> Option<StrongProcess<'s, G>> {
		self.calendar.borrow_mut().next_process()
	}
}

/// The (private) scheduler for processes.
struct Calendar<'s, G> {
	/// The current simulation time.
	now: Time,
	/// The event-calendar organized chronologically.
	events: BinaryHeap<NextEvent<'s, G>>,
	/// The currently active process.
	active: Option<Process<'s, G>>,
}

impl<G> Default for Calendar<'_, G> {
	fn default() -> Self {
		Self {
			now: Time::default(),
			events: Default::default(),
			active: Default::default(),
		}
	}
}

impl<'s, G> Calendar<'s, G> {
	/// Schedules a process at a later simulation time.
	#[inline]
	fn schedule_in(&mut self, dt: Time, process: Process<'s, G>) {
		let calender = &mut self.events;
		let strong_process = process.upgrade().expect("already terminated");
		let is_scheduled = &strong_process.0.is_scheduled;

		assert!(!is_scheduled.get(), "already scheduled");
		is_scheduled.set(true);

		calender.push(NextEvent {
			move_time: self.now + dt,
			process,
		});
	}

	/// Removes the process with the next event time from the calendar and
	/// activates it.
	#[inline]
	fn next_process(&mut self) -> Option<StrongProcess<'s, G>> {
		loop {
			let NextEvent {
				move_time: now,
				process,
			} = self.events.pop()?;
			let Some(strong_process) = process.upgrade() else {
				// process was terminated after being queued
				continue;
			};
			strong_process.0.is_scheduled.set(false);
			self.now = now;
			self.active = Some(process);

			return Some(strong_process);
		}
	}
}

/// A light-weight handle to the simulation context.
pub struct Sim<'s, G = ()> {
	handle: &'s Simulator<'s, G>,
}

// this allows the creation of copies
impl<G> Clone for Sim<'_, G> {
	#[inline]
	fn clone(&self) -> Self {
		*self
	}
}

// this allows moving the context without invalidating it (copy semantics)
impl<G> Copy for Sim<'_, G> {}

impl<'s, G> Sim<'s, G> {
	/// Returns a (reference-counted) copy of the currently active process.
	#[inline]
	pub fn active(self) -> Process<'s, G> {
		self.handle.active()
	}

	/// Activates a new process with the given future.
	#[inline]
	pub fn activate(self, f: impl Future<Output = ()> + 's) {
		self.reactivate(Process::new(self, f));
	}

	/// Reactivates a process that has been suspended with wait().
	#[inline]
	pub fn reactivate(self, process: Process<'s, G>) {
		self.handle.schedule(process);
	}

	/// Reactivates the currently active process after some time has passed.
	#[inline]
	pub async fn advance(self, dt: Time) {
		self.handle.schedule_in(dt, self.handle.active());
		sleep().await
	}

	/// Returns the current simulation time.
	#[inline]
	pub fn now(self) -> Time {
		self.handle.now()
	}

	/// Returns a shared reference to the global data.
	#[inline]
	pub fn global(self) -> &'s G {
		self.handle.shared
	}
}

/// A bare-bone process type that can also be used as a waker.
pub struct Process<'s, G>(Weak<ProcessInner<'s, G>>);

/// A private accessor to the inner parts of a [`Process`].
///
/// **WARNING**: This must never leak into user code! Forgetting a
/// `StrongProcess` (`std::mem::forget`) causes *undefined behavior* because
/// it will keep the processes alive past the simulation, leading to dangling
/// pointers due to the lifetime erasure experienced by the wakers.
struct StrongProcess<'s, G>(Rc<ProcessInner<'s, G>>);

/// The private details of the [`Process`](struct.Process.html) type.
struct ProcessInner<'s, G> {
	/// The simulation context needed to implement the `Waker` interface.
	context: Sim<'s, G>,
	/// A boolean flag indicating whether this process is currently scheduled.
	is_scheduled: Cell<bool>,
	/// `Some` [`Future`] associated with this process or `None` if it has been
	/// terminated externally.
	///
	/// [`Future`]: https://doc.rust-lang.org/std/future/trait.Future.html
	state: RefCell<Option<Pin<Box<dyn Future<Output = ()> + 's>>>>,
}

impl<'s, G> Process<'s, G> {
	/// Combines a future and a simulation context to a process.
	#[inline]
	pub fn new(sim: Sim<'s, G>, fut: impl Future<Output = ()> + 's) -> Self {
		let strong_process = StrongProcess(Rc::new(ProcessInner {
			context: sim,
			is_scheduled: Cell::new(false),
			state: RefCell::new(Some(Box::pin(fut))),
		}));

		let process = strong_process.downgrade();

		let registry = sim
			.handle
			.registry
			.upgrade()
			.expect("attempted to create process after simulation ended");
		registry.borrow_mut().insert(strong_process);

		process
	}

	/// Releases the [`Future`] contained in this process.
	///
	/// This will also execute all the destructors for the local variables
	/// initialized by this process.
	#[inline]
	pub fn terminate(&self) {
		let Some(strong_process) = self.upgrade() else {
			// ok, we're already terminated
			return;
		};
		let sim = strong_process.0.context;
		assert!(
			sim.handle
				.calendar
				.borrow()
				.active
				.as_ref()
				.is_none_or(|active| active != self),
			"attempted to terminate active process"
		);
		if let Some(registry) = sim.handle.registry.upgrade() {
			registry.borrow_mut().swap_remove(&strong_process);
		} else {
			// This can happen if we're dropping the `registry` and the
			// destructor of another process tries to terminate this process.
			// In this case, we need to manually drop our future now to ensure
			// that there are no dangling references from our future to their
			// future.
			*strong_process.0.state.borrow_mut() = None;
		};
		drop(strong_process);
		assert!(self.is_terminated(), "failed to terminate process");
	}

	/// Returns a `Waker` for this process.
	#[inline]
	pub fn waker(self) -> task::Waker {
		unsafe { task::Waker::from_raw(self.raw_waker()) }
	}

	/// Returns whether this process has finished its life-cycle.
	#[inline]
	pub fn is_terminated(&self) -> bool {
		self.upgrade()
			.is_none_or(|strong_process| strong_process.0.state.borrow().is_none())
	}

	/// Gets private access to the process.
	///
	/// # Safety
	///
	/// The caller must ensure that the [`StrongProcess`] is not leaked.
	/// In particular, it must not be passed to user code.
	#[inline]
	fn upgrade(&self) -> Option<StrongProcess<'s, G>> {
		self.0.upgrade().map(StrongProcess)
	}
}

impl<'s, G> StrongProcess<'s, G> {
	/// Private function for polling the process.
	#[inline]
	fn poll(&self, cx: &mut Context<'_>) -> Poll<()> {
		self.0
			.state
			.borrow_mut()
			.as_mut()
			.expect("attempted to poll terminated task")
			.as_mut()
			.poll(cx)
	}

	/// Converts the owning process into a weaker referencing process.
	#[inline]
	fn downgrade(&self) -> Process<'s, G> {
		Process(Rc::downgrade(&self.0))
	}

	/// Returns whether this process is currently scheduled.
	#[inline]
	fn is_scheduled(&self) -> bool {
		self.0.is_scheduled.get()
	}
}

// Increases the reference counter of this process.
impl<G> Clone for Process<'_, G> {
	#[inline]
	fn clone(&self) -> Self {
		Process(self.0.clone())
	}
}

// allows processes to be compared for equality
impl<G> PartialEq for Process<'_, G> {
	#[inline]
	fn eq(&self, other: &Self) -> bool {
		Weak::ptr_eq(&self.0, &other.0)
	}
}

// marks the equality-relation as total
impl<G> Eq for Process<'_, G> {}

// hash processes for pointer equality
impl<G> Hash for Process<'_, G> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.0.as_ptr().hash(state);
	}
}

// allows processes to be compared for equality
impl<G> PartialEq for StrongProcess<'_, G> {
	#[inline]
	fn eq(&self, other: &Self) -> bool {
		Rc::ptr_eq(&self.0, &other.0)
	}
}

impl<'s, G> PartialEq<Process<'s, G>> for StrongProcess<'s, G> {
	#[inline]
	fn eq(&self, other: &Process<'s, G>) -> bool {
		Rc::as_ptr(&self.0) == other.0.as_ptr()
	}
}

// marks the equality-relation as total
impl<G> Eq for StrongProcess<'_, G> {}

// hash processes for pointer equality
impl<G> Hash for StrongProcess<'_, G> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		Rc::as_ptr(&self.0).hash(state);
	}
}

/// Time-process-pair that has a total order defined based on the time.
struct NextEvent<'p, G> {
	move_time: Time,
	process: Process<'p, G>,
}

impl<G> PartialEq for NextEvent<'_, G> {
	#[inline]
	fn eq(&self, other: &Self) -> bool {
		self.move_time == other.move_time
	}
}

impl<G> Eq for NextEvent<'_, G> {}

impl<G> PartialOrd for NextEvent<'_, G> {
	#[inline]
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl<G> Ord for NextEvent<'_, G> {
	#[inline]
	fn cmp(&self, other: &Self) -> Ordering {
		self.move_time
			.partial_cmp(&other.move_time)
			.expect("illegal event time NaN")
			.reverse()
	}
}

/* **************************** specialized waker *************************** */

impl<G> Process<'_, G> {
	/// Virtual function table for the waker.
	const VTABLE: task::RawWakerVTable =
		task::RawWakerVTable::new(Self::clone, Self::wake, Self::wake_by_ref, Self::drop);

	/// Constructs a raw waker from a simulation context and a process.
	#[inline]
	fn raw_waker(self) -> task::RawWaker {
		task::RawWaker::new(Weak::into_raw(self.0) as *const (), &Self::VTABLE)
	}

	unsafe fn clone(this: *const ()) -> task::RawWaker {
		let waker_ref = &*ManuallyDrop::new(Weak::from_raw(this as *const ProcessInner<'_, G>));

		// increase the reference counter once
		// this is technically unsafe because Wakers are Send + Sync and so this
		// call might be executed from a different thread, creating a data race
		// hazard; we leave preventing this as an exercise to the reader!
		let waker = waker_ref.clone();

		task::RawWaker::new(Weak::into_raw(waker) as *const (), &Self::VTABLE)
	}

	unsafe fn wake(this: *const ()) {
		let waker = Weak::from_raw(this as *const ProcessInner<'_, G>);
		let process = Process(waker);
		process.wake_impl();
	}

	unsafe fn wake_by_ref(this: *const ()) {
		// keep the waker alive by incrementing the reference count
		let waker = Weak::from_raw(this as *const ProcessInner<'_, G>);
		let process = &*ManuallyDrop::new(Process(waker));

		// this is technically unsafe because Wakers are Send + Sync and so this
		// call might be executed from a different thread, creating a data race
		// hazard; we leave preventing this as an exercise to the reader!
		process.clone().wake_impl();
	}

	fn wake_impl(self) {
		// this is technically unsafe because Wakers are Send + Sync and so this
		// call might be executed from a different thread, creating a data race
		// hazard; we leave preventing this as an exercise to the reader!
		let Some(strong_process) = self.upgrade() else {
			// this can happen if a synchronization structure forgets to clean
			// up registered Waker objects on destruct; this would lead to
			// hard-to-diagnose bugs if we were to ignore it
			panic!("attempted to wake a terminated process");
		};

		if !strong_process.is_scheduled() {
			strong_process.0.context.reactivate(self);
		}
	}

	unsafe fn drop(this: *const ()) {
		// this is technically unsafe because Wakers are Send + Sync and so this
		// call might be executed from a different thread, creating a data race
		// hazard; we leave preventing this as an exercise to the reader!
		Weak::from_raw(this as *const RefCell<ProcessInner<'_, G>>);
	}
}

/// Complex waker that is used to implement state events.
///
/// This is the shallow version that is created on the stack of the function
/// running the event loop. It creates the deep version when it is cloned.
struct StateEventWaker<'s, G> {
	context: Sim<'s, G>,
}

impl<'s, G> StateEventWaker<'s, G> {
	/// Virtual function table for the waker.
	const VTABLE: task::RawWakerVTable =
		task::RawWakerVTable::new(Self::clone, Self::wake, Self::wake_by_ref, Self::drop);

	/// Creates a new (shallow) waker using only the simulation context.
	#[inline]
	fn new(sim: Sim<'s, G>) -> Self {
		StateEventWaker { context: sim }
	}

	/// Constructs a new waker using only a reference.
	///
	/// # Safety
	///
	/// This function is unsafe because it is up to the caller to ensure that
	/// the waker doesn't outlive the reference.
	#[inline]
	unsafe fn as_waker(&self) -> task::Waker {
		task::Waker::from_raw(task::RawWaker::new(
			self as *const _ as *const (),
			&Self::VTABLE,
		))
	}

	unsafe fn clone(this: *const ()) -> task::RawWaker {
		// return the currently active process as a raw waker
		(*(this as *const Self)).context.active().raw_waker()
	}

	unsafe fn wake(_this: *const ()) {
		// waking the active process can safely be ignored
	}

	unsafe fn wake_by_ref(_this: *const ()) {
		// waking the active process can safely be ignored
	}

	unsafe fn drop(_this: *const ()) {
		// memory is released in the main event loop
	}
}

/* *************************** specialized futures ************************** */

/// Returns a future that unconditionally puts the calling process to sleep.
pub fn sleep() -> impl Future<Output = ()> {
	Sleep { ready: false }
}

/// Custom structure that implements [`Future`] by suspending on the first call
/// and returning on the second one.
///
/// # Note
/// This can be expressed more succinctly using the `poll_fn` function, but it
/// is easier to see what's happening in this version.
struct Sleep {
	ready: bool,
}

impl Future for Sleep {
	type Output = ();

	fn poll(mut self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
		if self.ready {
			Poll::Ready(())
		} else {
			self.ready = true;
			Poll::Pending
		}
	}
}

/// Returns a future that can be awaited to produce the currently set waker.
pub fn waker() -> impl Future<Output = task::Waker> {
	poll_fn(|cx| Poll::Ready(cx.waker().clone()))
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::panic::{catch_unwind, AssertUnwindSafe};

	/// Test that waking a leaked waker does not cause UB.
	#[test]
	#[should_panic = "attempted to wake a terminated process"]
	fn leak_waker_simple() {
		let shared = RefCell::new(None);
		let shared = simulation(shared, |sim| {
			Process::new(sim, async move {
				poll_fn(|cx| {
					*sim.global().borrow_mut() = Some(cx.waker().clone());
					Poll::Ready(())
				})
				.await;
			})
		});
		shared.take().unwrap().wake();
	}

	/// Test that all processes are dropped even if a destructor of a process unwinds.
	/// This is required to properly invalidate all wakers that may be leaked.
	#[test]
	#[should_panic = "attempted to wake a terminated process"]
	fn leak_waker_unwind() {
		struct PanicOnDrop;
		impl Drop for PanicOnDrop {
			fn drop(&mut self) {
				panic!("PanicOnDrop");
			}
		}

		struct PushDropOrderOnDrop<'s> {
			me: i32,
			drop_order: &'s RefCell<Vec<i32>>,
		}

		impl Drop for PushDropOrderOnDrop<'_> {
			fn drop(&mut self) {
				self.drop_order.borrow_mut().push(self.me);
			}
		}

		#[derive(Default)]
		struct Shared {
			channel: RefCell<Option<task::Waker>>,
			drop_order: RefCell<Vec<i32>>,
		}

		let shared = Shared::default();
		catch_unwind(AssertUnwindSafe(|| {
			simulation(&shared, |sim| {
				Process::new(sim, async move {
					// This process is dropped 2nd and will begin the unwind.
					sim.activate(async move {
						let _guard = PushDropOrderOnDrop {
							me: 2,
							drop_order: &sim.global().drop_order,
						};
						let _guard = PanicOnDrop;
						sim.advance(2.0).await;
						unreachable!();
					});

					// This process is dropped 3rd, after the unwind started.
					// We test that this process cannot be woken from a leaked waker.
					sim.activate(async move {
						let _guard = PushDropOrderOnDrop {
							me: 3,
							drop_order: &sim.global().drop_order,
						};
						poll_fn(|cx| {
							*sim.global().channel.borrow_mut() = Some(cx.waker().clone());
							Poll::Ready(())
						})
						.await;
						sim.advance(2.0).await;
						unreachable!();
					});

					let _guard: PushDropOrderOnDrop = PushDropOrderOnDrop {
						me: 1,
						drop_order: &sim.global().drop_order,
					};
					sim.advance(1.0).await;
					// Finish the main process here, this will drop the other processes.
				})
			});
		}))
		.unwrap_err();

		shared.drop_order.borrow_mut().sort();
		assert_eq!(*shared.drop_order.borrow(), [1, 2, 3]);

		shared.channel.take().unwrap().wake();
	}
}
