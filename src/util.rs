//! This module provides a collection of utilities for asynchronous simulation
//! and concurrency control, inspired by GPSS (General Purpose Simulation
//! System). It includes:
//!
//! - **Facility**: A synchronization primitive representing an exclusive
//!   resource that can be seized and released by multiple processes. Processes
//!   can queue and wait for access to the resource.
//!
//! - **Promise**: A one-shot, writable container that awakens a pre-registered
//!   process when a value is written to it. Useful for coordinating between
//!   asynchronous tasks.
//!
//! - **select** function: An asynchronous function that takes two futures and
//!   returns the result of the first one to complete, canceling the other.
//!   Useful for implementing race conditions where the first completion
//!   determines the outcome.
//!
//! - **Channel**, **Sender**, and **Receiver**: An unbounded channel
//!   implementation supporting multiple readers and writers. Processes can send
//!   and receive messages asynchronously, using wakers to reactivate suspended
//!   processes when messages become available.
//!
//! - **Controlled** trait and **Control** struct: The trait signifies that a
//!   type can broadcast state changes to wakers, allowing processes to wait on
//!   arbitrary conditions. The **Control** struct is a state variable that can
//!   be used with the `until()` function to suspend execution until a specified
//!   condition is met.
//!
//! - **until** function: Returns a future that suspends until a given boolean
//!   condition involving controlled expressions evaluates to `true`.
//!
//! - **RandomVar**: A statistical data collector that records observations,
//!   computes statistics like mean and standard deviation, and can merge data
//!   from other collectors. Inspired by SLX's `random_variable`.
//!
//! These utilities are designed to facilitate the development of simulations
//! and asynchronous systems by providing essential components for process
//! synchronization, inter-process communication, and statistical analysis.

use crate::{sleep, waker, Process, Sim};
use std::{
	cell::{Cell, RefCell},
	collections::VecDeque,
	fmt,
	future::Future,
	pin::Pin,
	rc::{Rc, Weak},
	task::{self, Context, Poll},
};

/// GPSS-inspired facility that houses one exclusive resource to be used by
/// arbitrary many processes.
#[derive(Default)]
pub struct Facility {
	/// A queue of waiting-to-be-activated processes.
	queue: RefCell<VecDeque<task::Waker>>,
	/// A boolean indicating whether the facility is in use or not.
	in_use: Cell<bool>,
}

impl Facility {
	/// Creates a new facility.
	pub fn new() -> Self {
		Facility {
			queue: RefCell::new(VecDeque::new()),
			in_use: Cell::new(false),
		}
	}

	/// Attempts to seize the facility, blocking until it's possible.
	pub async fn seize(&self) {
		if self.in_use.replace(true) {
			let waker = waker().await;
			self.queue.borrow_mut().push_back(waker);
			sleep().await;
		}
	}

	/// Releases the facility and activates the next waiting process.
	pub fn release(&self) {
		if let Some(process) = self.queue.borrow_mut().pop_front() {
			process.wake();
		} else {
			self.in_use.set(false);
		}
	}
}

/// A one-shot, writable container type that awakens a pre-registered process
/// on write.
pub struct Promise<T> {
	/// The waker of the process to awaken on write.
	caller: task::Waker,
	/// The written value to be extracted by the caller.
	result: Cell<Option<T>>,
}

impl<T> Promise<T> {
	/// Creates a new promise with an unwritten result.
	pub fn new(waker: task::Waker) -> Self {
		Self {
			caller: waker,
			result: Cell::new(None),
		}
	}

	/// Writes the result value and reawakens the caller.
	pub fn fulfill(&self, result: T) {
		if self.result.replace(Some(result)).is_none() {
			self.caller.wake_by_ref();
		}
	}

	/// Extracts the written value and resets the state of the promise.
	pub fn redeem(&self) -> Option<T> {
		self.result.replace(None)
	}
}

/// Returns a future that takes two other futures and completes as soon as
/// one of them returns, canceling the other future before returning.
///
/// This design guarantees that the two futures passed as input to this
/// function cannot outlive its returned future, enabling us to allow
/// references to variables in the local scope. The passed futures may
/// compute a value, as long as the return type is identical in both cases.
pub async fn select<'s, 'u, G, E, O, R>(sim: Sim<'s, G>, either: E, or: O) -> R
where
	E: Future<Output = R> + 'u,
	O: Future<Output = R> + 'u,
	R: 'u,
{
	use std::mem::transmute;

	// create a one-shot channel that reactivates the caller on write
	let promise = Promise::new(sim.active().waker());

	// this unsafe block shortens the guaranteed lifetimes of the processes
	// contained in the scheduler; it is safe because the constructed future
	// ensures that both futures are terminated before the promise and
	// itself are terminated, thereby preventing references to the lesser
	// constrained processes to survive the call to select()
	let sim = unsafe { transmute::<Sim<'s, G>, Sim<'_, G>>(sim) };

	{
		// create the two competing processes
		// a future optimization would be to keep them on the stack
		let p1 = Process::new(sim, async {
			promise.fulfill(either.await);
		});
		let p2 = Process::new(sim, async {
			promise.fulfill(or.await);
		});

		struct TerminateOnDrop<'s, G>(Process<'s, G>);
		impl<G> Drop for TerminateOnDrop<'_, G> {
			fn drop(&mut self) {
				self.0.terminate();
			}
		}

		// `p1` and `p2` borrow from `promise` and must therefore be dropped
		// before `promise` is dropped. In particular, we must ensure that the
		// future returned by the outer `async fn` outlives the futures crated
		// by the `async` blocks above.
		let _g1 = TerminateOnDrop(p1.clone());
		let _g2 = TerminateOnDrop(p2.clone());

		// activate them
		sim.reactivate(p1);
		sim.reactivate(p2);

		// wait for reactivation; the promise will wake us on fulfillment
		sleep().await;
	}

	// extract the promised value
	promise.redeem().unwrap()
}

/// Complex channel with space for infinitely many elements of arbitrary type.
///
/// This channel supports arbitrary many readers and writers and uses wakers
/// to reactivate suspended processes.
struct Channel<T> {
	/// A queue of messages.
	store: VecDeque<T>,
	/// A queue of processes waiting to receive a message.
	waiting: VecDeque<(task::Waker, usize)>,
	/// The number of unique wakers waiting so far.
	ids: usize,
}

/// Creates a channel and returns a pair of read and write ends.
pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
	let channel = Rc::new(RefCell::new(Channel {
		store: VecDeque::new(),
		waiting: VecDeque::new(),
		ids: 0,
	}));
	(Sender(Rc::downgrade(&channel)), Receiver(channel))
}

/// Write-end of a channel.
pub struct Sender<T>(Weak<RefCell<Channel<T>>>);

/// Read-end of a channel.
pub struct Receiver<T>(Rc<RefCell<Channel<T>>>);

// Allow senders to be duplicated.
impl<T> Clone for Sender<T> {
	fn clone(&self) -> Self {
		Self(self.0.clone())
	}
}

// Allow receivers to be duplicated.
impl<T> Clone for Receiver<T> {
	fn clone(&self) -> Self {
		Self(self.0.clone())
	}
}

impl<T: Unpin> Sender<T> {
	/// Returns a future that can be awaited to send a message.
	pub fn send(&self, elem: T) -> SendFuture<'_, T> {
		SendFuture(&self.0, Some(elem))
	}
}

impl<T> Drop for Sender<T> {
	#[inline]
	fn drop(&mut self) {
		// check if there are still receivers
		if let Some(chan) = self.0.upgrade() {
			// check if we're the last sender to drop
			if Rc::weak_count(&chan) == 1 {
				// awake all waiting receivers so that they get to return
				for (process, _) in chan.borrow_mut().waiting.drain(..) {
					process.wake();
				}
			}
		}
	}
}

impl<T: Unpin> Receiver<T> {
	/// Returns a future that can be awaited to receive a message.
	pub fn recv(&self) -> ReceiveFuture<'_, T> {
		ReceiveFuture(&self.0, 0)
	}

	/// Returns the number of elements that can be received before model time
	/// has to be consumed.
	pub fn len(&self) -> usize {
		self.0.borrow().len()
	}
	
	/// Returns if the receiver is empty at the current model time.
	pub fn is_empty(&self) -> bool {
		self.len() == 0
	}
}

impl<T> Channel<T> {
	/// Private method returning the number of elements left in the channel.
	fn len(&self) -> usize {
		self.store.len()
	}

	/// Private method enqueuing a process into the waiting list.
	fn enqueue(&mut self, process: task::Waker) -> usize {
		self.ids = self.ids.checked_add(1).expect("ID overflow");
		self.waiting.push_back((process, self.ids));
		self.ids
	}

	/// Private method removing a process from the waiting list.
	fn dequeue(&mut self) -> Option<task::Waker> {
		self.waiting.pop_front().map(|(p, _)| p)
	}

	/// Private method that unregisters a previously registered waker.
	fn unregister(&mut self, id: usize) {
		if let Some(pos) = self.waiting.iter().position(|(_, tid)| *tid == id) {
			self.waiting.remove(pos);
		}
	}

	/// Private method inserting a message into the queue.
	fn send(&mut self, value: T) {
		self.store.push_back(value);
	}

	/// Private method extracting a message from the queue non-blocking.
	fn recv(&mut self) -> Option<T> {
		self.store.pop_front()
	}
}

/// Future for the [`send()`] operation on a channel sender.
///
/// [`send()`]: struct.Sender.html#method.send
pub struct SendFuture<'c, T>(&'c Weak<RefCell<Channel<T>>>, Option<T>);

/// Future for the [`recv()`] operation on a channel receiver.
///
/// [`recv()`]: struct.Receiver.html#method.recv
pub struct ReceiveFuture<'c, T>(&'c Rc<RefCell<Channel<T>>>, usize);

impl<T: Unpin> Future for SendFuture<'_, T> {
	type Output = Result<(), T>;

	fn poll(mut self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<Self::Output> {
		let elem = self.1.take().unwrap();

		// test if there are still readers left to listen
		if let Some(channel) = self.0.upgrade() {
			let mut channel = channel.borrow_mut();
			channel.send(elem);

			// awake a waiting process
			if let Some(process) = channel.dequeue() {
				process.wake();
			}

			Poll::Ready(Ok(()))
		} else {
			Poll::Ready(Err(elem))
		}
	}
}

impl<T: Unpin> Future for ReceiveFuture<'_, T> {
	type Output = Option<T>;

	fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
		let mut channel = self.0.borrow_mut();

		// clear the local copy of our waker to prevent unnecessary
		// de-registration attempts in our destructor
		self.1 = 0;

		if let Some(c) = channel.recv() {
			// there are elements in the channel
			Poll::Ready(Some(c))
		} else if Rc::weak_count(self.0) == 0 {
			// no elements in the channel and no potential senders exist
			Poll::Ready(None)
		} else {
			// no elements, but potential senders exist: check back later
			let waker = cx.waker().clone();
			self.1 = channel.enqueue(waker);
			Poll::Pending
		}
	}
}

impl<T> Drop for ReceiveFuture<'_, T> {
	#[inline]
	fn drop(&mut self) {
		// take care to unregister our waker from the channel
		if self.1 > 0 {
			self.0.borrow_mut().unregister(self.1);
		}
	}
}

/// A trait signifying that the type implementing it is able to broadcast state
/// changes to [wakers].
///
/// Types implementing it can participate in [`until()`] expressions, the most
/// commonly-used of them being [`Control`] variables.
///
/// [wakers]: https://doc.rust-lang.org/std/task/struct.Waker.html
/// [`until()`]: fn.until.html
/// [`Control`]: struct.Control.html
pub trait Publisher {
	/// Allows a [waker] to subscribe to the controlled expression to be
	/// informed about any and all state changes.
	///
	/// # Safety
	/// This method is unsafe, because the controlled expression will call the
	/// [`wake_by_ref()`] method on the waker during every change in state. The caller
	/// is responsible to ensure the validity of all subscribed wakers at
	/// the time of the state change.
	///
	/// The [`span()`] method provides a safe alternative.
	///
	/// [waker]: https://doc.rust-lang.org/std/task/struct.Waker.html
	/// [`span()`]: trait.Controlled.html#method.span
	/// [`wake_by_ref()`]: https://doc.rust-lang.org/std/task/struct.Waker.html#method.wake_by_ref
	unsafe fn subscribe(&self, waker: &task::Waker);

	/// Unsubscribes a previously subscribed [waker] from the controlled
	/// expression.
	///
	/// # Safety
	/// This method is unsafe, because it asserts that the waker that it
	/// receives has been registered using [subscribe] previously, and has not
	/// been unsubscribed from yet.
	///
	/// [waker]: https://doc.rust-lang.org/std/task/struct.Waker.html
	/// [subscribe]: Self::subscribe
	unsafe fn unsubscribe(&self, waker: &task::Waker);
}

/// Guarding structure that ensures that waker and controlled expression are
/// valid and fixed in space.
pub struct WakerSpan<'s, C: ?Sized + Publisher> {
	/// The controlled expression.
	cv: &'s C,
	/// The waker.
	waker: &'s task::Waker,
}

// implement a constructor for the guard
impl<'s, C: ?Sized + Publisher> WakerSpan<'s, C> {
	/// Subscribes a [waker] to a controlled expression for a certain duration.
	///
	/// This method subscribes the waker to the controlled expression and
	/// returns an object that unsubscribes the waker from the same controlled
	/// expression upon drop. The borrow-checker secures the correct lifetimes
	/// of waker and controlled expression so that this method can be safe.
	///
	/// [waker]: https://doc.rust-lang.org/std/task/struct.Waker.html
	#[inline]
	pub fn new(cv: &'s C, waker: &'s task::Waker) -> Self {
		// this is safe because we bind the lifetime of the waker to the guard
		unsafe {
			cv.subscribe(waker);
		}
		WakerSpan { cv, waker }
	}
}

// implement drop for the guard
impl<C: ?Sized + Publisher> Drop for WakerSpan<'_, C> {
	#[inline]
	fn drop(&mut self) {
		// this is safe because we subscribed in the only public constructor
		unsafe {
			self.cv.unsubscribe(self.waker);
		}
	}
}

// a reference of a controlled expression is also a controlled expression
impl<T: Publisher> Publisher for &'_ T {
	#[inline]
	unsafe fn subscribe(&self, waker: &task::Waker) {
		Publisher::subscribe(*self, waker);
	}

	#[inline]
	unsafe fn unsubscribe(&self, waker: &task::Waker) {
		Publisher::unsubscribe(*self, waker);
	}
}

/// Marks a variable as a state variable which can be used in conjunction with
/// the [`until()`] function.
///
/// Whenever a control variable changes its value, the runtime system checks
/// whether any other parts of the model are currently waiting for the variable
/// to attain a certain value.
///
/// [`until()`]: fn.until.html
pub struct Control<T> {
	/// The inner value.
	value: Cell<T>,
	/// A list of waiting processes, identified by their [wakers].
	///
	/// [wakers]: https://doc.rust-lang.org/std/task/struct.Waker.html
	waiting: RefCell<Vec<task::Waker>>,
}

impl<T> Control<T> {
	/// Creates a new control variable and initializes its value.
	#[inline]
	pub const fn new(value: T) -> Self {
		Self {
			value: Cell::new(value),
			waiting: RefCell::new(Vec::new()),
		}
	}

	/// Assigns a new value to the control variable.
	///
	/// This can lead to potential activations of waiting processes.
	#[inline]
	pub fn set(&self, val: T) {
		self.notify();
		self.value.set(val);
	}

	/// Extracts the current value from the control variable.
	#[inline]
	pub fn get(&self) -> T
	where
		T: Copy,
	{
		self.value.get()
	}

	/// Notifies all the waiting processes to re-check their state condition.
	///
	/// This action is usually performed automatically when the value of the
	/// control variable is changed through the [`set()`]-method but may be
	/// triggered manually when this mechanism is bypassed somehow.
	///
	/// [`set()`]: #method.set
	pub fn notify(&self) {
		for waker in self.waiting.borrow().iter() {
			waker.wake_by_ref();
		}
	}
}

// implement the trait marking control variables as controlled expressions
impl<T: Copy> Publisher for Control<T> {
	#[inline]
	unsafe fn subscribe(&self, waker: &task::Waker) {
		self.waiting.borrow_mut().push(waker.clone());
	}

	unsafe fn unsubscribe(&self, waker: &task::Waker) {
		let mut waiting = self.waiting.borrow_mut();
		let pos = waiting.iter().position(|w| w.will_wake(waker));

		if let Some(pos) = pos {
			waiting.remove(pos);
		} else {
			panic!("attempt to unsubscribe waker that isn't subscribed to");
		}
	}
}

impl<T: Default> Default for Control<T> {
	fn default() -> Self {
		Self {
			value: Cell::new(T::default()),
			waiting: RefCell::new(Vec::new()),
		}
	}
}

/// An internal macro to generate implementations of the [`Publisher`] trait
/// for tuples of expressions.
macro_rules! controlled_tuple_impl {
	// base rule generating an implementation for a concrete tuple
	($($T:ident -> $ID:tt),* .) => {
		impl<$($T:Publisher),*> Publisher for ($($T,)*) {
			unsafe fn subscribe(&self, _waker: &task::Waker) {
				$(self.$ID.subscribe(_waker);)*
			}

			unsafe fn unsubscribe(&self, _waker: &task::Waker) {
				$(self.$ID.unsubscribe(_waker);)*
			}
		}
	};

	($($T:ident -> $ID:tt),* . $HEAD:ident -> $HID:tt $(, $TAIL:ident -> $TID:tt)*) => {
		controlled_tuple_impl!($($T -> $ID),* .);
		controlled_tuple_impl!($($T -> $ID,)* $HEAD -> $HID . $($TAIL -> $TID),*);
	};

	($HEAD:ident -> $HID:tt $(, $TAIL:ident -> $TID:tt)* $(,)?) => {
		controlled_tuple_impl!($HEAD -> $HID . $($TAIL -> $TID),*);
	};
}

controlled_tuple_impl! {
	A -> 0, B -> 1, C -> 2, D -> 3, E -> 4, F -> 5, G -> 6, H -> 7,
	I -> 8, J -> 9, K ->10, L ->11, M ->12, N ->13, O ->14, P ->15,
}

/// Returns a future that suspends until an arbitrary boolean condition
/// involving control expressions evaluates to `true`.
#[inline]
pub async fn until<C: Publisher>(set: C, pred: impl Fn(&C) -> bool) {
	if !pred(&set) {
		let waker = waker().await;
		let _span = WakerSpan::new(&set, &waker);

		loop {
			sleep().await;
			if pred(&set) {
				break;
			}
		}
	}
}

/* ************************* statistical facilities ************************* */

/// A simple collector for statistical data, inspired by SLX's random_variable.
///
/// # Warning
/// This implementation is not numerically stable, since it keeps sums of the
/// passed values and of the squared values. It would be better to use a
/// numerically stable online algorithm instead.
#[derive(Clone)]
pub struct RandomVariable {
	total: Cell<u32>,
	sum: Cell<f64>,
	sqr: Cell<f64>,
	min: Cell<f64>,
	max: Cell<f64>,
}

impl RandomVariable {
	/// Creates a new random variable.
	#[inline]
	pub fn new() -> Self {
		RandomVariable::default()
	}

	/// Resets all stored statistical data.
	pub fn clear(&self) {
		self.total.set(0);
		self.sum.set(0.0);
		self.sqr.set(0.0);
		self.min.set(f64::INFINITY);
		self.max.set(f64::NEG_INFINITY);
	}

	/// Adds another value to the statistical collection.
	pub fn tabulate<T: Into<f64>>(&self, val: T) {
		let val: f64 = val.into();

		self.total.set(self.total.get() + 1);
		self.sum.set(self.sum.get() + val);
		self.sqr.set(self.sqr.get() + val * val);

		if self.min.get() > val {
			self.min.set(val);
		}
		if self.max.get() < val {
			self.max.set(val);
		}
	}

	/// Combines the statistical collection of two random variables into one.
	pub fn merge(&self, other: &Self) {
		self.total.set(self.total.get() + other.total.get());
		self.sum.set(self.sum.get() + other.sum.get());
		self.sqr.set(self.sqr.get() + other.sqr.get());

		if self.min.get() > other.min.get() {
			self.min.set(other.min.get());
		}
		if self.max.get() < other.max.get() {
			self.max.set(other.max.get());
		}
	}
}

impl Default for RandomVariable {
	fn default() -> Self {
		RandomVariable {
			total: Cell::default(),
			sum: Cell::default(),
			sqr: Cell::default(),
			min: Cell::new(f64::INFINITY),
			max: Cell::new(f64::NEG_INFINITY),
		}
	}
}

impl fmt::Debug for RandomVariable {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let total = self.total.get();
		let mean = self.sum.get() / f64::from(total);
		let variance = self.sqr.get() / f64::from(total) - mean * mean;
		let std_dev = variance.sqrt();

		f.debug_struct("RandomVariable")
			.field("n", &total)
			.field("min", &self.min.get())
			.field("max", &self.max.get())
			.field("mean", &mean)
			.field("sdev", &std_dev)
			.finish()
	}
}
