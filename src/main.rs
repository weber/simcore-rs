use simcore_rs::util::channel;
use simcore_rs::*;

fn main() {
	simulation((), |sim| {
		Process::new(sim, async move {
			let (sx, rx) = channel();

			sim.activate(async move {
				rx.recv().await;
			});

			sim.advance(1.0).await;
			sx.send(1).await.unwrap();
		})
	});
}
